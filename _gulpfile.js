'use strict';

require('es6-promise').polyfill();

// var fs = require('fs');
var gulp = require('gulp'); // gulp core
var runSequence = require('run-sequence');
var plugins = require('gulp-load-plugins')();
var settings = require('./package.json');

var BUILD_VERSION = settings.version;
var REQUIREJS_VERSION = settings.devDependencies.requirejs;
var JQUERY_VERSION = settings.devDependencies.jquery;

var _plumberPipe = function (type) {
  return {
    inherit: true,
    errorHandler: function (err) {
      console.log(type + " error: ", err);
      var notifier = require('gulp-notify/node_modules/node-notifier');
      notifier.notify({title: settings.name, message: type + " error: " + err.message, onLast: true});
      this.emit('end');
    }
  };
};

var _copyFile = function(filePath, DIST, renameOpts) {
  if(!renameOpts) renameOpts = {};
  return gulp.src(filePath)
      .pipe(plugins.plumber(_plumberPipe(filePath)))
      .pipe(plugins.replace(/\{\{DOC_TITLE\}\}/g, settings.doc_title ))
      .pipe(plugins.replace(/\{\{ROOT_EL_ID\}\}/g, settings.rootElId ))
      .pipe(plugins.replace(/\{\{STYLES_PATH\}\}/g, settings.path.styles ))
      .pipe(plugins.replace(/\{\{SCRIPTS_PATH\}\}/g, settings.path.scripts ))
      .pipe(plugins.replace(/\{\{BUILD_VERSION\}\}/g, BUILD_VERSION  ))
      .pipe(plugins.replace(/\{\{REQUIREJS_VERSION\}\}/g, REQUIREJS_VERSION))
      .pipe(plugins.replace(/\{\{JQUERY_VERSION\}\}/g, JQUERY_VERSION))
      .pipe(plugins.rename(renameOpts))
      .pipe(plugins.notify({title: settings.name, message: filePath + " updated", onLast: true}))
      .pipe(gulp.dest(DIST));
};

////////////////////////
// COMMON
////////////////////////

gulp.task('copy:scripts', function () {
  return gulp.src([
    settings.src + settings.path.scripts + '**/*'
  ])
      // .pipe(plugins.sourcemaps.init())
      .pipe(plugins.plumber(_plumberPipe('scripts')))
      // .pipe(plugins.uglify())
      .pipe(plugins.notify({title: settings.name, message: "scripts updated", onLast: true}))
      // .pipe(plugins.sourcemaps.write('.'))
      .pipe(gulp.dest(settings.dist + settings.path.scripts))
  ;
});
gulp.task('copy:img', function () {
  return gulp.src([settings.src + settings.path.images + '**/*'])
      .pipe(plugins.plumber(_plumberPipe('images')))
      .pipe(gulp.dest(settings.dist + settings.path.images))
  ;
});
gulp.task('copy:fonts', function () {
  return gulp.src([settings.src + settings.path.fonts + '**/*'])
      .pipe(plugins.plumber(_plumberPipe('fonts')))
      .pipe(plugins.notify({title: settings.name, message: "fonts updated", onLast: true}))
      .pipe(gulp.dest(settings.dist + settings.path.fonts))
  ;
});

gulp.task('copy:jsx', function () {
  return gulp.src([settings.src + settings.path.jsx + '**/*.jsx'])
      .pipe(plugins.plumber(_plumberPipe('jsx')))
      // .pipe(plugins.sourcemaps.init())
      // .pipe(plugins.babel({
      //   presets: ['es2015']
      // }))
      .pipe(plugins.react({harmony: true}))
      // .pipe(plugins.concat('ui.components.min.js'))
      // .pipe(plugins.uglify({
      //   mangle : false
      // }))
      // .pipe(plugins.sourcemaps.write('.'))
      .pipe(plugins.notify({title: settings.name, message: "jsx components updated", onLast: true}))
      .pipe(gulp.dest(settings.dist + settings.path.scripts + 'components/'))
      ;
});

gulp.task('copy:vendor', [
  'copy:vendor:jquery',
  // 'copy:vendor:react',
  // 'copy:vendor:classnames',
  'copy:vendor:requirejs',
  'copy:vendor:hydra',
  'copy:vendor:requirejs-plugins'
]);

gulp.task('copy:vendor:jquery', function () {
  gulp.src([ settings.node + 'jquery/dist/jquery.min.js'])
      .pipe(plugins.plumber(_plumberPipe('vendor:jquery')))
      .pipe(gulp.dest( settings.dist + settings.path.scripts + 'vendor/' ));
});
// gulp.task('copy:vendor:react', function () {
//   gulp.src([
//     settings.node + 'react/dist/react.js',
//     settings.node + 'react/dist/react-with-addons.js',
//     settings.node + 'react-dom/dist/react-dom.js'
//   ])
//       .pipe(plugins.plumber(_plumberPipe('vendor:react')))
//       // .pipe(plugins.uglify())
//       .pipe(plugins.rename({dirname: ''}))
//       .pipe(gulp.dest( settings.dist + settings.path.scripts + 'vendor/react/' ));
// });
// gulp.task('copy:vendor:classnames', function () {
//   gulp.src([ settings.node + 'classnames/index.js'])
//       .pipe(plugins.plumber(_plumberPipe('vendor:classnames')))
//       .pipe(plugins.uglify())
//       .pipe(plugins.rename('classnames.min.js'))
//       .pipe(gulp.dest( settings.dist + settings.path.scripts + 'vendor/' ));
// });
gulp.task('copy:vendor:hydra', function () {
  gulp.src([ settings.node + 'hydra.js/versions/hydra.min.js'])
      .pipe(plugins.plumber(_plumberPipe('vendor:hydra')))
      .pipe(plugins.uglify())
      .pipe(plugins.rename('hydra.min.js'))
      .pipe(gulp.dest( settings.dist + settings.path.scripts + 'vendor/' ));
});
gulp.task('copy:vendor:requirejs', function () {
  gulp.src([ settings.node + 'requirejs/require.js'])
      .pipe(plugins.plumber(_plumberPipe('vendor:requirejs')))
      .pipe(plugins.uglify())
      .pipe(plugins.rename('require-' + REQUIREJS_VERSION + '.min.js'))
      .pipe(gulp.dest( settings.dist + settings.path.scripts + 'vendor/' ));
});
gulp.task('copy:vendor:requirejs-plugins', function () {
  gulp.src([
    settings.node + 'requirejs-plugins/lib/text.js',
    settings.node + 'requirejs-plugins/src/*.js'
  ])
      .pipe(plugins.plumber(_plumberPipe('vendor:requirejs-plugins')))
      .pipe(plugins.uglify())
      .pipe(gulp.dest( settings.dist + settings.path.scripts + 'vendor/require/' ));
});

var _copyStyles = function(fileName) {
  return gulp.src([settings.src + settings.path.less + fileName])
      .pipe(plugins.plumber(_plumberPipe('styles')))
      .pipe(plugins.sourcemaps.init())
      .pipe(plugins.less())
      .pipe(plugins.cssnano(settings.cssnano))
      .pipe(plugins.autoprefixer({
        browsers: ['last 3 versions', 'ie >= 8', '> 5% in RU'],
        cascade: false
      }))
      //.pipe(plugins.concat('app.css'))
      .pipe(plugins.rename({extname: ".min.css"}))
      .pipe(plugins.sourcemaps.write('.'))
      .pipe(plugins.notify({title: settings.name, message: "styles updated " + fileName, onLast: true}))
      .pipe(gulp.dest(settings.dist + settings.path.styles))
      ;
};

gulp.task('copy:styles', function () {
  return _copyStyles('app.less');
});

////////////////////////
// COPY
////////////////////////

gulp.task('copy:php', ['copy::index', 'copy::root']);
gulp.task('copy::root', function(){
  return gulp.src([settings.src + '.*', '!' +settings.src + 'index.html'], {dot:true})
      .pipe(gulp.dest(settings.dist));
});
gulp.task('copy::index', function () {
  return _copyFile(
      settings.src + 'index.html',
      settings.dist,
      {extname: '.html'}
  );
});

gulp.task('copy', [
  'copy:vendor',
  // 'copy:jsx',
  'copy:img',
  'copy:fonts',
  'copy:php',
  'copy:scripts',
  'copy:styles'
]);

gulp.task('es6', () => {
  var path = settings.src + settings.path.scripts + 'graphics/**/*';
  console.log('path', path);
  return gulp.src([path])
      .pipe(plugins.sourcemaps.init())
      .pipe(plugins.babel({
        plugins: ["transform-class-properties"],
        presets: ['latest']
      }))
      .pipe(plugins.concat('graphics.min.js'))
      .pipe(plugins.sourcemaps.write('.'))
      .pipe(gulp.dest(settings.dist + settings.path.scripts + 'pack/'));
});

////////////////////////
// WATCH
////////////////////////

gulp.task('watch-styles', function () {
  gulp.watch([settings.src + settings.path.less + '**/*.less'], ['copy:styles']);
});
// gulp.task('watch-scripts', function () {
//   gulp.watch([settings.src + settings.path.scripts + '**/*.*'], ['copy:scripts']);
// });
// gulp.task('watch-jsx', function () {
//   gulp.watch([settings.src + settings.path.jsx + '**/*.jsx'], ['copy:jsx']);
// });
// gulp.task('watch-php', function () {
//   gulp.watch([settings.src + '**/*.php'], ['copy:php']);
// });
// gulp.task('watch-base', function () {
//   gulp.watch([settings.src + 'base/**/*'], ['copy:base']);
// });
// gulp.task('watch-img', function () {
//   gulp.watch([settings.src + settings.path.images + '**/*'], ['copy:img']);
// });

////////////////////////
// BUILD
////////////////////////

gulp.task('clean', function (done) {
  require('del')([
    settings.dist + '**/*',
    // The glob pattern ** matches all children and the parent.
    // You have to explicitly ignore the parent directories too
    '!' + settings.dist + 'img',
    '!' + settings.dist + 'img/st',
    '!' + settings.dist + 'img/st/**'
  ]).then(function () {
    done();
  });
});

gulp.task('build', function (done) {
  runSequence(
      // ['clean'/*, 'lint:js'/**/],
      // ['copy:scripts', 'copy:styles', 'copy:jsx'],
      'copy',
      done);
});
//
gulp.task('watch', [
  // 'watch-scripts',
  'watch-styles',
  // 'watch-jsx',
  // 'watch-php',
  // 'watch-base',
  // 'watch-img'
]);

gulp.task('all', ['copy:styles']);
// gulp.task('all', ['build']);