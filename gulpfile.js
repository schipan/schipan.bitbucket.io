'use strict';

require('es6-promise').polyfill();

// var fs = require('fs');
var gulp = require('gulp'); // gulp core
var plugins = require('gulp-load-plugins')();
var settings = require('./package.json');

var JQUERY_VERSION = settings.devDependencies.jquery;

var _plumberPipe = function (type) {
  return {
    inherit: true,
    errorHandler: function (err) {
      console.log(type + " error: ", err);
      var notifier = require('gulp-notify/node_modules/node-notifier');
      notifier.notify({title: settings.name, message: type + " error: " + err.message, onLast: true});
      this.emit('end');
    }
  };
};

var _copyFile = function(filePath, DIST, renameOpts) {
  if(!renameOpts) renameOpts = {};
  return gulp.src(filePath)
      .pipe(plugins.plumber(_plumberPipe(filePath)))
      .pipe(plugins.replace(/\{\{DOC_TITLE\}\}/g, settings.docTitle ))
      .pipe(plugins.replace(/\{\{ROOT_EL_ID\}\}/g, settings.rootElId ))
      .pipe(plugins.replace(/\{\{STYLES_PATH\}\}/g, settings.path.styles ))
      .pipe(plugins.replace(/\{\{IMAGES_PATH\}\}/g, settings.path.images ))
      .pipe(plugins.replace(/\{\{SCRIPTS_PATH\}\}/g, settings.path.scripts ))
      .pipe(plugins.replace(/\{\{BUILD_VERSION\}\}/g, (new Date().getTime())  ))
      .pipe(plugins.replace(/\{\{JQUERY_VERSION\}\}/g, JQUERY_VERSION))
      .pipe(plugins.rename(renameOpts))
      .pipe(plugins.notify({title: settings.name, message: filePath + " updated", onLast: true}))
      .pipe(gulp.dest(DIST));
};

////////////////////////
// COMMON
////////////////////////

gulp.task('copy:scripts', ['copy:vendor'], function () {
  var PATH = settings.src + settings.path.scripts;
  return gulp.src([
    PATH + 'polyfill/**.js',
    PATH + 'vendor/hydra.min.js',
    PATH + 'lib/activetheory.js',
    PATH + 'vendor/jquery.min.js',
    PATH + 'plugins/**/**.js',
    PATH + 'base/**/**.js',
    PATH + 'app.js'
  ])
      // .pipe(plugins.sourcemaps.init())
      .pipe(plugins.plumber(_plumberPipe('scripts')))
      .pipe(plugins.uglify({ mangle: {keep_fnames: true}, compress: {keep_fnames: true}}))
      .pipe(plugins.concat('build.min.js'))
      // .pipe(plugins.sourcemaps.write('.'))
      .pipe(plugins.notify({title: settings.name, message: "scripts updated", onLast: true}))
      .pipe(gulp.dest(settings.dist + settings.path.scripts))
  ;
});

gulp.task('copy:vendor', [
  'copy:vendor:jquery',
]);

gulp.task('copy:vendor:jquery', function () {
  gulp.src([ settings.node + 'jquery/dist/jquery.min.js'])
      .pipe(plugins.plumber(_plumberPipe('vendor:jquery')))
      .pipe(gulp.dest( settings.dist + settings.path.scripts + 'vendor/' ));
});

var _copyStyles = function(fileName) {
  return gulp.src([settings.src + settings.path.less + fileName])
      .pipe(plugins.plumber(_plumberPipe('styles')))
      .pipe(plugins.sourcemaps.init())
      .pipe(plugins.less())
      .pipe(plugins.cssnano(settings.cssnano))
      .pipe(plugins.autoprefixer({
        browsers: ['last 3 versions', 'ie >= 8', '> 5% in RU'],
        cascade: false
      }))
      //.pipe(plugins.concat('app.css'))
      .pipe(plugins.rename({extname: ".min.css"}))
      .pipe(plugins.sourcemaps.write('.'))
      .pipe(plugins.notify({title: settings.name, message: "styles updated " + fileName, onLast: true}))
      .pipe(gulp.dest(settings.dist + settings.path.styles))
      ;
};

gulp.task('copy:styles', function () {
  return _copyStyles('app.less');
});

////////////////////////
// COPY
////////////////////////

gulp.task('copy:index', ['copy:scripts'], function () {
  return _copyFile(
      settings.src + 'index.template.html',
      settings.dist,
      'index.html'
  );
});

gulp.task('copy', [
  'copy:index',
  'copy:scripts',
  'copy:styles'
]);

////////////////////////
// WATCH
////////////////////////

gulp.task('watch-index', function () {
  gulp.watch([settings.src + 'index.template.html'], ['copy:index']);
});
gulp.task('watch-styles', function () {
  gulp.watch([settings.src + settings.path.less + '**/*.less'], ['copy:styles']);
});
gulp.task('watch-scripts', function () {
  gulp.watch([
      settings.src + settings.path.scripts + '**/*.js',
      '!' + settings.src + settings.path.scripts + 'build*.js'
  ], ['copy:scripts']);
});

////////////////////////
// BUILD
////////////////////////

//
gulp.task('watch', [
  'watch-scripts',
  'watch-styles',
]);

gulp.task('build', ['copy']);