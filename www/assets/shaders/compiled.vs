{@}MenuBG.fs{@}varying vec3 vPos;
varying float vFalloff;

uniform float time;

#require(simplex3d.glsl)
#require(range.glsl)

void main() {
    float noise = snoise(time + vPos * 0.0007);

    noise = range(noise, -1.0, 1.0, 0.0, 0.2) * 0.45;

    vec3 color = vec3(noise);
    color *= vFalloff;

    gl_FragColor = vec4(color, 1.0);
}{@}MenuBG.vs{@}varying vec3 vPos;
varying float vFalloff;

void main() {
    vPos = position;

    vFalloff = smoothstep(-100.0, 70.0, position.y);

    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}{@}LogoBlur.fs{@}varying vec2 vUv;

uniform sampler2D tDiffuse;
uniform vec2 resolution;
uniform vec2 direction;

vec4 blur5(sampler2D image, vec2 uv, vec2 resolution, vec2 direction) {
    vec4 color = vec4(0.0);
    vec2 off1 = vec2(1.3333333333333333) * direction;
    color += texture2D(image, uv) * 0.29411764705882354;
    color += texture2D(image, uv + (off1 / resolution)) * 0.35294117647058826;
    color += texture2D(image, uv - (off1 / resolution)) * 0.35294117647058826;
    return color;
}

vec4 blur13(sampler2D image, vec2 uv, vec2 resolution, vec2 direction) {
    vec4 color = vec4(0.0);
    vec2 off1 = vec2(1.411764705882353) * direction;
    vec2 off2 = vec2(3.2941176470588234) * direction;
    vec2 off3 = vec2(5.176470588235294) * direction;
    color += texture2D(image, uv) * 0.1964825501511404;
    color += texture2D(image, uv + (off1 / resolution)) * 0.2969069646728344;
    color += texture2D(image, uv - (off1 / resolution)) * 0.2969069646728344;
    color += texture2D(image, uv + (off2 / resolution)) * 0.09447039785044732;
    color += texture2D(image, uv - (off2 / resolution)) * 0.09447039785044732;
    color += texture2D(image, uv + (off3 / resolution)) * 0.010381362401148057;
    color += texture2D(image, uv - (off3 / resolution)) * 0.010381362401148057;
    return color;
}

void main() {
    gl_FragColor = blur13(tDiffuse, vUv, resolution, direction);
}{@}LogoOcclusion.fs{@}varying float vBlendAmount;

void main() {
    gl_FragColor = vec4(vec3(vBlendAmount), 1.0);
}{@}LogoOcclusion.vs{@}varying float vBlendAmount;

float calcBlend() {
    vec3 posNormal = normalize(vec3(0.0, 0.0, 1000.0));
    float amount = dot(posNormal, normal);
    amount = step(amount, 0.6);
    return amount;
}

void main() {
    vBlendAmount = calcBlend();
    
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}
{@}LogoRefract.fs{@}uniform samplerCube tRefract;
uniform samplerCube tReflect;
uniform float time;
uniform float reflBlend;
uniform vec3 lightColor;
uniform float brightness;

varying vec2 vUv;
varying vec3 vReflect;
varying vec3 vNormal;
varying vec3 vPos;
varying float vBlendAmount;

#require(range.glsl)
#require(refl.fs)

vec4 envColorInv(samplerCube map, vec3 vec) {
    return textureCube(map, vec3(-vec.xy, vec.z));
}

vec3 calcNoise() {
    float dist = abs(vPos.z);
    float volume = range(dist, 0.0, 100.0, 0.0, 1.0);
    return mix(vec3(1.0), lightColor, volume);
}

vec3 calcRefl() {
    vec4 color = envColor(tReflect, vReflect);
    return color.rgb * 1.5;
}

vec3 blend(vec3 noise, vec3 refr) {
    return mix(noise, refr, vBlendAmount);
}

void main() {
    vec3 noise = vec3(1.0);
    vec3 ref = calcRefl();

    vec3 color = blend(noise, ref) * range(brightness, 0.0, 1.0, 1.0, 4.0);
    
    color = mix(color, vec3(1.0), brightness * 0.2);

    gl_FragColor = vec4(color, 1.0);
}{@}LogoRefract.vs{@}uniform float time;

varying vec3 vReflect;
varying vec2 vUv;
varying vec3 vNormal;
varying vec3 vPos;
varying float vBlendAmount;

#require(refl.vs)

float calcBlend() {
    vec3 posNormal = normalize(vec3(0.0, 0.0, 1000.0));
    float amount = dot(posNormal, normal);
    amount = 1.0 - step(amount, 0.6);
    return amount;
}

void main() {
    vec4 worldPosition = modelMatrix * vec4(position, 1.0);
    vReflect = reflection(worldPosition);
    
    vec4 mvPosition = modelViewMatrix * vec4(position, 1.0);
    
    vec3 lightPos = normalize(vec3(0.0, 1000.0, 1000.0));
    float volume = dot(lightPos, normal);
    
    vUv = uv;
    vNormal = normal;
    vPos = position;
    vBlendAmount = calcBlend();
    
    gl_Position = projectionMatrix * mvPosition;
}{@}MenuParticles.fs{@}varying vec3 vPos;

#require(range.glsl)

float calculateAlpha() {
    return smoothstep(-1500.0, 0.0, vPos.z);
}

void main() {
    gl_FragColor = vec4(1.0);
    gl_FragColor.a = calculateAlpha() * 0.5;
}{@}MenuParticles.vs{@}uniform float size;

varying vec3 vPos;

void main() {
    vPos = position;
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
    gl_PointSize = size;
}{@}Chroma.fs{@}uniform sampler2D tDiffuse;
uniform sampler2D lightGlow;
uniform float brightness;

varying vec2 vUv;

void main() {
    vec3 glow = texture2D(lightGlow, vUv).rgb;
    vec3 texel = texture2D(tDiffuse, vUv).rgb;
    
    texel += glow * brightness;
    
    gl_FragColor = vec4(texel, 1.0);
}{@}ColorPass.fs{@}varying vec2 vUv;

uniform sampler2D tDiffuse;
uniform sampler2D tMask;
uniform vec3 tint;
uniform float dark;
uniform vec3 darkColor;
uniform float time;
uniform float noiseAmount;
uniform float rgbAmount;
uniform float glitchAmount;

#require(brightness.glsl)
#require(range.glsl)
#require(simplex2d.glsl)
#require(transformUV.glsl)

vec4 getRGB(sampler2D tDiffuse, vec2 uv, float angle, float amount) {
    vec2 offset = vec2(cos(angle), sin(angle)) * amount;
    vec4 r = texture2D(tDiffuse, uv + offset);
    vec4 g = texture2D(tDiffuse, uv);
    vec4 b = texture2D(tDiffuse, uv - offset);
    return vec4(r.r, b.b, b.b, g.a);
}

vec3 getDarkValue(vec3 color, float amt) {
    float lum = brightness(color);
    return mix(darkColor, color, lum * (1.0 - amt));
}

vec2 scale(vec2 uv, float amt) {
    float y = amt - 0.5;
    
    float scale = range(1.0 - y, 0.0, 1.0, 1.4, 1.0);
    
    float a[9];
    a[0] = 0.0;
    a[1] = y * 0.1;
    a[2] = 0.0;
    a[3] = 0.0;
    a[4] = 0.0;
    a[5] = scale;
    a[6] = scale;
    a[7] = 0.5;
    a[8] = 0.5;
    
    return transformUV(uv, a);
}

void main() {
    vec4 mask = texture2D(tMask, vUv);
    vec2 uv = vUv;//scale(vUv, mask.g);
    
//    uv.x += snoise(vec2(uv.y - (time * 0.001), 0.0) * 5.0) * 0.07 * glitchAmount;
    
    vec4 color = getRGB(tDiffuse, uv, radians(10.0), 0.0003 * rgbAmount);
    color.rgb *= tint;
    
    color.rgb = mix(color.rgb, getDarkValue(color.rgb, dark), dark);
    
    float noise = snoise(time + vUv * 800.0);
    noise = range(noise, -1.0, 1.0, 0.75, 1.0);
    noise = mix(noise, 1.0, brightness(color.rgb));
    noise = max(noise, 1.0 - noiseAmount);

    color *= noise;
    
    vec2 fc = gl_FragCoord.xy;
    if (mod( fc.x + fc.y, 20.0 ) > 2.0) color *= range(glitchAmount, 0.0, 1.0, 1.0, 0.8);
    gl_FragColor = color * mask.r;
}{@}Terrain.fs{@}varying vec3 vColor;
varying vec3 vPos;
varying float vLightVolume;

uniform float brightness;

float calculateLightShape() {
    float z = vPos.z;
    float x = vPos.x;

    float l = smoothstep(-400.0, -50.0, x);
    float r = 1.0 - smoothstep(150.0, 500.0, x);
    float b = 1.0 - smoothstep(0.0, 200.0, z);
    float t = smoothstep(-200.0, 0.0, z);
    return l * r * b * t;
}

void main() {
    vec3 color = vColor;
    color += calculateLightShape() * vLightVolume * 0.25 * brightness;
    
    gl_FragColor = vec4(color, 1.0);
}{@}Terrain.vs{@}varying vec3 vColor;
varying vec3 vPos;
varying float vLightVolume;

uniform vec3 logoPos;
uniform float brightness;

#require(range.glsl)
#require(transforms.glsl)

vec3 calculateAmbient() {
    vec3 lightPos = normalize(vec3(0.0, 2000.0, 200.0));
    float volume = max(dot(lightPos, normal), 0.0);
    volume = range(volume, 0.0, 1.0, 0.0, 0.05);
    
    return vec3(volume);
}

float calculateFalloff(float z) {
    return smoothstep(-1500.0, -400.0, z);
}

float calculateVolume(float y) {
    float dist = abs(y - logoPos.y);
    float volume = range(dist, 100.0, 600.0, 1.0, 0.5);
    volume = clamp(volume, 0.0, 1.0) * 2.5;
    return volume;
}

float calculateLogo(vec3 pos, mat4 viewMat, vec4 mvPos) {
    vec3 lightPos = normalize(transformPosition(logoPos, viewMat, mvPos));
    float volume = max(dot(lightPos, normal), 0.0);
    float dist = length(pos - logoPos);
    
    volume *= clamp(range(dist, 0.0, 900.0, 1.0, 0.0), 0.0, 1.0);
    
    return volume * 0.5;
}

void main() {
    vec4 worldPosition = modelMatrix * vec4(position, 1.0);
    vPos = worldPosition.xyz;
    
    vec4 mvPosition = modelViewMatrix * vec4(position, 1.0);
    
    vColor = calculateAmbient();
    vColor += calculateLogo(worldPosition.xyz, viewMatrix, mvPosition);
    vColor *= calculateFalloff(worldPosition.z) * brightness;
    
    vLightVolume = calculateVolume(worldPosition.y);
    
    gl_Position = projectionMatrix * mvPosition;
}{@}brightness.glsl{@}float brightness(vec3 c) {
    return sqrt(
                c.r * c.r * .241 +
                c.g * c.g * .691 +
                c.b * c.b * .068
                );
}{@}perlin3d.glsl{@}//
// GLSL textureless classic 3D noise "cnoise",
// with an RSL-style periodic variant "pnoise".
// Author:  Stefan Gustavson (stefan.gustavson@liu.se)
// Version: 2011-10-11
//
// Many thanks to Ian McEwan of Ashima Arts for the
// ideas for permutation and gradient selection.
//
// Copyright (c) 2011 Stefan Gustavson. All rights reserved.
// Distributed under the MIT license. See LICENSE file.
// https://github.com/ashima/webgl-noise
//

vec3 mod289(vec3 x)
{
    return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec4 mod289(vec4 x)
{
    return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec4 permute(vec4 x)
{
    return mod289(((x*34.0)+1.0)*x);
}

vec4 taylorInvSqrt(vec4 r)
{
    return 1.79284291400159 - 0.85373472095314 * r;
}

vec3 fade(vec3 t) {
    return t*t*t*(t*(t*6.0-15.0)+10.0);
}

// Classic Perlin noise
float cnoise(vec3 P)
{
    vec3 Pi0 = floor(P); // Integer part for indexing
    vec3 Pi1 = Pi0 + vec3(1.0); // Integer part + 1
    Pi0 = mod289(Pi0);
    Pi1 = mod289(Pi1);
    vec3 Pf0 = fract(P); // Fractional part for interpolation
    vec3 Pf1 = Pf0 - vec3(1.0); // Fractional part - 1.0
    vec4 ix = vec4(Pi0.x, Pi1.x, Pi0.x, Pi1.x);
    vec4 iy = vec4(Pi0.yy, Pi1.yy);
    vec4 iz0 = Pi0.zzzz;
    vec4 iz1 = Pi1.zzzz;
    
    vec4 ixy = permute(permute(ix) + iy);
    vec4 ixy0 = permute(ixy + iz0);
    vec4 ixy1 = permute(ixy + iz1);
    
    vec4 gx0 = ixy0 * (1.0 / 7.0);
    vec4 gy0 = fract(floor(gx0) * (1.0 / 7.0)) - 0.5;
    gx0 = fract(gx0);
    vec4 gz0 = vec4(0.5) - abs(gx0) - abs(gy0);
    vec4 sz0 = step(gz0, vec4(0.0));
    gx0 -= sz0 * (step(0.0, gx0) - 0.5);
    gy0 -= sz0 * (step(0.0, gy0) - 0.5);
    
    vec4 gx1 = ixy1 * (1.0 / 7.0);
    vec4 gy1 = fract(floor(gx1) * (1.0 / 7.0)) - 0.5;
    gx1 = fract(gx1);
    vec4 gz1 = vec4(0.5) - abs(gx1) - abs(gy1);
    vec4 sz1 = step(gz1, vec4(0.0));
    gx1 -= sz1 * (step(0.0, gx1) - 0.5);
    gy1 -= sz1 * (step(0.0, gy1) - 0.5);
    
    vec3 g000 = vec3(gx0.x,gy0.x,gz0.x);
    vec3 g100 = vec3(gx0.y,gy0.y,gz0.y);
    vec3 g010 = vec3(gx0.z,gy0.z,gz0.z);
    vec3 g110 = vec3(gx0.w,gy0.w,gz0.w);
    vec3 g001 = vec3(gx1.x,gy1.x,gz1.x);
    vec3 g101 = vec3(gx1.y,gy1.y,gz1.y);
    vec3 g011 = vec3(gx1.z,gy1.z,gz1.z);
    vec3 g111 = vec3(gx1.w,gy1.w,gz1.w);
    
    vec4 norm0 = taylorInvSqrt(vec4(dot(g000, g000), dot(g010, g010), dot(g100, g100), dot(g110, g110)));
    g000 *= norm0.x;
    g010 *= norm0.y;
    g100 *= norm0.z;
    g110 *= norm0.w;
    vec4 norm1 = taylorInvSqrt(vec4(dot(g001, g001), dot(g011, g011), dot(g101, g101), dot(g111, g111)));
    g001 *= norm1.x;
    g011 *= norm1.y;
    g101 *= norm1.z;
    g111 *= norm1.w;
    
    float n000 = dot(g000, Pf0);
    float n100 = dot(g100, vec3(Pf1.x, Pf0.yz));
    float n010 = dot(g010, vec3(Pf0.x, Pf1.y, Pf0.z));
    float n110 = dot(g110, vec3(Pf1.xy, Pf0.z));
    float n001 = dot(g001, vec3(Pf0.xy, Pf1.z));
    float n101 = dot(g101, vec3(Pf1.x, Pf0.y, Pf1.z));
    float n011 = dot(g011, vec3(Pf0.x, Pf1.yz));
    float n111 = dot(g111, Pf1);
    
    vec3 fade_xyz = fade(Pf0);
    vec4 n_z = mix(vec4(n000, n100, n010, n110), vec4(n001, n101, n011, n111), fade_xyz.z);
    vec2 n_yz = mix(n_z.xy, n_z.zw, fade_xyz.y);
    float n_xyz = mix(n_yz.x, n_yz.y, fade_xyz.x);
    return 2.2 * n_xyz;
}

// Classic Perlin noise, periodic variant
float pnoise(vec3 P, vec3 rep)
{
    vec3 Pi0 = mod(floor(P), rep); // Integer part, modulo period
    vec3 Pi1 = mod(Pi0 + vec3(1.0), rep); // Integer part + 1, mod period
    Pi0 = mod289(Pi0);
    Pi1 = mod289(Pi1);
    vec3 Pf0 = fract(P); // Fractional part for interpolation
    vec3 Pf1 = Pf0 - vec3(1.0); // Fractional part - 1.0
    vec4 ix = vec4(Pi0.x, Pi1.x, Pi0.x, Pi1.x);
    vec4 iy = vec4(Pi0.yy, Pi1.yy);
    vec4 iz0 = Pi0.zzzz;
    vec4 iz1 = Pi1.zzzz;
    
    vec4 ixy = permute(permute(ix) + iy);
    vec4 ixy0 = permute(ixy + iz0);
    vec4 ixy1 = permute(ixy + iz1);
    
    vec4 gx0 = ixy0 * (1.0 / 7.0);
    vec4 gy0 = fract(floor(gx0) * (1.0 / 7.0)) - 0.5;
    gx0 = fract(gx0);
    vec4 gz0 = vec4(0.5) - abs(gx0) - abs(gy0);
    vec4 sz0 = step(gz0, vec4(0.0));
    gx0 -= sz0 * (step(0.0, gx0) - 0.5);
    gy0 -= sz0 * (step(0.0, gy0) - 0.5);
    
    vec4 gx1 = ixy1 * (1.0 / 7.0);
    vec4 gy1 = fract(floor(gx1) * (1.0 / 7.0)) - 0.5;
    gx1 = fract(gx1);
    vec4 gz1 = vec4(0.5) - abs(gx1) - abs(gy1);
    vec4 sz1 = step(gz1, vec4(0.0));
    gx1 -= sz1 * (step(0.0, gx1) - 0.5);
    gy1 -= sz1 * (step(0.0, gy1) - 0.5);
    
    vec3 g000 = vec3(gx0.x,gy0.x,gz0.x);
    vec3 g100 = vec3(gx0.y,gy0.y,gz0.y);
    vec3 g010 = vec3(gx0.z,gy0.z,gz0.z);
    vec3 g110 = vec3(gx0.w,gy0.w,gz0.w);
    vec3 g001 = vec3(gx1.x,gy1.x,gz1.x);
    vec3 g101 = vec3(gx1.y,gy1.y,gz1.y);
    vec3 g011 = vec3(gx1.z,gy1.z,gz1.z);
    vec3 g111 = vec3(gx1.w,gy1.w,gz1.w);
    
    vec4 norm0 = taylorInvSqrt(vec4(dot(g000, g000), dot(g010, g010), dot(g100, g100), dot(g110, g110)));
    g000 *= norm0.x;
    g010 *= norm0.y;
    g100 *= norm0.z;
    g110 *= norm0.w;
    vec4 norm1 = taylorInvSqrt(vec4(dot(g001, g001), dot(g011, g011), dot(g101, g101), dot(g111, g111)));
    g001 *= norm1.x;
    g011 *= norm1.y;
    g101 *= norm1.z;
    g111 *= norm1.w;
    
    float n000 = dot(g000, Pf0);
    float n100 = dot(g100, vec3(Pf1.x, Pf0.yz));
    float n010 = dot(g010, vec3(Pf0.x, Pf1.y, Pf0.z));
    float n110 = dot(g110, vec3(Pf1.xy, Pf0.z));
    float n001 = dot(g001, vec3(Pf0.xy, Pf1.z));
    float n101 = dot(g101, vec3(Pf1.x, Pf0.y, Pf1.z));
    float n011 = dot(g011, vec3(Pf0.x, Pf1.yz));
    float n111 = dot(g111, Pf1);
    
    vec3 fade_xyz = fade(Pf0);
    vec4 n_z = mix(vec4(n000, n100, n010, n110), vec4(n001, n101, n011, n111), fade_xyz.z);
    vec2 n_yz = mix(n_z.xy, n_z.zw, fade_xyz.y);
    float n_xyz = mix(n_yz.x, n_yz.y, fade_xyz.x);
    return 2.2 * n_xyz;
}{@}simplex2d.glsl{@}//
// Description : Array and textureless GLSL 2D simplex noise function.
//      Author : Ian McEwan, Ashima Arts.
//  Maintainer : ijm
//     Lastmod : 20110822 (ijm)
//     License : Copyright (C) 2011 Ashima Arts. All rights reserved.
//               Distributed under the MIT License. See LICENSE file.
//               https://github.com/ashima/webgl-noise
//

vec3 mod289(vec3 x) {
    return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec2 mod289(vec2 x) {
    return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec3 permute(vec3 x) {
    return mod289(((x*34.0)+1.0)*x);
}

float snoise(vec2 v)
{
    const vec4 C = vec4(0.211324865405187,  // (3.0-sqrt(3.0))/6.0
                        0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)
                        -0.577350269189626,  // -1.0 + 2.0 * C.x
                        0.024390243902439); // 1.0 / 41.0
    // First corner
    vec2 i  = floor(v + dot(v, C.yy) );
    vec2 x0 = v -   i + dot(i, C.xx);
    
    // Other corners
    vec2 i1;
    //i1.x = step( x0.y, x0.x ); // x0.x > x0.y ? 1.0 : 0.0
    //i1.y = 1.0 - i1.x;
    i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);
    // x0 = x0 - 0.0 + 0.0 * C.xx ;
    // x1 = x0 - i1 + 1.0 * C.xx ;
    // x2 = x0 - 1.0 + 2.0 * C.xx ;
    vec4 x12 = x0.xyxy + C.xxzz;
    x12.xy -= i1;
    
    // Permutations
    i = mod289(i); // Avoid truncation effects in permutation
    vec3 p = permute( permute( i.y + vec3(0.0, i1.y, 1.0 ))
                     + i.x + vec3(0.0, i1.x, 1.0 ));
    
    vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12.xy), dot(x12.zw,x12.zw)), 0.0);
    m = m*m ;
    m = m*m ;
    
    // Gradients: 41 points uniformly over a line, mapped onto a diamond.
    // The ring size 17*17 = 289 is close to a multiple of 41 (41*7 = 287)
    
    vec3 x = 2.0 * fract(p * C.www) - 1.0;
    vec3 h = abs(x) - 0.5;
    vec3 ox = floor(x + 0.5);
    vec3 a0 = x - ox;
    
    // Normalise gradients implicitly by scaling m
    // Approximation of: m *= inversesqrt( a0*a0 + h*h );
    m *= 1.79284291400159 - 0.85373472095314 * ( a0*a0 + h*h );
    
    // Compute final noise value at P
    vec3 g;
    g.x  = a0.x  * x0.x  + h.x  * x0.y;
    g.yz = a0.yz * x12.xz + h.yz * x12.yw;
    return 130.0 * dot(m, g);
}{@}simplex3d.glsl{@}// Description : Array and textureless GLSL 2D/3D/4D simplex
//               noise functions.
//      Author : Ian McEwan, Ashima Arts.
//  Maintainer : ijm
//     Lastmod : 20110822 (ijm)
//     License : Copyright (C) 2011 Ashima Arts. All rights reserved.
//               Distributed under the MIT License. See LICENSE file.
//               https://github.com/ashima/webgl-noise
//

vec3 mod289(vec3 x) {
    return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec4 mod289(vec4 x) {
    return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec4 permute(vec4 x) {
    return mod289(((x*34.0)+1.0)*x);
}

vec4 taylorInvSqrt(vec4 r) {
    return 1.79284291400159 - 0.85373472095314 * r;
}

float snoise(vec3 v) {
    const vec2  C = vec2(1.0/6.0, 1.0/3.0) ;
    const vec4  D = vec4(0.0, 0.5, 1.0, 2.0);

    vec3 i  = floor(v + dot(v, C.yyy) );
    vec3 x0 =   v - i + dot(i, C.xxx) ;

    vec3 g = step(x0.yzx, x0.xyz);
    vec3 l = 1.0 - g;
    vec3 i1 = min( g.xyz, l.zxy );
    vec3 i2 = max( g.xyz, l.zxy );

    vec3 x1 = x0 - i1 + C.xxx;
    vec3 x2 = x0 - i2 + C.yyy; // 2.0*C.x = 1/3 = C.y
    vec3 x3 = x0 - D.yyy;      // -1.0+3.0*C.x = -0.5 = -D.y

    i = mod289(i);
    vec4 p = permute( permute( permute(
          i.z + vec4(0.0, i1.z, i2.z, 1.0 ))
        + i.y + vec4(0.0, i1.y, i2.y, 1.0 ))
        + i.x + vec4(0.0, i1.x, i2.x, 1.0 ));

    float n_ = 0.142857142857; // 1.0/7.0
    vec3  ns = n_ * D.wyz - D.xzx;

    vec4 j = p - 49.0 * floor(p * ns.z * ns.z);  //  mod(p,7*7)

    vec4 x_ = floor(j * ns.z);
    vec4 y_ = floor(j - 7.0 * x_ );    // mod(j,N)

    vec4 x = x_ *ns.x + ns.yyyy;
    vec4 y = y_ *ns.x + ns.yyyy;
    vec4 h = 1.0 - abs(x) - abs(y);

    vec4 b0 = vec4( x.xy, y.xy );
    vec4 b1 = vec4( x.zw, y.zw );

    vec4 s0 = floor(b0)*2.0 + 1.0;
    vec4 s1 = floor(b1)*2.0 + 1.0;
    vec4 sh = -step(h, vec4(0.0));

    vec4 a0 = b0.xzyw + s0.xzyw*sh.xxyy ;
    vec4 a1 = b1.xzyw + s1.xzyw*sh.zzww ;

    vec3 p0 = vec3(a0.xy,h.x);
    vec3 p1 = vec3(a0.zw,h.y);
    vec3 p2 = vec3(a1.xy,h.z);
    vec3 p3 = vec3(a1.zw,h.w);

    vec4 norm = taylorInvSqrt(vec4(dot(p0,p0), dot(p1,p1), dot(p2, p2), dot(p3,p3)));
    p0 *= norm.x;
    p1 *= norm.y;
    p2 *= norm.z;
    p3 *= norm.w;

    vec4 m = max(0.6 - vec4(dot(x0,x0), dot(x1,x1), dot(x2,x2), dot(x3,x3)), 0.0);
    m = m * m;
    return 42.0 * dot( m*m, vec4( dot(p0,x0), dot(p1,x1), dot(p2,x2), dot(p3,x3) ) );
}

//float surface(vec3 coord) {
//    float n = 0.0;
//    n += 1.0 * abs(snoise(coord));
//    n += 0.5 * abs(snoise(coord * 2.0));
//    n += 0.25 * abs(snoise(coord * 4.0));
//    n += 0.125 * abs(snoise(coord * 8.0));
//    float rn = 1.0 - n;
//    return rn * rn;
//}{@}phong.fs{@}#define saturate(a) clamp( a, 0.0, 1.0 )

float dPhong(float shininess, float dotNH) {
    return (shininess * 0.5 + 1.0) * pow(dotNH, shininess);
}

vec3 schlick(vec3 specularColor, float dotLH) {
    float fresnel = exp2((-5.55437 * dotLH - 6.98316) * dotLH);
    return (1.0 - specularColor) * fresnel + specularColor;
}

vec3 calcBlinnPhong(vec3 specularColor, float shininess, vec3 normal, vec3 lightDir, vec3 viewDir) {
    vec3 halfDir = normalize(lightDir + viewDir);
    
    float dotNH = saturate(dot(normal, halfDir));
    float dotLH = saturate(dot(lightDir, halfDir));
    
    vec3 F = schlick(specularColor, dotLH);
    float G = 0.85;
    float D = dPhong(shininess, dotNH);
    
    return F * G * D;
}

vec3 phong(float amount, vec3 diffuse, vec3 specular, float shininess, float attenuation, vec3 normal, vec3 lightDir, vec3 viewDir) {
    float cosineTerm = saturate(dot(normal, lightDir));
    vec3 brdf = calcBlinnPhong(specular, shininess, normal, lightDir, viewDir);
    return brdf * amount * diffuse * attenuation * cosineTerm;
}

//viewDir = vViewPosition
//lightDir = normalize(lightPos){@}range.glsl{@}float range(float oldValue, float oldMin, float oldMax, float newMin, float newMax) {
    float oldRange = oldMax - oldMin;
    float newRange = newMax - newMin;
    return (((oldValue - oldMin) * newRange) / oldRange) + newMin;
}{@}refl.fs{@}vec3 reflection(vec3 worldPosition, vec3 normal) {
    vec3 cameraToVertex = normalize(worldPosition - cameraPosition);
    
    return reflect(cameraToVertex, normal);
}

vec3 refraction(vec3 worldPosition, vec3 normal, float rRatio) {
    vec3 cameraToVertex = normalize(worldPosition - cameraPosition);
    
    return refract(cameraToVertex, normal, rRatio);
}

vec4 envColor(samplerCube map, vec3 vec) {
    float flipNormal = 1.0;
    return textureCube(map, flipNormal * vec3(-1.0 * vec.x, vec.yz));
}{@}refl.vs{@}vec3 inverseTransformDirection(in vec3 normal, in mat4 matrix) {
    return normalize((matrix * vec4(normal, 0.0) * matrix).xyz);
}

vec3 reflection(vec4 worldPosition) {
    vec3 transformedNormal = normalMatrix * normal;
    vec3 cameraToVertex = normalize(worldPosition.xyz - cameraPosition);
    vec3 worldNormal = inverseTransformDirection(transformedNormal, viewMatrix);
    
    return reflect(cameraToVertex, worldNormal);
}

vec3 refraction(vec4 worldPosition, float refractionRatio) {
    vec3 transformedNormal = normalMatrix * normal;
    vec3 cameraToVertex = normalize(worldPosition.xyz - cameraPosition);
    vec3 worldNormal = inverseTransformDirection(transformedNormal, viewMatrix);
    
    return refract(cameraToVertex, worldNormal, refractionRatio);
}{@}rgbshift.fs{@}vec4 getRGB(sampler2D tDiffuse, vec2 uv, float angle, float amount) {
    vec2 offset = vec2(cos(angle), sin(angle)) * amount;
    vec4 r = texture2D(tDiffuse, uv + offset);
    vec4 g = texture2D(tDiffuse, uv);
    vec4 b = texture2D(tDiffuse, uv - offset);
    return vec4(r.r, g.g, b.b, g.a);
}{@}transformUV.glsl{@}vec2 transformUV(vec2 uv, float a[9]) {

    // Convert UV to vec3 to apply matrices
	vec3 u = vec3(uv, 1.0);

    // Array consists of the following
    // 0 translate.x
    // 1 translate.y
    // 2 skew.x
    // 3 skew.y
    // 4 rotate
    // 5 scale.x
    // 6 scale.y
    // 7 origin.x
    // 8 origin.y

    // Origin before matrix
    mat3 mo1 = mat3(
        1, 0, -a[7],
        0, 1, -a[8],
        0, 0, 1);

    // Origin after matrix
    mat3 mo2 = mat3(
        1, 0, a[7],
        0, 1, a[8],
        0, 0, 1);

    // Translation matrix
    mat3 mt = mat3(
        1, 0, -a[0],
        0, 1, -a[1],
    	0, 0, 1);

    // Skew matrix
    mat3 mh = mat3(
        1, a[2], 0,
        a[3], 1, 0,
    	0, 0, 1);

    // Rotation matrix
    mat3 mr = mat3(
        cos(a[4]), sin(a[4]), 0,
        -sin(a[4]), cos(a[4]), 0,
    	0, 0, 1);

    // Scale matrix
    mat3 ms = mat3(
        1.0 / a[5], 0, 0,
        0, 1.0 / a[6], 0,
    	0, 0, 1);

	// apply translation
   	u = u * mt;

	// apply skew
   	u = u * mh;

    // apply rotation relative to origin
    u = u * mo1;
    u = u * mr;
    u = u * mo2;

    // apply scale relative to origin
    u = u * mo1;
    u = u * ms;
    u = u * mo2;

    // Return vec2 of new UVs
    return u.xy;
}{@}transforms.glsl{@}vec3 transformPosition(vec3 pos, mat4 viewMat, vec3 mvPos) {
    vec4 worldPosition = viewMat * vec4(pos, 1.0);
    return worldPosition.xyz - mvPos;
}

vec3 transformPosition(vec3 pos, mat4 viewMat, vec4 mvPos) {
    vec4 worldPosition = viewMat * vec4(pos, 1.0);
    return worldPosition.xyz - mvPos.xyz;
}{@}Sizzle.fs{@}uniform sampler2D tMap;

varying vec2 vUv;
varying float volume;

#require(rgbshift.fs)

void main() {
    vec4 texel = getRGB(tMap, vUv, 0.2, 0.004 * (1.0 - volume));
//    texel.rgb *= volume;
    
    gl_FragColor = texel;
}{@}Sizzle.vs{@}uniform float aspect[9];

varying vec2 vUv;
varying float volume;

#require(transformUV.glsl)
#require(range.glsl)

void main() {
    vec4 pos = modelMatrix * vec4(position, 1.0);
    
    vec3 p = position;
    p.z = 0.0;
    
    gl_Position = projectionMatrix * modelViewMatrix * vec4(p, 1.0);
    
    vUv = transformUV(uv, aspect);
    vUv += range(pos.z, 0.0, -300.0, 0.0, 0.05);
    
    volume = range(pos.z, 0.0, -300.0, 1.0, 0.2);
}{@}SizzleBG.fs{@}uniform sampler2D tMap;
uniform vec2 res;

varying vec2 vUv;

vec4 blur5(sampler2D image, vec2 uv, vec2 resolution, vec2 direction) {
    vec4 color = vec4(0.0);
    vec2 off1 = vec2(1.3333333333333333) * direction;
    color += texture2D(image, uv) * 0.29411764705882354;
    color += texture2D(image, uv + (off1 / resolution)) * 0.35294117647058826;
    color += texture2D(image, uv - (off1 / resolution)) * 0.35294117647058826;
    return color;
}

void main() {
//    gl_FragColor = blur5(tMap, vUv, res, vec2(3.0, 3.0)) * 0.15;
    gl_FragColor = texture2D(tMap, vUv);
}{@}SizzleBG.vs{@}uniform float aspect[9];

varying vec2 vUv;

#require(transformUV.glsl)

void main() {
    vUv = transformUV(uv, aspect);
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}{@}SizzleContainer.fs{@}uniform sampler2D tMap;
uniform sampler2D tOutline;

varying vec2 vUv;
varying vec2 nUv;

void main() {
    vec2 uv = vUv;
    uv.x -= 0.02;

    float alpha = step(0.9, texture2D(tOutline, nUv).a);

    gl_FragColor = texture2D(tMap, uv);
    gl_FragColor *= alpha;
    gl_FragColor += 0.03 * alpha;
}{@}SizzleContainer.vs{@}varying vec2 vUv;
varying vec2 nUv;

uniform float aspect[9];

#require(transformUV.glsl)

void main() {
    vUv = transformUV(uv, aspect);
    nUv = uv;
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}{@}Transition.fs{@}uniform vec3 color;

varying float vAlpha;

void main() {
    gl_FragColor = vec4(color, 1.0);
}{@}Transition.vs{@}attribute vec2 aCentroid;

uniform vec2 origin;
uniform float radius;
uniform float moveRange;
uniform float dir;

varying float vAlpha;

#require(range.glsl)

float cubicInOut(float t) {
    return t < 0.5
    ? 4.0 * t * t * t
    : 0.5 * pow(2.0 * t - 2.0, 3.0) + 1.0;
}

float cubicOut(float t) {
    float f = t - 1.0;
    return f * f * f + 1.0;
}

void main() {
    float dist = length(aCentroid - origin);
    float rDelta = radius - dist;
    float elapsed = range(rDelta, moveRange, 0.0, 0.0, 1.0);
    elapsed = clamp(elapsed, 0.0, 1.0);
    float scale = 1.0 - cubicInOut(elapsed);
    
    vAlpha = scale;
    
    vec3 pos = position;
    pos *= scale;
    pos.z += -150.0 * (1.0 - scale);
    
    
    gl_Position = projectionMatrix * modelViewMatrix * vec4(pos, 1.0);
}{@}VideoTexture.fs{@}uniform sampler2D tMap;

varying vec2 vUv;

void main() {
    gl_FragColor = texture2D(tMap, vUv);
}{@}VideoTexture.vs{@}varying vec2 vUv;

void main() {
    vUv = uv;
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}