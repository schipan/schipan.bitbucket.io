// --------------------------------------
//
//    _  _ _/ .  _  _/ /_ _  _  _
//   /_|/_ / /|//_  / / //_ /_// /_/
//   http://activetheory.net     _/
//
// --------------------------------------
//   10/3/16 6:24p
// --------------------------------------
window.Global = {};
window.getURL = function(a, b) {
  if (!b) {
    b = "_blank"
  }
  window.open(a, b)
};
if (typeof(console) === "undefined") {
  window.console = {};
  console.log = console.error = console.info = console.debug = console.warn = console.trace = function() {}
}
if (!window.requestAnimationFrame) {
  window.requestAnimationFrame = (function() {
    return window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function(b, a) {
          window.setTimeout(b, 1000 / 60)
        }
  })()
}
window.performance = (function() {
  if (window.performance && window.performance.now) {
    return window.performance
  } else {
    return Date
  }
})();
Date.now = Date.now || function() {
      return +new Date
    };
window.Class = function(b, c) {
  var f = this || window;
  var d = b.toString();
  var a = b.toString().match(/function ([^\(]+)/)[1];
  var e = null;
  if (typeof c === "function") {
    e = c;
    c = null
  }
  c = (c || "").toLowerCase();
  b.prototype.__call = function() {
    if (this.events) {
      this.events.scope(this)
    }
  };
  if (!c) {
    f[a] = b;
    e && e()
  } else {
    if (c == "static") {
      f[a] = new b()
    } else {
      if (c == "singleton") {
        f[a] = (function() {
          var h = {};
          var g;
          h.instance = function() {
            if (!g) {
              g = new b()
            }
            return g
          };
          return h
        })()
      }
    }
  }
  if (this !== window) {
    if (!this.__namespace) {
      this.__namespace = this.constructor.toString().match(/function ([^\(]+)/)[1]
    }
    this[a]._namespace = this.__namespace
  }
};
window.Inherit = function(f, a, d) {
  if (typeof d === "undefined") {
    d = f
  }
  var c = new a(d, true);
  var b = {};
  for (var e in c) {
    f[e] = c[e];
    b[e] = c[e]
  }
  if (f.__call) {
    f.__call()
  }
  defer(function() {
    for (e in c) {
      if ((f[e] && b[e]) && f[e] !== b[e]) {
        f["_" + e] = b[e]
      }
    }
    c = b = null;
    f = a = d = null
  })
};
window.Implement = function(b, a) {
  Render.nextFrame(function() {
    var c = new a();
    for (var e in c) {
      if (typeof b[e] === "undefined") {
        throw "Interface Error: Missing Property: " + e + " ::: " + a
      } else {
        var d = typeof c[e];
        if (typeof b[e] != d) {
          throw "Interface Error: Property " + e + " is Incorrect Type ::: " + a
        }
      }
    }
  })
};
window.Namespace = function(a) {
  if (typeof a === "string") {
    window[a] = {
      Class: window.Class
    }
  } else {
    a.Class = window.Class
  }
};
window.Interface = function(b) {
  var a = b.toString().match(/function ([^\(]+)/)[1];
  Hydra.INTERFACES[a] = b
};
window.THREAD = false;
Class(function HydraObject(a, b, d, c) {
  this._children = new LinkedList();
  this.__useFragment = c;
  this._initSelector(a, b, d)
}, function() {
  var a = HydraObject.prototype;
  a._initSelector = function(b, d, f) {
    if (b && typeof b !== "string") {
      this.div = b
    } else {
      var e = b ? b.charAt(0) : null;
      var c = b ? b.slice(1) : null;
      if (e != "." && e != "#") {
        c = b;
        e = "."
      }
      if (!f) {
        this._type = d || "div";
        if (this._type == "svg") {
          this.div = document.createElementNS("http://www.w3.org/2000/svg", this._type);
          this.div.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink")
        } else {
          this.div = document.createElement(this._type);
          if (e) {
            if (e == "#") {
              this.div.id = c
            } else {
              this.div.className = c
            }
          }
        }
      } else {
        if (e != "#") {
          throw "Hydra Selectors Require #ID"
        }
        this.div = document.getElementById(c)
      }
    }
    this.div.hydraObject = this
  };
  a.addChild = a.add = function(d) {
    var c = this.div;
    var b = function() {
      if (this.__useFragment) {
        if (!this._fragment) {
          this._fragment = document.createDocumentFragment();
          var e = this;
          defer(function() {
            if (!e._fragment || !e.div) {
              return e._fragment = null
            }
            e.div.appendChild(e._fragment);
            e._fragment = null
          })
        }
        c = this._fragment
      }
    };
    if (d.element && d.element instanceof HydraObject) {
      b();
      c.appendChild(d.element.div);
      this._children.push(d.element);
      d.element._parent = this;
      d.element.div.parentNode = this.div
    } else {
      if (d.div) {
        b();
        c.appendChild(d.div);
        this._children.push(d);
        d._parent = this;
        d.div.parentNode = this.div
      } else {
        if (d.nodeName) {
          b();
          c.appendChild(d);
          d.parentNode = this.div
        }
      }
    }
    return this
  };
  a.clone = function() {
    return $(this.div.cloneNode(true))
  };
  a.create = function(b, c) {
    var d = $(b, c);
    this.addChild(d);
    if (this.__root) {
      this.__root.__append[b] = d;
      d.__root = this.__root
    }
    return d
  };
  a.empty = function() {
    var b = this._children.start();
    while (b) {
      if (b && b.remove) {
        b.remove()
      }
      b = this._children.next()
    }
    this.div.innerHTML = "";
    return this
  };
  a.parent = function() {
    return this._parent
  };
  a.children = function() {
    return this.div.children ? this.div.children : this.div.childNodes
  };
  a.append = function(c, b) {
    if (!this.__root) {
      this.__root = this;
      this.__append = {}
    }
    return c.apply(this, b)
  };
  a.removeChild = function(c, b) {
    try {
      c.div.parentNode.removeChild(c.div)
    } catch (d) {}
    if (!b) {
      this._children.remove(c)
    }
  };
  a.remove = a.destroy = function() {
    this.removed = true;
    var b = this._parent;
    if (!!(b && !b.removed && b.removeChild)) {
      b.removeChild(this, true)
    }
    var c = this._children.start();
    while (c) {
      if (c && c.remove) {
        c.remove()
      }
      c = this._children.next()
    }
    this._children.destroy();
    this.div.hydraObject = null;
    Utils.nullObject(this)
  }
});
Class(function Hydra() {
  var f = this;
  var d, b;
  var e = [];
  this.READY = false;
  this.HASH = window.location.hash.slice(1);
  this.LOCAL = location.hostname.indexOf("local") > -1 || location.hostname.split(".")[0] == "10" || location.hostname.split(".")[0] == "192" || location.hostname.split(".")[0] == "127";
  (function() {
    a()
  })();

  function a() {
    if (!document || !window) {
      return setTimeout(a, 1)
    }
    if (window._NODE_) {
      f.addEvent = "addEventListener";
      f.removeEvent = "removeEventListener";
      return setTimeout(c, 1)
    }
    if (window.addEventListener) {
      f.addEvent = "addEventListener";
      f.removeEvent = "removeEventListener";
      window.addEventListener("load", c, false)
    } else {
      f.addEvent = "attachEvent";
      f.removeEvent = "detachEvent";
      window.attachEvent("onload", c)
    }
  }

  function c() {
    if (window.removeEventListener) {
      window.removeEventListener("load", c, false)
    }
    for (var g = 0; g < e.length; g++) {
      e[g]()
    }
    e = null;
    f.READY = true;
    if (window.Main) {
      Hydra.Main = new window.Main()
    }
  }
  this.development = function(g) {
    if (!g) {
      clearInterval(d)
    } else {
      d = setInterval(function() {
        for (var l in window) {
          if (l.strpos("webkit")) {
            continue
          }
          var k = window[l];
          if (typeof k !== "function" && l.length > 2) {
            if (l.strpos("_ga") || l.strpos("_typeface_js")) {
              continue
            }
            var j = l.charAt(0);
            var h = l.charAt(1);
            if (j == "_" || j == "$") {
              if (h !== h.toUpperCase()) {
                console.log(window[l]);
                throw "Hydra Warning:: " + l + " leaking into global scope"
              }
            }
          }
        }
      }, 1000)
    }
  };
  this.getArguments = function(k) {
    var j = this.arguments;
    var g = [];
    for (var h = 1; h < j.length; h++) {
      if (j[h] !== null) {
        g.push(j[h])
      }
    }
    return g
  };
  this.getClassName = function(g) {
    return g.constructor.name || g.constructor.toString().match(/function ([^\(]+)/)[1]
  };
  this.ready = function(g) {
    if (this.READY) {
      return g()
    }
    e.push(g)
  };
  this.$ = function(g, h, j) {
    return new HydraObject(g, h, j)
  };
  this.__triggerReady = function() {
    c()
  };
  this.INTERFACES = {};
  this.HTML = {};
  this.JSON = {};
  this.$.fn = HydraObject.prototype;
  window.$ = this.$;
  window.ready = this.ready
}, "Static");
Hydra.ready(function() {
  window.__window = $(window);
  window.__document = $(document);
  window.__body = $(document.getElementsByTagName("body")[0]);
  var stageHolder = $(document.getElementById("stage-bg"));
  window.Stage = stageHolder.create("#Stage");
  Stage.size("100%");
  Stage.__useFragment = true;
  Stage.width = document.body.clientWidth || document.documentElement.offsetWidth || window.innerWidth;
  Stage.height = document.body.clientHeight || document.documentElement.offsetHeight || window.innerHeight;
  (function() {
    var b = Date.now();
    var a;
    setTimeout(function() {
      var g = ["hidden", "msHidden", "webkitHidden"];
      var f, e;
      (function() {
        for (var h in g) {
          if (document[g[h]] !== "undefined") {
            f = g[h];
            switch (f) {
              case "hidden":
                e = "visibilitychange";
                break;
              case "msHidden":
                e = "msvisibilitychange";
                break;
              case "webkitHidden":
                e = "webkitvisibilitychange";
                break
            }
            return
          }
        }
      })();
      if (typeof document[f] === "undefined") {
        if (Device.browser.ie) {
          document.onfocus = c;
          document.onblur = d
        } else {
          window.onfocus = c;
          window.onblur = d
        }
      } else {
        document.addEventListener(e, function() {
          var h = Date.now();
          if (h - b > 10) {
            if (document[f] === false) {
              c()
            } else {
              d()
            }
          }
          b = h
        })
      }
    }, 250);

    function c() {
      if (a != "focus") {
        HydraEvents._fireEvent(HydraEvents.BROWSER_FOCUS, {
          type: "focus"
        })
      }
      a = "focus"
    }

    function d() {
      if (a != "blur") {
        HydraEvents._fireEvent(HydraEvents.BROWSER_FOCUS, {
          type: "blur"
        })
      }
      a = "blur"
    }
  })();
  window.onresize = function() {
    if (!Device.mobile) {
      Stage.width = document.body.clientWidth || document.documentElement.offsetWidth || window.innerWidth;
      Stage.height = document.body.clientHeight || document.documentElement.offsetHeight || window.innerHeight;
      HydraEvents._fireEvent(HydraEvents.RESIZE)
    }
  }
});
(function() {
  $.fn.text = function(a) {
    if (typeof a !== "undefined") {
      this.div.textContent = a;
      return this
    } else {
      return this.div.textContent
    }
  };
  $.fn.html = function(b, a) {
    if (b && !b.strpos("<") && !a) {
      return this.text(b)
    }
    if (typeof b !== "undefined") {
      this.div.innerHTML = b;
      return this
    } else {
      return this.div.innerHTML
    }
  };
  $.fn.hide = function() {
    this.div.style.display = "none";
    return this
  };
  $.fn.show = function() {
    this.div.style.display = "";
    return this
  };
  $.fn.visible = function() {
    this.div.style.visibility = "visible";
    return this
  };
  $.fn.invisible = function() {
    this.div.style.visibility = "hidden";
    return this
  };
  $.fn.setZ = function(a) {
    this.div.style.zIndex = a;
    return this
  };
  $.fn.clearAlpha = function() {
    this.div.style.opacity = "";
    return this
  };
  $.fn.size = function(a, b, c) {
    if (typeof a === "string") {
      if (typeof b === "undefined") {
        b = "100%"
      } else {
        if (typeof b !== "string") {
          b = b + "px"
        }
      }
      this.div.style.width = a;
      this.div.style.height = b
    } else {
      this.div.style.width = a + "px";
      this.div.style.height = b + "px";
      if (!c) {
        this.div.style.backgroundSize = a + "px " + b + "px"
      }
    }
    this.width = a;
    this.height = b;
    return this
  };
  $.fn.mouseEnabled = function(a) {
    this.div.style.pointerEvents = a ? "auto" : "none";
    return this
  };
  // $.fn.fontStyle = function(e, c, b, d) {
  //   var a = {};
  //   if (e) {
  //     a.fontFamily = e
  //   }
  //   if (c) {
  //     a.fontSize = c
  //   }
  //   if (b) {
  //     a.color = b
  //   }
  //   if (d) {
  //     a.fontStyle = d
  //   }
  //   this.css(a);
  //   return this
  // };
  $.fn.bg = function(c, a, d, b) {
    if (!c) {
      return this
    }
    if (Hydra.CDN) {
      if (!c.strpos("http") && c.strpos(".")) {
        c = Hydra.CDN + c
      }
    }
    if (!c.strpos(".")) {
      this.div.style.backgroundColor = c
    } else {
      this.div.style.backgroundImage = "url(" + c + ")"
    }
    if (typeof a !== "undefined") {
      a = typeof a == "number" ? a + "px" : a;
      d = typeof d == "number" ? d + "px" : d;
      this.div.style.backgroundPosition = a + " " + d
    }
    if (b) {
      this.div.style.backgroundSize = "";
      this.div.style.backgroundRepeat = b
    }
    if (a == "cover" || a == "contain") {
      this.div.style.backgroundSize = a;
      this.div.style.backgroundPosition = typeof d != "undefined" ? d + " " + b : "center"
    }
    return this
  };
  $.fn.center = function(a, d, b) {
    var c = {};
    if (typeof a === "undefined") {
      c.left = "50%";
      c.top = "50%";
      c.marginLeft = -this.width / 2;
      c.marginTop = -this.height / 2
    } else {
      if (a) {
        c.left = "50%";
        c.marginLeft = -this.width / 2
      }
      if (d) {
        c.top = "50%";
        c.marginTop = -this.height / 2
      }
    }
    if (b) {
      delete c.left;
      delete c.top
    }
    this.css(c);
    return this
  };
  $.fn.mask = function(b, a, e, c, d) {
    this.div.style[CSS.prefix("Mask")] = (b.strpos(".") ? "url(" + b + ")" : b) + " no-repeat";
    return this
  };
  $.fn.blendMode = function(b, a) {
    if (a) {
      this.div.style["background-blend-mode"] = b
    } else {
      this.div.style["mix-blend-mode"] = b
    }
    return this
  };
  $.fn.css = function(d, c) {
    if (typeof c == "boolean") {
      skip = c;
      c = null
    }
    if (typeof d !== "object") {
      if (!c) {
        var b = this.div.style[d];
        if (typeof b !== "number") {
          if (b.strpos("px")) {
            b = Number(b.slice(0, -2))
          }
          if (d == "opacity") {
            b = !isNaN(Number(this.div.style.opacity)) ? Number(this.div.style.opacity) : 1
          }
        }
        if (!b) {
          b = 0
        }
        return b
      } else {
        this.div.style[d] = c;
        return this
      }
    }
    TweenManager.clearCSSTween(this);
    for (var a in d) {
      var e = d[a];
      if (!(typeof e === "string" || typeof e === "number")) {
        continue
      }
      if (typeof e !== "string" && a != "opacity" && a != "zIndex") {
        e += "px"
      }
      this.div.style[a] = e
    }
    return this
  };
  $.fn.transform = function(c) {
    if (this.multiTween && this._cssTweens.length > 1 && this.__transformTime && Render.TIME - this.__transformTime < 15) {
      return
    }
    this.__transformTime = Render.TIME;
    TweenManager.clearCSSTween(this);
    if (Device.tween.css2d) {
      if (!c) {
        c = this
      } else {
        for (var b in c) {
          if (typeof c[b] === "number") {
            this[b] = c[b]
          }
        }
      }
      var a;
      if (!this._matrix) {
        a = TweenManager.parseTransform(c)
      } else {
        if (this._matrix.type == "matrix2") {
          this._matrix.setTRS(this.x, this.y, this.rotation, this.scaleX || this.scale, this.scaleY || this.scale)
        } else {
          this._matrix.setTRS(this.x, this.y, this.z, this.rotationX, this.rotationY, this.rotationZ, this.scaleX || this.scale, this.scaleY || this.scale, this.scaleZ || this.scale)
        }
        a = this._matrix.getCSS()
      }
      if (this.__transformCache != a) {
        this.div.style[Device.styles.vendorTransform] = a;
        this.__transformCache = a
      }
    }
    return this
  };
  $.fn.useMatrix3D = function() {
    this._matrix = new Matrix4();
    this.x = 0;
    this.y = 0;
    this.z = 0;
    this.rotationX = 0;
    this.rotationY = 0;
    this.rotationZ = 0;
    this.scale = 1;
    return this
  };
  $.fn.useMatrix2D = function() {
    this._matrix = new Matrix2();
    this.x = 0;
    this.y = 0;
    this.rotation = 0;
    this.scale = 1;
    return this
  };
  $.fn.willChange = function(b) {
    if (typeof b === "boolean") {
      if (b === true) {
        this._willChangeLock = true
      } else {
        this._willChangeLock = false
      }
    } else {
      if (this._willChangeLock) {
        return
      }
    }
    var a = typeof b === "string";
    if ((!this._willChange || a) && typeof b !== "null") {
      this._willChange = true;
      this.div.style["will-change"] = a ? b : Device.transformProperty + ", opacity"
    } else {
      this._willChange = false;
      this.div.style["will-change"] = ""
    }
  };
  $.fn.backfaceVisibility = function(a) {
    if (a) {
      this.div.style[CSS.prefix("BackfaceVisibility")] = "visible"
    } else {
      this.div.style[CSS.prefix("BackfaceVisibility")] = "hidden"
    }
  };
  $.fn.enable3D = function(b, a, c) {
    this.div.style[CSS.prefix("TransformStyle")] = "preserve-3d";
    if (b) {
      this.div.style[CSS.prefix("Perspective")] = b + "px"
    }
    if (typeof a !== "undefined") {
      a = typeof a === "number" ? a + "px" : a;
      c = typeof c === "number" ? c + "px" : c;
      this.div.style[CSS.prefix("PerspectiveOrigin")] = a + " " + c
    }
    return this
  };
  $.fn.disable3D = function() {
    this.div.style[CSS.prefix("TransformStyle")] = "";
    this.div.style[CSS.prefix("Perspective")] = "";
    return this
  };
  $.fn.transformPoint = function(a, d, c) {
    var b = "";
    if (typeof a !== "undefined") {
      b += (typeof a === "number" ? a + "px " : a + " ")
    }
    if (typeof d !== "undefined") {
      b += (typeof d === "number" ? d + "px " : d + " ")
    }
    if (typeof c !== "undefined") {
      b += (typeof c === "number" ? c + "px" : c)
    }
    this.div.style[CSS.prefix("TransformOrigin")] = b;
    return this
  };
  $.fn.tween = function(c, d, e, a, f, b) {
    if (typeof a === "boolean") {
      b = a;
      a = 0;
      f = null
    } else {
      if (typeof a === "function") {
        f = a;
        a = 0
      }
    }
    if (typeof f === "boolean") {
      b = f;
      f = null
    }
    if (!a) {
      a = 0
    }
    return TweenManager._detectTween(this, c, d, e, a, f, b)
  };
  $.fn.clearTransform = function() {
    if (typeof this.x === "number") {
      this.x = 0
    }
    if (typeof this.y === "number") {
      this.y = 0
    }
    if (typeof this.z === "number") {
      this.z = 0
    }
    if (typeof this.scale === "number") {
      this.scale = 1
    }
    if (typeof this.scaleX === "number") {
      this.scaleX = 1
    }
    if (typeof this.scaleY === "number") {
      this.scaleY = 1
    }
    if (typeof this.rotation === "number") {
      this.rotation = 0
    }
    if (typeof this.rotationX === "number") {
      this.rotationX = 0
    }
    if (typeof this.rotationY === "number") {
      this.rotationY = 0
    }
    if (typeof this.rotationZ === "number") {
      this.rotationZ = 0
    }
    if (typeof this.skewX === "number") {
      this.skewX = 0
    }
    if (typeof this.skewY === "number") {
      this.skewY = 0
    }
    this.div.style[Device.styles.vendorTransform] = "";
    return this
  };
  $.fn.stopTween = function() {
    if (this._cssTween) {
      this._cssTween.stop()
    }
    if (this._mathTween) {
      this._mathTween.stop()
    }
    return this
  };
  $.fn.keypress = function(a) {
    this.div.onkeypress = function(b) {
      b = b || window.event;
      b.code = b.keyCode ? b.keyCode : b.charCode;
      if (a) {
        a(b)
      }
    }
  };
  $.fn.keydown = function(a) {
    this.div.onkeydown = function(b) {
      b = b || window.event;
      b.code = b.keyCode;
      if (a) {
        a(b)
      }
    }
  };
  $.fn.keyup = function(a) {
    this.div.onkeyup = function(b) {
      b = b || window.event;
      b.code = b.keyCode;
      if (a) {
        a(b)
      }
    }
  };
  $.fn.attr = function(a, b) {
    if (a && b) {
      if (b == "") {
        this.div.removeAttribute(a)
      } else {
        this.div.setAttribute(a, b)
      }
    } else {
      if (a) {
        return this.div.getAttribute(a)
      }
    }
    return this
  };
  $.fn.val = function(a) {
    if (typeof a === "undefined") {
      return this.div.value
    } else {
      this.div.value = a
    }
    return this
  };
  $.fn.change = function(b) {
    var a = this;
    if (this._type == "select") {
      this.div.onchange = function() {
        b({
          object: a,
          value: a.div.value || ""
        })
      }
    }
  };
  $.fn.svgSymbol = function(e, d, a) {
    var c = SVG.getSymbolConfig(e);
    var b = '<svg viewBox="0 0 ' + c.width + " " + c.height + '" width="' + d + '" height="' + a + '"><use xlink:href="#' + c.id + '" x="0" y="0" /></svg>';
    this.html(b, true)
  }
})();
(function() {
  var a = !!window.MSGesture;
  var b = function(c) {
    if (Hydra.addEvent == "attachEvent") {
      switch (c) {
        case "click":
          return "onclick";
          break;
        case "mouseover":
          return "onmouseover";
          break;
        case "mouseout":
          return "onmouseleave";
          break;
        case "mousedown":
          return "onmousedown";
          break;
        case "mouseup":
          return "onmouseup";
          break;
        case "mousemove":
          return "onmousemove";
          break
      }
    }
    if (a) {
      switch (c) {
        case "touchstart":
          return "pointerdown";
          break;
        case "touchmove":
          return "MSGestureChange";
          break;
        case "touchend":
          return "pointerup";
          break
      }
    }
    return c
  };
  $.fn.click = function(e) {
    var d = this;

    function c(f) {
      if (!d.div) {
        return false
      }
      if (Mouse._preventClicks) {
        return false
      }
      f.object = d.div.className == "hit" ? d.parent() : d;
      f.action = "click";
      if (!f.pageX) {
        f.pageX = f.clientX;
        f.pageY = f.clientY
      }
      if (e) {
        e(f)
      }
      if (Mouse.autoPreventClicks) {
        Mouse.preventClicks()
      }
    }
    this.div[Hydra.addEvent](b("click"), c, true);
    this.div.style.cursor = "pointer";
    return this
  };
  $.fn.hover = function(h) {
    var g = this;
    var f = false;
    var e;

    function c(l) {
      if (!g.div) {
        return false
      }
      var k = Date.now();
      var j = l.toElement || l.relatedTarget;
      if (e && (k - e) < 5) {
        e = k;
        return false
      }
      e = k;
      l.object = g.div.className == "hit" ? g.parent() : g;
      switch (l.type) {
        case "mouseout":
          l.action = "out";
          break;
        case "mouseleave":
          l.action = "out";
          break;
        default:
          l.action = "over";
          break
      }
      if (f) {
        if (Mouse._preventClicks) {
          return false
        }
        if (l.action == "over") {
          return false
        }
        if (l.action == "out") {
          if (d(g.div, j)) {
            return false
          }
        }
        f = false
      } else {
        if (l.action == "out") {
          return false
        }
        f = true
      }
      if (!l.pageX) {
        l.pageX = l.clientX;
        l.pageY = l.clientY
      }
      if (h) {
        h(l)
      }
    }

    function d(m, k) {
      var j = m.children.length - 1;
      for (var l = j; l > -1; l--) {
        if (k == m.children[l]) {
          return true
        }
      }
      for (l = j; l > -1; l--) {
        if (d(m.children[l], k)) {
          return true
        }
      }
    }
    this.div[Hydra.addEvent](b("mouseover"), c, true);
    this.div[Hydra.addEvent](b("mouseout"), c, true);
    return this
  };
  $.fn.press = function(e) {
    var d = this;

    function c(f) {
      if (!d.div) {
        return false
      }
      f.object = d.div.className == "hit" ? d.parent() : d;
      switch (f.type) {
        case "mousedown":
          f.action = "down";
          break;
        default:
          f.action = "up";
          break
      }
      if (!f.pageX) {
        f.pageX = f.clientX;
        f.pageY = f.clientY
      }
      if (e) {
        e(f)
      }
    }
    this.div[Hydra.addEvent](b("mousedown"), c, true);
    this.div[Hydra.addEvent](b("mouseup"), c, true);
    return this
  };
  $.fn.bind = function(d, j) {
    // console.warn('bind', d, j);
    if (!this._events) {
      this._events = {}
    }
    if (a && this == __window) {
      return Stage.bind(d, j)
    }
    if (d == "touchstart") {
      if (!Device.mobile) {
        d = "mousedown"
      }
    } else {
      if (d == "touchmove") {
        if (!Device.mobile) {
          d = "mousemove"
        }
        if (a && !this.div.msGesture) {
          this.div.msGesture = new MSGesture();
          this.div.msGesture.target = this.div
        }
      } else {
        if (d == "touchend") {
          if (!Device.mobile) {
            d = "mouseup"
          }
        }
      }
    }
    this._events["bind_" + d] = this._events["bind_" + d] || [];
    var h = this._events["bind_" + d];
    var g = {};
    var f = this.div;
    g.callback = j;
    g.target = this.div;
    h.push(g);

    function c(o) {
      if (a && f.msGesture && d == "touchstart") {
        f.msGesture.addPointer(o.pointerId)
      }
      var p = Utils.touchEvent(o);
      if (a) {
        var n = o;
        o = {};
        o.x = Number(n.pageX || n.clientX);
        o.y = Number(n.pageY || n.clientY);
        o.target = n.target;
        o.currentTarget = n.currentTarget;
        o.path = [];
        var m = o.target;
        while (m) {
          o.path.push(m);
          m = m.parentElement || null
        }
        o.windowsPointer = true
      } else {
        o.x = p.x;
        o.y = p.y
      }
      for (var k = 0; k < h.length; k++) {
        var l = h[k];
        if (l.target == o.currentTarget) {
          l.callback(o)
        }
      }
    }
    if (!this._events["fn_" + d]) {
      this._events["fn_" + d] = c;
      this.div[Hydra.addEvent](b(d), c, true)
    }
    return this
  };
  $.fn.unbind = function(c, g) {
    if (!this._events) {
      this._events = {}
    }
    if (a && this == __window) {
      return Stage.unbind(c, g)
    }
    if (c == "touchstart") {
      if (!Device.mobile) {
        c = "mousedown"
      }
    } else {
      if (c == "touchmove") {
        if (!Device.mobile) {
          c = "mousemove"
        }
      } else {
        if (c == "touchend") {
          if (!Device.mobile) {
            c = "mouseup"
          }
        }
      }
    }
    var f = this._events["bind_" + c];
    if (!f) {
      return this
    }
    for (var d = 0; d < f.length; d++) {
      var e = f[d];
      if (e.callback == g) {
        f.splice(d, 1)
      }
    }
    if (this._events["fn_" + c] && !f.length) {
      this.div[Hydra.removeEvent](b(c), this._events["fn_" + c], true);
      this._events["fn_" + c] = null
    }
    return this
  };
  $.fn.interact = function(d, c) {
    if (!this.hit) {
      this.hit = $(".hit");
      this.hit.css({
        width: "100%",
        height: "100%",
        zIndex: 99999,
        top: 0,
        left: 0,
        position: "absolute",
        background: "rgba(255, 255, 255, 0)"
      });
      this.addChild(this.hit)
    }
    if (!Device.mobile) {
      this.hit.hover(d).click(c)
    } else {
      this.hit.touchClick(d, c)
    }
  };
  $.fn.touchSwipe = function(k, c) {
    if (!window.addEventListener) {
      return this
    }
    var f = this;
    var d = c || 75;
    var m, l;
    var g = false;
    var n = {};
    if (Device.mobile) {
      this.div.addEventListener(b("touchstart"), e);
      this.div.addEventListener(b("touchend"), j);
      this.div.addEventListener(b("touchcancel"), j)
    }

    function e(o) {
      var p = Utils.touchEvent(o);
      if (!f.div) {
        return false
      }
      if (o.touches.length == 1) {
        m = p.x;
        l = p.y;
        g = true;
        f.div.addEventListener(b("touchmove"), h)
      }
    }

    function h(q) {
      if (!f.div) {
        return false
      }
      if (g) {
        var r = Utils.touchEvent(q);
        var p = m - r.x;
        var o = l - r.y;
        n.direction = null;
        n.moving = null;
        n.x = null;
        n.y = null;
        n.evt = q;
        if (Math.abs(p) >= d) {
          j();
          if (p > 0) {
            n.direction = "left"
          } else {
            n.direction = "right"
          }
        } else {
          if (Math.abs(o) >= d) {
            j();
            if (o > 0) {
              n.direction = "up"
            } else {
              n.direction = "down"
            }
          } else {
            n.moving = true;
            n.x = p;
            n.y = o
          }
        }
        if (k) {
          k(n, q)
        }
      }
    }

    function j(o) {
      if (!f.div) {
        return false
      }
      m = l = g = false;
      f.div.removeEventListener(b("touchmove"), h)
    }
    return this
  };
  $.fn.touchClick = function(f, l) {
    if (!window.addEventListener) {
      return this
    }
    var e = this;
    var n, m;
    var d = {};
    var g = {};
    if (Device.mobile) {
      this.div.addEventListener(b("touchmove"), h, false);
      this.div.addEventListener(b("touchstart"), c, false);
      this.div.addEventListener(b("touchend"), j, false)
    }

    function h(o) {
      if (!e.div) {
        return false
      }
      g = Utils.touchEvent(o);
      if (Utils.findDistance(d, g) > 5) {
        m = true
      } else {
        m = false
      }
    }

    function k(o) {
      var p = Utils.touchEvent(o);
      o.touchX = p.x;
      o.touchY = p.y;
      d.x = o.touchX;
      d.y = o.touchY
    }

    function c(o) {
      if (!e.div) {
        return false
      }
      n = Date.now();
      o.action = "over";
      o.object = e.div.className == "hit" ? e.parent() : e;
      k(o);
      if (f && !m) {
        f(o)
      }
    }

    function j(q) {
      if (!e.div) {
        return false
      }
      var p = Date.now();
      var o = false;
      q.object = e.div.className == "hit" ? e.parent() : e;
      k(q);
      if (n && p - n < 750) {
        if (Mouse._preventClicks) {
          return false
        }
        if (l && !m) {
          o = true;
          q.action = "click";
          if (l && !m) {
            l(q)
          }
          if (Mouse.autoPreventClicks) {
            Mouse.preventClicks()
          }
        }
      }
      if (f) {
        q.action = "out";
        if (!Mouse._preventFire) {
          f(q)
        }
      }
      m = false
    }
    return this
  }
})();
Class(function MVC() {
  Inherit(this, Events);
  var c = {};
  var b = {};
  var a = [];
  this.classes = {};

  function d(f, e) {
    c[e] = {};
    Object.defineProperty(f, e, {
      set: function(g) {
        if (c[e] && c[e].s) {
          c[e].s.call(f, g)
        }
        g = null
      },
      get: function() {
        if (c[e] && c[e].g) {
          return c[e].g.apply(f)
        }
      }
    })
  }
  this.set = function(f, e) {
    if (!c[f]) {
      d(this, f)
    }
    c[f].s = e
  };
  this.get = function(f, e) {
    if (!c[f]) {
      d(this, f)
    }
    c[f].g = e
  };
  this.delayedCall = function(j, e, f) {
    var h = this;
    var g = Timer.create(function() {
      if (h.destroy) {
        j(f)
      }
      h = j = null
    }, e || 0);
    a.push(g);
    if (a.length > 20) {
      a.shift()
    }
    return g
  };
  this.initClass = function(o, r, q, p, n, m, l, k) {
    var h = Utils.timestamp();
    if (window.Hydra) {
      Hydra.arguments = arguments
    }
    var j = new o(r, q, p, n, m, l, k);
    if (window.Hydra) {
      Hydra.arguments = null
    }
    j.parent = this;
    if (j.destroy) {
      this.classes[h] = j;
      this.classes[h].__id = h
    }
    var s = arguments[arguments.length - 1];
    if (Array.isArray(s) && s.length == 1 && s[0] instanceof HydraObject) {
      s[0].addChild(j)
    } else {
      if (this.element && s !== null) {
        this.element.addChild(j)
      }
    }
    return j
  };
  this.destroy = function() {
    if (this.onDestroy) {
      this.onDestroy()
    }
    for (var f in this.classes) {
      var e = this.classes[f];
      if (e && e.destroy) {
        e.destroy()
      }
    }
    this.clearTimers && this.clearTimers();
    this.classes = null;
    if (this.events) {
      this.events = this.events.destroy()
    }
    if (this.element && this.element.remove) {
      this.element = this.container = this.element.remove()
    }
    if (this.parent && this.parent.__destroyChild) {
      this.parent.__destroyChild(this.__id)
    }
    return Utils.nullObject(this)
  };
  this.clearTimers = function() {
    for (i = 0; i < a.length; i++) {
      clearTimeout(a[i])
    }
    a.length = 0
  };
  this.active = function(e, f) {
    if (typeof f !== "undefined") {
      b[e] = f
    } else {
      return b[e]
    }
  };
  this.__destroyChild = function(e) {
    delete this.classes[e]
  }
});
Class(function Model(a) {
  Inherit(this, MVC);
  var b = {};
  this.push = function(c, d) {
    b[c] = d
  };
  this.pull = function(c) {
    return b[c]
  };
  this.initWithData = function(f) {
    this.STATIC_DATA = f;
    for (var e in this) {
      var c = this[e];
      var g = false;
      for (var d in f) {
        if (d.toLowerCase().replace(/-/g, "") == e.toLowerCase()) {
          g = true;
          if (c.init) {
            c.init(f[d])
          }
        }
      }
      if (!g && c.init) {
        c.init()
      }
    }
  };
  this.loadData = function(c, e) {
    var d = this;
    XHR.get(c + "?" + Utils.timestamp(), function(f) {
      defer(function() {
        d.initWithData(f);
        e(f)
      })
    })
  };
  this.Class = function(d) {
    var c = d.toString().match(/function ([^\(]+)/)[1];
    this[c] = new d()
  }
});
Class(function View(e) {
  Inherit(this, MVC);
  var f;
  var c = Hydra.getClassName(e);
  this.element = $("." + c);
  this.element.__useFragment = true;
  this.css = function(h) {
    this.element.css(h);
    return this
  };
  this.transform = function(h) {
    this.element.transform(h || this);
    return this
  };
  this.tween = function(k, l, m, h, n, j) {
    return this.element.tween(k, l, m, h, n, j)
  };
  var b = Hydra.INTERFACES[c] || Hydra.INTERFACES[c + "UI"];
  if (b) {
    this.ui = {};
    var g = Hydra.getArguments();
    g.push(e);
    f = this.element.append(b, g);
    var a = this.element.__append;
    for (var d in a) {
      this.ui[d] = a[d]
    }
    if (f) {
      this.resize = function() {
        f.apply(this.ui, arguments)
      }
    }
  }
  this.__call = function() {
    this.events.scope(this)
  }
});
Class(function Controller(a) {
  Inherit(this, MVC);
  a = Hydra.getClassName(a);
  this.element = this.container = $("#" + a);
  this.element.__useFragment = true;
  this.css = function(b) {
    this.container.css(b)
  }
});
Class(function Component() {
  Inherit(this, MVC);
  this.__call = function() {
    this.events.scope(this);
    delete this.__call
  }
});
Class(function Utils() {
  var c = this;
  if (typeof Float32Array == "undefined") {
    Float32Array = Array
  }

  function b(e, d) {
    return a(Math.random(), e, d)
  }

  function a(e, f, d) {
    return f + (d - f) * e
  }
  this.doRandom = function(e, d) {
    return Math.round(b(e - 0.5, d + 0.5))
  };
  this.headsTails = function(d, e) {
    return !c.doRandom(0, 1) ? d : e
  };
  this.toDegrees = function(d) {
    return d * (180 / Math.PI)
  };
  this.toRadians = function(d) {
    return d * (Math.PI / 180)
  };
  this.findDistance = function(g, f) {
    var e = f.x - g.x;
    var d = f.y - g.y;
    return Math.sqrt(e * e + d * d)
  };
  this.timestamp = function() {
    var d = Date.now() + c.doRandom(0, 99999);
    return d.toString()
  };
  this.hitTestObject = function(k, j) {
    var e = k.x,
        o = k.y,
        p = k.width,
        l = k.height;
    var s = j.x,
        g = j.y,
        n = j.width,
        r = j.height;
    var d = e + p,
        m = o + l,
        q = s + n,
        f = g + r;
    if (s >= e && s <= d) {
      if (g >= o && g <= m) {
        return true
      } else {
        if (o >= g && o <= f) {
          return true
        }
      }
    } else {
      if (e >= s && e <= q) {
        if (g >= o && g <= m) {
          return true
        } else {
          if (o >= g && o <= f) {
            return true
          }
        }
      }
    }
    return false
  };
  this.randomColor = function() {
    var d = "#" + Math.floor(Math.random() * 16777215).toString(16);
    if (d.length < 7) {
      d = this.randomColor()
    }
    return d
  };
  this.touchEvent = function(f) {
    var d = {};
    d.x = 0;
    d.y = 0;
    if (f.windowsPointer) {
      return f
    }
    if (!f) {
      return d
    }
    if (Device.mobile && (f.touches || f.changedTouches)) {
      if (f.changedTouches.length) {
        d.x = f.changedTouches[0].pageX;
        d.y = f.changedTouches[0].pageY - Mobile.scrollTop
      } else {
        d.x = f.touches[0].pageX;
        d.y = f.touches[0].pageY - Mobile.scrollTop
      }
    } else {
      d.x = f.pageX;
      d.y = f.pageY
    }
    return d
  };
  this.clamp = function(e, f, d) {
    return Math.min(Math.max(e, f), d)
  };
  this.constrain = function(e, f, d) {
    return Math.min(Math.max(e, Math.min(f, d)), Math.max(f, d))
  };
  this.nullObject = function(d) {
    if (d.destroy || d.div) {
      for (var e in d) {
        if (typeof d[e] !== "undefined") {
          d[e] = null
        }
      }
    }
    return null
  };
  this.convertRange = this.range = function(d, m, g, f, j, l) {
    var h = (g - m);
    var k = (j - f);
    var e = (((d - m) * k) / h) + f;
    if (l) {
      return c.clamp(e, Math.min(f, j), Math.max(f, j))
    }
    return e
  };
  String.prototype.strpos = function(e) {
    if (Array.isArray(e)) {
      for (var d = 0; d < e.length; d++) {
        if (this.indexOf(e[d]) > -1) {
          return true
        }
      }
      return false
    } else {
      return this.indexOf(e) != -1
    }
  };
  String.prototype.clip = function(e, d) {
    return this.length > e ? this.slice(0, e) + d : this
  };
  String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1)
  };
  Array.prototype.findAndRemove = function(d) {
    var e = this.indexOf(d);
    if (e > -1) {
      return this.splice(e, 1)
    }
  }
}, "Static");
Class(function CSS() {
  var g = this;
  var f, b, a;
  Hydra.ready(function() {
    b = "";
    f = document.createElement("style");
    f.type = "text/css";
    document.getElementsByTagName("head")[0].appendChild(f)
  });

  function d(k) {
    var j = k.match(/[A-Z]/);
    var l = j ? j.index : null;
    if (l) {
      var m = k.slice(0, l);
      var h = k.slice(l);
      k = m + "-" + h.toLowerCase()
    }
    return k
  }

  function e(k) {
    var j = k.match(/\-/);
    var m = j ? j.index : null;
    if (m) {
      var n = k.slice(0, m);
      var h = k.slice(m).slice(1);
      var l = h.charAt(0);
      h = h.slice(1);
      h = l.toUpperCase() + h;
      k = n + h
    }
    return k
  }

  function c() {
    f.innerHTML = b;
    a = false
  }
  this._read = function() {
    return b
  };
  this._write = function(h) {
    b = h;
    if (!a) {
      a = true;
      Render.nextFrame(c)
    }
  };
  this._toCSS = d;
  this.style = function(h, l) {
    var k = h + " {";
    for (var j in l) {
      var n = d(j);
      var m = l[j];
      if (typeof m !== "string" && j != "opacity") {
        m += "px"
      }
      k += n + ":" + m + "!important;"
    }
    k += "}";
    f.innerHTML += k
  };
  this.get = function(k, h) {
    var q = new Object();
    var n = f.innerHTML.split(k + " {");
    for (var m = 0; m < n.length; m++) {
      var o = n[m];
      if (!o.length) {
        continue
      }
      var p = o.split("!important;");
      for (var l in p) {
        if (p[l].strpos(":")) {
          var r = p[l].split(":");
          if (r[1].slice(-2) == "px") {
            r[1] = Number(r[1].slice(0, -2))
          }
          q[e(r[0])] = r[1]
        }
      }
    }
    if (!h) {
      return q
    } else {
      return q[h]
    }
  };
  this.textSize = function(l) {
    var k = l.clone();
    k.css({
      position: "relative",
      cssFloat: "left",
      styleFloat: "left",
      marginTop: -99999,
      width: "",
      height: ""
    });
    __body.addChild(k);
    var j = k.div.offsetWidth;
    var h = k.div.offsetHeight;
    k.remove();
    return {
      width: j,
      height: h
    }
  };
  this.prefix = function(h) {
    return Device.styles.vendor == "" ? h.charAt(0).toLowerCase() + h.slice(1) : Device.styles.vendor + h
  }
}, "Static");
Class(function Device() {
  var g = this;
  var b;
  this.agent = navigator.userAgent.toLowerCase();
  this.detect = function(h) {
    if (typeof h === "string") {
      h = [h]
    }
    for (var e = 0; e < h.length; e++) {
      if (this.agent.strpos(h[e])) {
        return true
      }
    }
    return false
  };
  var d = (function() {
    var h = "";
    if (!window._NODE_) {
      var e = window.getComputedStyle(document.documentElement, "");
      h = (Array.prototype.slice.call(e).join("").match(/-(moz|webkit|ms)-/) || (e.OLink === "" && ["", "o"]))[1];
      var j = ("WebKit|Moz|MS|O").match(new RegExp("(" + h + ")", "i"))[1]
    } else {
      h = "webkit"
    }
    var k = g.detect("trident");
    return {
      unprefixed: k && !g.detect("msie 9"),
      dom: j,
      lowercase: h,
      css: "-" + h + "-",
      js: (k ? h[0] : h[0].toUpperCase()) + h.substr(1)
    }
  })();

  function a(k) {
    var j = b || document.createElement("div"),
        h = "Khtml ms O Moz Webkit".split(" "),
        e = h.length;
    b = j;
    if (k in j.style) {
      return true
    }
    k = k.replace(/^[a-z]/, function(l) {
      return l.toUpperCase()
    });
    while (e--) {
      if (h[e] + k in j.style) {
        return true
      }
    }
    return false
  }
  this.mobile = !window._NODE_ && (!!(("ontouchstart" in window) || ("onpointerdown" in window)) && this.detect(["ios", "iphone", "ipad", "windows", "android", "blackberry"])) ? {} : false;
  if (this.mobile && this.detect("windows") && !this.detect("touch")) {
    this.mobile = false
  }
  if (this.mobile) {
    this.mobile.tablet = Math.max(screen.width, screen.height) > 800;
    this.mobile.phone = !this.mobile.tablet
  }
  this.browser = {};
  this.browser.ie = (function() {
    if (g.detect("msie")) {
      return true
    }
    if (g.detect("trident") && g.detect("rv:")) {
      return true
    }
    if (g.detect("windows") && g.detect("edge")) {
      return true
    }
  })();
  this.browser.chrome = !this.browser.ie && this.detect("chrome");
  this.browser.safari = !this.browser.chrome && !this.browser.ie && this.detect("safari");
  this.browser.firefox = this.detect("firefox");
  this.browser.version = (function() {
    try {
      if (g.browser.chrome) {
        return Number(g.agent.split("chrome/")[1].split(".")[0])
      }
      if (g.browser.firefox) {
        return Number(g.agent.split("firefox/")[1].split(".")[0])
      }
      if (g.browser.safari) {
        return Number(g.agent.split("version/")[1].split(".")[0].charAt(0))
      }
      if (g.browser.ie) {
        if (g.detect("msie")) {
          return Number(g.agent.split("msie ")[1].split(".")[0])
        }
        if (g.detect("rv:")) {
          return Number(g.agent.split("rv:")[1].split(".")[0])
        }
        return Number(g.agent.split("edge/")[1].split(".")[0])
      }
    } catch (h) {
      return -1
    }
  })();
  this.vendor = d.css;
  this.transformProperty = (function() {
    switch (d.lowercase) {
      case "moz":
        return "-moz-transform";
        break;
      case "webkit":
        return "-webkit-transform";
        break;
      case "o":
        return "-o-transform";
        break;
      case "ms":
        return "-ms-transform";
        break;
      default:
        return "transform";
        break
    }
  })();
  this.system = {};
  this.system.retina = window.devicePixelRatio > 1;
  this.system.webworker = typeof window.Worker !== "undefined";
  this.system.offline = typeof window.applicationCache !== "undefined";
  if (!window._NODE_) {
    this.system.geolocation = typeof navigator.geolocation !== "undefined";
    this.system.pushstate = typeof window.history.pushState !== "undefined"
  }
  this.system.webcam = !!(navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);
  this.system.language = window.navigator.userLanguage || window.navigator.language;
  this.system.webaudio = typeof window.AudioContext !== "undefined";
  try {
    this.system.localStorage = typeof window.localStorage !== "undefined"
  } catch (f) {
    this.system.localStorage = false
  }
  this.system.fullscreen = typeof document[d.lowercase + "CancelFullScreen"] !== "undefined";
  this.system.os = (function() {
    if (g.detect("mac os")) {
      return "mac"
    } else {
      if (g.detect("windows nt 6.3")) {
        return "windows8.1"
      } else {
        if (g.detect("windows nt 6.2")) {
          return "windows8"
        } else {
          if (g.detect("windows nt 6.1")) {
            return "windows7"
          } else {
            if (g.detect("windows nt 6.0")) {
              return "windowsvista"
            } else {
              if (g.detect("windows nt 5.1")) {
                return "windowsxp"
              } else {
                if (g.detect("windows")) {
                  return "windows"
                } else {
                  if (g.detect("linux")) {
                    return "linux"
                  }
                }
              }
            }
          }
        }
      }
    }
    return "undetected"
  })();
  this.pixelRatio = window.devicePixelRatio;
  this.media = {};
  this.media.audio = (function() {
    if (!!document.createElement("audio").canPlayType) {
      return g.detect(["firefox", "opera"]) ? "ogg" : "mp3"
    } else {
      return false
    }
  })();
  this.media.video = (function() {
    var e = document.createElement("video");
    if (!!e.canPlayType) {
      if (Device.mobile) {
        return "mp4"
      }
      if (g.browser.chrome) {
        return "webm"
      }
      if (g.browser.firefox || g.browser.opera) {
        if (e.canPlayType('video/webm; codecs="vorbis,vp8"')) {
          return "webm"
        }
        return "ogv"
      }
      return "mp4"
    } else {
      return false
    }
  })();
  this.graphics = {};
  this.graphics.webgl = (function() {
    try {
      var p;
      var o = ["webgl", "experimental-webgl", "webkit-3d", "moz-webgl"];
      var k = document.createElement("canvas");
      for (var l = 0; l < o.length; l++) {
        p = k.getContext(o[l]);
        if (p) {
          break
        }
      }
      var n = p.getExtension("WEBGL_debug_renderer_info");
      var j = {};
      if (n) {
        var h = n.UNMASKED_RENDERER_WEBGL;
        j.gpu = p.getParameter(h).toLowerCase()
      }
      j.renderer = p.getParameter(p.RENDERER).toLowerCase();
      j.version = p.getParameter(p.VERSION).toLowerCase();
      j.glsl = p.getParameter(p.SHADING_LANGUAGE_VERSION).toLowerCase();
      j.extensions = p.getSupportedExtensions();
      j.detect = function(q) {
        if (j.gpu && j.gpu.toLowerCase().strpos(q)) {
          return true
        }
        if (j.version && j.version.toLowerCase().strpos(q)) {
          return true
        }
        for (var e = 0; e < j.extensions.length; e++) {
          if (j.extensions[e].toLowerCase().strpos(q)) {
            return true
          }
        }
        return false
      };
      return j
    } catch (m) {
      return false
    }
  })();
  this.graphics.canvas = (function() {
    var e = document.createElement("canvas");
    return e.getContext ? true : false
  })();
  this.styles = {};
  this.styles.filter = a("filter");
  this.styles.blendMode = a("mix-blend-mode");
  this.styles.vendor = d.unprefixed ? "" : d.js;
  this.styles.vendorTransition = this.styles.vendor.length ? this.styles.vendor + "Transition" : "transition";
  this.styles.vendorTransform = this.styles.vendor.length ? this.styles.vendor + "Transform" : "transform";
  this.tween = {};
  this.tween.transition = a("transition");
  this.tween.css2d = a("transform");
  this.tween.css3d = a("perspective");
  this.tween.complete = (function() {
    if (d.unprefixed) {
      return "transitionend"
    }
    return d.lowercase + "TransitionEnd"
  })();
  this.test = function(e, h) {
    this[e] = h()
  };
  this.detectGPU = function(h) {
    var e = g.graphics.webgl;
    if (e.gpu && e.gpu.strpos(h)) {
      return true
    }
    if (e.version && e.version.strpos(h)) {
      return true
    }
    return false
  };

  function c() {
    if (!g.getFullscreen()) {
      HydraEvents._fireEvent(HydraEvents.FULLSCREEN, {
        fullscreen: false
      });
      Render.stop(c)
    }
  }
  this.openFullscreen = function(e) {
    e = e || __body;
    if (e && g.system.fullscreen) {
      if (e == __body) {
        e.css({
          top: 0
        })
      }
      e.div[d.lowercase + "RequestFullScreen"]();
      HydraEvents._fireEvent(HydraEvents.FULLSCREEN, {
        fullscreen: true
      });
      Render.start(c, 10)
    }
  };
  this.closeFullscreen = function() {
    if (g.system.fullscreen) {
      document[d.lowercase + "CancelFullScreen"]()
    }
    Render.stop(c)
  };
  this.getFullscreen = function() {
    if (g.browser.firefox) {
      return document.mozFullScreen
    }
    return document[d.lowercase + "IsFullScreen"]
  }
}, "Static");
Class(function DynamicObject(a) {
  var b = DynamicObject.prototype;
  if (a) {
    for (var c in a) {
      this[c] = a[c]
    }
  }
  this._tweens = {};
  if (typeof b.tween !== "undefined") {
    return
  }
  b.tween = function(f, g, h, e, j, d) {
    if (typeof e !== "number") {
      d = j;
      j = e;
      e = 0
    }
    if (!this.multiTween) {
      this.stopTween()
    }
    if (typeof d !== "function") {
      d = null
    }
    if (typeof j !== "function") {
      j = null
    }
    this._tween = TweenManager.tween(this, f, g, h, e, d, j);
    return this._tween
  };
  b.stopTween = function(d) {
    var e = d || this._tween;
    if (e && e.stop) {
      e.stop()
    }
  };
  b.pause = function() {
    var d = this._tween;
    if (d && d.pause) {
      d.pause()
    }
  };
  b.resume = function() {
    var d = this._tween;
    if (d && d.resume) {
      d.resume()
    }
  };
  b.copy = function(e) {
    var f = e && e.get ? e.get() : new DynamicObject();
    for (var d in this) {
      if (typeof this[d] === "number") {
        f[d] = this[d]
      }
    }
    return f
  };
  b.copyFrom = function(e) {
    for (var d in e) {
      if (typeof e[d] == "number") {
        this[d] = e[d]
      }
    }
  };
  b.copyTo = function(e) {
    for (var d in e) {
      if (typeof this[d] == "number") {
        e[d] = this[d]
      }
    }
  };
  b.clear = function() {
    for (var d in this) {
      if (typeof this[d] !== "function") {
        delete this[d]
      }
    }
    return this
  }
});
Class(function ObjectPool(b, d) {
  Inherit(this, Component);
  var c = this;
  var a = [];
  (function() {
    if (b) {
      d = d || 10;
      b = b || Object;
      for (var e = 0; e < d; e++) {
        a.push(new b())
      }
    }
  })();
  this.get = function() {
    return a.shift()
  };
  this.empty = function() {
    a.length = 0
  };
  this.put = function(e) {
    if (e) {
      a.push(e)
    }
  };
  this.insert = function(f) {
    if (typeof f.push === "undefined") {
      f = [f]
    }
    for (var e = 0; e < f.length; e++) {
      a.push(f[e])
    }
  };
  this.onDestroy = function() {
    for (var e = 0; e < a.length; e++) {
      if (a[e].destroy) {
        a[e].destroy()
      }
    }
    a = null
  }
});
Class(function LinkedList() {
  var a = LinkedList.prototype;
  this.length = 0;
  this.first = null;
  this.last = null;
  this.current = null;
  this.prev = null;
  if (typeof a.push !== "undefined") {
    return
  }
  a.push = function(b) {
    if (!this.first) {
      this.first = b;
      this.last = b;
      b.__prev = b;
      b.__next = b
    } else {
      b.__next = this.first;
      b.__prev = this.last;
      this.last.__next = b;
      this.last = b
    }
    this.length++
  };
  a.remove = function(b) {
    if (!b || !b.__next) {
      return
    }
    if (this.length <= 1) {
      this.empty()
    } else {
      if (b == this.first) {
        this.first = b.__next;
        this.last.__next = this.first;
        this.first.__prev = this.last
      } else {
        if (b == this.last) {
          this.last = b.__prev;
          this.last.__next = this.first;
          this.first.__prev = this.last
        } else {
          b.__prev.__next = b.__next;
          b.__next.__prev = b.__prev
        }
      }
      this.length--
    }
    b.__prev = null;
    b.__next = null
  };
  a.empty = function() {
    this.first = null;
    this.last = null;
    this.current = null;
    this.prev = null;
    this.length = 0
  };
  a.start = function() {
    this.current = this.first;
    this.prev = this.current;
    return this.current
  };
  a.next = function() {
    if (!this.current) {
      return
    }
    this.current = this.current.__next;
    if (this.length == 1 || this.prev.__next == this.first) {
      return
    }
    this.prev = this.current;
    return this.current
  };
  a.destroy = function() {
    Utils.nullObject(this);
    return null
  }
});
Class(function Pact() {
  var a = this;
  Namespace(this);
  (function() {})();
  this.create = function() {
    return new a.Broadcaster(arguments)
  };
  this.batch = function() {
    return new a.Batch()
  }
}, "Static");
Pact.Class(function Broadcaster(e) {
  var g = this;
  var d, f;
  var b;
  var c = [];
  this._fire = this.fire = function() {
    if (b) {
      return
    }
    b = true;
    var h = arguments;
    var j = false;
    Render.nextFrame(function() {
      if (f || d) {
        var l = h[0];
        var k = h[1];
        if (l instanceof Error) {
          if (f) {
            f.apply(g, [l])
          }
          j = true
        } else {
          if (k instanceof Error) {
            if (f) {
              f.apply(g, [k])
            }
            j = true
          } else {
            if (!l && k && d) {
              d.apply(g, [k]);
              j = true
            }
            if (!k && l && d) {
              d.apply(g, [l]);
              j = true
            }
          }
        }
      }
      if (!j && c.length) {
        var m = c.shift();
        m.apply(g, h);
        if (c.length) {
          b = false
        }
      }
    })
  };
  this.exec = function() {
    a(arguments);
    return this
  };
  this.then = function(h) {
    c.push(h);
    return this
  };
  this.error = function(h) {
    f = h;
    return this
  };
  this.success = function(h) {
    d = h;
    return this
  };

  function a(l) {
    var h = [];
    var k = l[0];
    for (var j = 1; j < l.length; j++) {
      h.push(l[j])
    }
    h.push(g._fire);
    k.apply(k, h)
  }
  if (e.length) {
    a(e)
  }
});
Pact.Class(function Batch() {
  Inherit(this, Events);
  var e = this;
  var g = 0;
  var a = [];
  var j = [];
  var k = [];
  var c = [];
  this.push = function(l) {
    l.then(h).error(d).success(f);
    c.push(l)
  };
  this.timeout = function() {
    e.events.fire(HydraEvents.COMPLETE, {
      complete: a,
      success: j,
      error: k
    })
  };

  function f() {
    this.data = arguments;
    j.push(this);
    b();
    e.events.fire(HydraEvents.UPDATE)
  }

  function d() {
    this.data = arguments;
    k.push(this);
    b();
    e.events.fire(HydraEvents.UPDATE)
  }

  function h() {
    this.data = arguments;
    a.push(this);
    b();
    e.events.fire(HydraEvents.UPDATE)
  }

  function b() {
    g++;
    if (g == c.length) {
      e.events.fire(HydraEvents.COMPLETE, {
        complete: a,
        success: j,
        error: k
      })
    }
  }
});
Class(function Mouse() {
  var d = this;
  var b;
  this.x = 0;
  this.y = 0;
  this.lastX = 0;
  this.lastY = 0;
  this.moveX = 0;
  this.moveY = 0;
  this.autoPreventClicks = false;

  function c(g) {
    d.lastX = d.x;
    d.lastY = d.y;
    d.ready = true;
    if (g.windowsPointer) {
      d.x = g.x;
      d.y = g.y
    } else {
      var f = Utils.touchEvent(g);
      d.x = f.x;
      d.y = f.y
    }
    d.moveX = d.x - d.lastX;
    d.moveY = d.y - d.lastY;
    defer(a)
  }
  this.capture = function(e, f) {
    if (b) {
      return false
    }
    b = true;
    d.x = e || 0;
    d.y = f || 0;
    if (!Device.mobile) {
      __window.bind("mousemove", c)
    } else {
      __window.bind("touchmove", c);
      __window.bind("touchstart", c)
    }
  };
  this.stop = function() {
    if (!b) {
      return false
    }
    b = false;
    d.x = 0;
    d.y = 0;
    if (!Device.mobile) {
      __window.unbind("mousemove", c)
    } else {
      __window.unbind("touchmove", c);
      __window.unbind("touchstart", c)
    }
  };
  this.preventClicks = function() {
    d._preventClicks = true;
    Timer.create(function() {
      d._preventClicks = false
    }, 300)
  };
  this.preventFireAfterClick = function() {
    d._preventFire = true
  };

  function a() {
    d.moveX = 0;
    d.moveY = 0
  }
}, "Static");
Class(function Render() {
  var f = this;
  var c, j, a;
  var h = [];
  var o = Date.now();
  var m = new LinkedList();
  var l = new LinkedList();
  var d = m;
  var e = 0;
  this.TIME = Date.now();
  this.TARGET_FPS = 60;
  (function() {
    if (!THREAD) {
      requestAnimationFrame(b);
      Hydra.ready(k)
    }
  })();

  function b() {
    var q = Date.now();
    var v = q - o;
    var s = 0;
    var r = 60;
    if (j) {
      s = q - j;
      r = 1000 / s
    }
    j = q;
    f.FPS = r;
    f.TIME = q;
    f.DELTA = s;
    f.TSL = v;
    for (var p = h.length - 1; p > -1; p--) {
      var u = h[p];
      if (!u) {
        continue
      }
      if (u.fps) {
        e += s > 200 ? 0 : s;
        if (e < (1000 / u.fps)) {
          continue
        }
        e -= (1000 / u.fps)
      }
      u(q, v, s, r, u.frameCount++)
    }
    if (d.length) {
      g()
    }
    if (!THREAD) {
      requestAnimationFrame(b)
    }
  }

  function g() {
    var q = d;
    d = d == m ? l : m;
    var r = q.start();
    while (r) {
      var p = r;
      r();
      r = q.next();
      p.__prev = p.__next = p = null
    }
    q.empty()
  }

  function k() {
    HydraEvents._addEvent(HydraEvents.BROWSER_FOCUS, n, f)
  }

  function n(p) {
    if (p.type == "focus") {
      j = Date.now()
    }
  }
  this.startRender = this.start = function(s, q) {
    var r = true;
    var p = h.length - 1;
    if (this.TARGET_FPS < 60) {
      q = this.TARGET_FPS
    }
    if (typeof q == "number") {
      s.fps = q
    }
    s.frameCount = 0;
    if (h.indexOf(s) == -1) {
      h.push(s)
    }
  };
  this.stopRender = this.stop = function(q) {
    var p = h.indexOf(q);
    if (p > -1) {
      h.splice(p, 1)
    }
  };
  this.startTimer = function(p) {
    a = p || "Timer";
    if (console.time && !window._NODE_) {
      console.time(a)
    } else {
      c = Date.now()
    }
  };
  this.stopTimer = function() {
    if (console.time && !window._NODE_) {
      console.timeEnd(a)
    } else {
      console.log("Render " + a + ": " + (Date.now() - c))
    }
  };
  this.nextFrame = function(p) {
    d.push(p)
  };
  this.setupTween = function(p) {
    f.nextFrame(function() {
      f.nextFrame(p)
    })
  };
  this.tick = function() {
    b()
  };
  window.defer = this.nextFrame;
  window.nextFrame = this.setupTween
}, "Static");
Class(function HydraEvents() {
  var b = [];
  var a = {};
  this.BROWSER_FOCUS = "hydra_focus";
  this.HASH_UPDATE = "hydra_hash_update";
  this.COMPLETE = "hydra_complete";
  this.PROGRESS = "hydra_progress";
  this.UPDATE = "hydra_update";
  this.LOADED = "hydra_loaded";
  this.END = "hydra_end";
  this.FAIL = "hydra_fail";
  this.SELECT = "hydra_select";
  this.ERROR = "hydra_error";
  this.READY = "hydra_ready";
  this.RESIZE = "hydra_resize";
  this.CLICK = "hydra_click";
  this.HOVER = "hydra_hover";
  this.MESSAGE = "hydra_message";
  this.ORIENTATION = "orientation";
  this.BACKGROUND = "background";
  this.BACK = "hydra_back";
  this.PREVIOUS = "hydra_previous";
  this.NEXT = "hydra_next";
  this.RELOAD = "hydra_reload";
  this.FULLSCREEN = "hydra_fullscreen";
  this._checkDefinition = function(c) {
    if (typeof c == "undefined") {
      throw "Undefined event"
    }
  };
  this._addEvent = function(f, g, c) {
    if (this._checkDefinition) {
      this._checkDefinition(f)
    }
    var d = new Object();
    d.evt = f;
    d.object = c;
    d.callback = g;
    b.push(d)
  };
  this._removeEvent = function(c, d) {
    if (this._checkDefinition) {
      this._checkDefinition(c)
    }
    defer(function() {
      for (var e = b.length - 1; e > -1; e--) {
        if (b[e].evt == c && b[e].callback == d) {
          b[e] = null;
          b.splice(e, 1)
        }
      }
    })
  };
  this._destroyEvents = function(c) {
    for (var d = b.length - 1; d > -1; d--) {
      if (b[d].object == c) {
        b[d] = null;
        b.splice(d, 1)
      }
    }
  };
  this._fireEvent = function(c, f) {
    if (this._checkDefinition) {
      this._checkDefinition(c)
    }
    var e = true;
    f = f || a;
    f.cancel = function() {
      e = false
    };
    for (var d = 0; d < b.length; d++) {
      if (b[d].evt == c) {
        if (e) {
          b[d].callback(f)
        } else {
          return false
        }
      }
    }
  };
  this._consoleEvents = function() {
    console.log(b)
  };
  this.createLocalEmitter = function(d) {
    var c = new HydraEvents();
    d.on = c._addEvent;
    d.off = c._removeEvent;
    d.fire = c._fireEvent
  }
}, "Static");
Class(function Events(c) {
  this.events = {};
  var b = {};
  var a = {};
  this.events.subscribe = function(d, e) {
    HydraEvents._addEvent(d, !!e._fire ? e._fire : e, c);
    return e
  };
  this.events.unsubscribe = function(d, e) {
    HydraEvents._removeEvent(d, !!e._fire ? e._fire : e)
  };
  this.events.fire = function(d, f, e) {
    f = f || a;
    HydraEvents._checkDefinition(d);
    if (b[d]) {
      f.target = f.target || c;
      b[d](f);
      f.target = null
    } else {
      if (!e) {
        HydraEvents._fireEvent(d, f)
      }
    }
  };
  this.events.add = function(d, e) {
    HydraEvents._checkDefinition(d);
    b[d] = !!e._fire ? e._fire : e;
    return e
  };
  this.events.remove = function(d) {
    HydraEvents._checkDefinition(d);
    if (b[d]) {
      delete b[d]
    }
  };
  this.events.bubble = function(e, d) {
    HydraEvents._checkDefinition(d);
    var f = this;
    e.events.add(d, function(g) {
      f.fire(d, g)
    })
  };
  this.events.scope = function(d) {
    c = d
  };
  this.events.destroy = function() {
    HydraEvents._destroyEvents(c);
    b = null;
    c = null;
    return null
  }
});
Class(function Dispatch() {
  var c = this;
  var a = {};

  function b() {}
  this.register = function(d, e) {
    defer(function() {
      a[Hydra.getClassName(d) + "-" + e] = d[e]
    })
  };
  this.find = function(e, g, d) {
    var f = e.toString().match(/function ([^\(]+)/)[1] + "-" + g;
    if (a[f]) {
      return a[f]
    } else {
      delete a[f];
      return b
    }
  }
}, "static");
Class(function Mobile() {
  Inherit(this, Component);
  var s = this;
  var e;
  var f = true;
  var q = {};
  var n, l, w, o, c, k, t;
  this.sleepTime = 10000;
  this.scrollTop = 0;
  this.autoResizeReload = true;
  if (Device.mobile) {
    for (var v in Device.browser) {
      Device.browser[v] = false
    }
    setInterval(r, 250);
    this.phone = Device.mobile.phone;
    this.tablet = Device.mobile.tablet;
    this.orientation = Math.abs(window.orientation) == 90 ? "landscape" : "portrait";
    this.os = (function() {
      if (Device.detect("windows", "iemobile")) {
        return "Windows"
      }
      if (Device.detect(["ipad", "iphone"])) {
        return "iOS"
      }
      if (Device.detect(["android", "kindle"])) {
        return "Android"
      }
      if (Device.detect("blackberry")) {
        return "Blackberry"
      }
      return "Unknown"
    })();
    this.version = (function() {
      try {
        if (s.os == "iOS") {
          var y = Device.agent.split("os ")[1].split("_");
          var b = y[0];
          var z = y[1].split(" ")[0];
          return Number(b + "." + z)
        }
        if (s.os == "Android") {
          var x = Device.agent.split("android ")[1].split(";")[0];
          if (x.length > 3) {
            x = x.slice(0, -2)
          }
          return Number(x)
        }
        if (s.os == "Windows") {
          if (Device.agent.strpos("rv:11")) {
            return 11
          }
          return Number(Device.agent.split("windows phone ")[1].split(";")[0])
        }
      } catch (A) {}
      return -1
    })();
    this.browser = (function() {
      if (s.os == "iOS") {
        if (Device.detect(["twitter", "fbios"])) {
          return "Social"
        }
        if (Device.detect("crios")) {
          return "Chrome"
        }
        if (Device.detect("safari")) {
          return "Safari"
        }
        return "Unknown"
      }
      if (s.os == "Android") {
        if (Device.detect("chrome")) {
          return "Chrome"
        }
        if (Device.detect("firefox")) {
          return "Firefox"
        }
        return "Browser"
      }
      if (s.os == "Windows") {
        return "IE"
      }
      return "Unknown"
    })();
    Hydra.ready(function() {
      window.onresize = u;
      if (s.browser == "Safari" && (!s.NativeCore || !s.NativeCore.active)) {
        document.body.scrollTop = 0;
        __body.css({
          height: "101%"
        })
      }
      j();
      s.orientation = Stage.width > Stage.height ? "landscape" : "portrait";
      if (!(s.NativeCore && s.NativeCore.active)) {
        window.addEventListener("touchstart", d)
      } else {
        Stage.css({
          overflow: "hidden"
        })
      }
      m();
      t = s.phone ? "phone" : "tablet"
    });

    function m() {
      Device.mobile.tablet = (function() {
        if (Stage.width > Stage.height) {
          return document.body.clientWidth > 800
        } else {
          return document.body.clientHeight > 800
        }
      })();
      Device.mobile.phone = !Device.mobile.tablet;
      s.phone = Device.mobile.phone;
      s.tablet = Device.mobile.tablet
    }

    function j() {
      Stage.width = document.body.clientWidth;
      Stage.height = document.body.clientHeight;
      if (s.browser == "Social" && s.os == "iOS") {
        Stage.width = window.innerWidth;
        Stage.height = window.innerHeight
      }
    }

    function u() {
      clearTimeout(s.fireResize);
      if (!s.allowScroll) {
        document.body.scrollTop = 0
      }
      s.fireResize = s.delayedCall(function() {
        j();
        m();
        var b = s.phone ? "phone" : "tablet";
        if (s.os == "iOS" && b != t && s.autoResizeReload) {
          window.location.reload()
        }
        s.orientation = Stage.width > Stage.height ? "landscape" : "portrait";
        s.events.fire(HydraEvents.RESIZE)
      }, 32)
    }

    function a() {
      s.delayedCall(function() {
        Stage.width = document.body.clientWidth;
        Stage.height = document.body.clientHeight;
        HydraEvents._fireEvent(HydraEvents.ORIENTATION, {
          orientation: s.orientation
        })
      }, 32);
      if (s.tablet && s.browser == "Chrome" && l) {
        l = document.body.clientHeight
      }
      if (s.phone && l) {
        l = Stage.height;
        if (s.orientation == "portrait" && s.browser == "Safari") {
          w = false;
          document.body.scrollTop = 0;
          h(true);
          k = true;
          s.delayedCall(function() {
            k = false
          }, 100)
        }
      }
    }

    function d(z) {
      var A = Utils.touchEvent(z);
      var y = z.target;
      var x = y.nodeName == "INPUT" || y.nodeName == "TEXTAREA" || y.nodeName == "SELECT" || y.nodeName == "A";
      if (s.allowScroll || x) {
        return
      }
      if (l) {
        if (!w) {
          return
        }
        if (s.browser == "Chrome" && A.y < 50) {
          z.stopPropagation();
          return
        }
      }
      if (f) {
        return z.preventDefault()
      }
      var b = true;
      y = z.target;
      while (y.parentNode) {
        if (y._scrollParent) {
          b = false;
          q.target = y;
          q.y = A.y;
          y.hydraObject.__preventY = A.y
        }
        y = y.parentNode
      }
      if (b) {
        z.preventDefault()
      }
    }
  }

  function r() {
    var b = Date.now();
    if (e) {
      if (b - e > s.sleepTime) {
        s.events.fire(HydraEvents.BACKGROUND)
      }
    }
    e = b
  }

  function p() {
    n = true;
    f = false;
    l = Stage.height;
    __body.css({
      height: Stage.height * 3
    });
    Stage.css({
      position: "fixed"
    });
    __window.bind("scroll", g);
    setInterval(h, 1000)
  }

  function g(b) {
    if (k) {
      return
    }
    Stage.width = document.body.clientWidth;
    Stage.height = document.body.clientHeight;
    s.scrollTop = document.body.scrollTop;
    if (Stage.height != c) {
      s.events.fire(HydraEvents.RESIZE)
    }
    c = Stage.height;
    if (s.scrollTop > 20) {
      if (!w) {
        s.events.fire(HydraEvents.FULLSCREEN, {
          fullscreen: true
        })
      }
      w = true;
      clearTimeout(s.changeHeight);
      s.changeHeight = s.delayedCall(function() {
        l = Stage.height
      }, 100)
    }
    h()
  }

  function h(b) {
    if ((document.body.clientHeight < l && w) || b) {
      Stage.height = document.body.clientHeight;
      w = false;
      l = Stage.height;
      document.body.scrollTop = 0;
      u();
      s.events.fire(HydraEvents.FULLSCREEN, {
        fullscreen: false
      })
    }
  }
  this.Class = window.Class;
  this.fullscreen = function() {
    if (s.NativeCore && s.NativeCore.active) {
      return
    }
    if (s.os == "Android") {
      __window.bind("touchstart", function() {
        Device.openFullscreen()
      });
      return true
    } else {
      if (s.os == "iOS" && s.version >= 7) {
        if (s.browser == "Chrome" || s.browser == "Safari") {
          p();
          return true
        }
      }
    }
    return false
  };
  this.overflowScroll = function(A, z) {
    if (!Device.mobile) {
      return false
    }
    var b = !!z.x;
    var C = !!z.y;
    var B = {
      "-webkit-overflow-scrolling": "touch"
    };
    if ((!b && !C) || (b && C)) {
      B.overflow = "scroll"
    }
    if (!b && C) {
      B.overflowY = "scroll";
      B.overflowX = "hidden"
    }
    if (b && !C) {
      B.overflowX = "scroll";
      B.overflowY = "hidden"
    }
    A.css(B);
    A.div._scrollParent = true;
    f = false;
    A.div._preventEvent = function(x) {
      if (A.maxScroll) {
        var D = Utils.touchEvent(x);
        var y = D.y - A.__preventY < 0 ? 1 : -1;
        if (A.div.scrollTop < 2) {
          if (y == -1) {
            x.preventDefault()
          } else {
            x.stopPropagation()
          }
        } else {
          if (A.div.scrollTop > A.maxScroll - 2) {
            if (y == 1) {
              x.preventDefault()
            } else {
              x.stopPropagation()
            }
          }
        }
      } else {
        x.stopPropagation()
      }
    };
    A.div.addEventListener("touchmove", A.div._preventEvent)
  };
  this.removeOverflowScroll = function(b) {
    b.css({
      overflow: "hidden",
      overflowX: "",
      overflowY: "",
      "-webkit-overflow-scrolling": ""
    });
    b.div.removeEventListener("touchmove", b.div._preventEvent)
  };
  this.setOrientation = function(b) {
    if (s.System && s.NativeCore.active) {
      s.System.orientation = s.System[b.toUpperCase()];
      return
    }
    if (window.screen) {
      if (window.screen.lockOrientation) {
        if (b == "landscape") {
          window.screen.lockOrientation("landscape-primary", "landscape-secondary")
        } else {
          window.screen.lockOrientation("portrait-primary", "portrait-secondary")
        }
      }
      if (window.screen.orientation) {
        if (b == "landscape") {
          window.screen.orientation.lock("landscape-primary", "landscape-secondary")
        } else {
          window.screen.orientation.lock("portrait-primary", "portrait-secondary")
        }
      }
    }
  };
  this.isNative = function() {
    return s.NativeCore && s.NativeCore.active
  }
}, "Static");
Class(function Modules() {
  var d = this;
  var a = {};
  (function() {
    defer(b)
  })();

  function b() {
    for (var e in a) {
      for (var g in a[e]) {
        var f = a[e][g];
        if (f._ready) {
          continue
        }
        f._ready = true;
        if (f.exec) {
          f.exec()
        }
      }
    }
  }

  function c(e, g) {
    var f = a[e][g];
    if (!f._ready) {
      f._ready = true;
      if (f.exec) {
        f.exec()
      }
    }
    return f
  }
  this.push = function(e) {};
  this.Module = function(g) {
    var e = new g();
    var f = g.toString().slice(0, 100).match(/function ([^\(]+)/);
    if (f) {
      e._ready = true;
      f = f[1];
      a[f] = {
        index: e
      }
    } else {
      if (!a[e.module]) {
        a[e.module] = {}
      }
      a[e.module][e.path] = e
    }
  };
  this.require = function(f) {
    var e;
    if (!f.strpos("/")) {
      e = f;
      f = "index"
    } else {
      e = f.split("/")[0];
      f = f.replace(e + "/", "")
    }
    return c(e, f).exports
  };
  window.Module = this.Module;
  if (!window._NODE_) {
    window.requireNative = window.require;
    window.require = this.require
  }
}, "Static");
Class(function Timer() {
  var g = this;
  var f;
  var d = [];
  var c = [];
  var b = new ObjectPool(Object, 100);
  (function() {
    Render.start(a)
  })();

  function a(k, m, n) {
    var h = c.length;
    for (var j = 0; j < h; j++) {
      var l = c[j];
      l.callback = null;
      d.findAndRemove(l);
      b.put(l)
    }
    if (h > 0) {
      c.length = 0
    }
    if (n > 250) {
      return
    }
    h = d.length;
    for (var j = 0; j < h; j++) {
      var l = d[j];
      if (!l) {
        continue
      }
      if (l.frames) {
        ++l.current;
        if (l.current >= l.frames) {
          l.callback();
          c.push(l)
        }
      }
      if (l.time) {
        l.current += n;
        if (l.current >= l.time) {
          l.callback();
          c.push(l)
        }
      }
    }
  }

  function e(j) {
    for (var h = d.length - 1; h > -1; h--) {
      var k = d[h];
      if (k.ref == j) {
        return k
      }
    }
  }
  f = window.clearTimeout;
  window.clearTimeout = function(h) {
    var j = e(h);
    if (j) {
      d.findAndRemove(j)
    } else {
      f(h)
    }
  };
  this.create = function(k, j) {
    if (window._NODE_) {
      return setTimeout(k, j)
    }
    if (j <= 0) {
      return k()
    }
    var h = b.get() || {};
    h.time = j;
    h.current = 0;
    h.ref = Utils.timestamp();
    h.callback = k;
    d.push(h);
    return h.ref
  };
  this.waitFrames = function(k, j) {
    var h = b.get() || {};
    h.frames = j;
    h.current = 0;
    h.callback = k;
    d.push(h)
  }
}, "static");
Class(function Color(j) {
  Inherit(this, Component);
  var f = this;
  var b, g;
  this.r = 1;
  this.g = 1;
  this.b = 1;
  (function() {
    h(j)
  })();

  function h(k) {
    if (k instanceof Color) {
      a(k)
    } else {
      if (typeof k === "number") {
        e(k)
      } else {
        if (Array.isArray(k)) {
          d(k)
        } else {
          e(Number("0x" + k.slice(1)))
        }
      }
    }
  }

  function a(k) {
    f.r = k.r;
    f.g = k.g;
    f.b = k.b
  }

  function e(k) {
    k = Math.floor(k);
    f.r = (k >> 16 & 255) / 255;
    f.g = (k >> 8 & 255) / 255;
    f.b = (k & 255) / 255
  }

  function d(k) {
    f.r = k[0];
    f.g = k[1];
    f.b = k[2]
  }

  function c(m, l, k) {
    if (k < 0) {
      k += 1
    }
    if (k > 1) {
      k -= 1
    }
    if (k < 1 / 6) {
      return m + (l - m) * 6 * k
    }
    if (k < 1 / 2) {
      return l
    }
    if (k < 2 / 3) {
      return m + (l - m) * 6 * (2 / 3 - k)
    }
    return m
  }
  this.set = function(k) {
    h(k);
    return this
  };
  this.setRGB = function(m, l, k) {
    this.r = m;
    this.g = l;
    this.b = k;
    return this
  };
  this.setHSL = function(n, m, k) {
    if (m === 0) {
      this.r = this.g = this.b = k
    } else {
      var r = k <= 0.5 ? k * (1 + m) : k + m - (k * m);
      var o = (2 * k) - r;
      this.r = c(o, r, n + 1 / 3);
      this.g = c(o, r, n);
      this.b = c(o, r, n - 1 / 3)
    }
    return this
  };
  this.offsetHSL = function(o, n, k) {
    var m = this.getHSL();
    m.h += o;
    m.s += n;
    m.l += k;
    this.setHSL(m.h, m.s, m.l);
    return this
  };
  this.getStyle = function() {
    return "rgb(" + ((this.r * 255) | 0) + "," + ((this.g * 255) | 0) + "," + ((this.b * 255) | 0) + ")"
  };
  this.getHex = function() {
    return (this.r * 255) << 16 ^ (this.g * 255) << 8 ^ (this.b * 255) << 0
  };
  this.getHexString = function() {
    return "#" + ("000000" + this.getHex().toString(16)).slice(-6)
  };
  this.getHSL = function() {
    b = b || {
          h: 0,
          s: 0,
          l: 0
        };
    var u = b;
    var k = this.r,
        n = this.g,
        p = this.b;
    var q = Math.max(k, n, p);
    var l = Math.min(k, n, p);
    var o, m;
    var t = (l + q) / 2;
    if (l === q) {
      o = 0;
      m = 0
    } else {
      var s = q - l;
      m = t <= 0.5 ? s / (q + l) : s / (2 - q - l);
      switch (q) {
        case k:
          o = (n - p) / s + (n < p ? 6 : 0);
          break;
        case n:
          o = (p - k) / s + 2;
          break;
        case p:
          o = (k - n) / s + 4;
          break
      }
      o /= 6
    }
    u.h = o;
    u.s = m;
    u.l = t;
    return u
  };
  this.add = function(k) {
    this.r += k.r;
    this.g += k.g;
    this.b += k.b
  };
  this.mix = function(k, l) {
    this.r = this.r * (1 - l) + (k.r * l);
    this.g = this.g * (1 - l) + (k.g * l);
    this.b = this.b * (1 - l) + (k.b * l)
  };
  this.addScalar = function(k) {
    this.r += k;
    this.g += k;
    this.b += k
  };
  this.multiply = function(k) {
    this.r *= k.r;
    this.g *= k.g;
    this.b *= k.b
  };
  this.multiplyScalar = function(k) {
    this.r *= k;
    this.g *= k;
    this.b *= k
  };
  this.clone = function() {
    return new Color([this.r, this.g, this.b])
  };
  this.toArray = function() {
    if (!g) {
      g = []
    }
    g[0] = this.r;
    g[1] = this.g;
    g[2] = this.b;
    return g
  }
});
Class(function Noise() {
  var f = this;

  function e(n, p, o) {
    this.x = n;
    this.y = p;
    this.z = o
  }
  e.prototype.dot2 = function(n, o) {
    return this.x * n + this.y * o
  };
  e.prototype.dot3 = function(n, p, o) {
    return this.x * n + this.y * p + this.z * o
  };
  var k = [new e(1, 1, 0), new e(-1, 1, 0), new e(1, -1, 0), new e(-1, -1, 0), new e(1, 0, 1), new e(-1, 0, 1), new e(1, 0, -1), new e(-1, 0, -1), new e(0, 1, 1), new e(0, -1, 1), new e(0, 1, -1), new e(0, -1, -1)];
  var b = [151, 160, 137, 91, 90, 15, 131, 13, 201, 95, 96, 53, 194, 233, 7, 225, 140, 36, 103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23, 190, 6, 148, 247, 120, 234, 75, 0, 26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32, 57, 177, 33, 88, 237, 149, 56, 87, 174, 20, 125, 136, 171, 168, 68, 175, 74, 165, 71, 134, 139, 48, 27, 166, 77, 146, 158, 231, 83, 111, 229, 122, 60, 211, 133, 230, 220, 105, 92, 41, 55, 46, 245, 40, 244, 102, 143, 54, 65, 25, 63, 161, 1, 216, 80, 73, 209, 76, 132, 187, 208, 89, 18, 169, 200, 196, 135, 130, 116, 188, 159, 86, 164, 100, 109, 198, 173, 186, 3, 64, 52, 217, 226, 250, 124, 123, 5, 202, 38, 147, 118, 126, 255, 82, 85, 212, 207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42, 223, 183, 170, 213, 119, 248, 152, 2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43, 172, 9, 129, 22, 39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 104, 218, 246, 97, 228, 251, 34, 242, 193, 238, 210, 144, 12, 191, 179, 162, 241, 81, 51, 145, 235, 249, 14, 239, 107, 49, 192, 214, 31, 181, 199, 106, 157, 184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150, 254, 138, 236, 205, 93, 222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156, 180];
  var j = new Array(512);
  var c = new Array(512);
  f.seed = function(n) {
    if (n > 0 && n < 1) {
      n *= 65536
    }
    n = Math.floor(n);
    if (n < 256) {
      n |= n << 8
    }
    for (var p = 0; p < 256; p++) {
      var o;
      if (p & 1) {
        o = b[p] ^ (n & 255)
      } else {
        o = b[p] ^ ((n >> 8) & 255)
      }
      j[p] = j[p + 256] = o;
      c[p] = c[p + 256] = k[o % 12]
    }
  };
  f.seed(0);
  var m = 0.5 * (Math.sqrt(3) - 1);
  var d = (3 - Math.sqrt(3)) / 6;
  var l = 1 / 3;
  var a = 1 / 6;
  f.simplex2 = function(I, r) {
    var x, v, u;
    var B = (I + r) * m;
    var H = Math.floor(I + B);
    var G = Math.floor(r + B);
    var A = (H + G) * d;
    var L = I - H + A;
    var q = r - G + A;
    var F, n;
    if (L > q) {
      F = 1;
      n = 0
    } else {
      F = 0;
      n = 1
    }
    var K = L - F + d;
    var p = q - n + d;
    var J = L - 1 + 2 * d;
    var o = q - 1 + 2 * d;
    H &= 255;
    G &= 255;
    var E = c[H + j[G]];
    var D = c[H + F + j[G + n]];
    var C = c[H + 1 + j[G + 1]];
    var z = 0.5 - L * L - q * q;
    if (z < 0) {
      x = 0
    } else {
      z *= z;
      x = z * z * E.dot2(L, q)
    }
    var y = 0.5 - K * K - p * p;
    if (y < 0) {
      v = 0
    } else {
      y *= y;
      v = y * y * D.dot2(K, p)
    }
    var w = 0.5 - J * J - o * o;
    if (w < 0) {
      u = 0
    } else {
      w *= w;
      u = w * w * C.dot2(J, o)
    }
    return 70 * (x + v + u)
  };
  f.simplex3 = function(P, E, A) {
    var H, G, F, D;
    var S = (P + E + A) * l;
    var W = Math.floor(P + S);
    var U = Math.floor(E + S);
    var T = Math.floor(A + S);
    var R = (W + U + T) * a;
    var z = P - W + R;
    var n = E - U + R;
    var Q = A - T + R;
    var p, X, C;
    var o, V, B;
    if (z >= n) {
      if (n >= Q) {
        p = 1;
        X = 0;
        C = 0;
        o = 1;
        V = 1;
        B = 0
      } else {
        if (z >= Q) {
          p = 1;
          X = 0;
          C = 0;
          o = 1;
          V = 0;
          B = 1
        } else {
          p = 0;
          X = 0;
          C = 1;
          o = 1;
          V = 0;
          B = 1
        }
      }
    } else {
      if (n < Q) {
        p = 0;
        X = 0;
        C = 1;
        o = 0;
        V = 1;
        B = 1
      } else {
        if (z < Q) {
          p = 0;
          X = 1;
          C = 0;
          o = 0;
          V = 1;
          B = 1
        } else {
          p = 0;
          X = 1;
          C = 0;
          o = 1;
          V = 1;
          B = 0
        }
      }
    }
    var y = z - p + a;
    var aa = n - X + a;
    var O = Q - C + a;
    var x = z - o + 2 * a;
    var Z = n - V + 2 * a;
    var N = Q - B + 2 * a;
    var v = z - 1 + 3 * a;
    var Y = n - 1 + 3 * a;
    var M = Q - 1 + 3 * a;
    W &= 255;
    U &= 255;
    T &= 255;
    var w = c[W + j[U + j[T]]];
    var u = c[W + p + j[U + X + j[T + C]]];
    var r = c[W + o + j[U + V + j[T + B]]];
    var q = c[W + 1 + j[U + 1 + j[T + 1]]];
    var L = 0.6 - z * z - n * n - Q * Q;
    if (L < 0) {
      H = 0
    } else {
      L *= L;
      H = L * L * w.dot3(z, n, Q)
    }
    var K = 0.6 - y * y - aa * aa - O * O;
    if (K < 0) {
      G = 0
    } else {
      K *= K;
      G = K * K * u.dot3(y, aa, O)
    }
    var J = 0.6 - x * x - Z * Z - N * N;
    if (J < 0) {
      F = 0
    } else {
      J *= J;
      F = J * J * r.dot3(x, Z, N)
    }
    var I = 0.6 - v * v - Y * Y - M * M;
    if (I < 0) {
      D = 0
    } else {
      I *= I;
      D = I * I * q.dot3(v, Y, M)
    }
    return 32 * (H + G + F + D)
  };

  function h(n) {
    return n * n * n * (n * (n * 6 - 15) + 10)
  }

  function g(o, n, p) {
    return (1 - p) * o + p * n
  }
  f.perlin = function(n) {
    return f.perlin2(n, 0)
  };
  f.perlin2 = function(v, r) {
    var q = Math.floor(v),
        o = Math.floor(r);
    v = v - q;
    r = r - o;
    q = q & 255;
    o = o & 255;
    var p = c[q + j[o]].dot2(v, r);
    var n = c[q + j[o + 1]].dot2(v, r - 1);
    var t = c[q + 1 + j[o]].dot2(v - 1, r);
    var s = c[q + 1 + j[o + 1]].dot2(v - 1, r - 1);
    var w = h(v);
    return g(g(p, t, w), g(n, s, w), h(r))
  };
  f.perlin3 = function(D, B, A) {
    var r = Math.floor(D),
        p = Math.floor(B),
        n = Math.floor(A);
    D = D - r;
    B = B - p;
    A = A - n;
    r = r & 255;
    p = p & 255;
    n = n & 255;
    var J = c[r + j[p + j[n]]].dot3(D, B, A);
    var I = c[r + j[p + j[n + 1]]].dot3(D, B, A - 1);
    var t = c[r + j[p + 1 + j[n]]].dot3(D, B - 1, A);
    var s = c[r + j[p + 1 + j[n + 1]]].dot3(D, B - 1, A - 1);
    var q = c[r + 1 + j[p + j[n]]].dot3(D - 1, B, A);
    var o = c[r + 1 + j[p + j[n + 1]]].dot3(D - 1, B, A - 1);
    var F = c[r + 1 + j[p + 1 + j[n]]].dot3(D - 1, B - 1, A);
    var C = c[r + 1 + j[p + 1 + j[n + 1]]].dot3(D - 1, B - 1, A - 1);
    var H = h(D);
    var G = h(B);
    var E = h(A);
    return g(g(g(J, q, H), g(I, o, H), E), g(g(t, F, H), g(s, C, H), E), G)
  }
}, "Static");
Class(function Matrix2() {
  var p = this;
  var l = Matrix2.prototype;
  var g, f, e, o, n, m, v, u, t;
  var s, r, q, d, c, a, k, j, h;
  this.type = "matrix2";
  this.data = new Float32Array(9);
  (function() {
    w()
  })();

  function w(x) {
    x = x || p.data;
    x[0] = 1, x[1] = 0, x[2] = 0;
    x[3] = 0, x[4] = 1, x[5] = 0;
    x[6] = 0, x[7] = 0, x[8] = 1
  }

  function b(x) {
    x = Math.abs(x) < 0.000001 ? 0 : x;
    return x
  }
  if (typeof l.identity !== "undefined") {
    return
  }
  l.identity = function(x) {
    w(x);
    return this
  };
  l.transformVector = function(A) {
    var B = this.data;
    var z = A.x;
    var C = A.y;
    A.x = B[0] * z + B[1] * C + B[2];
    A.y = B[3] * z + B[4] * C + B[5];
    return A
  };
  l.setTranslation = function(z, y, x) {
    var A = x || this.data;
    A[0] = 1, A[1] = 0, A[2] = z;
    A[3] = 0, A[4] = 1, A[5] = y;
    A[6] = 0, A[7] = 0, A[8] = 1;
    return this
  };
  l.getTranslation = function(x) {
    var y = this.data;
    x = x || new Vector2();
    x.x = y[2];
    x.y = y[5];
    return x
  };
  l.setScale = function(A, z, x) {
    var y = x || this.data;
    y[0] = A, y[1] = 0, y[2] = 0;
    y[3] = 0, y[4] = z, y[5] = 0;
    y[6] = 0, y[7] = 0, y[8] = 1;
    return this
  };
  l.setShear = function(A, z, x) {
    var y = x || this.data;
    y[0] = 1, y[1] = A, y[2] = 0;
    y[3] = z, y[4] = 1, y[5] = 0;
    y[6] = 0, y[7] = 0, y[8] = 1;
    return this
  };
  l.setRotation = function(y, x) {
    var B = x || this.data;
    var A = Math.cos(y);
    var z = Math.sin(y);
    B[0] = A, B[1] = -z, B[2] = 0;
    B[3] = z, B[4] = A, B[5] = 0;
    B[6] = 0, B[7] = 0, B[8] = 1;
    return this
  };
  l.setTRS = function(z, x, y, E, D) {
    var C = this.data;
    var B = Math.cos(y);
    var A = Math.sin(y);
    C[0] = B * E, C[1] = -A * D, C[2] = z;
    C[3] = A * E, C[4] = B * D, C[5] = x;
    C[6] = 0, C[7] = 0, C[8] = 1;
    return this
  };
  l.translate = function(y, x) {
    this.identity(Matrix2.__TEMP__);
    this.setTranslation(y, x, Matrix2.__TEMP__);
    return this.multiply(Matrix2.__TEMP__)
  };
  l.rotate = function(x) {
    this.identity(Matrix2.__TEMP__);
    this.setTranslation(x, Matrix2.__TEMP__);
    return this.multiply(Matrix2.__TEMP__)
  };
  l.scale = function(y, x) {
    this.identity(Matrix2.__TEMP__);
    this.setScale(y, x, Matrix2.__TEMP__);
    return this.multiply(Matrix2.__TEMP__)
  };
  l.shear = function(y, x) {
    this.identity(Matrix2.__TEMP__);
    this.setRotation(y, x, Matrix2.__TEMP__);
    return this.multiply(Matrix2.__TEMP__)
  };
  l.multiply = function(y) {
    var z = this.data;
    var x = y.data || y;
    g = z[0], f = z[1], e = z[2];
    o = z[3], n = z[4], m = z[5];
    v = z[6], u = z[7], t = z[8];
    s = x[0], r = x[1], q = x[2];
    d = x[3], c = x[4], a = x[5];
    k = x[6], j = x[7], h = x[8];
    z[0] = g * s + f * d + e * k;
    z[1] = g * r + f * c + e * j;
    z[2] = g * q + f * a + e * h;
    z[3] = o * s + n * d + m * k;
    z[4] = o * r + n * c + m * j;
    z[5] = o * q + n * a + m * h;
    return this
  };
  l.inverse = function(y) {
    y = y || this;
    var z = y.data;
    var x = this.data;
    g = z[0], f = z[1], e = z[2];
    o = z[3], n = z[4], m = z[5];
    v = z[6], u = z[7], t = z[8];
    var A = y.determinant();
    if (Math.abs(A) < 1e-7) {}
    var B = 1 / A;
    x[0] = (n * t - u * m) * B;
    x[1] = (e * u - f * t) * B;
    x[2] = (f * m - e * n) * B;
    x[3] = (m * v - o * t) * B;
    x[4] = (g * t - e * v) * B;
    x[5] = (o * e - g * m) * B;
    x[6] = (o * u - v * n) * B;
    x[7] = (v * f - g * u) * B;
    x[8] = (g * n - o * f) * B;
    return y
  };
  l.determinant = function() {
    var x = this.data;
    g = x[0], f = x[1], e = x[2];
    o = x[3], n = x[4], m = x[5];
    v = x[6], u = x[7], t = x[8];
    return g * (n * t - u * m) - f * (o * t - m * v) + e * (o * u * n * v)
  };
  l.copyTo = function(y) {
    var z = this.data;
    var x = y.data || y;
    x[0] = z[0], x[1] = z[1], x[2] = z[2];
    x[3] = z[3], x[4] = z[4], x[5] = z[5];
    x[6] = z[6], x[7] = z[7], x[8] = z[8];
    return y
  };
  l.copyFrom = function(y) {
    var z = this.data;
    var x = y.data || y;
    x[0] = z[0], x[1] = z[1], x[2] = z[2];
    x[3] = z[3], x[4] = z[4], x[5] = z[5];
    x[6] = z[6], x[7] = z[7], x[8] = z[8];
    return this
  };
  l.getCSS = function(x) {
    var y = this.data;
    if (Device.tween.css3d && !x) {
      return "matrix3d(" + b(y[0]) + ", " + b(y[3]) + ", 0, 0, " + b(y[1]) + ", " + b(y[4]) + ", 0, 0, 0, 0, 1, 0, " + b(y[2]) + ", " + b(y[5]) + ", 0, 1)"
    } else {
      return "matrix(" + b(y[0]) + ", " + b(y[3]) + ", " + b(y[1]) + ", " + b(y[4]) + ", " + b(y[2]) + ", " + b(y[5]) + ")"
    }
  }
}, function() {
  Matrix2.__TEMP__ = new Matrix2().data
});
Class(function Matrix4() {
  var d = this;
  var b = Matrix4.prototype;
  this.type = "matrix4";
  this.data = new Float32Array(16);
  (function() {
    a()
  })();

  function a(e) {
    var f = e || d.data;
    f[0] = 1, f[4] = 0, f[8] = 0, f[12] = 0;
    f[1] = 0, f[5] = 1, f[9] = 0, f[13] = 0;
    f[2] = 0, f[6] = 0, f[10] = 1, f[14] = 0;
    f[3] = 0, f[7] = 0, f[11] = 0, f[15] = 1
  }

  function c(e) {
    return Math.abs(e) < 0.000001 ? 0 : e
  }
  if (typeof b.identity !== "undefined") {
    return
  }
  b.identity = function() {
    a();
    return this
  };
  b.transformVector = function(g, h) {
    var k = this.data;
    var e = g.x,
        l = g.y,
        j = g.z,
        f = g.w;
    h = h || g;
    h.x = k[0] * e + k[4] * l + k[8] * j + k[12] * f;
    h.y = k[1] * e + k[5] * l + k[9] * j + k[13] * f;
    h.z = k[2] * e + k[6] * l + k[10] * j + k[14] * f;
    return h
  };
  b.multiply = function(M, N) {
    var P = this.data;
    var O = M.data || M;
    var L, K, J, I, H, G, F, E, D, C, r, q, p, o, n, l;
    var B, A, z, y, x, w, v, u, t, s, k, j, h, g, f, e;
    L = P[0], K = P[1], J = P[2], I = P[3];
    H = P[4], G = P[5], F = P[6], E = P[7];
    D = P[8], C = P[9], r = P[10], q = P[11];
    p = P[12], o = P[13], n = P[14], l = P[15];
    B = O[0], A = O[1], z = O[2], y = O[3];
    x = O[4], w = O[5], v = O[6], u = O[7];
    t = O[8], s = O[9], k = O[10], j = O[11];
    h = O[12], g = O[13], f = O[14], e = O[15];
    P[0] = L * B + H * A + D * z + p * y;
    P[1] = K * B + G * A + C * z + o * y;
    P[2] = J * B + F * A + r * z + n * y;
    P[3] = I * B + E * A + q * z + l * y;
    P[4] = L * x + H * w + D * v + p * u;
    P[5] = K * x + G * w + C * v + o * u;
    P[6] = J * x + F * w + r * v + n * u;
    P[7] = I * x + E * w + q * v + l * u;
    P[8] = L * t + H * s + D * k + p * j;
    P[9] = K * t + G * s + C * k + o * j;
    P[10] = J * t + F * s + r * k + n * j;
    P[11] = I * t + E * s + q * k + l * j;
    P[12] = L * h + H * g + D * f + p * e;
    P[13] = K * h + G * g + C * f + o * e;
    P[14] = J * h + F * g + r * f + n * e;
    P[15] = I * h + E * g + q * f + l * e;
    return this
  };
  b.setTRS = function(p, o, n, g, f, e, w, v, u, l) {
    l = l || this;
    var s = l.data;
    a(l);
    var k = Math.sin(g);
    var t = Math.cos(g);
    var j = Math.sin(f);
    var r = Math.cos(f);
    var h = Math.sin(e);
    var q = Math.cos(e);
    s[0] = (r * q + j * k * h) * w;
    s[1] = (-r * h + j * k * q) * w;
    s[2] = j * t * w;
    s[4] = h * t * v;
    s[5] = q * t * v;
    s[6] = -k * v;
    s[8] = (-j * q + r * k * h) * u;
    s[9] = (h * j + r * k * q) * u;
    s[10] = r * t * u;
    s[12] = p;
    s[13] = o;
    s[14] = n;
    return l
  };
  b.setScale = function(j, h, f, e) {
    e = e || this;
    var g = e.data || e;
    a(e);
    g[0] = j, g[5] = h, g[10] = f;
    return e
  };
  b.setTranslation = function(g, f, j, e) {
    e = e || this;
    var h = e.data || e;
    a(e);
    h[12] = g, h[13] = f, h[14] = j;
    return e
  };
  b.setRotation = function(g, f, e, j) {
    j = j || this;
    var n = j.data || j;
    a(j);
    var q = Math.sin(g);
    var l = Math.cos(g);
    var p = Math.sin(f);
    var k = Math.cos(f);
    var o = Math.sin(e);
    var h = Math.cos(e);
    n[0] = k * h + p * q * o;
    n[1] = -k * o + p * q * h;
    n[2] = p * l;
    n[4] = o * l;
    n[5] = h * l;
    n[6] = -q;
    n[8] = -p * h + k * q * o;
    n[9] = o * p + k * q * h;
    n[10] = k * l;
    return j
  };
  b.setLookAt = function(j, h, g, e) {
    e = e || this;
    var o = e.data || e;
    var n = D3.m4v31;
    var l = D3.m4v32;
    var k = D3.m4v33;
    n.subVectors(h, j).normalize();
    l.cross(n, g).normalize();
    k.cross(l, n);
    o[0] = l.x;
    o[1] = k.x;
    o[2] = -n.x;
    o[3] = 0;
    o[4] = l.y;
    o[5] = k.y;
    o[6] = -n.y;
    o[7] = 0;
    o[8] = l.z;
    o[9] = k.z;
    o[10] = -n.z;
    o[11] = 0;
    o[12] = 0;
    o[13] = 0;
    o[14] = 0;
    o[15] = 1;
    this.translate(-j.x, -j.y, -j.z);
    return this
  };
  b.setPerspective = function(g, f, l, j, h) {
    var n, o, p, k;
    if (l === j || f === 0) {
      throw "null frustum"
    }
    if (l <= 0) {
      throw "near <= 0"
    }
    if (j <= 0) {
      throw "far <= 0"
    }
    g = Math.PI * g / 180 / 2;
    p = Math.sin(g);
    if (p === 0) {
      throw "null frustum"
    }
    o = 1 / (j - l);
    k = Math.cos(g) / p;
    n = h ? (h.data || h) : this.data;
    n[0] = k / f;
    n[1] = 0;
    n[2] = 0;
    n[3] = 0;
    n[4] = 0;
    n[5] = k;
    n[6] = 0;
    n[7] = 0;
    n[8] = 0;
    n[9] = 0;
    n[10] = -(j + l) * o;
    n[11] = -1;
    n[12] = 0;
    n[13] = 0;
    n[14] = -2 * l * j * o;
    n[15] = 0
  };
  b.perspective = function(g, f, h, e) {
    this.setPerspective(g, f, h, e, Matrix4.__TEMP__);
    return this.multiply(Matrix4.__TEMP__)
  };
  b.lookAt = function(g, f, e) {
    this.setLookAt(g, f, e, Matrix4.__TEMP__);
    return this.multiply(Matrix4.__TEMP__)
  };
  b.translate = function(f, e, g) {
    this.setTranslation(f, e, g, Matrix4.__TEMP__);
    return this.multiply(Matrix4.__TEMP__)
  };
  b.rotate = function(g, f, e) {
    this.setRotation(g, f, e, Matrix4.__TEMP__);
    return this.multiply(Matrix4.__TEMP__)
  };
  b.scale = function(g, f, e) {
    this.setScale(g, f, e, Matrix4.__TEMP__);
    return this.multiply(Matrix4.__TEMP__)
  };
  b.copyTo = function(f) {
    var g = this.data;
    var e = f.data || f;
    for (var h = 0; h < 16; h++) {
      e[h] = g[h]
    }
  };
  b.copyFrom = function(f) {
    var g = this.data;
    var e = f.data || f;
    for (var h = 0; h < 16; h++) {
      g[h] = e[h]
    }
    return this
  };
  b.copyRotationTo = function(f) {
    var g = this.data;
    var e = f.data || f;
    e[0] = g[0];
    e[1] = g[1];
    e[2] = g[2];
    e[3] = g[4];
    e[4] = g[5];
    e[5] = g[6];
    e[6] = g[8];
    e[7] = g[9];
    e[8] = g[10];
    return f
  };
  b.copyPosition = function(e) {
    var g = this.data;
    var f = e.data || e;
    g[12] = f[12];
    g[13] = f[13];
    g[14] = f[14];
    return this
  };
  b.getCSS = function() {
    var e = this.data;
    return "matrix3d(" + c(e[0]) + "," + c(e[1]) + "," + c(e[2]) + "," + c(e[3]) + "," + c(e[4]) + "," + c(e[5]) + "," + c(e[6]) + "," + c(e[7]) + "," + c(e[8]) + "," + c(e[9]) + "," + c(e[10]) + "," + c(e[11]) + "," + c(e[12]) + "," + c(e[13]) + "," + c(e[14]) + "," + c(e[15]) + ")"
  };
  b.extractPosition = function(e) {
    e = e || new Vector3();
    var f = this.data;
    e.set(f[12], f[13], f[14]);
    return e
  };
  b.determinant = function() {
    var e = this.data;
    return e[0] * (e[5] * e[10] - e[9] * e[6]) + e[4] * (e[9] * e[2] - e[1] * e[10]) + e[8] * (e[1] * e[6] - e[5] * e[2])
  };
  b.inverse = function(h) {
    var p = this.data;
    var r = (h) ? h.data || h : this.data;
    var n = this.determinant();
    if (Math.abs(n) < 0.0001) {
      console.warn("Attempt to inverse a singular Matrix4. ", this.data);
      console.trace();
      return h
    }
    var g = p[0],
        v = p[4],
        s = p[8],
        l = p[12],
        f = p[1],
        u = p[5],
        q = p[9],
        k = p[13],
        e = p[2],
        t = p[6],
        o = p[10],
        j = p[14];
    n = 1 / n;
    r[0] = (u * o - q * t) * n;
    r[1] = (s * t - v * o) * n;
    r[2] = (v * q - s * u) * n;
    r[4] = (q * e - f * o) * n;
    r[5] = (g * o - s * e) * n;
    r[6] = (s * f - g * q) * n;
    r[8] = (f * t - u * e) * n;
    r[9] = (v * e - g * t) * n;
    r[10] = (g * u - v * f) * n;
    r[12] = -(l * r[0] + k * r[4] + j * r[8]);
    r[13] = -(l * r[1] + k * r[5] + j * r[9]);
    r[14] = -(l * r[2] + k * r[6] + j * r[10]);
    return h
  };
  b.transpose = function(h) {
    var k = this.data;
    var n = h ? h.data || h : this.data;
    var g = k[0],
        r = k[4],
        o = k[8],
        f = k[1],
        q = k[5],
        l = k[9],
        e = k[2],
        p = k[6],
        j = k[10];
    n[0] = g;
    n[1] = r;
    n[2] = o;
    n[4] = f;
    n[5] = q;
    n[6] = l;
    n[8] = e;
    n[9] = p;
    n[10] = j
  }
}, function() {
  Matrix4.__TEMP__ = new Matrix4().data
});
Class(function Vector2(c, a) {
  var d = this;
  var b = Vector2.prototype;
  this.x = typeof c == "number" ? c : 0;
  this.y = typeof a == "number" ? a : 0;
  this.type = "vector2";
  if (typeof b.set !== "undefined") {
    return
  }
  b.set = function(e, f) {
    this.x = e;
    this.y = f;
    return this
  };
  b.clear = function() {
    this.x = 0;
    this.y = 0;
    return this
  };
  b.copyTo = function(e) {
    e.x = this.x;
    e.y = this.y;
    return this
  };
  b.copyFrom = b.copy = function(e) {
    this.x = e.x;
    this.y = e.y;
    return this
  };
  b.addVectors = function(f, e) {
    this.x = f.x + e.x;
    this.y = f.y + e.y;
    return this
  };
  b.subVectors = function(f, e) {
    this.x = f.x - e.x;
    this.y = f.y - e.y;
    return this
  };
  b.multiplyVectors = function(f, e) {
    this.x = f.x * e.x;
    this.y = f.y * e.y;
    return this
  };
  b.add = function(e) {
    this.x += e.x;
    this.y += e.y;
    return this
  };
  b.sub = function(e) {
    this.x -= e.x;
    this.y -= e.y;
    return this
  };
  b.multiply = function(e) {
    this.x *= e;
    this.y *= e;
    return this
  };
  b.divide = function(e) {
    this.x /= e;
    this.y /= e;
    return this
  };
  b.lengthSq = function() {
    return (this.x * this.x + this.y * this.y) || 0.00001
  };
  b.length = function() {
    return Math.sqrt(this.lengthSq())
  };
  b.setLength = function(e) {
    this.normalize().multiply(e);
    return this
  };
  b.normalize = function() {
    var e = this.length();
    this.x /= e;
    this.y /= e;
    return this
  };
  b.perpendicular = function(h, f) {
    var g = this.x;
    var e = this.y;
    this.x = -e;
    this.y = g;
    return this
  };
  b.lerp = function(e, f) {
    this.x += (e.x - this.x) * f;
    this.y += (e.y - this.y) * f;
    return this
  };
  b.interp = function(g, k, m) {
    var e = 0;
    var j = TweenManager.Interpolation.convertEase(m);
    var h = Vector2.__TEMP__;
    h.subVectors(this, g);
    var l = Utils.clamp(Utils.range(h.lengthSq(), 0, (5000 * 5000), 1, 0), 0, 1) * (k / 10);
    if (typeof j === "function") {
      e = j(l)
    } else {
      e = TweenManager.Interpolation.solve(j, l)
    }
    this.x += (g.x - this.x) * e;
    this.y += (g.y - this.y) * e
  };
  b.setAngleRadius = function(e, f) {
    this.x = Math.cos(e) * f;
    this.y = Math.sin(e) * f;
    return this
  };
  b.addAngleRadius = function(e, f) {
    this.x += Math.cos(e) * f;
    this.y += Math.sin(e) * f;
    return this
  };
  b.clone = function() {
    return new Vector2(this.x, this.y)
  };
  b.dot = function(f, e) {
    e = e || this;
    return (f.x * e.x + f.y * e.y)
  };
  b.distanceTo = function(g, h) {
    var f = this.x - g.x;
    var e = this.y - g.y;
    if (!h) {
      return Math.sqrt(f * f + e * e)
    }
    return f * f + e * e
  };
  b.solveAngle = function(f, e) {
    if (!e) {
      e = this
    }
    return Math.atan2(f.y - e.y, f.x - e.x)
  };
  b.equals = function(e) {
    return this.x == e.x && this.y == e.y
  };
  b.console = function() {
    console.log(this.x, this.y)
  }
}, function() {
  Vector2.__TEMP__ = new Vector2()
});
Class(function Vector3(d, b, a, e) {
  var f = this;
  var c = Vector3.prototype;
  this.x = typeof d === "number" ? d : 0;
  this.y = typeof b === "number" ? b : 0;
  this.z = typeof a === "number" ? a : 0;
  this.w = typeof e === "number" ? e : 1;
  this.type = "vector3";
  if (typeof c.set !== "undefined") {
    return
  }
  c.set = function(g, k, j, h) {
    this.x = g || 0;
    this.y = k || 0;
    this.z = j || 0;
    this.w = h || 1;
    return this
  };
  c.clear = function() {
    this.x = 0;
    this.y = 0;
    this.z = 0;
    this.w = 1;
    return this
  };
  c.copyTo = function(g) {
    g.x = this.x;
    g.y = this.y;
    g.z = this.z;
    g.w = this.w;
    return g
  };
  c.copyFrom = c.copy = function(g) {
    this.x = g.x || 0;
    this.y = g.y || 0;
    this.z = g.z || 0;
    this.w = g.w || 1;
    return this
  };
  c.lengthSq = function() {
    return this.x * this.x + this.y * this.y + this.z * this.z
  };
  c.length = function() {
    return Math.sqrt(this.lengthSq())
  };
  c.normalize = function() {
    var g = 1 / this.length();
    this.set(this.x * g, this.y * g, this.z * g);
    return this
  };
  c.setLength = function(g) {
    this.normalize().multiply(g);
    return this
  };
  c.addVectors = function(h, g) {
    this.x = h.x + g.x;
    this.y = h.y + g.y;
    this.z = h.z + g.z;
    return this
  };
  c.subVectors = function(h, g) {
    this.x = h.x - g.x;
    this.y = h.y - g.y;
    this.z = h.z - g.z;
    return this
  };
  c.multiplyVectors = function(h, g) {
    this.x = h.x * g.x;
    this.y = h.y * g.y;
    this.z = h.z * g.z;
    return this
  };
  c.add = function(g) {
    this.x += g.x;
    this.y += g.y;
    this.z += g.z;
    return this
  };
  c.sub = function(g) {
    this.x -= g.x;
    this.y -= g.y;
    this.z -= g.z;
    return this
  };
  c.multiply = function(g) {
    this.x *= g;
    this.y *= g;
    this.z *= g;
    return this
  };
  c.divide = function(g) {
    this.x /= g;
    this.y /= g;
    this.z /= g;
    return this
  };
  c.limit = function(g) {
    if (this.length() > g) {
      this.normalize();
      this.multiply(g)
    }
  };
  c.heading2D = function() {
    var g = Math.atan2(-this.y, this.x);
    return -g
  };
  c.lerp = function(g, h) {
    this.x += (g.x - this.x) * h;
    this.y += (g.y - this.y) * h;
    this.z += (g.z - this.z) * h;
    return this
  };
  c.deltaLerp = function(g, k, l) {
    l = l || 1;
    for (var h = 0; h < l; h++) {
      var j = k;
      this.x += ((g.x - this.x) * k);
      this.y += ((g.y - this.y) * k);
      this.z += ((g.z - this.z) * k)
    }
    return this
  };
  c.interp = function(h, l, n, m) {
    if (!Vector3.__TEMP__) {
      Vector3.__TEMP__ = new Vector3()
    }
    m = m || 5000;
    var g = 0;
    var k = TweenManager.Interpolation.convertEase(n);
    var j = Vector3.__TEMP__;
    j.subVectors(this, h);
    var m = Utils.clamp(Utils.range(j.lengthSq(), 0, (m * m), 1, 0), 0, 1) * (l / 10);
    if (typeof k === "function") {
      g = k(m)
    } else {
      g = TweenManager.Interpolation.solve(k, m)
    }
    this.x += (h.x - this.x) * g;
    this.y += (h.y - this.y) * g;
    this.z += (h.z - this.z) * g
  };
  c.setAngleRadius = function(g, h) {
    this.x = Math.cos(g) * h;
    this.y = Math.sin(g) * h;
    this.z = Math.sin(g) * h;
    return this
  };
  c.addAngleRadius = function(g, h) {
    this.x += Math.cos(g) * h;
    this.y += Math.sin(g) * h;
    this.z += Math.sin(g) * h;
    return this
  };
  c.dot = function(h, g) {
    g = g || this;
    return h.x * g.x + h.y * g.y + h.z * g.z
  };
  c.clone = function() {
    return new Vector3(this.x, this.y, this.z)
  };
  c.cross = function(j, h) {
    if (!h) {
      h = this
    }
    var g = j.y * h.z - j.z * h.y;
    var l = j.z * h.x - j.x * h.z;
    var k = j.x * h.y - j.y * h.x;
    this.set(g, l, k, this.w);
    return this
  };
  c.distanceTo = function(k, l) {
    var j = this.x - k.x;
    var h = this.y - k.y;
    var g = this.z - k.z;
    if (!l) {
      return Math.sqrt(j * j + h * h + g * g)
    }
    return j * j + h * h + g * g
  };
  c.solveAngle = function(h, g) {
    if (!g) {
      g = this
    }
    return Math.acos(h.dot(g) / (h.length() * g.length()))
  };
  c.equals = function(g) {
    return this.x == g.x && this.y == g.y && this.z == g.z
  };
  c.console = function() {
    console.log(this.x, this.y, this.z)
  }
}, function() {
  Vector3.__TEMP__ = new Vector3()
});
Mobile.Class(function Accelerometer() {
  var b = this;
  this.x = 0;
  this.y = 0;
  this.z = 0;
  this.toRadians = Mobile.os == "iOS" ? Math.PI / 180 : 1;

  function a(c) {
    switch (window.orientation) {
      case 0:
        b.x = -c.accelerationIncludingGravity.x;
        b.y = c.accelerationIncludingGravity.y;
        b.z = c.accelerationIncludingGravity.z;
        if (c.rotationRate) {
          b.alpha = c.rotationRate.beta * b.toRadians;
          b.beta = -c.rotationRate.alpha * b.toRadians;
          b.gamma = c.rotationRate.gamma * b.toRadians
        }
        break;
      case 180:
        b.x = c.accelerationIncludingGravity.x;
        b.y = -c.accelerationIncludingGravity.y;
        b.z = c.accelerationIncludingGravity.z;
        if (c.rotationRate) {
          b.alpha = -c.rotationRate.beta * b.toRadians;
          b.beta = c.rotationRate.alpha * b.toRadians;
          b.gamma = c.rotationRate.gamma * b.toRadians
        }
        break;
      case 90:
        b.x = c.accelerationIncludingGravity.y;
        b.y = c.accelerationIncludingGravity.x;
        b.z = c.accelerationIncludingGravity.z;
        if (c.rotationRate) {
          b.alpha = c.rotationRate.alpha * b.toRadians;
          b.beta = c.rotationRate.beta * b.toRadians;
          b.gamma = c.rotationRate.gamma * b.toRadians
        }
        break;
      case -90:
        b.x = -c.accelerationIncludingGravity.y;
        b.y = -c.accelerationIncludingGravity.x;
        b.z = c.accelerationIncludingGravity.z;
        if (c.rotationRate) {
          b.alpha = -c.rotationRate.alpha * b.toRadians;
          b.beta = -c.rotationRate.beta * b.toRadians;
          b.gamma = c.rotationRate.gamma * b.toRadians
        }
        break
    }
  }
  this.capture = function() {
    window.ondevicemotion = a
  };
  this.stop = function() {
    window.ondevicemotion = null;
    b.x = b.y = b.z = 0
  }
}, "Static");
Class(function Interaction() {
  Namespace(this)
}, "static");
Interaction.Class(function Input(k) {
  Inherit(this, Component);
  var e = this;
  var l = new Vector2();
  var f = new Vector2();
  var c = new Vector2();
  var a = Render.TIME;
  var b = Render.TIME;
  var h = false;
  this.velocity = new Vector2();
  (function() {
    if (k instanceof HydraObject) {
      g()
    }
  })();

  function g() {
    if (k == Stage || k == __window) {
      Interaction.Input.bind("touchstart", d)
    } else {
      k.bind("touchstart", d)
    }
    Interaction.Input.bind("touchmove", j);
    Interaction.Input.bind("touchend", m)
  }

  function d(n) {
    h = true;
    e.velocity.clear();
    l.copyFrom(n);
    if (e.onStart) {
      defer(function() {
        if (e.onStart) {
          e.onStart(n)
        }
      })
    }
  }

  function j(n) {
    if (!h) {
      return
    }
    f.subVectors(n, l);
    a = (Render.TIME - b) || 0.01;
    if (a >= 16) {
      e.velocity.subVectors(n, c);
      e.velocity.divide(a);
      b = Render.TIME;
      c.copyFrom(n)
    }
    if (e.onUpdate) {
      defer(function() {
        if (e.onUpdate) {
          e.onUpdate(f, n)
        }
      })
    }
  }

  function m(n) {
    if (!h) {
      return
    }
    h = false;
    if (e.onEnd) {
      defer(function() {
        if (e.onEnd) {
          e.onEnd(n)
        }
      })
    }
  }
  this.attach = function(n) {
    if (k instanceof HydraObject) {
      k.unbind("touchstart", d)
    }
    k = n;
    g()
  };
  this.touchStart = function(n) {
    d({
      x: Mouse.x,
      y: Mouse.y
    })
  };
  this.end = this.touchEnd = function() {
    m()
  };
  this.destroy = function() {
    Interaction.Input.unbind("touchmove", j);
    Interaction.Input.unbind("touchstart", d);
    Interaction.Input.unbind("touchend", m);
    k && k.unbind && k.unbind("touchstart", d);
    return this._destroy()
  }
}, function() {
  var d = {
    touchstart: [],
    touchmove: [],
    touchend: []
  };
  var a;

  function f() {
    a = true;
    __window.bind("touchstart", b);
    __window.bind("touchmove", e);
    __window.bind("touchend", c);
    __window.bind("touchcancel", c);
    __window.bind("contextmenu", c)
  }

  function e(g) {
    d.touchmove.forEach(function(h) {
      h(g)
    })
  }

  function b(g) {
    d.touchstart.forEach(function(h) {
      h(g)
    })
  }

  function c(g) {
    d.touchend.forEach(function(h) {
      h(g)
    })
  }
  Interaction.Input.bind = function(g, h) {
    d[g].push(h);
    if (!a) {
      f()
    }
  };
  Interaction.Input.unbind = function(g, h) {
    d[g].findAndRemove(h)
  }
});
Interaction.Class(function Renderer(b) {
  Inherit(this, Component);
  var c = this;
  var a = b instanceof HydraObject;
  this.needsUpdate = !!a;
  if (a) {
    b.x = b.x || 0;
    b.y = b.y || 0;
    b.z = b.z || 0;
    b.pos = {
      x: 0,
      y: 0,
      z: 0
    }
  }
  this.update = function() {
    b.transform()
  }
});
Interaction.Class(function SnapBack(j, g) {
  Inherit(this, Component);
  var f = this;
  var m, h, c, b;
  this.autoRender = true;
  this.elapsed = 0;
  this.friction = 0.3;
  this.easeTime = 500;
  this.ease = "easeOutCubic";
  this.damping = 1;
  (function() {
    k();
    l()
  })();

  function l() {
    b = {};
    if (!g) {
      g = {
        x: 1,
        y: 1
      }
    }
    for (var n in g) {
      b[n] = j[n]
    }
  }

  function k() {
    m = f.initClass(Interaction.Input, j);
    m.onUpdate = e;
    m.onStart = d;
    m.onEnd = a;
    f.input = m;
    c = new DynamicObject();
    h = f.initClass(Interaction.Renderer, j)
  }

  function e(o) {
    o.multiply(f.friction);
    for (var n in g) {
      j[n] = o[n]
    }
    if (f.onUpdate) {
      f.onUpdate(o)
    }
    if (f.autoRender && h.needsUpdate) {
      h.update()
    }
  }

  function d() {
    c.stopTween();
    if (f.onStart) {
      f.onStart()
    }
  }

  function a() {
    c.copyFrom(j);
    if (f.ease == "spring") {
      b.damping = f.damping
    }
    c.tween(b, f.easeTime, f.ease, function(n) {
      c.copyTo(j);
      if (f.autoRender && h.needsUpdate) {
        h.update()
      }
      f.elapsed = n
    });
    if (b.damping) {
      delete b.damping
    }
    if (f.onEnd) {
      f.onEnd()
    }
  }
  this.set("directions", function(n) {
    g = n;
    l()
  });
  this.resetOrigin = function() {
    l()
  }
});
Interaction.Class(function Drawer(j, m) {
  Inherit(this, Component);
  var s = this;
  var b, k, p, x, y, t, f, d;
  var w = 0;
  var n = new Vector2();
  var u = new Vector2();
  var q = new Vector2();
  this.autoRender = true;
  this.friction = 0.5;
  this.ease = "easeOutCubic";
  this.easeTime = 500;
  this.snap = true;
  this.prevent = null;
  this.enabled = true;
  (function() {
    l();
    r()
  })();

  function l() {
    b = s.initClass(Interaction.Input, j);
    b.onUpdate = g;
    b.onStart = o;
    b.onEnd = a;
    p = new DynamicObject();
    x = s.initClass(Interaction.Renderer, j);
    s.input = b
  }

  function r() {
    k = {};
    y = {};
    f = {};
    if (!m) {
      m = {
        x: 1,
        y: 1
      }
    }
    for (var z in m) {
      k[z] = j[z];
      y[z] = m[z]
    }
    t = z
  }

  function c() {
    if (y[t] < 1) {
      if (j[t] < y[t]) {
        return true
      }
      if (j[t] > w) {
        return true
      }
    } else {
      if (j[t] > y[t]) {
        return true
      }
      if (j[t] < w) {
        return true
      }
    }
  }

  function v() {
    var z = y[t];
    var A = j[t];
    if (z < 1) {
      if (A < z) {
        j[t] = z - ((z - A) * s.friction);
        f[t] = z
      }
      if (A > 0) {
        j[t] = A * s.friction;
        f[t] = w
      }
    } else {
      if (A > z) {
        j[t] = z + ((A - z) * s.friction);
        f[t] = z
      }
      if (A < 0) {
        j[t] = A * s.friction;
        f[t] = w
      }
    }
  }

  function h() {
    return j[t] / y[t]
  }

  function e(A, z) {
    p.copyFrom(j);
    if (s.ease == "spring") {
      z.damping = s.damping
    }
    p.tween(z, A, s.ease, function() {
      p.copyTo(j);
      if (s.autoRender && x.needsUpdate) {
        x.update()
      }
      if (s.onUpdate) {
        s.onUpdate(h())
      }
    });
    if (z.damping) {
      delete z.damping
    }
  }

  function g(B, A) {
    if (s.prevent && s.prevent.disabled) {
      return
    }
    if (s.prevent) {
      if (Math.abs(B[t]) < 20 && !s.prevent.frozen) {
        var z = s.prevent.axis;
        if (Math.abs(B[z]) > s.prevent.pixels) {
          return s.prevent.disabled = true
        }
      } else {
        if (!s.prevent.disabled) {
          s.prevent.frozen = true
        }
      }
    }
    if (!s.enabled) {
      return
    }
    d = true;
    j[t] = n[t] + B[t];
    u.subVectors(j, k);
    if (c()) {
      v()
    }
    if (s.onUpdate) {
      s.onUpdate(h())
    }
    if (s.autoRender && x.needsUpdate) {
      x.update()
    }
  }

  function o() {
    if (!s.enabled) {
      return
    }
    n.copyFrom(j);
    p.stopTween();
    if (s.onStart) {
      s.onStart()
    }
  }

  function a() {
    if (s.onEnd) {
      s.onEnd()
    }
    if (s.prevent) {
      s.prevent.disabled = false;
      s.prevent.frozen = false;
      if (Math.abs(b.velocity[s.prevent.axis]) > 1 && Math.abs(b.velocity[t]) < 1) {
        return
      }
    }
    if (!d || !s.enabled || s.active("toggle")) {
      return
    }
    d = false;
    if (s.snap) {
      var B = {};
      var A = s.easeTime;
      var z = Math.abs(b.velocity[t]);
      if (z > 0.5) {
        if (b.velocity[t] < 0 && y[t] < 0) {
          B[t] = y[t]
        } else {
          if (b.velocity[t] > 0 && y[t] > 0) {
            B[t] = y[t]
          } else {
            B[t] = w
          }
        }
      } else {
        if (h() > 0.5) {
          B[t] = y[t]
        } else {
          B[t] = w
        }
      }
      e(A, B)
    } else {
      if (c()) {
        e(s.easeTime, f)
      }
    }
  }
  this.set("directions", function(z) {
    m = z;
    r()
  });
  this.set("min", function(z) {
    w = z
  });
  this.resetOrigin = function() {
    r()
  };
  this.toggle = function(A) {
    s.active("toggle", true);
    var z = {};
    if (h() < 0.5) {
      z[t] = y[t]
    } else {
      z[t] = w
    }
    e(A || s.easeTime, z);
    s.delayedCall(function() {
      s.active("toggle", false)
    }, 250)
  };
  this.getElapsed = function() {
    return h()
  }
});
Interaction.Class(function Slider(m, h, j) {
  Inherit(this, Component);
  var s = this;
  var b, p, x, t, q;
  var u, z, y, w;
  var A = 0;
  this.autoRender = true;
  this.currentSlide = 0;
  this.maxSlides = j.slides;
  this.width = j.width;
  this.height = j.height;
  this.snapPoint = 0.3;
  this.ease = "easeOutCubic";
  this.easeTime = 500;
  this.friction = 0.3;
  this.infinite = false;
  this.desktopDrag = true;
  this.preventMovement = true;
  this.cssTransitions = false;
  (function() {
    n();
    d()
  })();

  function n() {
    b = s.initClass(Interaction.Input, m);
    b.onUpdate = k;
    b.onStart = o;
    b.onEnd = a;
    s.input = b;
    p = new DynamicObject();
    x = s.initClass(Interaction.Renderer, m)
  }

  function d() {
    t = h.x ? "width" : "height";
    u = h.x ? "x" : "y"
  }

  function l() {
    if (!q) {
      return
    }
    return -q[u] / s[t]
  }

  function f() {
    s.elapsed = Math.abs(m.x) / Math.abs(-s[t] * (s.maxSlides - 1));
    if (z) {
      for (var C = 0; C < z.length; C++) {
        var B = z[C];
        var E = s[t] * C;
        var D = (m[u] + E) / s[t];
        if (B.updatePosition) {
          B.updatePosition(D)
        }
      }
    }
  }

  function e(C) {
    var B = s.cssTransitions;
    p.copyFrom(m);
    p.tween(C, s.easeTime, s.ease, function() {
      p.copyTo((B ? m.pos : m));
      if (!B && s.autoRender && x.needsUpdate) {
        x.update()
      }
      s.elapsed = f();
      if (s.onUpdate) {
        s.onUpdate(s.elapsed)
      }
    });
    if (B) {
      var D = {};
      D[u] = C[u];
      m.tween(D, s.easeTime, s.ease)
    }
  }

  function r() {
    s.currentSlide++;
    if (s.repeatable && s.currentSlide > s.maxSlides - 1) {
      s.currentSlide = 0
    }
    if (!s.infinite && s.currentSlide > s.maxSlides - 1) {
      s.currentSlide = s.maxSlides - 1
    }
    if (s.onUpdateSlide) {
      s.onUpdateSlide(s.currentSlide, 1)
    }
  }

  function g() {
    s.currentSlide--;
    if (s.repeatable && s.currentSlide < 0) {
      s.currentSlide = s.maxSlides - 1
    }
    if (!s.infinite && s.currentSlide < 0) {
      s.currentSlide = 0
    }
    if (s.onUpdateSlide) {
      s.onUpdateSlide(s.currentSlide, -1)
    }
  }

  function c() {
    return m[u] > 0 || (s.currentSlide == s.maxSlides - 1 && m[u] < -s[t] * s.currentSlide)
  }

  function v() {
    var C = m[u];
    var B = -s[t] * s.currentSlide;
    if (C > 0) {
      m[u] = C * s.friction
    } else {
      if (C < B) {
        m[u] = B + ((C - B) * s.friction)
      }
    }
  }

  function k(B) {
    if ((!Device.mobile && !s.desktopDrag) || !w || s.preventMovement) {
      return
    }
    m[u] = (-s[t] * s.currentSlide) + B[u] + A;
    if (!s.infinite && c()) {
      v()
    }
    s.elapsed = f();
    if (s.cssTransitions) {
      m.stopTween()
    }
    if (s.onUpdate) {
      s.onUpdate(s.elapsed)
    }
    if (s.autoRender && x.needsUpdate) {
      m.pos[u] = m[u];
      x.update()
    }
    q = B
  }

  function o(B) {
    if ((!Device.mobile && !s.desktopDrag) || (B.target.className == "hit" && j.allowHit != true) || s.preventMovement) {
      return
    }
    w = true;
    p.stopTween();
    A = (p[u] + (s[t] * s.currentSlide)) || 0;
    y = Date.now()
  }

  function a(F) {
    if ((!Device.mobile && !s.desktopDrag) || !w || s.preventMovement) {
      return
    }
    var E = {};
    var B = l();
    var C = b.velocity[u];
    var D = Date.now() - y;
    if (D < 50) {
      return
    }
    if (Math.abs(C) > 0.2) {
      if (C > 0) {
        g()
      } else {
        r()
      }
    } else {
      if (C < 0) {
        if (B > s.snapPoint) {
          r()
        }
      } else {
        if (Math.abs(B) > s.snapPoint) {
          g()
        }
      }
    }
    E[u] = -s[t] * s.currentSlide;
    w = false;
    e(E)
  }
  this.set("views", function(B) {
    z = B
  });
  this.moveTo = function(B) {
    if (!s.infinite) {
      if (B < 0) {
        B = 0
      }
      if (B >= s.maxSlides) {
        B = s.maxSlides - 1
      }
    }
    s.currentSlide = B;
    var C = {};
    C[u] = -s[t] * s.currentSlide;
    e(C)
  };
  this.jumpTo = function(B) {
    if (!s.infinite) {
      if (B < 0) {
        B = 0
      }
      if (B >= s.maxSlides) {
        B = s.maxSlides - 1
      }
    }
    s.currentSlide = B;
    p[u] = -s[t] * s.currentSlide;
    p.copyTo(m);
    if (s.autoRender && x.needsUpdate) {
      m.pos[u] = m[u];
      x.update()
    }
  };
  this.next = function() {
    r();
    var B = {};
    B[u] = -s[t] * s.currentSlide;
    e(B)
  };
  this.prev = function() {
    g();
    var B = {};
    B[u] = -s[t] * s.currentSlide;
    e(B)
  }
});
Class(function ParticlePhysics(g) {
  Inherit(this, Component);
  var d = this;
  g = g || new EulerIntegrator();
  var a = 1 / 60;
  var n = 0;
  var m = 0;
  var c = null;
  var j = 0;
  var e = [];
  this.friction = 1;
  this.maxSteps = 1;
  this.emitters = new LinkedList();
  this.initializers = new LinkedList();
  this.behaviors = new LinkedList();
  this.particles = new LinkedList();
  this.springs = new LinkedList();

  function l(q) {
    var o = d.initializers.start();
    while (o) {
      o(q);
      o = d.initializers.next()
    }
  }

  function h(p) {
    var o = d.springs.start();
    while (o) {
      o.update(p);
      o = d.springs.next()
    }
  }

  function k() {
    for (var o = e.length - 1; o > -1; o--) {
      var p = e[o];
      d.particles.remove(p);
      p.system = null
    }
    e.length = 0
  }

  function f(r) {
    var q = 0;
    var s = d.particles.start();
    while (s) {
      if (!s.disabled) {
        var o = d.behaviors.start();
        while (o) {
          o.applyBehavior(s, r, q);
          o = d.behaviors.next()
        }
        if (s.behaviors.length) {
          s.update(r, q)
        }
      }
      q++;
      s = d.particles.next()
    }
  }

  function b(o) {
    f(o);
    if (d.springs.length) {
      h(o)
    }
    if (!d.skipIntegration) {
      g.integrate(d.particles, o, d.friction)
    }
  }
  this.addEmitter = function(o) {
    if (!(o instanceof Emitter)) {
      throw "Emitter must be Emitter"
    }
    this.emitters.push(o);
    o.parent = o.system = this
  };
  this.removeEmitter = function(o) {
    if (!(o instanceof Emitter)) {
      throw "Emitter must be Emitter"
    }
    this.emitters.remove(o);
    o.parent = o.system = null
  };
  this.addInitializer = function(o) {
    if (typeof o !== "function") {
      throw "Initializer must be a function"
    }
    this.initializers.push(o)
  };
  this.removeInitializer = function(o) {
    this.initializers.remove(o)
  };
  this.addBehavior = function(o) {
    this.behaviors.push(o);
    o.system = this
  };
  this.removeBehavior = function(o) {
    this.behaviors.remove(o)
  };
  this.addParticle = function(o) {
    if (!g.type) {
      if (typeof o.pos.z === "number") {
        g.type = "3D"
      } else {
        g.type = "2D"
      }
    }
    o.system = this;
    this.particles.push(o);
    if (this.initializers.length) {
      l(o)
    }
  };
  this.removeParticle = function(o) {
    o.system = null;
    e.push(o)
  };
  this.addSpring = function(o) {
    o.system = this;
    this.springs.push(o)
  };
  this.removeSpring = function(o) {
    o.system = null;
    this.springs.remove(o)
  };
  this.update = function(p) {
    if (!c) {
      c = THREAD ? Date.now() : Render.TIME
    }
    var q = THREAD ? Date.now() : Render.TIME;
    var r = q - c;
    if (!p && r <= 0) {
      return
    }
    r *= 0.001;
    c = q;
    j += r;
    if (!p) {
      var o = 0;
      while (j >= a && o++ < d.maxSteps) {
        b(a);
        j -= a;
        n += a
      }
    } else {
      b(0.016)
    }
    m = Date.now() - q;
    if (e.length) {
      k()
    }
  }
});
Class(function Particle(j, c, b) {
  var f = this;
  var a, g, d;
  var h = Particle.prototype;
  this.mass = c || 1;
  this.massInv = 1 / this.mass;
  this.radius = b || 1;
  this.radiusSq = this.radius * this.radius;
  this.behaviors = new LinkedList();
  this.fixed = false;
  (function() {
    e()
  })();

  function e() {
    var k = typeof j.z === "number" ? Vector3 : Vector2;
    j = j || new k();
    a = new k();
    g = new k();
    d = {};
    d.pos = new k();
    d.acc = new k();
    d.vel = new k();
    d.pos.copyFrom(j);
    f.pos = f.position = j;
    f.vel = f.velocity = a;
    f.acc = f.acceleration = g;
    f.old = d
  }
  this.moveTo = function(k) {
    j.copyFrom(k);
    d.pos.copyFrom(j);
    g.clear();
    a.clear()
  };
  if (typeof h.setMass !== "undefined") {
    return
  }
  h.setMass = function(k) {
    this.mass = k || 1;
    this.massInv = 1 / this.mass
  };
  h.setRadius = function(k) {
    this.radius = k;
    this.radiusSq = k * k
  };
  h.update = function(l) {
    if (!this.behaviors.length) {
      return
    }
    var k = this.behaviors.start();
    while (k) {
      k.applyBehavior(this, l);
      k = this.behaviors.next()
    }
  };
  h.applyForce = function(k) {
    this.acc.add(k)
  };
  h.addBehavior = function(k) {
    if (!k || typeof k.applyBehavior === "undefined") {
      throw "Behavior must have applyBehavior method"
    }
    this.behaviors.push(k)
  };
  h.removeBehavior = function(k) {
    if (!k || typeof k.applyBehavior === "undefined") {
      throw "Behavior must have applyBehavior method"
    }
    this.behaviors.remove(k)
  }
});
Class(function SpringPhysics(f, d, g, h) {
  var e = this;
  var j = Spring.prototype;
  var c = typeof f.pos.z !== "undefined" ? Vector3 : Vector2;
  var a = new c();
  var b = new c();
  h = h || b.subVectors(f.pos, d.pos).length();
  if (typeof j.update !== "undefined") {
    return
  }
  j.update = function(k) {
    a.copyFrom(d.pos).sub(f.pos);
    b.copyFrom(a);
    var m = a.length() + 1e-7;
    var l = (m - h) / (m * (f.massInv + d.massInv)) * g;
    if (!f.fixed) {
      f.pos.add(b.multiply(l * f.massInv))
    }
    if (!d.fixed) {
      d.pos.add(a.multiply(-l * d.massInv))
    }
  }
});
Class(function Spring(j, f, b, c) {
  Inherit(this, Component);
  var g = this;
  var e = typeof j.z !== "undefined" ? Vector3 : Vector2;
  var d = new e();
  var h = new e();
  var a = new e();
  this.update = function() {
    d.subVectors(f, j);
    h.copyFrom(d).multiply(b);
    a.add(h).multiply(c);
    j.add(a)
  };
  this.set("position", function(k) {
    j = k
  });
  this.set("target", function(k) {
    f = k
  });
  this.set("damping", function(k) {
    b = k
  });
  this.set("friction", function(k) {
    c = k
  })
});
Class(function EulerIntegrator() {
  Inherit(this, Component);
  var d = this;
  var b, c;
  this.useDeltaTime = false;
  (function() {})();

  function a() {
    var e = d.type == "3D" ? Vector3 : Vector2;
    b = new e();
    c = new e()
  }
  this.integrate = function(h, g, f) {
    if (!b) {
      a()
    }
    var e = g * g;
    var j = h.start();
    while (j) {
      if (!j.fixed && !j.disabled) {
        j.old.pos.copyFrom(j.pos);
        j.acc.multiply(j.massInv);
        b.copyFrom(j.vel);
        c.copyFrom(j.acc);
        if (this.useDeltaTime) {
          j.pos.add(b.multiply(g)).add(c.multiply(0.5 * e));
          j.vel.add(j.acc.multiply(g))
        } else {
          j.pos.add(b).add(c.multiply(0.5));
          j.vel.add(j.acc)
        }
        if (f) {
          j.vel.multiply(f)
        }
        j.acc.clear()
      }
      if (j.saveTo) {
        j.pos.copyTo(j.saveTo)
      }
      j = h.next()
    }
  }
});
Class(function Emitter(c, e) {
  Inherit(this, Component);
  var h = this;
  var d;
  var b = 0;
  var a = c.type == "vector3" ? Vector3 : Vector2;
  this.initializers = [];
  this.position = c;
  this.autoEmit = 1;
  (function() {
    g();
    if (e != 0) {
      f(e || 100)
    }
  })();

  function g() {
    d = h.initClass(ObjectPool)
  }

  function f(l) {
    b += l;
    var k = [];
    for (var j = 0; j < l; j++) {
      k.push(new Particle())
    }
    d.insert(k)
  }
  this.addInitializer = function(j) {
    if (typeof j !== "function") {
      throw "Initializer must be a function"
    }
    this.initializers.push(j)
  };
  this.removeInitializer = function(k) {
    var j = this.initializers.indexOf(k);
    if (j > -1) {
      this.initializers.splice(j, 1)
    }
  };
  this.emit = function(l) {
    if (!this.parent) {
      throw "Emitter needs to be added to a System"
    }
    l = l || this.autoEmit;
    for (var m = 0; m < l; m++) {
      var n = d.get();
      if (!n) {
        return
      }
      n.moveTo(this.position);
      n.emitter = this;
      if (!n.system) {
        this.parent.addParticle(n)
      }
      for (var k = 0; k < this.initializers.length; k++) {
        this.initializers[k](n)
      }
    }
  };
  this.remove = function(j) {
    d.put(j);
    h.parent.removeParticle(j)
  };
  this.addToPool = function(j) {
    d.put(j)
  }
});
Class(function D3() {
  Namespace(this);
  if (THREAD) {
    return
  }
  this.CSS3D = Device.tween.css3d;
  this.m4v31 = new Vector3();
  this.m4v32 = new Vector3();
  this.m4v33 = new Vector3();
  this.UP = new Vector3(0, 1, 0);
  this.FWD = new Vector3(0, 0, -1);
  this.CENTER = new Vector3(0, 0, 0);
  this.translate = function(a, c, b) {
    a = typeof a == "string" ? a : (a || 0) + "px";
    c = typeof c == "string" ? c : (c || 0) + "px";
    b = typeof b == "string" ? b : (b || 0) + "px";
    if (Device.browser.ie) {
      a = 0;
      c = 0
    }
    return "translate3d(" + a + "," + c + "," + b + ")"
  }
}, "Static");
D3.Class(function Camera(d, c, b) {
  Inherit(this, D3.Object3D);
  var h = this;
  var a, f, e, g;
  this.inverseWorldMatrix = new Matrix4();
  (function() {
    defer(function() {
      h.scene.setProjection(d, c, b)
    })
  })();
  this.set("fov", function(j) {
    d = j;
    h.scene.setProjection(d, c, b)
  });
  this.computeInverseMatrix = function() {
    this.worldMatrix.inverse(this.inverseWorldMatrix);
    return this.inverseWorldMatrix
  };
  this.unproject = function(r) {
    if (!a) {
      e = new Vector3();
      a = new Vector3();
      f = new Matrix4();
      g = new Matrix4()
    }
    var q = h.uniforms;
    a.set((r.x / q.width) * 2 - 1, -(r.y / q.height) * 2 + 1, 40);
    g.identity();
    f.copyFrom(h.worldMatrix).multiply(q.projection.inverse(g));
    var p = a.x,
        n = a.y,
        m = a.z;
    var j = f.data;
    var k = 1 / (j[3] * p + j[7] * n + j[11] * m + j[15]);
    a.x = (j[0] * p + j[4] * n + j[8] * m + j[12]) * k;
    a.y = (j[1] * p + j[5] * n + j[9] * m + j[13]) * k;
    a.z = (j[2] * p + j[6] * n + j[10] * m + j[14]) * k;
    var o = h.position;
    a.sub(o).normalize();
    var l = (-o.z / a.z);
    e.copyFrom(o).add(a.multiply(l));
    return e
  };
  this.render = function() {}
});
D3.Class(function CSSMaterial(p) {
  Inherit(this, Component);
  var l = this;
  var f, n;
  var d, o, g, e, b, h, k;
  var c = true;
  this.material = p;
  this.width = p.width;
  this.height = p.height;
  (function() {
    j();
    m()
  })();

  function j() {
    k = new Vector3();
    h = new Vector3();
    if (Device.browser.ie) {
      p.css({
        marginLeft: -p.width / 2,
        marginTop: -p.height / 2
      })
    }
    if (D3.CSS3D) {
      return false
    }
    d = new Matrix2();
    o = new Matrix4();
    g = new Vector3()
  }

  function m() {
    n = p.element || p;
    if (p.element) {
      Render.nextFrame(function() {
        p.material = l;
        p.object = l.object
      })
    }
  }

  function a(r) {
    var t = l.object._scene.fog;
    h.subVectors(r.camera.position, k);
    var u = h.length();
    if (u > t) {
      var q = (t * 2) - t;
      u -= t;
      var s = Utils.convertRange(u, 0, q, 0, 1);
      s = Utils.clamp(s, 0, 1);
      e = 1 - s;
      n.div.style.opacity = e
    } else {
      if (e < 1) {
        n.div.style.opacity = 1;
        e = 1
      }
    }
  }
  this.set("visible", function(q) {
    c = q;
    if (q) {
      p.show()
    } else {
      p.hide()
    }
  });
  this.draw = function(q) {
    q.renderer.addChild(p)
  };
  this.remove = function() {
    if (p.destroy) {
      p.destroy()
    } else {
      if (p.remove) {
        p.remove(true)
      }
    }
  };
  this.render = function(q) {
    if (!c) {
      return
    }
    l.object.worldMatrix.extractPosition(k);
    if (l.object && l.object._scene && l.object._scene.fog) {
      a(q)
    }
    if (D3.CSS3D) {
      var w = D3.translate("-50%", "-50%", q.cssDistance);
      var v = "perspective(" + q.cssDistance + "px)";
      var s = w + " " + l.object.viewMatrix.getCSS();
      if (Device.browser.ie) {
        n.div.style[CSS.prefix("Transform")] = v + s
      } else {
        n.div.style[CSS.prefix("Transform")] = s
      }
    } else {
      q.projection.copyTo(o);
      o.multiply(l.object.viewMatrix);
      g.set(0, 0, 0);
      o.transformVector(g);
      g.x = g.x / g.z * q.centerX;
      g.y = g.y / g.z * q.centerY;
      var u = 1 / (g.z / q.cssDistance);
      var r = l.object.rotation.z;
      d.setTRS(g.x, g.y, r, u, u);
      h.subVectors(q.camera.position, k);
      var t = h.length();
      p.setZ(~~(999999 - t)).matrix("translate(-50%, -50%) " + d.getCSS());
      if (g.z <= 0 && !p._meshHidden) {
        p._meshHidden = true;
        p.hide()
      } else {
        if (g.z > 0 && p._meshHidden) {
          p._meshHidden = false;
          p.show()
        }
      }
    }
  }
});
D3.Class(function AbstractMaterial() {
  Inherit(this, Component);
  var c = this;
  var b = new Matrix4();
  var a = new Vector3();
  this.draw = function() {};
  this.remove = function() {};
  this.render = function(d) {
    d.projection.copyTo(b);
    b.multiply(c.object.viewMatrix);
    a.set(0, 0, 0);
    b.transformVector(a);
    a.x = a.x / a.z * d.centerX;
    a.y = a.y / a.z * d.centerY;
    a.scale = 1 / (a.z / d.cssDistance);
    a.rotation = c.object.rotation.z;
    a.x += d.centerX;
    a.y += d.centerY;
    a.x += d.offsetX;
    a.y += d.offsetY;
    if (c.object.position.z > d.camera.position.z) {
      a.x = -9999999
    }
    c.object.screen = a
  }
});
D3.Class(function Object3D(f) {
  Inherit(this, Component);
  var h = this;
  var g, c, e;
  var a = true;
  var b = new Matrix4();
  var d = new Vector3();
  this.id = Utils.timestamp();
  this.directMatrix = false;
  this.billboard = false;
  this.material = f || null;
  this.position = new Vector3(0, 0, 0);
  this.rotation = new Vector3(0, 0, 0);
  this.scale = new Vector3(1, 1, 1);
  this.matrix = new Matrix4();
  this.worldMatrix = new Matrix4();
  this.viewMatrix = new Matrix4();
  this.children = new LinkedList();
  (function() {
    if (h.material) {
      h.material.object = h
    }
  })();
  this.get("numChildren", function() {
    return h.children.length
  });
  this.get("depth", function() {
    return h.viewMatrix.data[14]
  });
  this.get("globalPosition", function() {
    h.worldMatrix.extractPosition(d);
    return d
  });
  this.get("enabled", function() {
    return a
  });
  this.set("enabled", function(j) {
    a = j;
    if (h.material) {
      h.material.visibility(a)
    }
    var k = h.children.start();
    while (k) {
      k.enabled = j;
      k = h.children.next()
    }
  });
  this.set("scene", function(j) {
    if (!j) {
      return false
    }
    e = h._scene = j;
    if (h.material) {
      h.material.draw(j)
    }
  });
  this.add = function(j) {
    if (!(j instanceof D3.Object3D)) {
      throw "Can only add D3.Object3D"
    }
    j._parent = this;
    this.children.push(j);
    Render.nextFrame(function() {
      j.scene = e
    })
  };
  this.remove = function(j) {
    if (!(j instanceof D3.Object3D)) {
      throw "Can only remove D3.Object3D"
    }
    j._parent = null;
    j.removed();
    this.children.remove(j)
  };
  this.removed = function() {
    if (this.material) {
      this.material.remove()
    }
  };
  this.empty = function() {
    var j = this.children.start();
    while (j) {
      j._parent = null;
      j.removed();
      j = this.children.next()
    }
    this.children.empty()
  };
  this.updateMatrix = function() {
    if (!this.directMatrix) {
      var l = this.position;
      var k = this.rotation;
      var j = this.scale;
      this.matrix.setTRS(l.x, l.y, l.z, k.x, k.y, k.z, j.x, j.y, j.z)
    }
    if (g) {
      this.matrix.setLookAt(g, D3.CENTER, D3.UP)
    }
    if (this._parent && this._parent.worldMatrix) {
      this._parent.worldMatrix.copyTo(this.worldMatrix);
      this.worldMatrix.multiply(this.matrix)
    } else {
      this.matrix.copyTo(this.worldMatrix)
    }
    if (c) {
      this.worldMatrix.setLookAt(c.globalPosition, D3.CENTER, D3.UP)
    }
    var m = this.children.start();
    while (m) {
      m.updateMatrix();
      m = this.children.next()
    }
  };
  this.updateView = function(j) {
    if (!a) {
      return false
    }
    if (this.billboard) {
      j.copyTo(b);
      b.transpose();
      b.copyPosition(this.worldMatrix);
      b.scale(this.scale.x, this.scale.y, this.scale.z);
      b.data[3] = 0;
      b.data[7] = 0;
      b.data[11] = 0;
      b.data[15] = 1;
      b.copyTo(this.worldMatrix)
    }
    j.copyTo(this.viewMatrix);
    this.viewMatrix.multiply(this.worldMatrix);
    var k = this.children.start();
    while (k) {
      k.updateView(j);
      k = this.children.next()
    }
  };
  this.render = function(j) {
    if (!a) {
      return false
    }
    if (this.material) {
      this.material.render(j)
    }
    var k = this.children.start();
    while (k) {
      k.render(j);
      k = this.children.next()
    }
  };
  this.lookAt = function(j) {
    if (j instanceof Vector3) {
      g = j
    } else {
      c = j
    }
  };
  this.destroy = function() {
    if (!this._destroy) {
      return
    }
    this.empty();
    if (this._parent && this._parent.remove) {
      this._parent.remove(this)
    }
    return this._destroy()
  }
});
D3.Class(function PerspectiveProjection() {
  var c = this;
  var b = PerspectiveProjection.prototype;
  this.data = new Float32Array(16);
  (function() {
    a()
  })();

  function a() {
    var d = c.data;
    d[0] = 1, d[1] = 0, d[2] = 0, d[3] = 0;
    d[4] = 0, d[5] = 1, d[6] = 0, d[7] = 0;
    d[8] = 0, d[9] = 0, d[10] = 1, d[11] = 0;
    d[12] = 0, d[13] = 0, d[14] = 0, d[15] = 1
  }
  b.identity = function() {
    a();
    return this
  };
  b.perspective = function(g, f, j, e) {
    var d = this.data;
    var h = j * Math.tan(g * Math.PI / 360);
    var k = e - j;
    d[0] = j / (h * f);
    d[4] = 0;
    d[8] = 0;
    d[12] = 0;
    d[1] = 0;
    d[5] = j / h;
    d[9] = 0;
    d[13] = 0;
    d[2] = 0;
    d[6] = 0;
    d[10] = -(e + j) / k;
    d[14] = -(2 * e * j) / k;
    d[3] = 0;
    d[7] = 0;
    d[11] = -1;
    d[15] = 0
  };
  b.transformVector = function(g, h) {
    var e = g.x,
        k = g.y,
        j = g.z,
        f = g.w;
    var d = this.data;
    h = h || g;
    h.x = d[0] * e + d[4] * k + d[8] * j + d[12] * f;
    h.y = d[1] * e + d[5] * k + d[9] * j + d[13] * f;
    h.z = d[2] * e + d[6] * k + d[10] * j + d[14] * f;
    return h
  };
  b.inverse = function(y) {
    var B = this.data;
    y = y || this.data;
    var J = B[0],
        H = B[1],
        G = B[2],
        E = B[3],
        h = B[4],
        g = B[5],
        f = B[6],
        e = B[7],
        x = B[8],
        w = B[9],
        v = B[10],
        u = B[11],
        L = B[12],
        K = B[13],
        I = B[14],
        F = B[15],
        t = J * g - H * h,
        s = J * f - G * h,
        r = J * e - E * h,
        q = H * f - G * g,
        p = H * e - E * g,
        o = G * e - E * f,
        n = x * K - w * L,
        l = x * I - v * L,
        k = x * F - u * L,
        j = w * I - v * K,
        D = w * F - u * K,
        A = v * F - u * I,
        C = (t * A - s * D + r * j + q * k - p * l + o * n),
        z;
    if (!C) {
      return null
    }
    y[0] = (g * A - f * D + e * j) * z;
    y[1] = (-H * A + G * D - E * j) * z;
    y[2] = (K * o - I * p + F * q) * z;
    y[3] = (-w * o + v * p - u * q) * z;
    y[4] = (-h * A + f * k - e * l) * z;
    y[5] = (J * A - G * k + E * l) * z;
    y[6] = (-L * o + I * r - F * s) * z;
    y[7] = (x * o - v * r + u * s) * z;
    y[8] = (h * D - g * k + e * n) * z;
    y[9] = (-J * D + H * k - E * n) * z;
    y[10] = (L * p - K * r + F * t) * z;
    y[11] = (-x * p + w * r - u * t) * z;
    y[12] = (-h * j + g * l - f * n) * z;
    y[13] = (J * j - H * l + G * n) * z;
    y[14] = (-L * q + K * s - I * t) * z;
    y[15] = (x * q - w * s + v * t) * z;
    return y
  };
  b.copyTo = function(e) {
    var f = this.data;
    var d = e.data || e;
    for (var g = 0; g < 16; g++) {
      d[g] = f[g]
    }
    return e
  }
});
D3.Class(function Scene(l, e) {
  Inherit(this, Component);
  var g = this;
  var m;
  var n, b;
  var j, c, k, h;
  this.children = new LinkedList();
  this.center = new Vector3(0, 0, 0);
  (function() {
    d();
    a();
    f()
  })();

  function d() {
    if (!l || !e) {
      throw "D3.Scene requires width, height"
    }
    if (!THREAD) {
      n = $("#Scene3D");
      b = n.create("Renderer");
      b.center();
      g.container = n;
      g.renderer = b
    }
  }

  function a() {
    m = {};
    m.projection = new D3.PerspectiveProjection();
    m.scene = g;
    m.offsetX = 0;
    m.offsetY = 0;
    g.uniforms = m
  }

  function f() {
    m.width = l;
    m.height = e;
    m.aspect = l / e;
    m.centerX = l / 2;
    m.centerY = e / 2;
    if (n) {
      n.size(l, e)
    }
  }
  this.get("numChildren", function() {
    return g.children.length
  });
  this.get("distance", function() {
    return m.cssDistance
  });
  this.setProjection = function(p, q, o) {
    j = p || (j || 30);
    c = q || 0.1;
    k = o || 1000;
    m.cssDistance = 0.5 / Math.tan(j * Math.PI / 360) * m.height;
    m.projection.perspective(j, m.width / m.height, c, k);
    if (n) {
      n.div.style[CSS.prefix("Perspective")] = m.cssDistance + "px";
      b.div.style[CSS.prefix("TransformStyle")] = "preserve-3d"
    }
  };
  this.resize = function(o, p) {
    l = o;
    e = p;
    f();
    g.setProjection()
  };
  this.add = function(o) {
    if (!(o instanceof D3.Object3D) && !(o instanceof D3.Camera)) {
      throw "Can only add D3.Object3D"
    }
    o._parent = this;
    o.scene = this;
    this.children.push(o)
  };
  this.remove = function(o) {
    if (!(o instanceof D3.Object3D) && !(o instanceof D3.Camera)) {
      throw "Can only remove D3.Object3D"
    }
    o.removed();
    o._parent = null;
    this.children.remove(o)
  };
  this.empty = function() {
    var o = this.children.start();
    while (o) {
      o.removed();
      o = this.children.next()
    }
    this.children.empty()
  };
  this.offset = function(o, p) {
    m.offsetX = o;
    m.offsetY = p
  };
  this.render = function(o) {
    o.updateMatrix();
    m.camera = o;
    m.viewMatrix = o.computeInverseMatrix();
    o.uniforms = m;
    var p = this.children.start();
    while (p) {
      p.updateMatrix();
      p.updateView(m.viewMatrix);
      p.render(m);
      p = this.children.next()
    }
  }
});
Class(function SplitTextfield() {
  var a = {
    display: "block",
    position: "relative",
    padding: 0,
    margin: 0,
    cssFloat: "left",
    styleFloat: "left",
    width: "auto",
    height: "auto"
  };

  function c(j) {
    var f = [];
    var h = j.div.innerHTML;
    var e = h.split("");
    j.div.innerHTML = "";
    for (var d = 0; d < e.length; d++) {
      if (e[d] == " ") {
        e[d] = "&nbsp;"
      }
      var g = $("t", "span");
      g.html(e[d], true).css(a);
      f.push(g);
      j.addChild(g)
    }
    return f
  }

  function b(k) {
    var f = [];
    var j = k.div.innerHTML;
    var e = j.split(" ");
    k.empty();
    for (var d = 0; d < e.length; d++) {
      var h = $("t", "span");
      var g = $("t", "span");
      h.html(e[d]).css(a);
      g.html("&nbsp", true).css(a);
      f.push(h);
      f.push(g);
      k.addChild(h);
      k.addChild(g)
    }
    return f
  }
  this.split = function(e, d) {
    if (d == "word") {
      return b(e)
    } else {
      return c(e)
    }
  }
}, "Static");
Class(function CSSFilter(k, d, h) {
  Inherit(this, Component);
  var f = this;
  var g = "";
  var b = ["grayscale", "sepia", "saturate", "hue", "invert", "opacity", "brightness", "contrast", "blur"];
  var j, a;

  function c(n) {
    for (var m = b.length - 1; m > -1; m--) {
      if (b[m] == n) {
        return true
      }
    }
    return false
  }

  function l() {
    var q = "";
    var m = b.length - 1;
    for (var n in f) {
      if (!c(n)) {
        continue
      }
      var o = n;
      var p = f[n];
      if (typeof p === "number") {
        o = o == "hue" ? "hue-rotate" : o;
        p = o == "hue-rotate" ? p + "deg" : p;
        p = o == "blur" ? p + "px" : p;
        q += o + "(" + p + ") "
      }
    }
    g = q
  }

  function e() {
    if (a || !k || !k.div) {
      return false
    }
    k.div.style[CSS.prefix("Transition")] = ""
  }
  this.apply = function() {
    l();
    k.div.style[CSS.prefix("Filter")] = g
  };
  this.tween = function(o, p, q, m, r) {
    if (typeof m === "function") {
      r = m;
      m = 0
    }
    m = m || 0;
    j = false;
    var n = "-" + Device.styles.vendor.toLowerCase() + "-filter";
    k.willChange(n);
    Render.setupTween(function() {
      if (j) {
        return
      }
      k.div.style[CSS.prefix("Transition")] = n + " " + p + "ms " + TweenManager.getEase(q) + " " + m + "ms";
      for (var s in o) {
        f[s] = o[s]
      }
      a = f.delayedCall(function() {
        k.willChange(null);
        if (r) {
          r()
        }
      }, p + m);
      f.apply()
    })
  };
  this.stopTween = function() {
    clearTimeout(a);
    j = true;
    e()
  };
  this.clear = function() {
    for (var m in f) {
      if (c(m)) {
        delete f[m]
      }
    }
    if (a) {
      this.stopTween()
    }
    this.apply()
  };
  this.destroy = function() {
    this.clear();
    k = null;
    a = null;
    return this._destroy()
  }
});
Class(function CSSAnimation() {
  Inherit(this, Component);
  var k = this;
  var p = "a" + Utils.timestamp();
  var c, h, l;
  var j = 1000;
  var a = "linear";
  var m = 0;
  var e = false;
  var n = 1;
  var q = null;
  var d = [];
  (function() {})();

  function b() {
    k.playing = false;
    if (k.events) {
      k.events.fire(HydraEvents.COMPLETE, null, true)
    }
  }

  function f() {
    var w = CSS._read();
    var r = "/*" + p + "*/";
    var F = "@" + Device.vendor + "keyframes " + p + " {\n";
    var x = r + F;
    if (w.strpos(p)) {
      var z = w.split(r);
      w = w.replace(r + z[1] + r, "")
    }
    var B = c.length - 1;
    var C = Math.round(100 / B);
    var A = 0;
    for (var v = 0; v < c.length; v++) {
      var t = c[v];
      if (v == c.length - 1) {
        A = 100
      }
      x += (t.percent || A) + "% {\n";
      var s = false;
      var y = {};
      var E = {};
      for (var D in t) {
        if (TweenManager.checkTransform(D)) {
          y[D] = t[D];
          s = true
        } else {
          E[D] = t[D]
        }
      }
      if (s) {
        x += Device.vendor + "transform: " + TweenManager.parseTransform(y) + ";"
      }
      for (D in E) {
        var u = E[D];
        if (typeof u !== "string" && D != "opacity" && D != "zIndex") {
          u += "px"
        }
        x += CSS._toCSS(D) + ": " + u + ";"
      }
      x += "\n}\n";
      A += C
    }
    x += "}" + r;
    w += x;
    CSS._write(w)
  }

  function o() {
    var s = CSS._read();
    var t = "/*" + p + "*/";
    if (s.strpos(p)) {
      var r = s.split(t);
      s = s.replace(t + r[1] + t, "")
    }
    CSS._write(s)
  }

  function g(s) {
    for (var r = d.length - 1; r > -1; r--) {
      s(d[r])
    }
  }
  this.set("frames", function(r) {
    c = r;
    f()
  });
  this.set("steps", function(r) {
    q = r;
    if (k.playing) {
      g(function(s) {
        s.div.style[CSS.prefix("AnimationTimingFunction")] = "steps(" + r + ")"
      })
    }
  });
  this.set("duration", function(r) {
    j = Math.round(r);
    if (k.playing) {
      g(function(s) {
        s.div.style[CSS.prefix("AnimationDuration")] = k.duration + "ms"
      })
    }
  });
  this.get("duration", function() {
    return j
  });
  this.set("ease", function(r) {
    a = r;
    if (k.playing) {
      g(function(s) {
        s.div.style[CSS.prefix("AnimationTimingFunction")] = TweenManager.getEase(a)
      })
    }
  });
  this.get("ease", function() {
    return a
  });
  this.set("loop", function(r) {
    e = r;
    if (k.playing) {
      g(function(s) {
        s.div.style[CSS.prefix("AnimationIterationCount")] = e ? "infinite" : n
      })
    }
  });
  this.get("loop", function() {
    return e
  });
  this.set("count", function(r) {
    n = r;
    if (k.playing) {
      g(function(s) {
        s.div.style[CSS.prefix("AnimationIterationCount")] = e ? "infinite" : n
      })
    }
  });
  this.get("count", function() {
    return n
  });
  this.set("delay", function(r) {
    m = r;
    if (k.playing) {
      g(function(s) {
        s.div.style[CSS.prefix("AnimationDelay")] = m + "ms"
      })
    }
  });
  this.get("delay", function() {
    return m
  });
  this.play = function() {
    g(function(r) {
      r.div.style[CSS.prefix("AnimationName")] = p;
      r.div.style[CSS.prefix("AnimationDuration")] = k.duration + "ms";
      r.div.style[CSS.prefix("AnimationTimingFunction")] = q ? "steps(" + q + ")" : TweenManager.getEase(a);
      r.div.style[CSS.prefix("AnimationIterationCount")] = e ? "infinite" : n;
      r.div.style[CSS.prefix("AnimationPlayState")] = "running";
      r.div.style[CSS.prefix("AnimationDelay")] = m + "ms"
    });
    k.playing = true;
    clearTimeout(h);
    if (!k.loop) {
      l = Date.now();
      h = k.delayedCall(b, n * j)
    }
  };
  this.pause = function() {
    k.playing = false;
    clearTimeout(h);
    g(function(r) {
      r.div.style[CSS.prefix("AnimationPlayState")] = "paused"
    })
  };
  this.stop = function() {
    k.playing = false;
    clearTimeout(h);
    g(function(r) {
      r.div.style[CSS.prefix("AnimationName")] = ""
    })
  };
  this.applyTo = function(r) {
    d.push(r);
    if (k.playing) {
      r.div.style[CSS.prefix("AnimationName")] = p;
      r.div.style[CSS.prefix("AnimationDuration")] = k.duration + "ms";
      r.div.style[CSS.prefix("AnimationTimingFunction")] = q ? "steps(" + q + ")" : TweenManager.getEase(a);
      r.div.style[CSS.prefix("AnimationIterationCount")] = e ? "infinite" : n;
      r.div.style[CSS.prefix("AnimationPlayState")] = "running"
    }
  };
  this.remove = function(s) {
    s.div.style[CSS.prefix("AnimationName")] = "";
    var r = d.indexOf(s);
    if (r > -1) {
      d.splice(r, 1)
    }
  };
  this.destroy = function() {
    this.stop();
    c = null;
    o();
    return this._destroy()
  }
});
Class(function Warp(n, c) {
  Inherit(this, Component);
  var g = this;
  var o, f, d;
  this.useCSS = c;
  this.points = [new Vector2(), new Vector2(), new Vector2(), new Vector2()];
  this.tl = this.points[0];
  this.tr = this.points[1];
  this.bl = this.points[2];
  this.br = this.points[3];

  function h() {
    if (g.points[1].x == 0) {
      g.points[1].x = g.width
    }
    if (g.points[2].y == 0) {
      g.points[2].y = g.height
    }
    if (g.points[3].x == 0) {
      g.points[3].x = g.width
    }
    if (g.points[3].y == 0) {
      g.points[3].y = g.height
    }
  }

  function m(A, p, F, v, G, w, t, C, u, D, z, q, B, r, H, x) {
    var y = b(A, p, G, w, u, D, B, r);
    var E = b(F, v, t, C, z, q, H, x);
    return j(E, k(y))
  }

  function b(r, x, q, w, p, u, z, t) {
    var s = [r, q, p, x, w, u, 1, 1, 1];
    var y = e(k(s), [z, t, 1]);
    return j(s, [y[0], 0, 0, 0, y[1], 0, 0, 0, y[2]])
  }

  function k(p) {
    return [p[4] * p[8] - p[5] * p[7], p[2] * p[7] - p[1] * p[8], p[1] * p[5] - p[2] * p[4], p[5] * p[6] - p[3] * p[8], p[0] * p[8] - p[2] * p[6], p[2] * p[3] - p[0] * p[5], p[3] * p[7] - p[4] * p[6], p[1] * p[6] - p[0] * p[7], p[0] * p[4] - p[1] * p[3]]
  }

  function j(q, p) {
    var v = Array(9);
    for (var u = 0; u != 3; ++u) {
      for (var s = 0; s != 3; ++s) {
        var t = 0;
        for (var r = 0; r != 3; ++r) {
          t += q[3 * u + r] * p[3 * r + s]
        }
        v[3 * u + s] = t
      }
    }
    return v
  }

  function e(p, q) {
    return [p[0] * q[0] + p[1] * q[1] + p[2] * q[2], p[3] * q[0] + p[4] * q[1] + p[5] * q[2], p[6] * q[0] + p[7] * q[1] + p[8] * q[2]]
  }

  function a(B, A, u) {
    var r = B[0].x;
    var z = B[0].y;
    var q = B[1].x;
    var y = B[1].y;
    var p = B[2].x;
    var x = B[2].y;
    var D = B[3].x;
    var v = B[3].y;
    var C = m(0, 0, r, z, A, 0, q, y, 0, u, p, x, A, u, D, v);
    for (var s = 0; s < 9; s++) {
      C[s] = C[s] / C[8]
    }
    C = [C[0], C[3], 0, C[6], C[1], C[4], 0, C[7], 0, 0, 1, 0, C[2], C[5], 0, C[8]];
    if (Mobile.os == "iOS" || Device.browser.safari) {
      for (var s = 0; s < C.length; s++) {
        C[s] = C[s].toFixed(8)
      }
    }
    C = "matrix3d(" + C.join(", ") + ")";
    return C
  }

  function l(r, s) {
    f = false;
    var p = a(g.points, g.width, g.height);
    var q = Device.transformProperty + " " + r + "ms " + TweenManager.getEase(s) + " 0ms";
    n.div.style[Device.styles.vendorTransition] = q;
    n.div.style[Device.styles.vendorTransform] = p;
    g.delayedCall(function() {
      if (d) {
        d()
      }
      d = null;
      if (n.willChange) {
        n.willChange(false);
        n.div.style[Device.styles.vendorTransition] = ""
      }
    }, r)
  }(function() {})();
  this.render = function() {
    var p = Render.TIME;
    if (p - o < 5 || !g.points || !n.div) {
      return false
    }
    o = p;
    if (!g.width) {
      g.width = n.width;
      g.height = n.height;
      n.transformPoint(0, 0);
      if (!g.width) {
        throw "Warp requires width and height"
      }
      h()
    }
    n.div.style[CSS.prefix("Transform")] = a(g.points, g.width, g.height)
  };
  this.tween = function(q, t, v, w, s, r) {
    if (!this.points) {
      return
    }
    if (typeof s !== "number") {
      r = s;
      s = 0
    }
    var u;
    switch (q) {
      case "tl":
        u = this.points[0];
        break;
      case "tr":
        u = this.points[1];
        break;
      case "bl":
        u = this.points[2];
        break;
      case "br":
        u = this.points[3];
        break;
      default:
        throw q + "not found on WarpView. Only tl, tr, bl, br accepted.";
        break
    }
    if (this.useCSS) {
      if (!f) {
        n.willChange(Device.transformProperty);
        f = true;
        g.render();
        Render.setupTween(function() {
          l(v, w)
        })
      }
      if (r) {
        d = r
      }
      u.copyFrom(t)
    } else {
      return TweenManager.tween(u, t, v, w, s, r, this.render)
    }
  };
  this.destroy = function() {
    this.points.forEach(function(q) {
      TweenManager.clearTween(q)
    });
    return this._destroy()
  }
});
Class(function Canvas(d, f, l) {
  Inherit(this, Component);
  var h = this;
  var q, e, j, o, a;
  this.children = [];
  this.offset = {
    x: 0,
    y: 0
  };
  this.retina = l;
  (function() {
    if (l instanceof HydraObject) {
      m(l)
    } else {
      g()
    }
    h.width = d;
    h.height = f;
    h.context._matrix = new Matrix2();
    b(d, f, l)
  })();

  function m() {
    var r = "c" + Utils.timestamp();
    h.context = document.getCSSCanvasContext("2d", r, d, f);
    h.background = "-" + Device.styles.vendor.toLowerCase() + "-canvas(" + r + ")";
    l.css({
      backgroundImage: h.background
    });
    l = null
  }

  function g() {
    h.div = document.createElement("canvas");
    h.context = h.div.getContext("2d");
    h.object = $(h.div)
  }

  function b(r, u, s) {
    var t = s && Device.system.retina ? 2 : 1;
    if (h.div) {
      h.div.width = r * t;
      h.div.height = u * t
    }
    h.width = r;
    h.height = u;
    h.scale = t;
    if (h.object) {
      h.object.size(h.width, h.height)
    }
    if (Device.system.retina && s) {
      h.context.scale(t, t);
      h.div.style.width = r + "px";
      h.div.style.height = u + "px"
    }
  }

  function p(t) {
    t = Utils.touchEvent(t);
    t.x -= h.offset.x;
    t.y -= h.offset.y;
    t.width = 1;
    t.height = 1;
    for (var r = h.children.length - 1; r > -1; r--) {
      var s = h.children[r].hit(t);
      if (s) {
        return s
      }
    }
    return false
  }

  function c(s) {
    var r = p(s);
    if (!r) {
      return h.interacting = false
    }
    h.interacting = true;
    j = r;
    if (Device.mobile) {
      r.events.fire(HydraEvents.HOVER, {
        action: "over"
      }, true);
      r.__time = Date.now()
    }
  }

  function k(s) {
    var r = p(s);
    if (r) {
      h.interacting = true
    } else {
      h.interacting = false
    }
    if (!Device.mobile) {
      if (r && e) {
        if (r != e) {
          e.events.fire(HydraEvents.HOVER, {
            action: "out"
          }, true);
          r.events.fire(HydraEvents.HOVER, {
            action: "over"
          }, true);
          e = r
        }
      } else {
        if (r && !e) {
          e = r;
          r.events.fire(HydraEvents.HOVER, {
            action: "over"
          }, true)
        } else {
          if (!r && e) {
            if (e) {
              e.events.fire(HydraEvents.HOVER, {
                action: "out"
              }, true)
            }
            e = null
          }
        }
      }
    }
  }

  function n(s) {
    var r = p(s);
    if (r) {
      h.interacting = true
    } else {
      h.interacting = false
    }
    if (!j && !r) {
      return
    }
    if (!Device.mobile) {
      if (r && r == j) {
        r.events.fire(HydraEvents.CLICK, {
          action: "click"
        }, true)
      }
    } else {
      if (j) {
        j.events.fire(HydraEvents.HOVER, {
          action: "out"
        }, true)
      }
      if (r == j) {
        if (Date.now() - j.__time < 750) {
          r.events.fire(HydraEvents.CLICK, {
            action: "click"
          }, true)
        }
      }
    }
    j = null
  }
  this.set("interactive", function(r) {
    if (!q && r) {
      Stage.bind("touchstart", c);
      Stage.bind("touchmove", k);
      Stage.bind("touchend", n)
    } else {
      if (q && !r) {
        Stage.unbind("touchstart", c);
        Stage.unbind("touchmove", k);
        Stage.unbind("touchend", n)
      }
    }
    q = r
  });
  this.get("interactive", function() {
    return q
  });
  this.toDataURL = function(r, s) {
    return h.div.toDataURL(r, s)
  };
  this.sort = function() {
    _objects.sort(function(s, r) {
      return s.z - r.z
    })
  };
  this.render = function(t) {
    if (!(typeof t === "boolean" && t)) {
      h.clear()
    }
    var r = h.children.length;
    for (var s = 0; s < r; s++) {
      h.children[s].render()
    }
  };
  this.clear = function() {
    h.context.clearRect(0, 0, h.div.width, h.div.height)
  };
  this.add = function(r) {
    r.setCanvas(this);
    r._parent = this;
    this.children.push(r);
    r._z = this.children.length
  };
  this.remove = function(s) {
    s._canvas = null;
    s._parent = null;
    var r = this.children.indexOf(s);
    if (r > -1) {
      this.children.splice(r, 1)
    }
  };
  this.destroy = function() {
    if (q) {
      Stage.unbind("touchstart", c);
      Stage.unbind("touchmove", k);
      Stage.unbind("touchend", n)
    }
    this.stopRender();
    for (var r = 0; r < this.children.length; r++) {
      if (this.children[r].destroy) {
        this.children[r].destroy()
      }
    }
    return this._destroy()
  };
  this.startRender = function() {
    Render.startRender(h.render)
  };
  this.stopRender = function() {
    Render.stopRender(h.render)
  };
  this.getImageData = function(r, u, s, t) {
    this.imageData = this.context.getImageData(r || 0, u || 0, s || this.width, t || this.height);
    return this.imageData
  };
  this.getPixel = function(r, v, t) {
    if (!this.imageData || t) {
      h.getImageData(0, 0, h.width, h.height)
    }
    if (!a) {
      a = {}
    }
    var s = (r + v * h.width) * 4;
    var u = this.imageData.data;
    a.r = u[s];
    a.g = u[s + 1];
    a.b = u[s + 2];
    a.a = u[s + 3];
    return a
  };
  this.texture = function(s) {
    var r = new Image();
    r.src = s;
    return r
  };
  this.localizeMouse = function() {
    o = o || {};
    o.x = Mouse.x - h.offset.x;
    o.y = Mouse.y - h.offset.y;
    return o
  };
  this.size = b
});
Class(function CanvasTexture(b, d, f, a) {
  Inherit(this, CanvasObject);
  var h = this;
  var g;
  this.width = d || 0;
  this.height = f || 0;
  (function() {
    e()
  })();

  function e() {
    if (typeof b === "string") {
      b = CanvasTexture.createImage(b, a);
      if (b.width > 0) {
        c()
      } else {
        b.onload = c
      }
    } else {
      c()
    }
    h.texture = b
  }

  function c() {
    if (h.onload) {
      h.onload()
    }
    if (!h.width && !h.height) {
      h.width = b.width / (h._canvas && h._canvas.retina ? 2 : 1);
      h.height = b.height / (h._canvas && h._canvas.retina ? 2 : 1)
    }
  }
  this.set("texture", function(j) {
    b = j
  });
  this.draw = function(k) {
    var j = this._canvas.context;
    if (this.isMask() && !k) {
      return false
    }
    if (b) {
      this.startDraw(this.anchor.tx, this.anchor.ty, k);
      j.drawImage(b, -this.anchor.tx, -this.anchor.ty, this.width, this.height);
      this.endDraw()
    }
    if (g) {
      j.globalCompositeOperation = "source-in";
      g.render(true);
      j.globalCompositeOperation = "source-over"
    }
  };
  this.mask = function(j) {
    if (!j) {
      return g = null
    }
    if (!this._parent) {
      throw "CanvasTexture :: Must add to parent before masking."
    }
    var m = this._parent.children;
    var l = false;
    for (var k = 0; k < m.length; k++) {
      if (j == m[k]) {
        l = true
      }
    }
    if (l) {
      g = j;
      j.masked = this
    } else {
      throw "CanvasGraphics :: Can only mask a sibling"
    }
  }
}, function() {
  var a = {};
  CanvasTexture.createImage = function(d, c) {
    if (!a[d] || c) {
      var b = new Image();
      b.crossOrigin = "";
      b.src = d;
      if (c) {
        return b
      }
      a[d] = b
    }
    return a[d]
  }
});
Class(function CanvasGraphics(h, c) {
  Inherit(this, CanvasObject);
  var e = this;
  var k = {};
  var d = [];
  var a, f;
  this.width = h || 0;
  this.height = c || 0;
  (function() {
    j()
  })();

  function b(m) {
    for (var l in k) {
      var n = k[l];
      if (n instanceof Color) {
        m[l] = n.getHexString()
      } else {
        m[l] = n
      }
    }
  }

  function j() {
    a = new ObjectPool(Array, 25)
  }

  function g() {
    var m = a.get() || [];
    for (var l = 0; l < arguments.length; l++) {
      m[l] = arguments[l]
    }
    d.push(m)
  }
  this.set("strokeStyle", function(l) {
    k.strokeStyle = l
  });
  this.get("strokeStyle", function() {
    return k.strokeStyle
  });
  this.set("fillStyle", function(l) {
    k.fillStyle = l
  });
  this.get("fillStyle", function() {
    return k.fillStyle
  });
  this.set("lineWidth", function(l) {
    k.lineWidth = l
  });
  this.get("lineWidth", function() {
    return k.lineWidth
  });
  this.set("lineWidth", function(l) {
    k.lineWidth = l
  });
  this.get("lineWidth", function() {
    return k.lineWidth
  });
  this.set("lineCap", function(l) {
    k.lineCap = l
  });
  this.get("lineCap", function() {
    return k.lineCap
  });
  this.set("lineDashOffset", function(l) {
    k.lineDashOffset = l
  });
  this.get("lineDashOffset", function() {
    return k.lineDashOffset
  });
  this.set("lineJoin", function(l) {
    k.lineJoin = l
  });
  this.get("lineJoin", function() {
    return k.lineJoin
  });
  this.set("lineJoin", function(l) {
    k.lineJoin = l
  });
  this.get("lineJoin", function() {
    return k.lineJoin
  });
  this.set("lineJoin", function(l) {
    k.lineJoin = l
  });
  this.get("lineJoin", function() {
    return k.lineJoin
  });
  this.set("miterLimit", function(l) {
    k.miterLimit = l
  });
  this.get("miterLimit", function() {
    return k.miterLimit
  });
  this.set("font", function(l) {
    k.font = l
  });
  this.get("font", function(l) {
    return k.font
  });
  this.set("textAlign", function(l) {
    k.textAlign = l
  });
  this.get("textAlign", function(l) {
    return k.textAlign
  });
  this.set("textBaseline", function(l) {
    k.textBaseline = l
  });
  this.get("textBaseline", function(l) {
    return k.textBaseline
  });
  this.draw = function(n) {
    if (this.isMask() && !n) {
      return false
    }
    var m = this._canvas.context;
    this.startDraw(-this.anchor.tx, -this.anchor.ty);
    b(m);
    for (var l = 0; l < d.length; l++) {
      var p = d[l];
      if (!p) {
        continue
      }
      var o = p.shift();
      m[o].apply(m, p);
      p.unshift(o)
    }
    this.endDraw();
    if (f) {
      m.save();
      m.clip();
      f.render(true);
      m.restore()
    }
  };
  this.clear = function() {
    for (var l = 0; l < d.length; l++) {
      d[l].length = 0;
      a.put(d[l])
    }
    d.length = 0
  };
  this.arc = function(n, q, o, m, p, l) {
    if (n && !q) {
      o = n;
      n = 0;
      q = 0
    }
    n = n || 0;
    q = q || 0;
    o = o || 0;
    o -= 90;
    l = l || false;
    p = p || 0;
    p -= 90;
    m = m ? m : this.radius || this.width / 2;
    g("beginPath");
    g("arc", n, q, m, Utils.toRadians(p), Utils.toRadians(o), l)
  };
  this.quadraticCurveTo = function(n, m, l, o) {
    g("quadraticCurveTo", n, m, l, o)
  };
  this.bezierCurveTo = function(n, m, p, o, l, q) {
    g("bezierCurveTo", n, m, p, o, l, q)
  };
  this.fillRect = function(l, o, m, n) {
    g("fillRect", l, o, m, n)
  };
  this.clearRect = function(l, o, m, n) {
    g("clearRect", l, o, m, n)
  };
  this.strokeRect = function(l, o, m, n) {
    g("strokeRect", l, o, m, n)
  };
  this.moveTo = function(l, m) {
    g("moveTo", l, m)
  };
  this.lineTo = function(l, m) {
    g("lineTo", l, m)
  };
  this.stroke = function() {
    g("stroke")
  };
  this.fill = function() {
    if (!f) {
      g("fill")
    }
  };
  this.beginPath = function() {
    g("beginPath")
  };
  this.closePath = function() {
    g("closePath")
  };
  this.fillText = function(n, l, o, m) {
    g("fillText", n, l, o, m)
  };
  this.strokeText = function(n, l, o, m) {
    g("strokeText", n, l, o, m)
  };
  this.setLineDash = function(l) {
    g("setLineDash", l)
  };
  this.mask = function(l) {
    if (!l) {
      return f = null
    }
    if (!this._parent) {
      throw "CanvasTexture :: Must add to parent before masking."
    }
    var o = this._parent.children;
    var n = false;
    for (var m = 0; m < o.length; m++) {
      if (l == o[m]) {
        n = true
      }
    }
    if (n) {
      f = l;
      l.masked = this;
      for (m = 0; m < d.length; m++) {
        if (d[m][0] == "fill" || d[m][0] == "stroke") {
          d[m].length = 0;
          a.put(d[m]);
          d.splice(m, 1)
        }
      }
    } else {
      throw "CanvasGraphics :: Can only mask a sibling"
    }
  }
});
Class(function CanvasObject() {
  Inherit(this, Component);
  var a = this;
  this.alpha = 1;
  this.x = 0;
  this.y = 0;
  this.width = 0;
  this.height = 0;
  this.rotation = 0;
  this.scale = 1;
  this.visible = true;
  this.anchor = {
    x: 0.5,
    y: 0.5
  };
  this.values = new CanvasValues();
  this.styles = new CanvasValues(true);
  this.children = [];
  this.blendMode = "normal";
  this.updateValues = function() {
    this.anchor.tx = this.anchor.x <= 1 && !this.anchor.full ? this.anchor.x * this.width : this.anchor.x;
    this.anchor.ty = this.anchor.y <= 1 && !this.anchor.full ? this.anchor.y * this.height : this.anchor.y;
    this.values.setTRSA(this.x, this.y, Utils.toRadians(this.rotation), this.scaleX || this.scale, this.scaleY || this.scale, this.alpha);
    if (this._parent.values) {
      this.values.calculate(this._parent.values)
    }
    if (this._parent.styles) {
      this.styles.calculateStyle(this._parent.styles)
    }
  };
  this.render = function(d) {
    if (!this.visible) {
      return false
    }
    this.updateValues();
    if (this.draw) {
      this.draw(d)
    }
    var b = this.children.length;
    for (var c = 0; c < b; c++) {
      this.children[c].render(d)
    }
  };
  this.startDraw = function(e, d, b) {
    var c = this._canvas.context;
    var n = this.values.data;
    var j = n[0] + (e || 0);
    var h = n[1] + (d || 0);
    if (this.styles.styled) {
      c.save()
    }
    c._matrix.setTRS(j, h, n[2], n[3], n[4]);
    if (!b) {
      c.globalCompositeOperation = this.blendMode || "normal"
    }
    var g = c._matrix.data;
    c.transform(g[0], g[3], g[1], g[4], g[2], g[5]);
    c.globalAlpha = n[5];
    if (this.styles.styled) {
      var l = this.styles.values;
      for (var k in l) {
        var f = l[k];
        if (f instanceof Color) {
          c[k] = f.getHexString()
        } else {
          c[k] = f
        }
      }
    }
  };
  this.endDraw = function() {
    var c = this._canvas.context;
    c._matrix.inverse();
    var b = c._matrix.data;
    if (this.styles.styled) {
      c.restore()
    } else {
      c.transform(b[0], b[3], b[1], b[4], b[2], b[5])
    }
  };
  this.add = function(b) {
    b._canvas = this._canvas;
    b._parent = this;
    this.children.push(b);
    b._z = this.children.length
  };
  this.setCanvas = function(b) {
    this._canvas = b;
    for (var c = this.children.length - 1; c > -1; c--) {
      var d = this.children[c];
      d.setCanvas(b)
    }
  };
  this.remove = function(c) {
    c._canvas = null;
    c._parent = null;
    var b = this.children.indexOf(c);
    if (b > -1) {
      this.children.splice(b, 1)
    }
  };
  this.isMask = function() {
    var b = this;
    while (b) {
      if (b.masked) {
        return true
      }
      b = b._parent
    }
    return false
  };
  this.unmask = function() {
    this.masked.mask(null);
    this.masked = null
  };
  this.setZ = function(b) {
    if (!this._parent) {
      throw "CanvasObject :: Must add to parent before setZ"
    }
    this._z = b;
    this._parent.children.sort(function(d, c) {
      return d._z - c._z
    })
  };
  this.hit = function(d) {
    if (!this.ignoreHit) {
      var c = Utils.hitTestObject(d, this.values.hit(this));
      if (c) {
        return this
      }
    }
    for (var b = this.children.length - 1; b > -1; b--) {
      var f = this.children[b];
      c = f.hit(d);
      if (c) {
        return f
      }
    }
    return false
  };
  this.destroy = function() {
    for (var b = 0; b < this.children.length; b++) {
      if (this.children[b].destroy) {
        this.children[b].destroy()
      }
    }
    return Utils.nullObject(this)
  }
});
Class(function CanvasValues(a) {
  Inherit(this, Component);
  var d = this;
  var c = {};
  var b = {
    x: 0,
    y: 0,
    width: 0,
    height: 0
  };
  if (!a) {
    this.data = new Float32Array(6)
  } else {
    this.styled = false
  }
  this.set("shadowOffsetX", function(e) {
    d.styled = true;
    c.shadowOffsetX = e
  });
  this.get("shadowOffsetX", function() {
    return c.shadowOffsetX
  });
  this.set("shadowOffsetY", function(e) {
    d.styled = true;
    c.shadowOffsetY = e
  });
  this.get("shadowOffsetY", function() {
    return c.shadowOffsetY
  });
  this.set("shadowBlur", function(e) {
    d.styled = true;
    c.shadowBlur = e
  });
  this.get("shadowBlur", function() {
    return c.shadowBlur
  });
  this.set("shadowColor", function(e) {
    d.styled = true;
    c.shadowColor = e
  });
  this.get("shadowColor", function() {
    d.styled = true;
    return c.shadowColor
  });
  this.get("values", function() {
    return c
  });
  this.setTRSA = function(f, l, h, k, j, g) {
    var e = this.data;
    e[0] = f;
    e[1] = l;
    e[2] = h;
    e[3] = k;
    e[4] = j;
    e[5] = g
  };
  this.calculate = function(g) {
    var f = g.data;
    var e = this.data;
    e[0] = e[0] + f[0];
    e[1] = e[1] + f[1];
    e[2] = e[2] + f[2];
    e[3] = e[3] * f[3];
    e[4] = e[4] * f[4];
    e[5] = e[5] * f[5]
  };
  this.calculateStyle = function(g) {
    if (!g.styled) {
      return false
    }
    this.styled = true;
    var e = g.values;
    for (var f in e) {
      if (!c[f]) {
        c[f] = e[f]
      }
    }
  };
  this.hit = function(e) {
    b.x = this.data[0];
    b.y = this.data[1];
    b.width = e.width;
    b.height = e.height;
    return b
  }
});
Class(function GLStage(e, f, m, o) {
  Inherit(this, Component);
  var h = this;
  var k, c, b;
  var g, q, l;
  this.children = [];
  this.retina = m;
  (function() {
    p();
    d(e, f, m);
    n();
    j();
    HydraEvents.createLocalEmitter(h)
  })();

  function p() {
    k = document.createElement("canvas");
    h.div = k;
    h.object = $(k)
  }

  function n() {
    var t = ["webgl", "experimental-webgl", "webkit-3d", "moz-webgl"];
    for (var r = 0; r < t.length; r++) {
      try {
        c = k.getContext(t[r], o)
      } catch (s) {}
      if (c) {
        break
      }
    }
    for (r = 0; r < h.children.length; r++) {
      h.children[r].gl(c, h)
    }
    if (b) {
      b.gl(c, h)
    }
    c.blendFunc(c.SRC_ALPHA, c.ONE_MINUS_SRC_ALPHA);
    c.enable(c.BLEND);
    c.disable(c.DEPTH_TEST);
    if (h.fire && h.context != c) {
      h.fire("context", {
        gl: c
      })
    }
    h.context = c;
    if (l) {
      l.forEach(function(u) {
        c.getExtension(u)
      })
    }
  }

  function d(r, u, s) {
    var t = s && Device.system.retina ? 2 : 1;
    if (h.div) {
      h.div.width = r * t;
      h.div.height = u * t
    }
    h.width = r;
    h.height = u;
    h.scale = t;
    if (h.object) {
      h.object.size(h.width, h.height, true)
    }
    if (c) {
      c.viewport(0, 0, r * t, u * t)
    }
    if (b) {
      b.resize(h.width, h.height)
    }
  }

  function j() {
    k.addEventListener("webglcontextlost", a);
    k.addEventListener("webglcontextrestored", n)
  }

  function a() {
    c = null
  }
  this.size = function(t, r, s) {
    d(t, r, s);
    h.fire("resize")
  };
  this.startRender = function() {
    Render.startRender(h.render)
  };
  this.stopRender = function() {
    Render.stopRender(h.render)
  };
  this.add = function(r) {
    r.gl(c, this);
    r._parent = this;
    this.children.push(r);
    r._z = this.children.length
  };
  this.remove = function(s) {
    var r = this.children.indexOf(s);
    if (r > -1) {
      s._parent = null;
      this.children.splice(r, 1)
    }
  };
  this.render = function(r) {
    if (!c) {
      return
    }
    r = r && typeof r !== "number" ? r : null;
    if (r) {
      r._startDraw(c, h)
    }
    c.clear(c.COLOR_BUFFER_BIT | c.DEPTH_BIT);
    this.renderChildren();
    if (r) {
      r._endDraw()
    }
  };
  this.renderChildren = function() {
    for (var r = 0; r < h.children.length; r++) {
      var s = h.children[r];
      if (s.render) {
        s.render()
      }
    }
  };
  this.setClearColor = function(r, s) {
    c.clearColor(r.r, r.g, r.b, s)
  };
  this.enableExtension = function(r) {
    if (!l) {
      l = []
    }
    l.push(r);
    c.getExtension(r)
  };
  this._draw = function(s) {
    if (c.program != s.shader.program) {
      c.program = s.shader.program;
      c.useProgram(c.program)
    }
    if (s.blending == GLObject.ADDITIVE_BLENDING) {
      c.blendFunc(c.SRC_ALPHA, c.ONE)
    } else {
      c.blendFunc(c.SRC_ALPHA, c.ONE_MINUS_SRC_ALPHA)
    }
    c.uniform2f(c.getUniformLocation(c.program, "resolution"), h.width, h.height);
    s.geometry.setupBuffers(c);
    s.setupMatrices();
    s.shader.update();
    s.draw();
    var r;
    switch (s.getDrawMode()) {
      case "points":
        r = c.POINTS;
        break;
      case "wireframe":
        r = c.LINE_STRIP;
        break;
      default:
        r = c.TRIANGLES;
        break
    }
    c.drawArrays(r, 0, s.geometry._vertexCount)
  };
  this.destroy = function() {
    if (!this._destroy) {
      return
    }
    h.object.remove();
    this.stopRender();
    return this._destroy()
  }
});
Class(function TweenManager() {
  Namespace(this);
  var f = this;
  var a = [];
  (function() {
    if (window.Hydra) {
      Hydra.ready(b)
    }
    if (window.Render) {
      Render.startRender(c)
    }
  })();

  function b() {
    f._dynamicPool = new ObjectPool(DynamicObject, 100);
    f._arrayPool = new ObjectPool(Array, 100);
    f._dynamicPool.debug = true
  }

  function c(h) {
    for (var g = 0; g < a.length; g++) {
      a[g].update(h)
    }
  }

  function e(j) {
    var g = j.split("(")[1].slice(0, -1).split(",");
    for (var h = 0; h < g.length; h++) {
      g[h] = parseFloat(g[h])
    }
    return g
  }

  function d(g) {
    var j = f.CSSEases;
    for (var h = j.length - 1; h > -1; h--) {
      if (j[h].name == g) {
        return j[h]
      }
    }
    return false
  }
  this._addMathTween = function(g) {
    a.push(g)
  };
  this._removeMathTween = function(g) {
    a.findAndRemove(g)
  };
  this._detectTween = function(h, j, k, l, g, m) {
    if (l === "spring") {
      return new SpringTween(h, j, k, l, g, m)
    }
    if (!f.useCSSTrans(j, l, h)) {
      return new FrameTween(h, j, k, l, g, m)
    } else {
      if (Device.tween.webAnimation) {
        return new CSSWebAnimation(h, j, k, l, g, m)
      } else {
        return new CSSTransition(h, j, k, l, g, m)
      }
    }
  };
  this.tween = function(m, k, l, n, h, g, o, j) {
    if (typeof h !== "number") {
      o = g;
      g = h;
      h = 0
    }
    if (n === "spring") {
      return new SpringTween(m, k, l, n, h, o, g)
    } else {
      return new MathTween(m, k, l, n, h, o, g, j)
    }
  };
  this.iterate = function(p, q, h, j, k, n, r) {
    if (typeof n !== "number") {
      r = n;
      n = 0
    }
    q = new DynamicObject(q);
    if (!p.length) {
      throw "TweenManager.iterate :: array is empty"
    }
    var o = p.length;
    for (var m = 0; m < o; m++) {
      var l = p[m];
      var g = m == o - 1 ? r : null;
      l.tween(q.copy(), h, j, n + (k * m), g)
    }
  };
  this.clearTween = function(k) {
    if (k._mathTween && k._mathTween.stop) {
      k._mathTween.stop()
    }
    if (k._mathTweens) {
      var j = k._mathTweens;
      for (var h = 0; h < j.length; h++) {
        var g = j[h];
        if (g && g.stop) {
          g.stop()
        }
      }
      k._mathTweens = null
    }
  };
  this.clearCSSTween = function(g) {
    if (g && !g._cssTween && g.div._transition) {
      g.div.style[Device.styles.vendorTransition] = "";
      g.div._transition = false;
      g._cssTween = null
    }
  };
  this.checkTransform = function(h) {
    var g = f.Transforms.indexOf(h);
    return g > -1
  };
  this.addCustomEase = function(j) {
    var h = true;
    if (typeof j !== "object" || !j.name || !j.curve) {
      throw "TweenManager :: addCustomEase requires {name, curve}"
    }
    for (var g = f.CSSEases.length - 1; g > -1; g--) {
      if (j.name == f.CSSEases[g].name) {
        h = false
      }
    }
    if (h) {
      if (j.curve.charAt(0).toLowerCase() == "m") {
        j.path = new EasingPath(j.curve)
      } else {
        j.values = e(j.curve)
      }
      f.CSSEases.push(j)
    }
    return j
  };
  this.getEase = function(h, g) {
    if (Array.isArray(h)) {
      var k = d(h[0]);
      var j = d(h[1]);
      if (!k || !j) {
        throw "Multi-ease tween missing values " + JSON.stringify(h)
      }
      if (!k.values) {
        k.values = e(k.curve)
      }
      if (!j.values) {
        j.values = e(j.curve)
      }
      if (g) {
        return [k.values[0], k.values[1], j.values[2], j.values[3]]
      }
      return "cubic-bezier(" + k.values[0] + "," + k.values[1] + "," + j.values[2] + "," + j.values[3] + ")"
    } else {
      var l = d(h);
      if (!l) {
        return false
      }
      if (g) {
        return l.path ? l.path.solve : l.values
      } else {
        return l.curve
      }
    }
  };
  this.inspectEase = function(g) {
    return d(g)
  };
  this.getAllTransforms = function(g) {
    var k = {};
    for (var h = f.Transforms.length - 1; h > -1; h--) {
      var j = f.Transforms[h];
      var l = g[j];
      if (l !== 0 && typeof l === "number") {
        k[j] = l
      }
    }
    return k
  };
  this.parseTransform = function(j) {
    var h = "";
    var l = "";
    if (j.perspective > 0) {
      h += "perspective(" + j.perspective + "px)"
    }
    if (typeof j.x !== "undefined" || typeof j.y !== "undefined" || typeof j.z !== "undefined") {
      var g = (j.x || 0);
      var m = (j.y || 0);
      var k = (j.z || 0);
      l += g + "px, ";
      l += m + "px";
      if (Device.tween.css3d) {
        l += ", " + k + "px";
        h += "translate3d(" + l + ")"
      } else {
        h += "translate(" + l + ")"
      }
    }
    if (typeof j.scale !== "undefined") {
      h += "scale(" + j.scale + ")"
    } else {
      if (typeof j.scaleX !== "undefined") {
        h += "scaleX(" + j.scaleX + ")"
      }
      if (typeof j.scaleY !== "undefined") {
        h += "scaleY(" + j.scaleY + ")"
      }
    }
    if (typeof j.rotation !== "undefined") {
      h += "rotate(" + j.rotation + "deg)"
    }
    if (typeof j.rotationX !== "undefined") {
      h += "rotateX(" + j.rotationX + "deg)"
    }
    if (typeof j.rotationY !== "undefined") {
      h += "rotateY(" + j.rotationY + "deg)"
    }
    if (typeof j.rotationZ !== "undefined") {
      h += "rotateZ(" + j.rotationZ + "deg)"
    }
    if (typeof j.skewX !== "undefined") {
      h += "skewX(" + j.skewX + "deg)"
    }
    if (typeof j.skewY !== "undefined") {
      h += "skewY(" + j.skewY + "deg)"
    }
    return h
  };
  this.interpolate = function(g, j, k) {
    var h = f.Interpolation.convertEase(k);
    return g * (typeof h == "function" ? h(j) : f.Interpolation.solve(h, j))
  };
  this.interpolateValues = function(l, g, j, k) {
    var h = f.Interpolation.convertEase(k);
    return l + (g - l) * (typeof h == "function" ? h(j) : f.Interpolation.solve(h, j))
  }
}, "Static");
(function() {
  TweenManager.Transforms = ["scale", "scaleX", "scaleY", "x", "y", "z", "rotation", "rotationX", "rotationY", "rotationZ", "skewX", "skewY", "perspective", ];
  TweenManager.CSSEases = [{
    name: "easeOutCubic",
    curve: "cubic-bezier(0.215, 0.610, 0.355, 1.000)"
  }, {
    name: "easeOutQuad",
    curve: "cubic-bezier(0.250, 0.460, 0.450, 0.940)"
  }, {
    name: "easeOutQuart",
    curve: "cubic-bezier(0.165, 0.840, 0.440, 1.000)"
  }, {
    name: "easeOutQuint",
    curve: "cubic-bezier(0.230, 1.000, 0.320, 1.000)"
  }, {
    name: "easeOutSine",
    curve: "cubic-bezier(0.390, 0.575, 0.565, 1.000)"
  }, {
    name: "easeOutExpo",
    curve: "cubic-bezier(0.190, 1.000, 0.220, 1.000)"
  }, {
    name: "easeOutCirc",
    curve: "cubic-bezier(0.075, 0.820, 0.165, 1.000)"
  }, {
    name: "easeOutBack",
    curve: "cubic-bezier(0.175, 0.885, 0.320, 1.275)"
  }, {
    name: "easeInCubic",
    curve: "cubic-bezier(0.550, 0.055, 0.675, 0.190)"
  }, {
    name: "easeInQuad",
    curve: "cubic-bezier(0.550, 0.085, 0.680, 0.530)"
  }, {
    name: "easeInQuart",
    curve: "cubic-bezier(0.895, 0.030, 0.685, 0.220)"
  }, {
    name: "easeInQuint",
    curve: "cubic-bezier(0.755, 0.050, 0.855, 0.060)"
  }, {
    name: "easeInSine",
    curve: "cubic-bezier(0.470, 0.000, 0.745, 0.715)"
  }, {
    name: "easeInCirc",
    curve: "cubic-bezier(0.600, 0.040, 0.980, 0.335)"
  }, {
    name: "easeInBack",
    curve: "cubic-bezier(0.600, -0.280, 0.735, 0.045)"
  }, {
    name: "easeInOutCubic",
    curve: "cubic-bezier(0.645, 0.045, 0.355, 1.000)"
  }, {
    name: "easeInOutQuad",
    curve: "cubic-bezier(0.455, 0.030, 0.515, 0.955)"
  }, {
    name: "easeInOutQuart",
    curve: "cubic-bezier(0.770, 0.000, 0.175, 1.000)"
  }, {
    name: "easeInOutQuint",
    curve: "cubic-bezier(0.860, 0.000, 0.070, 1.000)"
  }, {
    name: "easeInOutSine",
    curve: "cubic-bezier(0.445, 0.050, 0.550, 0.950)"
  }, {
    name: "easeInOutExpo",
    curve: "cubic-bezier(1.000, 0.000, 0.000, 1.000)"
  }, {
    name: "easeInOutCirc",
    curve: "cubic-bezier(0.785, 0.135, 0.150, 0.860)"
  }, {
    name: "easeInOutBack",
    curve: "cubic-bezier(0.680, -0.550, 0.265, 1.550)"
  }, {
    name: "easeInOut",
    curve: "cubic-bezier(.42,0,.58,1)"
  }, {
    name: "linear",
    curve: "linear"
  }];
  TweenManager.useCSSTrans = function(b, c, a) {
    if (b.math) {
      return false
    }
    if (typeof c === "string" && (c.strpos("Elastic") || c.strpos("Bounce"))) {
      return false
    }
    if (a.multiTween || TweenManager.inspectEase(c).path) {
      return false
    }
    if (!Device.tween.transition) {
      return false
    }
    return true
  }
})();
Class(function CSSTransition(g, p, t, f, k, j) {
  var o = this;
  var e, l, q, b;
  var h, u;
  this.playing = true;
  (function() {
    if (typeof t !== "number") {
      throw "CSSTween Requires object, props, time, ease"
    }
    d();
    if (typeof f == "object" && !Array.isArray(f)) {
      w()
    } else {
      a()
    }
  })();

  function v() {
    return !o || o.kill || !g || !g.div
  }

  function d() {
    var x = TweenManager.getAllTransforms(g);
    var z = [];
    for (var y in p) {
      if (TweenManager.checkTransform(y)) {
        x.use = true;
        x[y] = p[y];
        delete p[y]
      } else {
        if (typeof p[y] === "number" || y.strpos("-")) {
          z.push(y)
        }
      }
    }
    if (x.use) {
      z.push(Device.transformProperty)
    }
    delete x.use;
    e = x;
    l = z
  }

  function w() {
    r();
    var y = 0;
    var D = function(P, J, N, O, M, K) {
      var L = M[H];
      if (L) {
        P += L
      }
      return TweenManager.interpolateValues(P, J, N, O)
    };
    q = [];
    b = 0;
    for (var z in f) {
      var G = z.strpos("%") ? Number(z.replace("%", "")) / 100 : ((Number(z) + 1) / f.length);
      if (isNaN(G)) {
        continue
      }
      var C = f[z];
      b++;
      var A = {};
      var F = {};
      var I = q[q.length - 1];
      var x = I ? I.props : {};
      var E = !I;
      for (var H in e) {
        if (!h[H]) {
          h[H] = H.strpos("scale") ? 1 : 0
        }
        A[H] = D(h[H], e[H], G, C, x, H);
        if (E) {
          x[H] = h[H]
        }
      }
      for (H in p) {
        F[H] = D(u[H], p[H], G, C, x, H);
        if (E) {
          x[H] = u[H]
        }
      }
      var B = (G * t) - y;
      y += B;
      q.push({
        percent: G,
        ease: C,
        transform: A,
        props: F,
        delay: b == 1 ? k : 0,
        time: B
      })
    }
    a(q.shift())
  }

  function r() {
    h = TweenManager.getAllTransforms(g);
    var x = TweenManager.parseTransform(h);
    if (!x.length) {
      for (var z = TweenManager.Transforms.length - 1; z > -1; z--) {
        var y = TweenManager.Transforms[z];
        h[y] = y == "scale" ? 1 : 0
      }
    }
    u = {};
    for (y in p) {
      u[y] = g.css(y)
    }
  }

  function a(y) {
    if (v()) {
      return
    }
    if (g._cssTween) {
      g._cssTween.kill = true
    }
    g._cssTween = o;
    g.div._transition = true;
    var x = (function() {
      if (!y) {
        return s(t, f, k)
      } else {
        return s(y.time, y.ease, y.delay)
      }
    })();
    g.willChange(x.props);
    var C = y ? y.time : t;
    var z = y ? y.delay : k;
    var B = y ? y.props : p;
    var A = y ? y.transform : e;
    Timer.create(function() {
      if (v()) {
        return
      }
      g.div.style[Device.styles.vendorTransition] = x.transition;
      o.playing = true;
      if (Device.browser.safari) {
        Timer.create(function() {
          if (v()) {
            return
          }
          g.css(B);
          g.transform(A)
        }, 16)
      } else {
        g.css(B);
        g.transform(A)
      }
      Timer.create(function() {
        if (v()) {
          return
        }
        if (!q) {
          c();
          if (j) {
            j()
          }
        } else {
          m()
        }
      }, C + z)
    }, 50)
  }

  function m() {
    if (v()) {
      return
    }
    var y = q.shift();
    if (!y) {
      c();
      if (j) {
        j
      }
    } else {
      var x = s(y.time, y.ease, y.delay);
      g.div.style[Device.styles.vendorTransition] = x.transition;
      g.css(y.props);
      g.transform(y.transform);
      Timer.create(m, y.time)
    }
  }

  function s(C, E, z) {
    var B = "";
    var D = "";
    var x = l.length;
    for (var A = 0; A < x; A++) {
      var y = l[A];
      B += (B.length ? ", " : "") + y;
      D += (D.length ? ", " : "") + y + " " + C + "ms " + TweenManager.getEase(E) + " " + z + "ms"
    }
    return {
      props: B,
      transition: D
    }
  }

  function c() {
    if (v()) {
      return
    }
    o.playing = false;
    g._cssTween = null;
    g.willChange(null);
    g = p = null;
    o = null;
    Utils.nullObject(this)
  }

  function n() {
    if (!j && o.playing) {
      c()
    }
  }
  this.stop = function() {
    if (!this.playing) {
      return
    }
    this.kill = true;
    this.playing = false;
    g.div.style[Device.styles.vendorTransition] = "";
    g.div._transition = false;
    g.willChange(null);
    g._cssTween = null;
    o = null;
    Utils.nullObject(this)
  }
});
Class(function FrameTween(h, s, u, f, n, m, l) {
  var q = this;
  var v, c, j, x;
  var r, a, e;
  var d, g;
  this.playing = true;
  (function() {
    if (typeof f === "object") {
      f = "easeOutCubic"
    }
    if (h && s) {
      if (typeof u !== "number") {
        throw "FrameTween Requires object, props, time, ease"
      }
      t();
      b()
    }
  })();

  function w() {
    return q.kill || !h || !h.div
  }

  function t() {
    if (s.math) {
      delete s.math
    }
    if (Device.tween.transition && h.div._transition) {
      h.div.style[Device.styles.vendorTransition] = "";
      h.div._transition = false
    }
    v = new DynamicObject();
    c = new DynamicObject();
    j = new DynamicObject();
    x = new DynamicObject();
    if (!h.multiTween) {
      if (typeof s.x === "undefined") {
        s.x = h.x
      }
      if (typeof s.y === "undefined") {
        s.y = h.y
      }
      if (typeof s.z === "undefined") {
        s.z = h.z
      }
    }
    for (var z in s) {
      if (TweenManager.checkTransform(z)) {
        r = true;
        j[z] = h[z] || (z == "scale" ? 1 : 0);
        c[z] = s[z]
      } else {
        a = true;
        var y = s[z];
        if (typeof y === "string") {
          h.div.style[z] = y
        } else {
          if (typeof y === "number") {
            x[z] = Number(h.css(z));
            v[z] = y
          }
        }
      }
    }
  }

  function b() {
    if (h._cssTween && !l && !h.multiTween) {
      h._cssTween.kill = true
    }
    if (h.multiTween) {
      if (!h._cssTweens) {
        h._cssTweens = []
      }
      h._cssTweens.push(q)
    }
    h._cssTween = q;
    q.playing = true;
    s = x.copy();
    e = j.copy();
    if (a) {
      d = TweenManager.tween(s, v, u, f, n, p, k, l)
    }
    if (r) {
      g = TweenManager.tween(e, c, u, f, n, (!a ? p : null), (!a ? k : null), l)
    }
  }

  function o() {
    if (h._cssTweens) {
      h._cssTweens.findAndRemove(q)
    }
    q.playing = false;
    h._cssTween = null;
    h = s = null
  }

  function k() {
    if (w()) {
      return
    }
    if (a) {
      h.css(s)
    }
    if (r) {
      if (h.multiTween) {
        for (var y in e) {
          if (typeof e[y] === "number") {
            h[y] = e[y]
          }
        }
        h.transform()
      } else {
        h.transform(e)
      }
    }
  }

  function p() {
    if (q.playing) {
      o();
      if (m) {
        m()
      }
    }
  }
  this.stop = function() {
    if (!this.playing) {
      return
    }
    if (d && d.stop) {
      d.stop()
    }
    if (g && g.stop) {
      g.stop()
    }
    o()
  };
  this.interpolate = function(y) {
    if (d) {
      d.interpolate(y)
    }
    if (g) {
      g.interpolate(y)
    }
    k()
  };
  this.setEase = function(y) {
    if (d) {
      d.setEase(y)
    }
    if (g) {
      g.setEase(y)
    }
  }
});
Class(function CSSWebAnimation(j, s, z, e, m, l) {
  var r = this;
  var x, w, f, v;
  var p, h, c, k;
  (function() {
    if (j._cssTween) {
      j._cssTween.stop()
    }
    d();
    t();
    y();
    u();
    Render.setupTween(b)
  })();

  function d() {
    var D = [];
    var B = false;
    for (var C in s) {
      if (TweenManager.checkTransform(C)) {
        B = true
      } else {
        if (typeof s[C] === "number" || C.strpos("-")) {
          D.push(C)
        }
      }
    }
    if (B) {
      D.push(Device.transformProperty)
    }
    j.willChange(D);
    if (j._cssTween) {
      j._cssTween.kill = true
    }
    j._cssTween = r;
    j.div._transition = true
  }

  function t() {
    var B = TweenManager.getAllTransforms(j);
    for (var C in s) {
      if (TweenManager.checkTransform(C)) {
        B[C] = s[C];
        delete s[C]
      }
    }
    c = B;
    x = TweenManager.parseTransform(B)
  }

  function y() {
    k = TweenManager.getAllTransforms(j);
    var B = TweenManager.parseTransform(k);
    if (!B.length) {
      B = "translate3d(0, 0, 0)";
      for (var D = TweenManager.Transforms.length - 1; D > -1; D--) {
        var C = TweenManager.Transforms[D];
        k[C] = C == "scale" ? 1 : 0
      }
    }
    w = {};
    if (x) {
      w.transform = B
    }
    for (var C in s) {
      w[C] = j.css(C)
    }
  }

  function u() {
    f = {};
    if (x) {
      f.transform = x
    }
    for (var B in s) {
      f[B] = s[B]
    }
  }

  function b() {
    r.playing = true;
    v = j.div.animate([w, f], {
      duration: z,
      delay: m,
      easing: TweenManager.getEase(e),
      fill: "forwards"
    });
    v.addEventListener("finish", q)
  }

  function A() {
    return !r || r.kill || !j || !j.div
  }

  function o() {
    r.playing = false;
    j = s = null;
    r = null;
    v = null;
    Utils.nullObject(this)
  }

  function a() {
    j.css(s);
    j.transform(c)
  }

  function g(D, B, C) {
    return TweenManager.interpolate(D + (B - D), C, e)
  }

  function n() {
    if (!v) {
      return
    }
    var B = v.currentTime / z;
    var C = {};
    var E = {};
    for (var D in c) {
      C[D] = g(k[D], c[D], B)
    }
    for (D in s) {
      E[D] = TweenManager.interpolate(w[D], s[D], B)
    }
    j.css(E);
    j.transform(C)
  }

  function q() {
    if (A()) {
      return
    }
    a();
    j.willChange(null);
    if (l) {
      Render.nextFrame(l)
    }
    o()
  }
  this.stop = function() {
    if (!r || !r.playing) {
      return
    }
    n();
    r.kill = true;
    r.playing = false;
    j.willChange(null);
    v.pause();
    o()
  }
});
TweenManager.Class(function Interpolation() {
  function d(j, g, h) {
    return ((a(g, h) * j + f(g, h)) * j + e(g)) * j
  }

  function b(k, n, l) {
    var h = k;
    for (var j = 0; j < 4; j++) {
      var m = c(h, n, l);
      if (m == 0) {
        return h
      }
      var g = d(h, n, l) - k;
      h -= g / m
    }
    return h
  }

  function c(j, g, h) {
    return 3 * a(g, h) * j * j + 2 * f(g, h) * j + e(g)
  }

  function a(g, h) {
    return 1 - 3 * h + 3 * g
  }

  function f(g, h) {
    return 3 * h - 6 * g
  }

  function e(g) {
    return 3 * g
  }
  this.convertEase = function(j) {
    var g = (function() {
      switch (j) {
        case "easeInQuad":
          return TweenManager.Interpolation.Quad.In;
          break;
        case "easeInCubic":
          return TweenManager.Interpolation.Cubic.In;
          break;
        case "easeInQuart":
          return TweenManager.Interpolation.Quart.In;
          break;
        case "easeInQuint":
          return TweenManager.Interpolation.Quint.In;
          break;
        case "easeInSine":
          return TweenManager.Interpolation.Sine.In;
          break;
        case "easeInExpo":
          return TweenManager.Interpolation.Expo.In;
          break;
        case "easeInCirc":
          return TweenManager.Interpolation.Circ.In;
          break;
        case "easeInElastic":
          return TweenManager.Interpolation.Elastic.In;
          break;
        case "easeInBack":
          return TweenManager.Interpolation.Back.In;
          break;
        case "easeInBounce":
          return TweenManager.Interpolation.Bounce.In;
          break;
        case "easeOutQuad":
          return TweenManager.Interpolation.Quad.Out;
          break;
        case "easeOutCubic":
          return TweenManager.Interpolation.Cubic.Out;
          break;
        case "easeOutQuart":
          return TweenManager.Interpolation.Quart.Out;
          break;
        case "easeOutQuint":
          return TweenManager.Interpolation.Quint.Out;
          break;
        case "easeOutSine":
          return TweenManager.Interpolation.Sine.Out;
          break;
        case "easeOutExpo":
          return TweenManager.Interpolation.Expo.Out;
          break;
        case "easeOutCirc":
          return TweenManager.Interpolation.Circ.Out;
          break;
        case "easeOutElastic":
          return TweenManager.Interpolation.Elastic.Out;
          break;
        case "easeOutBack":
          return TweenManager.Interpolation.Back.Out;
          break;
        case "easeOutBounce":
          return TweenManager.Interpolation.Bounce.Out;
          break;
        case "easeInOutQuad":
          return TweenManager.Interpolation.Quad.InOut;
          break;
        case "easeInOutCubic":
          return TweenManager.Interpolation.Cubic.InOut;
          break;
        case "easeInOutQuart":
          return TweenManager.Interpolation.Quart.InOut;
          break;
        case "easeInOutQuint":
          return TweenManager.Interpolation.Quint.InOut;
          break;
        case "easeInOutSine":
          return TweenManager.Interpolation.Sine.InOut;
          break;
        case "easeInOutExpo":
          return TweenManager.Interpolation.Expo.InOut;
          break;
        case "easeInOutCirc":
          return TweenManager.Interpolation.Circ.InOut;
          break;
        case "easeInOutElastic":
          return TweenManager.Interpolation.Elastic.InOut;
          break;
        case "easeInOutBack":
          return TweenManager.Interpolation.Back.InOut;
          break;
        case "easeInOutBounce":
          return TweenManager.Interpolation.Bounce.InOut;
          break;
        case "linear":
          return TweenManager.Interpolation.Linear.None;
          break
      }
    })();
    if (!g) {
      var h = TweenManager.getEase(j, true);
      if (h) {
        g = h
      } else {
        g = TweenManager.Interpolation.Cubic.Out
      }
    }
    return g
  };
  this.solve = function(h, g) {
    if (h[0] == h[1] && h[2] == h[3]) {
      return g
    }
    return d(b(g, h[0], h[2]), h[1], h[3])
  };
  this.Linear = {
    None: function(g) {
      return g
    }
  };
  this.Quad = {
    In: function(g) {
      return g * g
    },
    Out: function(g) {
      return g * (2 - g)
    },
    InOut: function(g) {
      if ((g *= 2) < 1) {
        return 0.5 * g * g
      }
      return -0.5 * (--g * (g - 2) - 1)
    }
  };
  this.Cubic = {
    In: function(g) {
      return g * g * g
    },
    Out: function(g) {
      return --g * g * g + 1
    },
    InOut: function(g) {
      if ((g *= 2) < 1) {
        return 0.5 * g * g * g
      }
      return 0.5 * ((g -= 2) * g * g + 2)
    }
  };
  this.Quart = {
    In: function(g) {
      return g * g * g * g
    },
    Out: function(g) {
      return 1 - --g * g * g * g
    },
    InOut: function(g) {
      if ((g *= 2) < 1) {
        return 0.5 * g * g * g * g
      }
      return -0.5 * ((g -= 2) * g * g * g - 2)
    }
  };
  this.Quint = {
    In: function(g) {
      return g * g * g * g * g
    },
    Out: function(g) {
      return --g * g * g * g * g + 1
    },
    InOut: function(g) {
      if ((g *= 2) < 1) {
        return 0.5 * g * g * g * g * g
      }
      return 0.5 * ((g -= 2) * g * g * g * g + 2)
    }
  };
  this.Sine = {
    In: function(g) {
      return 1 - Math.cos(g * Math.PI / 2)
    },
    Out: function(g) {
      return Math.sin(g * Math.PI / 2)
    },
    InOut: function(g) {
      return 0.5 * (1 - Math.cos(Math.PI * g))
    }
  };
  this.Expo = {
    In: function(g) {
      return g === 0 ? 0 : Math.pow(1024, g - 1)
    },
    Out: function(g) {
      return g === 1 ? 1 : 1 - Math.pow(2, -10 * g)
    },
    InOut: function(g) {
      if (g === 0) {
        return 0
      }
      if (g === 1) {
        return 1
      }
      if ((g *= 2) < 1) {
        return 0.5 * Math.pow(1024, g - 1)
      }
      return 0.5 * (-Math.pow(2, -10 * (g - 1)) + 2)
    }
  };
  this.Circ = {
    In: function(g) {
      return 1 - Math.sqrt(1 - g * g)
    },
    Out: function(g) {
      return Math.sqrt(1 - --g * g)
    },
    InOut: function(g) {
      if ((g *= 2) < 1) {
        return -0.5 * (Math.sqrt(1 - g * g) - 1)
      }
      return 0.5 * (Math.sqrt(1 - (g -= 2) * g) + 1)
    }
  };
  this.Elastic = {
    In: function(h) {
      var j, g = 0.1,
          l = 0.4;
      if (h === 0) {
        return 0
      }
      if (h === 1) {
        return 1
      }
      if (!g || g < 1) {
        g = 1;
        j = l / 4
      } else {
        j = l * Math.asin(1 / g) / (2 * Math.PI)
      }
      return -(g * Math.pow(2, 10 * (h -= 1)) * Math.sin((h - j) * (2 * Math.PI) / l))
    },
    Out: function(h) {
      var j, g = 0.1,
          l = 0.4;
      if (h === 0) {
        return 0
      }
      if (h === 1) {
        return 1
      }
      if (!g || g < 1) {
        g = 1;
        j = l / 4
      } else {
        j = l * Math.asin(1 / g) / (2 * Math.PI)
      }
      return (g * Math.pow(2, -10 * h) * Math.sin((h - j) * (2 * Math.PI) / l) + 1)
    },
    InOut: function(h) {
      var j, g = 0.1,
          l = 0.4;
      if (h === 0) {
        return 0
      }
      if (h === 1) {
        return 1
      }
      if (!g || g < 1) {
        g = 1;
        j = l / 4
      } else {
        j = l * Math.asin(1 / g) / (2 * Math.PI)
      }
      if ((h *= 2) < 1) {
        return -0.5 * (g * Math.pow(2, 10 * (h -= 1)) * Math.sin((h - j) * (2 * Math.PI) / l))
      }
      return g * Math.pow(2, -10 * (h -= 1)) * Math.sin((h - j) * (2 * Math.PI) / l) * 0.5 + 1
    }
  };
  this.Back = {
    In: function(g) {
      var h = 1.70158;
      return g * g * ((h + 1) * g - h)
    },
    Out: function(g) {
      var h = 1.70158;
      return --g * g * ((h + 1) * g + h) + 1
    },
    InOut: function(g) {
      var h = 1.70158 * 1.525;
      if ((g *= 2) < 1) {
        return 0.5 * (g * g * ((h + 1) * g - h))
      }
      return 0.5 * ((g -= 2) * g * ((h + 1) * g + h) + 2)
    }
  };
  this.Bounce = {
    In: function(g) {
      return 1 - this.Bounce.Out(1 - g)
    },
    Out: function(g) {
      if (g < (1 / 2.75)) {
        return 7.5625 * g * g
      } else {
        if (g < (2 / 2.75)) {
          return 7.5625 * (g -= (1.5 / 2.75)) * g + 0.75
        } else {
          if (g < (2.5 / 2.75)) {
            return 7.5625 * (g -= (2.25 / 2.75)) * g + 0.9375
          } else {
            return 7.5625 * (g -= (2.625 / 2.75)) * g + 0.984375
          }
        }
      }
    },
    InOut: function(g) {
      if (g < 0.5) {
        return this.Bounce.In(g * 2) * 0.5
      }
      return this.Bounce.Out(g * 2 - 1) * 0.5 + 0.5
    }
  }
}, "Static");
Class(function EasingPath(d) {
  Inherit(this, Component);
  var x = this;
  var h, o, t, f;
  var g = 1450;
  var A = 1 / g;
  var u = 100;
  var e = 5;
  var v = 0.001;
  var F = -1;
  var c = {};
  var w = {};
  var E = [];
  var j = [];
  (function() {
    q();
    r();
    D()
  })();

  function q() {
    f = x.initClass(ObjectPool, Object, 100)
  }

  function r() {
    h = document.createElementNS("http://www.w3.org/2000/svg", "path");
    h.setAttributeNS(null, "d", C(d));
    t = h.getTotalLength()
  }

  function D() {
    var J, I, L, G, H, K;
    for (J = I = 0, K = g; 0 <= K ? I <= K : I >= K; J = 0 <= K ? ++I : --I) {
      H = J * A;
      L = t * H;
      G = h.getPointAtLength(L);
      E.push({
        point: G,
        length: L,
        progress: H
      })
    }
  }

  function C(K) {
    var H = /[M|L|H|V|C|S|Q|T|A]/gim;
    var I = K.split(H);
    I.shift();
    var G = K.match(H);
    var L = 0;
    I[L] = l(I[L], 0);
    var J = I.length - 1;
    I[J] = l(I[J], u);
    return b(G, I)
  }

  function l(K, O) {
    O = O || 0;
    K = K.trim();
    var H = /(-|\+)?((\d+(\.(\d|\e(-|\+)?)+)?)|(\.?(\d|\e|(\-|\+))+))/gim;
    var I = p(K.match(H));
    var L = I[I.length - 1];
    var M = L[0];
    var P = Number(M);
    if (P !== O) {
      K = "";
      L[0] = O;
      for (var J = 0; J < I.length; J++) {
        var N = I[J];
        var G = J === 0 ? "" : " ";
        K += "" + G + N[0] + "," + N[1]
      }
    }
    return K
  }

  function b(H, J) {
    var G = "";
    for (var I = 0; I < H.length; I++) {
      var L = H[I];
      var K = I === 0 ? "" : " ";
      G += "" + K + L + (J[I].trim())
    }
    return G
  }

  function p(K) {
    if (K.length % 2 !== 0) {
      throw "EasingPath :: Failed to parse path -- segment pairs are not even."
    }
    var G = [];
    for (var H = 0; H < K.length; H += 2) {
      var I = K[H];
      var J = [K[H], K[H + 1]];
      G.push(J)
    }
    return G
  }

  function n(R, I) {
    if (I == F) {
      return c
    }
    if (!o) {
      o = 0
    }
    var P = R.length;
    var K, S, H;
    if (F > I) {
      K = 0;
      S = "reverse"
    } else {
      K = P;
      S = "forward"
    }
    if (S == "forward") {
      H = R[0];
      end = R[R.length - 1]
    } else {
      H = R[R.length - 1];
      end = R[0]
    }
    var O, N, J, Q, M;
    for (O = N = J = o, Q = K; J <= Q ? N < Q : N > Q; O = J <= Q ? ++N : --N) {
      var T = R[O];
      var G = T.point.x / u;
      var L = I;
      if (S == "reverse") {
        M = G;
        G = L;
        L = M
      }
      if (G < L) {
        H = T;
        o = O
      } else {
        end = T;
        break
      }
    }
    F = I;
    c.start = H;
    c.end = end;
    return c
  }

  function a(I, H) {
    var G;
    var J = s(I, H.start.point);
    if (J) {
      return J
    }
    return s(I, H.end.point)
  }

  function m(M, N, K, L) {
    L = L || e;
    var J = B(N, K, M);
    var H = h.getPointAtLength(J);
    var G = H.x / u;
    if (k(M, G)) {
      return z(H)
    } else {
      if (L-- < 1) {
        return z(H)
      }
      var I = f.get();
      I.point = H;
      I.length = J;
      j.push(I);
      if (M < G) {
        return m(M, N, I, L)
      } else {
        return m(M, I, K, L)
      }
    }
  }

  function B(K, G, I) {
    var J = G.point.x - K.point.x;
    var H = (I - (K.point.x / u)) / (J / u);
    return K.length + H * (G.length - K.length)
  }

  function s(H, G) {
    if (k(H, G.x / u)) {
      return z(G)
    }
  }

  function k(H, G) {
    return Math.abs(H - G) < v
  }

  function z(G) {
    return 1 - (G.y / u)
  }

  function y() {
    for (var G = j.length - 1; G > -1; G--) {
      f.put(j[G])
    }
    j.length = 0
  }
  this.solve = function(J) {
    J = Utils.clamp(J, 0, 1);
    var I = n(E, J);
    var H = a(J, I);
    var G = H;
    if (!G) {
      G = m(J, I.start, I.end)
    }
    y();
    return G
  }
});
Class(function MathTween(h, q, u, f, l, r, k, j) {
  var p = this;
  var w, y, v, g;
  var s, d, n, t, a;
  var m = 0;
  (function() {
    if (h && q) {
      if (typeof u !== "number") {
        throw "MathTween Requires object, props, time, ease"
      }
      c();
      if (typeof f == "object" && !Array.isArray(f)) {
        x()
      }
    }
  })();

  function c() {
    if (!h.multiTween && h._mathTween && !j) {
      TweenManager.clearTween(h)
    }
    if (!j) {
      TweenManager._addMathTween(p)
    }
    h._mathTween = p;
    if (h.multiTween) {
      if (!h._mathTweens) {
        h._mathTweens = []
      }
      h._mathTweens.push(p)
    }
    if (typeof f == "string") {
      f = TweenManager.Interpolation.convertEase(f);
      s = typeof f === "function"
    } else {
      if (Array.isArray(f)) {
        s = false;
        f = TweenManager.getEase(f, true)
      }
    }
    w = Date.now();
    w += l;
    v = q;
    y = {};
    p.startValues = y;
    for (var z in v) {
      if (typeof h[z] === "number") {
        y[z] = h[z]
      }
    }
  }

  function x() {
    var A = 0;
    var E = function(Q, K, O, P, N, L) {
      var M = N[L];
      if (M) {
        Q += M
      }
      return TweenManager.interpolateValues(Q, K, O, P)
    };
    t = [];
    for (var B in f) {
      var H = B.strpos("%") ? Number(B.replace("%", "")) / 100 : ((Number(B) + 1) / f.length);
      if (isNaN(H)) {
        continue
      }
      var D = f[B];
      var J = t[t.length - 1];
      var G = {};
      var z = J ? J.end : {};
      var F = !J;
      for (var I in y) {
        G[I] = E(y[I], v[I], H, D, z, I);
        if (F) {
          z[I] = y[I]
        }
      }
      var C = (H * u) - A;
      A += C;
      t.push({
        percent: H,
        ease: D,
        start: z,
        end: G,
        time: C
      })
    }
    g = t.shift()
  }

  function o() {
    if (!h && !q) {
      return false
    }
    h._mathTween = null;
    TweenManager._removeMathTween(p);
    Utils.nullObject(p);
    if (h._mathTweens) {
      h._mathTweens.findAndRemove(p)
    }
  }

  function e(A) {
    m = (A - w) / u;
    m = m > 1 ? 1 : m;
    var D = s ? f(m) : TweenManager.Interpolation.solve(f, m);
    for (var C in y) {
      if (typeof y[C] === "number") {
        var B = y[C];
        var z = v[C];
        h[C] = B + (z - B) * D
      }
    }
    if (r) {
      r(D)
    }
    if (m == 1) {
      if (k) {
        k()
      }
      o()
    }
  }

  function b(A) {
    var z = g;
    if (!z.elapsed) {
      z.elapsed = 0;
      z.timer = 0
    }
    z.timer += Render.DELTA;
    z.elapsed = z.timer / z.time;
    if (z.elapsed < 1) {
      for (var B in z.start) {
        h[B] = TweenManager.interpolateValues(z.start[B], z.end[B], z.elapsed, z.ease)
      }
      if (r) {
        r(z.elapsed)
      }
    } else {
      g = t.shift();
      if (!g) {
        if (k) {
          k()
        }
        o()
      }
    }
  }
  this.update = function(z) {
    if (d || z < w) {
      return
    }
    if (t) {
      b(z)
    } else {
      e(z)
    }
  };
  this.pause = function() {
    d = true
  };
  this.resume = function() {
    d = false;
    w = Date.now() - (m * u)
  };
  this.stop = function() {
    p.stopped = true;
    o();
    return null
  };
  this.setEase = function(z) {
    if (n != z) {
      n = z;
      f = TweenManager.Interpolation.convertEase(z);
      s = typeof f === "function"
    }
  };
  this.interpolate = function(A) {
    var D = s ? f(A) : TweenManager.Interpolation.solve(f, A);
    for (var C in y) {
      if (typeof y[C] === "number" && typeof v[C] === "number") {
        var B = y[C];
        var z = v[C];
        h[C] = B + (z - B) * D
      }
    }
  }
});
Class(function SpringTween(o, q, j, b, m, p, r) {
  var l = this;
  var d, e, h, a;
  var f, j, n, g;
  (function() {
    if (o && q) {
      if (typeof j !== "number") {
        throw "SpringTween Requires object, props, time, ease"
      }
      c()
    }
  })();

  function c() {
    TweenManager.clearTween(o);
    o._mathTween = l;
    TweenManager._addMathTween(l);
    d = Date.now();
    d += m;
    h = {};
    a = {};
    e = {};
    if (q.x || q.y || q.z) {
      if (typeof q.x === "undefined") {
        q.x = o.x
      }
      if (typeof q.y === "undefined") {
        q.y = o.y
      }
      if (typeof q.z === "undefined") {
        q.z = o.z
      }
    }
    n = 0;
    f = q.damping || 0.5;
    delete q.damping;
    for (var s in q) {
      if (typeof q[s] === "number") {
        e[s] = 0;
        h[s] = q[s]
      }
    }
    for (s in q) {
      if (typeof o[s] === "number") {
        a[s] = o[s] || 0;
        q[s] = a[s]
      }
    }
  }

  function k(s) {
    if (o) {
      o._mathTween = null;
      if (!s) {
        for (var t in h) {
          if (typeof h[t] === "number") {
            o[t] = h[t]
          }
        }
        if (o.transform) {
          o.transform()
        }
      }
    }
    TweenManager._removeMathTween(l)
  }
  this.update = function(v) {
    if (v < d || g) {
      return
    }
    var u;
    for (var z in a) {
      if (typeof a[z] === "number") {
        var y = a[z];
        var t = h[z];
        var x = q[z];
        var w = t - x;
        var s = w * f;
        e[z] += s;
        e[z] *= j;
        q[z] += e[z];
        o[z] = q[z];
        u = e[z]
      }
    }
    if (Math.abs(u) < 0.1) {
      n++;
      if (n > 30) {
        if (r) {
          r.apply(o)
        }
        k()
      }
    }
    if (p) {
      p(v)
    }
    if (o.transform) {
      o.transform()
    }
  };
  this.pause = function() {
    g = true
  };
  this.stop = function() {
    k(true);
    return null
  }
});
Class(function TweenTimeline() {
  Inherit(this, Component);
  var f = this;
  var e;
  var c = 0;
  var b = [];
  this.elapsed = 0;
  (function() {})();

  function d() {
    b.sort(function(k, h) {
      var l = k.time + k.delay;
      var j = h.time + h.delay;
      return j - l
    });
    var g = b[0];
    c = g.time + g.delay
  }

  function a() {
    var l = f.elapsed * c;
    for (var k = b.length - 1; k > -1; k--) {
      var j = b[k];
      var h = l - j.delay;
      var g = Utils.clamp(h / j.time, 0, 1);
      j.interpolate(g)
    }
  }
  this.add = function(h, k, l, m, g) {
    var j;
    if (h instanceof HydraObject) {
      j = new FrameTween(h, k, l, m, g, null, true)
    } else {
      j = new MathTween(h, k, l, m, g, null, null, true)
    }
    b.push(j);
    j.time = l;
    j.delay = g || 0;
    d();
    return j
  };
  this.tween = function(l, h, j, g, k) {
    this.stopTween();
    e = TweenManager.tween(f, {
      elapsed: l
    }, h, j, g, k, a)
  };
  this.stopTween = function() {
    if (e && e.stop) {
      e.stop()
    }
  };
  this.startRender = function() {
    Render.startRender(a)
  };
  this.stopRender = function() {
    Render.stopRender(a)
  };
  this.update = function() {
    a()
  };
  this.calculateRemainingTime = function() {
    return c - (f.elapsed * c)
  };
  this.destroy = function() {
    Render.stopRender(a);
    for (var g = 0; g < b.length; g++) {
      b[g].stop()
    }
    return this._destroy()
  }
});
Class(function Shaders() {
  var d = this;
  (function() {})();

  function c(f) {
    var h = f.split("{@}");
    h.shift();
    for (var g = 0; g < h.length; g += 2) {
      var e = h[g];
      var j = h[g + 1];
      d[e] = j
    }
  }

  function b() {
    for (var e in d) {
      var f = d[e];
      if (typeof f === "string") {
        d[e] = a(f)
      }
    }
  }

  function a(g) {
    if (!g.strpos("require")) {
      return g
    }
    g = g.replace(/# require/g, "#require");
    while (g.strpos("#require")) {
      var f = g.split("#require(");
      var e = f[1].split(")")[0];
      e = e.replace(/ /g, "");
      if (!d[e]) {
        throw "Shader required " + e + ", but not found in compiled shaders.\n" + g
      }
      g = g.replace("#require(" + e + ")", d[e])
    }
    return g
  }
  this.parse = function(f, e) {
    if (!f.strpos("{@}")) {
      e = e.split("/");
      e = e[e.length - 1];
      d[e] = f
    } else {
      c(f);
      b()
    }
  };
  this.getShader = function(e) {
    if (d.FALLBACKS) {
      if (d.FALLBACKS[e]) {
        e = d.FALLBACKS[e]
      }
    }
    return d[e]
  }
}, "static");
Class(function RenderPerformance() {
  Inherit(this, Component);
  var d = this;
  var b;
  var a = [];
  var c = [];
  this.enabled = true;
  this.pastFrames = 60;
  this.time = function() {
    if (!this.enabled) {
      return
    }
    if (!b) {
      b = performance.now()
    } else {
      var g = performance.now() - b;
      b = null;
      a.unshift(g);
      if (a.length > this.pastFrames) {
        a.pop()
      }
      c.unshift(Render.FPS);
      if (c.length > this.pastFrames) {
        c.pop()
      }
      this.average = 0;
      var e = a.length;
      for (var f = 0; f < e; f++) {
        this.average += a[f]
      }
      this.average /= e;
      this.averageFPS = 0;
      e = c.length;
      for (f = 0; f < e; f++) {
        this.averageFPS += c[f]
      }
      this.averageFPS /= e
    }
  };
  this.clear = function() {
    a.length = 0
  };
  this.dump = function() {
    console.log(a)
  };
  this.get("times", function() {
    return a
  });
  this.get("median", function() {
    a.sort(function(f, e) {
      return f - e
    });
    return a[~~(a.length / 2)]
  })
});
Class(function Video(o) {
  Inherit(this, Component);
  var h = this;
  var f, p, a, l, n, c, k;
  var b = 0;
  var d = {};
  this.loop = false;
  this.playing = false;
  this.width = o.width || 0;
  this.height = o.height || 0;
  (function() {
    j();
    if (o.preload !== false) {
      m()
    }
  })();

  function j() {
    var q = o.src;
    if (q && !q.strpos("webm") && !q.strpos("mp4") && !q.strpos("ogv")) {
      q += "." + Device.media.video
    }
    h.div = document.createElement("video");
    if (q) {
      h.div.src = q
    }
    h.div.controls = o.controls;
    h.div.id = o.id || "";
    h.div.width = o.width;
    h.div.height = o.height;
    c = h.div.loop = o.loop;
    if (Mobile.os == "iOS" && Mobile.version >= 9 && !h.div.controls) {
      h.div.autoplay = true;
      h.div.load()
    }
    h.object = $(h.div);
    h.width = o.width;
    h.height = o.height;
    h.object.size(h.width, h.height);
    if (Mobile.isNative() && Mobile.os == "iOS") {
      h.object.attr("webkit-playsinline", true)
    }
  }

  function m() {
    if (Device.mobile) {
      return
    }
    h.div.preload = "none";
    h.div.load();
    h.div.addEventListener("canplaythrough", function() {
      if (h.div && !h.playing && !h.div.preloadThroguh) {
        h.div.play();
        h.div.pause();
        h.div.preloadThrough = true
      }
    })
  }

  function e() {
    if (!h.div || !h.events) {
      return Render.stopRender(e)
    }
    h.duration = h.div.duration;
    h.time = h.div.currentTime;
    if (h.div.currentTime == a) {
      b++;
      if (b > 30 && !l) {
        l = true;
        h.events.fire(HydraEvents.ERROR, null, true)
      }
    } else {
      b = 0;
      if (l) {
        h.events.fire(HydraEvents.READY, null, true);
        l = false
      }
    }
    a = h.div.currentTime;
    if (h.div.currentTime >= (h.duration || h.div.duration) - 0.001) {
      if (!c) {
        if (!k) {
          Render.stopRender(e)
        }
        h.events.fire(HydraEvents.COMPLETE, null, true)
      }
    }
    d.time = h.div.currentTime;
    d.duration = h.div.duration;
    h.events.fire(HydraEvents.UPDATE, d, true)
  }

  function g() {
    if (!h.div) {
      return false
    }
    if (!Device.mobile) {
      if (!n) {
        h.buffered = h.div.readyState == h.div.HAVE_ENOUGH_DATA
      } else {
        var q = -1;
        var s = h.div.seekable;
        if (s) {
          for (var r = 0; r < s.length; r++) {
            if (s.start(r) < n) {
              q = s.end(r) - 0.5
            }
          }
          if (q >= n) {
            h.buffered = true
          }
        } else {
          h.buffered = true
        }
      }
    } else {
      h.buffered = true
    }
    if (h.buffered) {
      Render.stopRender(g);
      h.events.fire(HydraEvents.READY, null, true)
    }
  }
  this.set("loop", function(q) {
    if (!h.div) {
      return
    }
    c = q;
    h.div.loop = q
  });
  this.get("loop", function() {
    return c
  });
  this.set("src", function(q) {
    if (q && !q.strpos("webm") && !q.strpos("mp4") && !q.strpos("ogv")) {
      q += "." + Device.media.video
    }
    h.div.src = q
  });
  this.get("src", function() {
    return h.div.src
  });
  this.play = function() {
    if (!h.div) {
      return false
    }
    h.playing = true;
    h.div.play();
    Render.startRender(e)
  };
  this.pause = function() {
    if (!h.div) {
      return false
    }
    h.playing = false;
    h.div.pause();
    Render.stopRender(e)
  };
  this.stop = function() {
    h.playing = false;
    Render.stopRender(e);
    if (!h.div) {
      return false
    }
    h.div.pause();
    if (h.ready()) {
      h.div.currentTime = 0
    }
  };
  this.volume = function(q) {
    if (!h.div) {
      return false
    }
    h.div.volume = q
  };
  this.seek = function(q) {
    if (!h.div) {
      return false
    }
    if (h.div.readyState <= 1) {
      Render.nextFrame(function() {
        h.seek && h.seek(q)
      });
      return
    }
    h.div.currentTime = q
  };
  this.canPlayTo = function(q) {
    n = null;
    if (q) {
      n = q
    }
    if (!h.div) {
      return false
    }
    if (!h.buffered) {
      Render.startRender(g)
    }
    return this.buffered
  };
  this.ready = function() {
    if (!h.div) {
      return false
    }
    return h.div.readyState >= 2
  };
  this.size = function(q, r) {
    if (!h.div) {
      return false
    }
    this.div.width = this.width = q;
    this.div.height = this.height = r;
    this.object.css({
      width: q,
      height: r
    })
  };
  this.forceRender = function() {
    k = true;
    Render.startRender(e)
  };
  this.destroy = function() {
    this.stop();
    this.object.remove();
    this.div.src = "";
    return this._destroy()
  }
});
// Class(function SVG() {
//   var a = [];
//   (function() {
//     (function(k) {
//       var f = ["SVGSVGElement", "SVGGElement"],
//           m = document.createElement("dummy");
//       if (!f[0] in k) {
//         return !1
//       }
//       if (Object.defineProperty) {
//         var l = {
//           get: function() {
//             m.innerHTML = "";
//             Array.prototype.slice.call(this.childNodes).forEach(function(d) {
//               m.appendChild(d.cloneNode(!0))
//             });
//             return m.innerHTML
//           },
//           set: function(g) {
//             var d = this,
//                 n = Array.prototype.slice.call(d.childNodes),
//                 h = function(o, p) {
//                   if (1 !== p.nodeType) {
//                     return !1
//                   }
//                   var e = document.createElementNS("http://www.w3.org/2000/svg", p.nodeName.toLowerCase());
//                   Array.prototype.slice.call(p.attributes).forEach(function(q) {
//                     e.setAttribute(q.name, q.value)
//                   });
//                   "TEXT" === p.nodeName && (e.textContent = p.innerHTML);
//                   o.appendChild(e);
//                   p.childNodes.length && Array.prototype.slice.call(p.childNodes).forEach(function(q) {
//                     h(e, q)
//                   })
//                 },
//                 g = g.replace(/<(\w+)([^<]+?)\/>/, "<$1$2></$1>");
//             n.forEach(function(e) {
//               e.parentNode.removeChild(e)
//             });
//             m.innerHTML = g;
//             Array.prototype.slice.call(m.childNodes).forEach(function(e) {
//               h(d, e)
//             })
//           },
//           enumerable: !0,
//           configurable: !0
//         };
//         try {
//           f.forEach(function(d) {
//             Object.defineProperty(window[d].prototype, "innerHTML", l)
//           })
//         } catch (j) {}
//       } else {
//         Object.prototype.__defineGetter__ && f.forEach(function(d) {
//           window[d].prototype.__defineSetter__("innerHTML", l.set);
//           window[d].prototype.__defineGetter__("innerHTML", l.get)
//         })
//       }
//     })(window)
//   }());
//
//   function c(e) {
//     for (var d = 0; d < a.length; d++) {
//       if (a[d].id == e) {
//         return true
//       }
//     }
//   }
//
//   function b(e) {
//     for (var d = 0; d < a.length; d++) {
//       if (a[d].id == e) {
//         return a[d]
//       }
//     }
//   }
//   this.defineSymbol = function(h, f, d, g) {
//     if (c(h)) {
//       throw "SVG symbol " + h + " is already defined"
//     }
//     var e = document.createElementNS("http://www.w3.org/2000/svg", "svg");
//     e.setAttribute("style", "display: none;");
//     e.setAttribute("width", f);
//     e.setAttribute("height", d);
//     e.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink");
//     e.innerHTML = '<symbol id="' + h + '">' + g + "</symbol>";
//     document.body.insertBefore(e, document.body.firstChild);
//     a.push({
//       id: h,
//       width: f,
//       height: d
//     })
//   };
//   this.getSymbolConfig = function(e) {
//     var d = b(e);
//     if (typeof d == "undefined") {
//       throw "SVG symbol " + e + " is not defined"
//     }
//     return d
//   }
// }, "Static");
Class(function AssetLoader(_assets, _complete, _images) {
  Inherit(this, Component);
  var _this = this;
  var _total = 0;
  var _loaded = 0;
  var _added = 0;
  var _triggered = 0;
  var _lastTriggered = 0;
  var _queue, _qLoad;
  var _output, _loadedFiles;
  (function() {
    if (typeof _complete !== "function") {
      _images = _complete;
      _complete = null
    }
    _queue = [];
    _loadedFiles = [];
    prepareAssets();
    _this.delayedCall(startLoading, 10)
  })();

  function prepareAssets() {
    for (var i = 0; i < _assets.length; i++) {
      if (typeof _assets[i] !== "undefined") {
        _total++;
        _queue.push(_assets[i])
      }
    }
  }

  function startLoading() {
    _qLoad = Math.round(_total * 0.5);
    for (var i = 0; i < _qLoad; i++) {
      loadAsset(_queue[i])
    }
  }

  function missingFiles() {
    if (!_queue) {
      return
    }
    var missing = [];
    for (var i = 0; i < _queue.length; i++) {
      var loaded = false;
      for (var j = 0; j < _loadedFiles.length; j++) {
        if (_loadedFiles[j] == _queue[i]) {
          loaded = true
        }
      }
      if (!loaded) {
        missing.push(_queue[i])
      }
    }
    if (missing.length) {
      console.log("AssetLoader Files Failed To Load:");
      console.log(missing)
    }
  }

  function loadAsset(asset) {
    if (!asset) {
      return
    }
    var name = asset.split("/");
    name = name[name.length - 1];
    var split = name.split(".");
    var ext = split[split.length - 1].split("?")[0];
    switch (ext) {
      case "html":
        XHR.get(asset, function(contents) {
          Hydra.HTML[split[0]] = contents;
          assetLoaded(asset)
        }, "text");
        break;
      case "js":
      case "php":
      case undefined:
        XHR.get(asset, function(script) {
          script = script.replace("use strict", "");
          eval.call(window, script);
          assetLoaded(asset)
        }, "text");
        break;
      case "csv":
      case "json":
        XHR.get(asset, function(contents) {
          Hydra.JSON[split[0]] = contents;
          assetLoaded(asset)
        }, ext == "csv" ? "text" : null);
        break;
      case "fs":
      case "vs":
        XHR.get(asset, function(contents) {
          Shaders.parse(contents, asset);
          assetLoaded(asset)
        }, "text");
        break;
      default:
        var image = new Image();
        image.src = asset;
        image.onload = function() {
          assetLoaded(asset);
          if (_images) {
            _images[asset] = image
          }
        };
        break
    }
  }

  function checkQ() {
    if (_loaded == _qLoad && _loaded < _total) {
      var start = _qLoad;
      _qLoad *= 2;
      for (var i = start; i < _qLoad; i++) {
        if (_queue[i]) {
          loadAsset(_queue[i])
        }
      }
    }
  }

  function assetLoaded(asset) {
    if (_queue) {
      _loaded++;
      _this.events.fire(HydraEvents.PROGRESS, {
        percent: _loaded / _total
      });
      _loadedFiles.push(asset);
      clearTimeout(_output);
      checkQ();
      if (_loaded == _total) {
        _this.complete = true;
        Render.nextFrame(function() {
          if (_this.events) {
            _this.events.fire(HydraEvents.COMPLETE, null, true)
          }
          if (_complete) {
            _complete()
          }
        })
      } else {
        _output = _this.delayedCall(missingFiles, 5000)
      }
    }
  }
  this.add = function(num) {
    _total += num;
    _added += num
  };
  this.trigger = function(num) {
    num = num || 1;
    for (var i = 0; i < num; i++) {
      assetLoaded("trigger")
    }
  };
  this.triggerPercent = function(percent, num) {
    num = num || _added;
    var trigger = Math.ceil(num * percent);
    if (trigger > _lastTriggered) {
      this.trigger(trigger - _lastTriggered)
    }
    _lastTriggered = trigger
  };
  this.destroy = function() {
    _assets = null;
    _loaded = null;
    _queue = null;
    _qLoad = null;
    return this._destroy()
  }
}, function() {
  AssetLoader.loadAllAssets = function(e, a) {
    a = a || "";
    var d = [];
    for (var b = 0; b < ASSETS.length; b++) {
      d.push(a + ASSETS[b])
    }
    var c = new AssetLoader(d, function() {
      if (e) {
        e()
      }
      c = c.destroy()
    })
  };
  AssetLoader.loadAssets = function(b, c) {
    var a = new AssetLoader(b, function() {
      if (c) {
        c()
      }
      a = a.destroy()
    })
  }
});
// Class(function FontLoader(d, a) {
//   Inherit(this, Component);
//   var f = this;
//   var b;
//   (function() {
//     e();
//     c()
//   })();
//
//   function e() {
//     if (!Array.isArray(d)) {
//       d = [d]
//     }
//     b = Stage.create("FontLoader");
//     for (var h = 0; h < d.length; h++) {
//       var g = b.create("font");
//       g.fontStyle(d[h], 12, "#000").text("LOAD").css({
//         top: -999
//       })
//     }
//   }
//
//   function c() {
//     f.delayedCall(function() {
//       b.remove();
//       if (a) {
//         a()
//       } else {
//         f.events.fire(HydraEvents.COMPLETE)
//       }
//     }, 500)
//   }
// }, function() {
//   FontLoader.loadFonts = function(c, b) {
//     var a = new FontLoader(c, function() {
//       if (b) {
//         b()
//       }
//       a = null
//     })
//   }
// });
Class(function PushState(a) {
  var b = this;
  if (typeof a !== "boolean") {
    a = Hydra.LOCAL
  }
  this.locked = false;
  this.dispatcher = new StateDispatcher(a);
  this.getState = function() {
    return this.dispatcher.getState()
  };
  this.setState = function(c) {
    this.dispatcher.setState(c)
  };
  this.replaceState = function(c) {
    this.dispatcher.replaceState(c)
  };
  this.setTitle = function(c) {
    // this.dispatcher.setTitle(c)
  };
  this.lock = function() {
    this.locked = true;
    this.dispatcher.lock()
  };
  this.unlock = function() {
    this.locked = false;
    this.dispatcher.unlock()
  };
  this.setPathRoot = function(c) {
    this.dispatcher.setPathRoot(c)
  }
});
Class(function StateDispatcher(g) {
  Inherit(this, Events);
  var f = this;
  var j, a;
  var d = "/";
  this.locked = false;
  (function() {
    b();
    j = c();
    a = j
  })();

  function b() {
    if (!Device.system.pushstate || g) {
      window.addEventListener("hashchange", function() {
        h(c())
      }, false)
    } else {
      window.onpopstate = history.onpushstate = e
    }
  }

  function c() {
    if (!Device.system.pushstate || g) {
      var k = window.location.hash;
      k = k.slice(3);
      return String(k)
    } else {
      var l = location.pathname.toString();
      l = d != "/" ? l.split(d)[1] : l.slice(1);
      l = l || "";
      return l
    }
  }

  function e() {
    var k = location.pathname;
    if (!f.locked && k != a) {
      k = d != "/" ? k.split(d)[1] : k.slice(1);
      k = k || "";
      a = k;
      f.events.fire(HydraEvents.UPDATE, {
        value: k,
        split: k.split("/")
      })
    } else {
      if (k != a) {
        if (a) {
          window.history.pushState(null, null, d + k)
        }
      }
    }
  }

  function h(k) {
    if (!f.locked && k != a) {
      a = k;
      f.events.fire(HydraEvents.UPDATE, {
        value: k,
        split: k.split("/")
      })
    } else {
      if (k != a) {
        if (a) {
          window.location.hash = "!/" + a
        }
      }
    }
  }
  this.getState = function() {
    if (Mobile.NativeCore && Mobile.NativeCore.active) {
      return Storage.get("app_state") || ""
    }
    return c()
  };
  this.setPathRoot = function(k) {
    if (k.charAt(0) == "/") {
      d = k
    } else {
      d = "/" + k
    }
  };
  this.setState = function(k) {
    if (Mobile.NativeCore && Mobile.NativeCore.active) {
      Storage.set("app_state", k)
    }
    if (!Device.system.pushstate || g) {
      if (k != a) {
        window.location.hash = "!/" + k;
        a = k
      }
    } else {
      if (k != a) {
        window.history.pushState(null, null, d + k);
        a = k
      }
    }
  };
  this.replaceState = function(k) {
    if (!Device.system.pushstate || g) {
      if (k != a) {
        window.location.hash = "!/" + k;
        a = k
      }
    } else {
      if (k != a) {
        window.history.replaceState(null, null, d + k);
        a = k
      }
    }
  };
  this.setTitle = function(k) {
    document.title = k
  };
  this.lock = function() {
    this.locked = true
  };
  this.unlock = function() {
    this.locked = false
  };
  this.forceHash = function() {
    g = true
  }
});
Class(function GATracker() {
  this.trackPage = function(a) {
    if (typeof ga !== "undefined") {
      ga("send", "pageview", a)
    }
  };
  this.trackEvent = function(b, d, a, c) {
    if (typeof ga !== "undefined") {
      ga("send", "event", b, d, a, (c || 0))
    }
  }
}, "Static");
Class(function XHR() {
  var d = this;
  var b;
  var c = window.location.href.strpos("file://");
  this.headers = {};

  function a(f, g) {
    if (typeof g === "object") {
      for (var e in g) {
        var h = f + "[" + e + "]";
        if (typeof g[e] === "object") {
          a(h, g[e])
        } else {
          b.push(h + "=" + g[e])
        }
      }
    } else {
      b.push(f + "=" + g)
    }
  }
  this.get = function(f, j, l, h) {
    if (typeof j === "function") {
      h = l;
      l = j;
      j = null
    } else {
      if (typeof j === "object") {
        var e = "?";
        for (var g in j) {
          e += g + "=" + j[g] + "&"
        }
        e = e.slice(0, -1);
        f += e
      }
    }
    var k = new XMLHttpRequest();
    k.open("GET", f, true);
    for (var g in d.headers) {
      k.setRequestHeader(g, d.headers)
    }
    k.send();
    k.onreadystatechange = function() {
      if (k.readyState == 4 && (c || k.status == 200)) {
        if (typeof l === "function") {
          var m = k.responseText;
          if (h == "text") {
            l(m)
          } else {
            try {
              l(JSON.parse(m))
            } catch (n) {
              console.error(n)
            }
          }
        }
        k = null
      }
    }
  };
  this.post = function(e, h, l, g, k) {
    if (typeof h === "function") {
      k = g;
      g = l;
      l = h;
      h = null
    } else {
      if (typeof h === "object") {
        if (l == "json" || g == "json" || k == "json") {
          h = JSON.stringify(h)
        } else {
          b = new Array();
          for (var f in h) {
            a(f, h[f])
          }
          h = b.join("&");
          h = h.replace(/\[/g, "%5B");
          h = h.replace(/\]/g, "%5D");
          b = null
        }
      }
    }
    var j = new XMLHttpRequest();
    j.open("POST", e, true);
    switch (k) {
      case "upload":
        k = "application/upload";
        break;
      default:
        k = "application/x-www-form-urlencoded";
        break
    }
    j.setRequestHeader("Content-type", k);
    for (var f in d.headers) {
      j.setRequestHeader(f, d.headers)
    }
    j.onreadystatechange = function() {
      if (j.readyState == 4 && (c || j.status == 200)) {
        if (typeof l === "function") {
          var m = j.responseText;
          if (g == "text") {
            l(m)
          } else {
            try {
              l(JSON.parse(m))
            } catch (n) {
              console.error(n)
            }
          }
        }
        j = null
      }
    };
    j.send(h)
  }
}, "Static");
Class(function Storage() {
  var d = this;
  var c;
  (function() {
    a()
  })();

  function a() {
    try {
      if (window.localStorage) {
        try {
          window.localStorage.test = 1;
          window.localStorage.removeItem("test");
          c = true
        } catch (f) {
          c = false
        }
      } else {
        c = false
      }
    } catch (f) {
      c = false
    }
  }

  function b(j, k, f) {
    var g;
    if (arguments.length > 1 && (k === null || typeof k !== "object")) {
      g = {};
      g.path = "/";
      g.expires = f || 1;
      if (k === null) {
        g.expires = -1
      }
      if (typeof g.expires === "number") {
        var m = g.expires,
            h = g.expires = new Date();
        h.setDate(h.getDate() + m)
      }
      return (document.cookie = [encodeURIComponent(j), "=", g.raw ? String(k) : encodeURIComponent(String(k)), g.expires ? "; expires=" + g.expires.toUTCString() : "", g.path ? "; path=" + g.path : "", g.domain ? "; domain=" + g.domain : "", g.secure ? "; secure" : ""].join(""))
    }
    g = k || {};
    var e, l = g.raw ? function(n) {
      return n
    } : decodeURIComponent;
    return (e = new RegExp("(?:^|; )" + encodeURIComponent(j) + "=([^;]*)").exec(document.cookie)) ? l(e[1]) : null
  }
  this.setCookie = function(f, g, e) {
    b(f, g, e)
  };
  this.getCookie = function(e) {
    return b(e)
  };
  this.set = function(e, f) {
    if (typeof f === "object") {
      f = JSON.stringify(f)
    }
    if (c) {
      if (typeof f === "null") {
        window.localStorage.removeItem(e)
      } else {
        window.localStorage[e] = f
      }
    } else {
      b(e, f, 365)
    }
  };
  this.get = function(e) {
    var g;
    if (c) {
      g = window.localStorage[e]
    } else {
      g = b(e)
    }
    if (g) {
      var f;
      if (g.charAt) {
        f = g.charAt(0)
      }
      if (f == "{" || f == "[") {
        g = JSON.parse(g)
      }
    }
    return g
  }
}, "Static");
Class(function Thread(c) {
  Inherit(this, Component);
  var d = this;
  var k, f, j, e;
  (function() {
    m();
    g();
    h()
  })();

  function m() {
    j = Thread.PATH;
    f = {};
    k = new Worker(j + "assets/js/hydra/hydra-thread.js")
  }

  function g() {
    a(Utils);
    a(MVC);
    a(Component);
    a(Events);
    a(c, true)
  }

  function a(n, t) {
    if (!n) {
      return
    }
    var r, q;
    if (!t) {
      if (typeof n !== "function") {
        q = n.constructor._namespace ? n.constructor._namespace + "." : "";
        r = q + "Class(" + n.constructor.toString() + ', "static");'
      } else {
        q = n._namespace ? n._namespace + "." : "";
        r = q + "Class(" + n.toString() + ");"
      }
    } else {
      r = n.toString().replace("{", "!!!");
      r = r.split("!!!")[1];
      var s = window._MINIFIED_ ? "=" : " ";
      while (r.strpos("this")) {
        var p = r.slice(r.indexOf("this."));
        var o = p.split("this.")[1].split(s)[0];
        r = r.replace("this", "self");
        b(o)
      }
      r = r.slice(0, -1)
    }
    k.postMessage({
      code: r
    })
  }

  function b(n) {
    d[n] = function(o, p) {
      d.send(n, o, p)
    }
  }

  function h() {
    k.addEventListener("message", l)
  }

  function l(n) {
    if (n.data.console) {
      console.log(n.data.message)
    } else {
      if (n.data.id) {
        var o = f[n.data.id];
        if (o) {
          o(n.data.message)
        }
        delete f[n.data.id]
      } else {
        if (n.data.emit) {
          var o = f[n.data.evt];
          if (o) {
            o(n.data.msg)
          }
        } else {
          var o = f.transfer;
          if (o) {
            o(n.data)
          }
        }
      }
    }
  }
  this.on = function(n, o) {
    f[n] = o
  };
  this.off = function(n) {
    delete f[n]
  };
  this.loadFunctions = function() {
    for (var n = 0; n < arguments.length; n++) {
      this.loadFunction(arguments[n])
    }
  };
  this.loadFunction = function(p) {
    p = p.toString();
    p = p.replace("(", "!!!");
    var o = p.split("!!!");
    var n = o[0].split(" ")[1];
    p = "self." + n + " = function(" + o[1];
    k.postMessage({
      code: p
    });
    b(n)
  };
  this.importScript = function(n) {
    k.postMessage({
      path: n,
      importScript: true
    })
  };
  this.importClass = function() {
    for (var n = 0; n < arguments.length; n++) {
      var o = arguments[n];
      a(o)
    }
  };
  this.send = function(n, p, r) {
    if (typeof n === "string") {
      var o = n;
      p = p || {};
      p.fn = n
    } else {
      r = p;
      p = n
    }
    var q = Utils.timestamp();
    if (r) {
      f[q] = r
    }
    if (p.transfer) {
      p.msg.id = q;
      p.msg.fn = p.fn;
      p.msg.transfer = true;
      k.postMessage(p.msg, p.buffer)
    } else {
      k.postMessage({
        message: p,
        id: q
      })
    }
  };
  this.destroy = function() {
    if (k.terminate) {
      k.terminate()
    }
    return this._destroy()
  }
}, function() {
  Thread.PATH = ""
});
Class(function Dev() {
  var e = this;
  var a, c;
  (function() {
    if (Hydra.LOCAL) {
      Hydra.development(true)
    }
  })();

  function d() {
    window.onerror = function(j, h, f) {
      var g = j + " ::: " + h + " : " + f;
      if (c) {
        alert(g)
      }
      if (a) {
        XHR.post(a + "/api/data/debug", b(g), "json")
      }
    }
  }

  function b(f) {
    var g = {};
    g.err = f;
    g.ua = Device.agent;
    g.browser = {
      width: Stage.width,
      height: Stage.height
    };
    return g
  }
  this.alertErrors = function(f) {
    c = true;
    if (typeof f === "string") {
      f = [f]
    }
    for (var g = 0; g < f.length; g++) {
      if (location.href.strpos(f[g]) || location.hash.strpos(f[g])) {
        return d()
      }
    }
  };
  this.postErrors = function(f, h) {
    a = h;
    if (typeof f === "string") {
      f = [f]
    }
    for (var g = 0; g < f.length; g++) {
      if (location.href.strpos(f[g])) {
        return d()
      }
    }
  };
  this.expose = function(f, h, g) {
    if (Hydra.LOCAL || g) {
      window[f] = h
    }
  }
}, "Static");
window.ASSETS = [
  "img/bg-intro.jpg",
    // "assets/geometry/logo.json",
  // "assets/geometry/terrain.json",
  // "assets/images/about/awards/adc.png",
  // "assets/images/about/awards/addy.png",
  // "assets/images/about/awards/awwwards.png",
  // "assets/images/about/awards/cannes.png",
  // "assets/images/about/awards/dnad.png",
  // "assets/images/about/awards/emmy.png",
  // "assets/images/about/awards/fwa.png",
  // "assets/images/about/awards/oneshow.png",
  // "assets/images/about/awards/webby.png",
  // "assets/images/about/awards.jpg",
  // "assets/images/about/bg-blur.jpg",
  // "assets/images/about/bg.jpg",
  // "assets/images/about/design.jpg",
  // "assets/images/about/logo-wide-invert.png",
  // "assets/images/about/logo-wide.png",
  // "assets/images/about/process_design.jpg",
  // "assets/images/about/process_tech.jpg",
  // "assets/images/about/shadow.png",
  // "assets/images/common/awards/adc.png",
  // "assets/images/common/awards/addy.png",
  // "assets/images/common/awards/awwwards.png",
  // "assets/images/common/awards/cannes.png",
  // "assets/images/common/awards/dnad.png",
  // "assets/images/common/awards/emmy.png",
  // "assets/images/common/awards/fwa.png",
  // "assets/images/common/awards/oneshow.png",
  // "assets/images/common/awards/webby.png",
  // "assets/images/common/circlelogo.png",
  // "assets/images/common/fb.png",
  // "assets/images/common/gradient.png",
  // "assets/images/common/loader.png",
  // "assets/images/common/logos/53.png",
  // "assets/images/common/logos/adultswim.png",
  // "assets/images/common/logos/android.png",
  // "assets/images/common/logos/ashworth.png",
  // "assets/images/common/logos/chelsea.png",
  // "assets/images/common/logos/chevy.png",
  // "assets/images/common/logos/cocacola.png",
  // "assets/images/common/logos/dicks.png",
  // "assets/images/common/logos/fox.png",
  // "assets/images/common/logos/google.png",
  // "assets/images/common/logos/gradient.png",
  // "assets/images/common/logos/halo.png",
  // "assets/images/common/logos/hennessy.png",
  // "assets/images/common/logos/ibm.png",
  // "assets/images/common/logos/la24.png",
  // "assets/images/common/logos/pottermore.png",
  // "assets/images/common/logos/spotify.png",
  // "assets/images/common/logos/stance.png",
  // "assets/images/common/logos/toyota.png",
  // "assets/images/common/logos/underarmour.png",
  // "assets/images/common/logos/xbox.png", "assets/images/common/shadow.png", "assets/images/common/tw.png", "assets/images/fallback/logo.png", "assets/images/home/frame2.jpg", "assets/images/home/logo_2.png", "assets/images/home/logo_invert.png", "assets/images/home/outline.png", "assets/images/icons/apple-touch-icon-120.png", "assets/images/icons/apple-touch-icon-152.png", "assets/images/icons/apple-touch-icon-180.png", "assets/images/icons/apple-touch-icon-76.png", "assets/images/icons/icon-128.png", "assets/images/icons/icon-144.png", "assets/images/icons/icon-192.png", "assets/images/icons/icon-36.png", "assets/images/icons/icon-48.png", "assets/images/icons/icon-72.png", "assets/images/icons/icon-96.png", "assets/images/menu/brightness.jpg", "assets/images/menu/fallback.jpg", "assets/images/share/share_facebook.jpg", "assets/images/share/share_twitter.jpg", "assets/images/temp/bg.jpg", "assets/images/temp/gotham.jpg", "assets/images/temp/logo.jpg", "assets/images/temp/screenshot.jpg", "assets/images/temp/thumb.jpg", "assets/images/temp/uv.jpg", "assets/js/lib/three.min.js",
  "assets/shaders/compiled.vs"
];
window.PROJECTS = {
  work: {},
  ashworth: {
    bg: true,
    thumb: true,
    video: true
  },
  block: {
    bg: true,
    thumb: true,
    video: true
  },
  chelsea: {
    bg: true,
    thumb: true,
    video: true
  },
  clouds: {
    bg: true,
    thumb: true,
    video: true
  },
  contact: {
    bg: true,
    thumb: true,
    video: true
  },
  fifa: {
    bg: true,
    thumb: true,
    video: true
  },
  gisele: {
    bg: true,
    thumb: true,
    video: true
  },
  googleio: {
    bg: true,
    thumb: true,
    video: true
  },
  gotham: {
    bg: true,
    thumb: true,
    video: true
  },
  halo: {
    bg: true,
    thumb: true,
    video: true
  },
  hennessy: {
    bg: true,
    thumb: true,
    video: true
  },
  la24: {
    bg: true,
    thumb: true,
    video: true
  },
  leapsecond: {
    bg: true,
    thumb: true,
    video: true
  },
  patronus: {
    bg: true,
    thumb: true,
    video: true
  },
  penguin: {
    bg: true,
    thumb: true,
    video: true
  },
  planes: {
    bg: true,
    thumb: true,
    video: true
  },
  racer: {
    bg: true,
    thumb: true,
    video: true
  },
  racer_installation: {
    bg: true,
    thumb: true,
    video: true
  },
  singles: {
    bg: true,
    thumb: true,
    video: true
  },
  source: {
    bg: true,
    thumb: true,
    video: true
  },
  stance: {
    bg: true,
    thumb: true,
    video: true
  },
  stingray: {
    bg: true,
    thumb: true,
    video: true
  },
  storm: {
    bg: true,
    thumb: true,
    video: true
  },
  sutc: {
    bg: true,
    thumb: true,
    video: true
  },
  ua: {
    bg: true,
    thumb: true,
    video: true
  },
  watson: {
    bg: true,
    thumb: true,
    video: true
  },
  lab: {},
  finding: {
    bg: true,
    thumb: true,
    video: true
  },
  mira: {
    bg: true,
    thumb: true,
    video: true
  },
  neve: {
    bg: true,
    thumb: true,
    video: true
  },
  nova: {
    bg: true,
    thumb: true,
    video: true
  },
  p2p: {
    bg: true,
    thumb: true,
    video: true
  },
  webvr: {
    bg: true,
    thumb: true,
    video: true
  }
};
Class(function Config() {
  var a = window.innerWidth > window.innerHeight ? (Mobile.browser === "Safari" ? 20 : 20) : 25;
  this.UI_OFFSET = Mobile.phone ? 25 : Device.mobile ? 70 : 80;
  this.UI_NAV_WIDTH = 427;
  this.UI_NAV_ITEM_HEIGHT = Mobile.phone ? 130 : 160;
  this.SCENE_OFFSET = 0;
  this.BG_COLOR = "#080808";
  this.DATA_API = (function() {
    if (window.location.href.strpos("v3.activetheory.net") || location.hostname == "activetheory.net") {
      return "https://at-v3-prod.s3.amazonaws.com/assets/data/data.js"
    }
    return "http://stage.activetheory.net/api/data"
  })();
  this.CDN = (function() {
    if (Hydra.LOCAL) {
      return ""
    }
    if (window.location.href.strpos("stage.activetheory")) {
      return "https://at-v3-stage.s3.amazonaws.com/"
    }
    if (window.location.href.strpos("v3.activetheory.net")) {
      return "https://at-v3-prod.s3.amazonaws.com/"
    }
    if (window.location.href.strpos("activetheory.net")) {
      return "https://at-v3-prod.s3.amazonaws.com/"
    }
    return ""
  })();
  this.PROXY = (function() {
    if (Hydra.LOCAL) {
      return ""
    }
    if (window.location.href.strpos("stage.activetheory")) {
      return "http://stage.activetheory.net/cdn/"
    }
    if (window.location.href.strpos("v3.activetheory")) {
      return location.protocol + "//v3.activetheory.net/cdn/"
    }
    if (window.location.href.strpos("activetheory.net")) {
      return location.protocol + "//activetheory.net/cdn/"
    }
    return ""
  })();
  this.MENU_TINT = {
    home: 16777215,
    work: 16776387,
    lab: 12184831,
    about: 16761574
  };
  this.MENU_COLORS = [9888767, 8447945, 15965116, 11268809, 13612543];
  this.PAGES = ["home"/*, "work", "about", "lab"/**/]
}, "Static");
Class(function ATEvents() {
  var a = this;
  this.RESIZE = "resize";
  this.SIZZLE_CHANGE = "sizzle_change";
  this.FILTER_CHANGE = "filter_change";
  this.STATE_CHANGE = "state_change";
  this.RESET_FILTERS = "reset_filters"
}, "static");
Module(function GPUBlacklist() {
  this.exports = {
    match: function() {
      if (!Device.graphics.webgl) {
        return true
      }
      return Device.graphics.webgl.detect(["radeon hd 6970m", "radeon hd 6770m", "radeon hd 6490m", "radeon hd 6630m", "radeon hd 6750m", "radeon hd 5750", "radeon hd 5670", "radeon hd 4850", "radeon hd 4870", "radeon hd 4670", "geforce 9400m", "geforce 320m", "geforce 330m", "geforce gt 130", "geforce gt 120", "geforce gtx 285", "geforce 8600", "geforce 9600m", "geforce 9400m", "geforce 8800 gs", "geforce 8800 gt", "quadro fx 5", "quadro fx 4", "radeon hd 2600", "radeon hd 2400", "radeon hd 2600", "radeon r9 200", "mali-4", "mali-3", "mali-2", ])
    }
  }
});
Class(function AssetUtil() {
  var d = this;
  var c = {};
  var b = ["!!!"];
  this.PATH = "";

  function a(g, e) {
    for (var f = 0; f < b.length; f++) {
      var h = b[f];
      if (g.strpos(h) && e != h) {
        return false
      }
    }
    return true
  }
  this.loadAssets = function(j) {
    var h = this.get(j);
    var e = [];
    for (var f = h.length - 1; f > -1; f--) {
      var g = h[f];
      if (!c[g]) {
        e.push(g.strpos("http") ? g : d.PATH + g);
        c[g] = 1
      }
    }
    return e
  };
  this.get = function(l) {
    if (!Array.isArray(l)) {
      l = [l]
    }
    var k = [];
    for (var g = ASSETS.length - 1; g > -1; g--) {
      var h = ASSETS[g];
      for (var f = l.length - 1; f > -1; f--) {
        var e = l[f];
        if (h.strpos(e)) {
          if (a(h, e)) {
            k.push(h)
          }
        }
      }
    }
    return k
  };
  this.exclude = function(f) {
    if (!Array.isArray(f)) {
      f = [f]
    }
    for (var e = 0; e < f.length; e++) {
      b.push(f[e])
    }
  };
  this.removeExclude = function(f) {
    if (!Array.isArray(f)) {
      f = [f]
    }
    for (var e = 0; e < f.length; e++) {
      b.findAndRemove(f[e])
    }
  };
  this.loadAllAssets = function(g) {
    var f = d.loadAssets(g || "/");
    var e = new AssetLoader(f)
  };
  this.exists = function(e) {
    for (var f = ASSETS.length - 1; f > -1; f--) {
      var g = ASSETS[f];
      if (g.strpos(e)) {
        return true
      }
    }
    return false
  };
  this.prependPath = function(h, g) {
    if (!Array.isArray(g)) {
      g = [g]
    }
    for (var e = ASSETS.length - 1; e > -1; e--) {
      var f = ASSETS[e];
      g.forEach(function(j) {
        if (f.strpos(j)) {
          ASSETS[e] = h + f
        }
      })
    }
  }
}, "Static");
Class(function ATUtil() {
  Inherit(this, Component);
  var e = this;
  var a;
  (function() {
    d();
    c()
  })();

  function d() {
    TweenManager.addCustomEase({
      name: "homeIn",
      curve: "M0,100c3.3,0,32.8-0.8,52-18.3C81.5,54.7,74.7,0,100,0"
    });
    TweenManager.addCustomEase({
      name: "homeOut",
      curve: "M0,100c14.6-2.2,27.4-10,41.5-39.6C59.6,22.5,80.9,0,100,0"
    });
    TweenManager.addCustomEase({
      name: "wipeInOut",
      curve: "cubic-bezier(.29,.23,.13,1)"
    });
    TweenManager.addCustomEase({
      name: "wipe",
      curve: Device.mobile ? "cubic-bezier(.08,.3,.02,.98)" : "cubic-bezier(.31,.19,.16,1)"
    })
  }

  function c() {
    e.events.subscribe(HydraEvents.RESIZE, b)
  }

  function b() {
    if (Device.mobile) {
      e.events.fire(ATEvents.RESIZE)
    } else {
      clearTimeout(a);
      a = e.delayedCall(function() {
        e.events.fire(ATEvents.RESIZE)
      }, 100)
    }
  }
  this.exists = function(f, g) {
    var h = PROJECTS[f];
    return h && h[g]
  };
  this.matchTag = function(f, g) {
    return g.tags.indexOf(f) > -1
  }
}, "static");
Class(function DelaunayTriangulation(d, f, e) {
  var g = this;
  this.rect = [{
    x: 0,
    y: 0
  }, {
    x: d,
    y: 0
  }, {
    x: 0,
    y: f
  }, {
    x: d,
    y: f
  }];
  this.points = e;
  this.triangles = [
    [this.rect[0], this.rect[1], this.rect[2]],
    [this.rect[1], this.rect[2], this.rect[3]]
  ];

  function j(m, o) {
    var n = l(m);
    return n ? n.r2 >= b(o, n.o) : 0
  }

  function b(n, m) {
    return Math.pow((n.x - m.x), 2) + Math.pow((n.y - m.y), 2)
  }

  function l(x) {
    var t, s, E, C, p, n;
    var r, q, A, z;
    var B, F, D, v, m, y, u, w;
    t = x[0].x;
    s = x[0].y;
    E = x[1].x;
    C = x[1].y;
    p = x[2].x;
    n = x[2].y;
    r = t - p;
    q = s - n;
    A = E - p;
    z = C - n;
    B = r * z - q * A;
    if (B === 0) {
      return null
    }
    v = t * t + s * s;
    m = E * E + C * C;
    y = p * p + n * n;
    F = v - y;
    D = m - y;
    u = {
      x: (z * F - q * D) / B / 2,
      y: (-A * F + r * D) / B / 2
    };
    w = b(u, x[2]);
    return {
      o: u,
      r2: w
    }
  }

  function h(r, s) {
    var o = [];
    var q = [];
    for (var m = 0, t = r.length; m < t; m++) {
      if (j(r[m], s)) {
        o.push(r[m])
      } else {
        q.push(r[m])
      }
    }
    return {
      ok: o,
      ng: q
    }
  }

  function c(o) {
    var u = [];
    var q = a(o);
    var m = q.length;
    for (var r = 0; r < m - 1; r++) {
      var t = q[r];
      if (t.skip) {
        continue
      }
      var v = false;
      for (var p = r + 1; p < m; p++) {
        var s = q[p];
        if ((t[0].x == s[0].x && t[0].y == s[0].y && t[1].x == s[1].x && t[1].y == s[1].y) || (t[0].x == s[1].x && t[0].y == s[1].y && t[1].x == s[0].x && t[1].y == s[0].y)) {
          q[p].skip = true;
          v = true;
          break
        }
      }
      v || u.push([t[0], t[1]])
    }
    if (!q[m - 1].skip) {
      u.push([q[m - 1][0], q[m - 1][1]])
    }
    return u
  }

  function a(q) {
    var m = [];
    for (var p = 0, r = q.length; p < r; p++) {
      var o = q[p];
      m.push([o[0], o[1]], [o[1], o[2]], [o[2], o[0]])
    }
    return m
  }

  function k(o, s) {
    var r = [];
    for (var m = 0, t = o.length; m < t; m++) {
      var q = o[m];
      r.push([q[0], q[1], s])
    }
    return r
  }
  this.updateRect = function(n, m) {
    this.rect = [{
      x: 0,
      y: 0
    }, {
      x: n,
      y: 0
    }, {
      x: 0,
      y: m
    }, {
      x: n,
      y: m
    }];
    this.triangles = [
      [this.rect[0], this.rect[1], this.rect[2]],
      [this.rect[1], this.rect[2], this.rect[3]]
    ]
  };
  this.split = function() {
    var r = this.triangles;
    for (var o = 0, u = this.points.length; o < u; o++) {
      var s = this.points[o];
      var m = h(r, s);
      var q = c(m.ok);
      r = m.ng.concat(k(q, s))
    }
    return r
  }
});
Class(function GPU() {
  var d = this;
  var c = {};
  Mobile.iOS = (function() {
    if (Mobile.os != "iOS") {
      return ""
    }
    if (!Device.graphics.webgl) {
      return "legacy"
    }
    var e = Device.graphics.webgl.detect;
    if (e(["a9", "a10", "a11", "a12", "a13", "a14"]) || navigator.platform.toLowerCase().strpos("mac")) {
      return Mobile.phone ? "6s" : "ipad pro"
    }
    if (e("a8")) {
      return Mobile.phone ? "6x" : "ipad air 2, ipad mini 4"
    }
    if (e("a7")) {
      return Mobile.phone ? "5s" : "ipad air 1, ipad mini 2, ipad mini 3"
    }
    if (e(["sgx554", "sgx 554"])) {
      return Mobile.phone ? "" : "ipad 4"
    }
    if (e(["sgx543", "sgx 543"])) {
      return Mobile.phone ? "5x, 5c, 4s" : "ipad mini 1, ipad 2"
    }
    return "legacy"
  })();
  d.detect = function(e) {
    if (!Device.graphics.webgl) {
      return
    }
    return Device.graphics.webgl.detect(e)
  };
  d.detectAll = function() {
    if (!Device.graphics.webgl) {
      return
    }
    var e = true;
    for (var f = 0; f < arguments.length; f++) {
      if (!Device.graphics.webgl.detect(arguments[f])) {
        e = false
      }
    }
    return e
  };
  d.gpu = Device.graphics.webgl ? Device.graphics.webgl.gpu : "";

  function b(g) {
    if (c[g]) {
      return c[g]
    }
    if (!d.detect(g)) {
      return -1
    }
    try {
      var f = Number(d.gpu.split(g)[1].split(" ")[0]);
      c[g] = f;
      return f
    } catch (h) {
      return -1
    }
  }
  d.BLACKLIST = require("GPUBlacklist").match();
  d.T0 = (function() {
    if (Device.mobile) {
      return false
    }
    if (d.BLACKLIST) {
      return true
    }
    if (d.detectAll("intel", "hd")) {
      var e = b("hd graphics ");
      if (e == 0) {
        return true
      }
      if (e > -1) {
        return e > 1000 && e < 4000
      }
    }
    return false
  })();
  d.T1 = (function() {
    if (Device.mobile) {
      return false
    }
    if (d.T0) {
      return false
    }
    if (!d.detect(["nvidia", "amd"])) {
      return true
    }
    return false
  })();
  d.T2 = (function() {
    if (Device.mobile) {
      return false
    }
    if (d.T0) {
      return false
    }
    if (d.detect(["nvidia", "amd"])) {
      return true
    }
    return false
  })();
  d.T3 = (function() {
    if (Device.mobile) {
      return false
    }
    if (d.detect(["titan"])) {
      return true
    }
    return false
  })();
  d.MT0 = (function() {
    if (!Device.mobile) {
      return false
    }
    if (Mobile.iOS.strpos(["legacy", "ipad mini 1", "5x", "ipad 4"])) {
      return true
    }
    var f = b("adreno (tm) ");
    d.ADRENO = f;
    if (f > -1) {
      return f <= 330
    }
    var e = b("mali-t");
    if (e > -1) {
      return e < 628
    }
    return false
  })();
  d.MT1 = (function() {
    if (!Device.mobile) {
      return false
    }
    if (Mobile.iOS.strpos(["5s", "ipad air 1"])) {
      return true
    }
    if (Mobile.os == "Android" && !d.MT0) {
      return true
    }
    return false
  })();
  d.MT2 = (function() {
    if (!Device.mobile) {
      return false
    }
    if (Mobile.iOS.strpos(["6x", "ipad air 2"])) {
      return true
    }
    var e = b("adreno (tm) ");
    if (e > -1 && Mobile.os == "Android") {
      console.log(e);
      return e > 400
    }
    return false
  })();
  d.MT3 = (function() {
    if (!Device.mobile) {
      return false
    }
    if (Mobile.iOS.strpos(["6s", "ipad pro"])) {
      return true
    }
    if (d.detect("nvidia tegra") && Device.detect("pixel c")) {
      d.MT1 = false;
      d.MT2 = false;
      d.MT0 = false;
      return true
    }
    return false
  })();
  d.lt = function(e) {
    if (d.TIER > -1) {
      return d.TIER <= e
    }
    return false
  };
  d.gt = function(e) {
    if (d.TIER > -1) {
      return d.TIER >= e
    }
    return false
  };
  d.eq = function(e) {
    if (d.TIER > -1) {
      return d.TIER == e
    }
    return false
  };
  d.mobileEq = function(e) {
    if (d.M_TIER > -1) {
      return d.M_TIER == e
    }
    return false
  };
  d.mobileLT = function(e) {
    if (d.M_TIER > -1) {
      return d.M_TIER <= e
    }
    return false
  };
  d.mobileGT = function(e) {
    if (d.M_TIER > -1) {
      return d.M_TIER >= e
    }
    return false
  };
  for (var a in d) {
    if (a.charAt(0) == "T" && d[a] === true) {
      d.TIER = Number(a.charAt(1))
    }
    if (a.slice(0, 2) == "MT" && d[a] === true) {
      d.M_TIER = Number(a.charAt(2))
    }
  }
  d.OVERSIZED = !Device.mobile && d.TIER < 2 && Math.max(window.innerWidth, window.innerHeight) > 1440
}, "static");
Class(function Hardware() {
  var b = this;
  if (Mobile.os == "Android") {
    try {
      Mobile.browserVersion = Number(Device.agent.split("chrome/")[1].split(".")[0])
    } catch (a) {}
  }
  this.OLD_IE = Device.browser.ie && Device.browser.version < 13;
  this.iOSCHROME = (function() {
    var c = Device.detect("crios/") ? Number(Device.agent.split("crios/")[1].split(".")[0]) : 0;
    if (Mobile.os == "iOS" && Mobile.browser == "Chrome" && Mobile.version >= 9 && c >= 48) {
      return false
    }
    return Mobile.os == "iOS" && Mobile.browser == "Chrome"
  })();
  this.BAD_MBP = (function() {
    if (Device.system.os == "mac" && Device.pixelRatio == 2 && screen.width == 1280 && screen.height == 800) {
      return true
    }
    if (Device.system.os.strpos("windows") && !Device.browser.chrome) {
      return true
    }
  })();
  this.OLD_ANDROID = Mobile.os == "Android" && GPU.ADRENO < 530;
  this.BASIC_NAV = b.OLD_ANDROID || this.iOSCHROME;
  this.WEBGL = Device.graphics.webgl && !Mobile.iOS.strpos(["legacy", "ipad 4", "5x"]);
  this.FORCE_WEBGL_FAIL = false;
  this.FORCE_WEBGL_FALLBACK = false;
  this.MAC = Device.system.os == "mac";
  this.NEW_IPHONE = Device.graphics.webgl.detect(["a8", "a9", "a10", "a11"]);
  this.PERF_RESULTS = {};
  this.REDUCED_WORK = b.OLD_ANDROID || b.OLD_IE;
  this.LONG_LOAD = b.OLD_ANDROID || b.OLD_IE;
  this.PREVENT_TILT = false;
  this.FORCE_RETINA = Device.detect("pixel c") || b.NEW_IPHONE;
  this.HOME_VIDEO = (function() {
    if (GPU.mobileLT(1)) {
      return false
    }
    if (Mobile.os == "iOS" && Mobile.version >= 10) {
      return true
    }
    if (Mobile.os == "Android" && Mobile.browserVersion >= 53) {
      return true
    }
    return !Device.mobile && !Device.browser.ie
  })();
  this.ANIMATE_BG = !Device.browser.firefox && !b.OLD_IE && !b.iOSCHROME;
  this.SIMPLE_OPEN = Device.browser.safari || Device.browser.firefox || Device.browser.ie || b.iOSCHROME;
  this.DEDICATED_GRAPHICS = Device.graphics.webgl.detect(["nvidia", "amd"]);
  Hydra.ready(function() {
    if (b.OLD_IE) {
      b.WEBGL = false;
      b.REDUCED_WORK = true
    }
    nextFrame(function() {
      if (Mobile.os == "Android" && Mobile.tablet && Math.min(Stage.width, Stage.height) < 580) {
        Mobile.tablet = false;
        Mobile.phone = true;
        b.FORCE_PHONE = true;
        Render.start(function() {
          Mobile.tablet = false;
          Mobile.phone = true
        })
      }
    })
  });
  this.test = function() {
    if (!b.WEBGL) {
      b.ANIMATE_BG = false;
      b.SIMPLE_OPEN = true;
      b.LONG_LOAD = true;
      b.BASIC_NAV = true;
      b.REDUCED_WORK = true
    }
    var c = this.PERF_RESULTS;
    if (Device.mobile) {
      if (Mobile.os == "Android") {
        if (Device.detect("pixel c")) {
          this.BASIC_NAV = false
        }
        if (this.REDUCED_WORK && Mobile.phone) {
          this.REDUCED_CLOSE = true
        }
        this.ANIMATE_BG = false
      }
      if (Mobile.os == "iOS") {
        if (!this.NEW_IPHONE) {
          this.LONG_LOAD = true;
          this.REDUCED_WORK = true;
          this.BASIC_NAV = true;
          this.SIMPLE_OPEN = true;
          this.ANIMATE_BG = false
        }
        if (Mobile.version < 9) {
          this.HOME_VIDEO = false
        }
      }
      if (Mobile.iOS.strpos(["legacy", "sgx"])) {
        this.LONG_LOAD = true;
        this.REDUCED_WORK = true;
        this.BASIC_NAV = true;
        this.PREVENT_TILT = true;
        this.SIMPLE_OPEN = true;
        this.ANIMATE_BG = false
      }
      if (b.REDUCED_WORK) {}
    } else {
      if (b.FALLBACK_TRANSITION) {
        b.LONG_LOAD = true
      }
      if (!b.DEDICATED_GRAPHICS && !b.MAC) {
        this.ANIMATE_BG = false;
        this.SIMPLE_OPEN = true;
        if (Stage.width > 1500) {
          this.NO_VIDEO = true;
          this.HOME_VIDEO = false
        }
      }
    }
  };
  if (Mobile.os == "Android" && Mobile.browser != "Chrome") {
    window.location.href = "http://activetheory.net/fallback/unsupported.html"
  }
}, "static");
Class(function BasicPass() {
  Inherit(this, NukePass);
  var a = this;
  this.fragmentShader = ["varying vec2 vUv;", "uniform sampler2D tDiffuse;", "void main() {", "gl_FragColor = texture2D(tDiffuse, vUv);", "}"];
  this.init(this.fragmentShader)
});
Class(function Nuke(l, m) {
  Inherit(this, Component);
  var h = this;
  if (!m.renderer) {
    console.error("Nuke :: Must define renderer")
  }
  h.stage = l;
  h.renderer = m.renderer;
  h.camera = m.camera;
  h.scene = m.scene;
  h.rtt = m.rtt;
  h.enabled = m.enabled || true;
  h.passes = m.passes || [];
  var a = m.dpr || 1;
  var f, k, o, d, c;
  var n = {
    minFilter: THREE.LinearFilter,
    magFilter: THREE.LinearFilter,
    format: THREE.RGBAFormat,
    stencilBuffer: false
  };
  (function() {
    g();
    j()
  })();

  function g() {
    var r = h.stage.width * a;
    var p = h.stage.height * a;
    f = new THREE.WebGLRenderTarget(r, p, n);
    k = new THREE.WebGLRenderTarget(r, p, n);
    c = new THREE.OrthographicCamera(h.stage.width / -2, h.stage.width / 2, h.stage.height / 2, h.stage.height / -2, 1, 1000);
    o = new THREE.Scene();
    var q = new THREE.PlaneBufferGeometry(2, 2, 1, 1);
    d = new THREE.Mesh(q, new THREE.MeshBasicMaterial());
    o.add(d)
  }

  function e(q, p) {
    if (h.rtt) {
      h.renderer.render(q, p || h.camera, h.rtt)
    } else {
      h.renderer.render(q, p || h.camera)
    }
  }

  function j() {
    h.events.subscribe(HydraEvents.RESIZE, b)
  }

  function b() {
    var q = h.stage.width * a;
    var p = h.stage.height * a;
    if (f) {
      f.dispose()
    }
    if (k) {
      k.dispose()
    }
    f = new THREE.WebGLRenderTarget(q, p, n);
    k = new THREE.WebGLRenderTarget(q, p, n);
    c.left = h.stage.width / -2;
    c.right = h.stage.width / 2;
    c.top = h.stage.height / 2;
    c.bottom = h.stage.height / -2;
    c.updateProjectionMatrix()
  }
  h.add = function(q, p) {
    if (typeof p == "number") {
      h.passes.splice(p, 0, q);
      return
    }
    h.passes.push(q)
  };
  h.remove = function(p) {
    if (typeof p == "number") {
      h.passes.splice(p)
    } else {
      h.passes.findAndRemove(p)
    }
  };
  h.renderToTexture = function(q, p) {
    h.renderer.render(h.scene, h.camera, p || f, typeof q == "boolean" ? q : true)
  };
  h.render = function() {
    if (!h.enabled || !h.passes.length) {
      e(h.scene);
      return
    }
    if (!h.multiRender) {
      h.renderer.render(h.scene, h.camera, f, true)
    }
    var q = true;
    for (var p = 0; p < h.passes.length - 1; p++) {
      d.material = h.passes[p].pass;
      d.material.uniforms.tDiffuse.value = q ? f : k;
      h.renderer.render(o, c, q ? k : f);
      q = !q
    }
    d.material = h.passes[h.passes.length - 1].pass;
    d.material.uniforms.tDiffuse.value = q ? f : k;
    e(o, c)
  };
  h.set("dpr", function(p) {
    a = p || Device.pixelRatio;
    b()
  });
  this.get("dpr", function() {
    return a
  })
});
Class(function NukePass(b, a) {
  Inherit(this, Component);
  var c = this;
  this.init = function(d) {
    c = this;
    var f = d || this.constructor.toString().match(/function ([^\(]+)/)[1];
    var e = Array.isArray(d) ? d.join("") : null;
    c.uniforms = c.uniforms || {};
    c.uniforms.tDiffuse = {
      type: "t",
      value: null
    };
    c.pass = new THREE.ShaderMaterial({
      uniforms: c.uniforms,
      vertexShader: typeof a === "string" ? Shaders[f + ".vs"] : "varying vec2 vUv; void main() { vUv = uv; gl_Position = vec4(position, 1.0); }",
      fragmentShader: e || Shaders[f + ".fs"]
    });
    c.uniforms = c.pass.uniforms
  };
  this.set = function(d, e) {
    TweenManager.clearTween(c.uniforms[d]);
    this.uniforms[d].value = e
  };
  this.tween = function(e, f, g, h, d, k, j) {
    TweenManager.tween(c.uniforms[e], {
      value: f
    }, g, h, d, k, j)
  };
  if (typeof b === "string") {
    this.init(b)
  }
});
Class(function Raycaster(f) {
  Inherit(this, Component);
  var g = this;
  var e = new THREE.Vector3();
  var b = new THREE.Raycaster();
  var d = null;
  (function() {})();

  function a(j) {
    var h;
    if (Array.isArray(j)) {
      h = b.intersectObjects(j)
    } else {
      h = b.intersectObject(j)
    }
    if (d) {
      c()
    }
    return h
  }

  function c() {
    var h = d.geometry.vertices;
    h[0].copy(b.ray.origin.clone());
    h[1].copy(b.ray.origin.clone().add(b.ray.direction.clone().multiplyScalar(10000)));
    h[0].x += 1;
    d.geometry.verticesNeedUpdate = true
  }
  this.set("camera", function(h) {
    f = h
  });
  this.debug = function(k) {
    var j = new THREE.Geometry();
    j.vertices.push(new THREE.Vector3(-100, 0, 0));
    j.vertices.push(new THREE.Vector3(100, 0, 0));
    var h = new THREE.LineBasicMaterial({
      color: 16711680
    });
    d = new THREE.Line(j, h);
    k.add(d)
  };
  this.checkHit = function(k, h) {
    h = h || Mouse;
    var j = g.rect || Stage;
    e.x = (h.x / j.width) * 2 - 1;
    e.y = -(h.y / j.height) * 2 + 1;
    b.setFromCamera(e, f);
    return a(k)
  };
  this.checkFromValues = function(j, h, k) {
    b.set(h, k, 0, Number.POSITIVE_INFINITY);
    return a(j)
  }
});
Class(function ScreenProjection(c) {
  Inherit(this, Component);
  var d = this;
  var b = new THREE.Vector3();
  var a = new THREE.Vector3();
  (function() {})();
  this.set("camera", function(e) {
    c = e
  });
  this.unproject = function(e) {
    var f = d.rect || Stage;
    b.set((e.x / f.width) * 2 - 1, -(e.y / f.height) * 2 + 1, 0.5);
    b.unproject(c);
    var h = c.position;
    b.sub(h).normalize();
    var g = -h.z / b.z;
    a.copy(h).add(b.multiplyScalar(g));
    return a
  };
  this.project = function(f, e) {
    e = e || Stage;
    if (f instanceof THREE.Object3D) {
      f.updateMatrixWorld();
      b.set(0, 0, 0).setFromMatrixPosition(f.matrixWorld)
    } else {
      b.copy(f)
    }
    b.project(c);
    b.x = (b.x + 1) / 2 * e.width;
    b.y = -(b.y - 1) / 2 * e.height;
    return b
  }
});
Class(function RandomEulerRotation(b) {
  var e = this;
  var c = ["x", "y", "z"];
  var a;
  this.speed = 1;
  (function() {
    d()
  })();

  function d() {
    a = {};
    a.x = Utils.doRandom(0, 2);
    a.y = Utils.doRandom(0, 2);
    a.z = Utils.doRandom(0, 2);
    a.vx = Utils.doRandom(-5, 5) * 0.0025;
    a.vy = Utils.doRandom(-5, 5) * 0.0025;
    a.vz = Utils.doRandom(-5, 5) * 0.0025
  }
  this.update = function() {
    var h = Render.TIME;
    for (var g = 0; g < 3; g++) {
      var f = c[g];
      switch (a[f]) {
        case 0:
          b.rotation[f] += Math.cos(Math.sin(h * 0.25)) * a["v" + f] * e.speed;
          break;
        case 1:
          b.rotation[f] += Math.cos(Math.sin(h * 0.25)) * a["v" + f] * e.speed;
          break;
        case 2:
          b.rotation[f] += Math.cos(Math.cos(h * 0.25)) * a["v" + f] * e.speed;
          break
      }
    }
  }
});
Class(function Shader(f, b, a, d) {
  Inherit(this, Component);
  var e = this;
  (function() {
    // if (Hydra.LOCAL && a) {
    //   c()
    // }
    if (d) {
      e.uniforms = d.uniforms;
      e.attributes = d.attributes
    }
  })();

  function c() {
    Dev.expose(a, e)
  }
  this.get("material", function() {
    if (!d) {
      var g = {};
      g.vertexShader = Shaders.getShader(f + ".vs");
      g.fragmentShader = Shaders.getShader(b + ".fs");
      if (e.attributes) {
        g.attributes = e.attributes
      }
      if (e.uniforms) {
        g.uniforms = e.uniforms
      }
      d = new THREE.ShaderMaterial(g);
      d.shader = e
    }
    return d
  });
  this.set = function(g, h) {
    TweenManager.clearTween(e.uniforms[g]);
    if (typeof h !== "undefined") {
      e.uniforms[g].value = h
    }
    return e.uniforms[g].value
  };
  this.getValues = function() {
    var g = {};
    for (var h in e.uniforms) {
      g[h] = e.uniforms[h].value
    }
    return g
  };
  this.copyUniformsTo = function(h) {
    for (var g in e.uniforms) {
      h.uniforms[g] = e.uniforms[g]
    }
  };
  this.tween = function(h, j, k, l, g, n, m) {
    TweenManager.tween(e.uniforms[h], {
      value: j
    }, k, l, g, n, m)
  };
  this.clone = function(g) {
    return new Shader(f, b, g || a, e.material.clone())
  }
});
Class(function Utils3D() {
  var e = this;
  var d, c, a;
  var b = {};
  this.PATH = "";
  this.decompose = function(f, g) {
    f.matrixWorld.decompose(g.position, g.quaternion, g.scale)
  };
  this.createDebug = function(j, f) {
    var h = new THREE.IcosahedronGeometry(j || 40, 1);
    var g = f ? new THREE.MeshBasicMaterial({
      color: f
    }) : new THREE.MeshNormalMaterial();
    return new THREE.Mesh(h, g)
  };
  this.createRT = function(g, f) {
    var h = {
      minFilter: THREE.LinearFilter,
      magFilter: THREE.LinearFilter,
      format: THREE.RGBAFormat,
      stencilBuffer: false
    };
    return new THREE.WebGLRenderTarget(g, f, h)
  };
  this.getTexture = function(j, h) {
    if (!b[j]) {
      var f = new Image();
      f.crossOrigin = "";
      f.src = e.PATH + j;
      var g = new THREE.Texture(f);
      f.onload = function() {
        g.needsUpdate = true;
        if (g.onload) {
          g.onload(g, f);
          g.onload = null
        }
      };
      b[j] = g;
      if (!h) {
        g.minFilter = THREE.LinearFilter
      }
    }
    return b[j]
  };
  this.setInfinity = function(f) {
    var g = Number.POSITIVE_INFINITY;
    f.set(g, g, g);
    return f
  };
  this.freezeMatrix = function(f) {
    f.matrixAutoUpdate = false;
    f.updateMatrix()
  };
  this.getCubemap = function(k) {
    var j = "cube_" + (Array.isArray(k) ? k[0] : k);
    if (!b[j]) {
      var f = [];
      for (var h = 0; h < 6; h++) {
        var g = new Image();
        g.crossOrigin = "";
        g.src = e.PATH + (Array.isArray(k) ? k[h] : k);
        f.push(g);
        g.onload = function() {
          b[j].needsUpdate = true
        }
      }
      b[j] = new THREE.Texture();
      b[j].image = f;
      b[j].minFilter = THREE.LinearFilter
    }
    return b[j]
  };
  this.loadObject = function(f) {
    if (!d) {
      d = new THREE.ObjectLoader()
    }
    return d.parse(Hydra.JSON[f])
  };
  this.loadGeometry = function(f) {
    if (!c) {
      c = new THREE.JSONLoader()
    }
    if (!a) {
      a = new THREE.BufferGeometryLoader()
    }
    var g = Hydra.JSON[f];
    if (g.type == "BufferGeometry") {
      return a.parse(g)
    } else {
      return c.parse(g.data).geometry
    }
  };
  this.disposeAllTextures = function() {
    for (var f in b) {
      b[f].dispose()
    }
  };
  this.disableWarnings = function() {
    window.console.warn = function(g, f) {};
    window.console.error = function() {}
  }
}, "static");
Class(function InlineVideo(c) {
  Inherit(this, Component);
  var l = this;
  var m = {
    time: 0,
    duration: 0
  };
  this.width = c.width || 0;
  this.height = c.height || 0;
  this.loop = c.loop || true;
  this.fps = 1000 / (c.fps || 24);
  this.autoPlay = c.autoPlay || false;
  this.playing = false;
  this.loaded = false;
  var d, h, j, a;
  var q = 0;
  var n = 0;
  (function() {
    g();
    e();
    f();
    d.div.addEventListener("timeupdate", s, false)
  })();

  function g() {
    if (!c.src) {
      console.warn("src is required")
    }
    d = l.initClass(Video, {
      src: c.src,
      loop: c.loop,
      width: c.width,
      height: c.height
    });
    d.div.load();
    l.video = d
  }

  function e() {
    h = l.initClass(Canvas, l.width, l.height);
    j = h.context;
    l.element = h.object;
    l.div = h.div;
    l.object = h.object
  }

  function p(u, v, w) {
    q += w;
    if (q < l.fps) {
      return
    }
    q -= l.fps;
    n++;
    if (n * l.fps > a) {
      n = 0;
      if (!l.loop) {
        b()
      }
    }
    d.div.currentTime = (n * l.fps) / 1000
  }

  function r() {
    Render.start(p);
    l.playing = true
  }

  function b() {
    Render.stop(p);
    l.playing = false
  }

  function f() {
    d.div.addEventListener("canplay", o, false);
    d.div.addEventListener("timeupdate", s, false)
  }

  function o() {
    d.div.removeEventListener("canplay", o, false);
    l.loaded = true;
    a = d.div.duration * 1000;
    if (l.autoPlay) {
      r()
    }
    if (l.onLoad && typeof l.onLoad == "function") {
      l.onLoad()
    }
  }

  function s() {
    if (!l.loaded) {
      return
    }
    j.drawImage(d.div, 0, 0, h.width, h.height);
    m.time = d.div.currentTime;
    m.duration = d.div.duration;
    l.events.fire(HydraEvents.UPDATE, m, true)
  }

  function k(u, t) {
    l.width = u;
    l.height = t;
    h.size(u, t)
  }
  this.play = r;
  this.pause = b;
  this.size = k;
  this.render = s;
  this.seek = d.seek;
  this.stop = d.stop;
  this.ready = function() {
    return false
  }
});
Class(function KeyboardUtil() {
  Inherit(this, Component);
  var e = this;
  e.DOWN = "keyboard_down";
  e.PRESS = "keyboard_press";
  e.UP = "keyboard_up";
  (function() {
    Hydra.ready(c)
  })();

  function c() {
    __window.keydown(b);
    __window.keyup(d);
    __window.keypress(a)
  }

  function b(f) {
    e.events.fire(e.DOWN, f)
  }

  function d(f) {
    e.events.fire(e.UP, f)
  }

  function a(f) {
    e.events.fire(e.PRESS, f)
  }
}, "static");
Module(function Randomizr() {
  this.exports = b;
  var a = [];

  function b(f, c, d) {
    var e = Utils.doRandom(f, c);
    if (c > 3) {
      while (a.indexOf(e) > -1) {
        e = Utils.doRandom(f, c)
      }
      a.push(e);
      if (a.length > d) {
        a.shift()
      }
    }
    return e
  }
});
Class(function ScrollUtil() {
  Inherit(this, Component);
  var d = this;
  var m;
  var e = [];
  var j = false;
  var b = {};
  var f = {};
  var k = {};
  d.lerp = Device.mobile ? 1 : 0.075;
  (function() {
    c();
    Hydra.ready(g)
  })();

  function c() {
    if (Device.browser.ie) {
      return m = 2
    }
    if (Device.system.os == "mac") {
      if (Device.browser.chrome || Device.browser.safari) {
        m = 40
      } else {
        m = 1
      }
    } else {
      if (Device.browser.chrome) {
        m = 15
      } else {
        m = 0.5
      }
    }
  }

  function g() {
    // scroll down from intro
    // if (!Device.mobile) {
    //   __document.bind("wheel", h);
    //   __document.bind("mousewheel", h);
    //   d.events.subscribe(KeyboardUtil.DOWN, a)
    // }
  }

  function a(o) {
    var n = 750;
    if (o.code == 40) {
      h({
        deltaY: n,
        deltaX: 0,
        key: true
      })
    }
    if (o.code == 39) {
      h({
        deltaY: 0,
        deltaX: n,
        key: true
      })
    }
    if (o.code == 38) {
      h({
        deltaY: -n,
        deltaX: 0,
        key: true
      })
    }
    if (o.code == 37) {
      h({
        deltaY: 0,
        deltaX: -n,
        key: true
      })
    }
  }

  function h(q) {
    if (d.stopDefault) {
      return
    }
    if (q.preventDefault && !d.disabled) {
      q.preventDefault()
    }
    if (Device.browser.ie && !q.key) {
      q = window.event || q
    }
    var o = q.wheelDelta || -q.detail;
    if (typeof q.deltaX !== "undefined" || q.key) {
      b.x = -q.deltaX * 0.4;
      b.y = q.deltaY * 0.4
    } else {
      b.x = 0;
      var r = Math.ceil(-o / m);
      if (q.preventDefault) {
        q.preventDefault()
      }
      if (r <= 0) {
        r -= 1
      }
      r = Utils.clamp(r, -60, 60);
      b.y = r * 3.5
    }
    var n = Render.TIME - f.time;
    var p = b.y - f.y;
    f.time = Render.TIME;
    f.x = b.x;
    f.y = b.y;
    if (k.allow) {
      if (n == 0 && Math.abs(p) > 5 || n > 50) {
        k.allow = false;
        k.prevent = false;
        k.y = null
      }
    }
    if (k.prevent) {
      return
    }
    if (k.y != null) {
      if (k.y < 0) {
        if (b.y <= f.y) {
          k.prevent = true;
          return
        }
      } else {
        if (b.y >= f.y) {
          k.prevent = true;
          return
        }
      }
    }
    if (Math.abs(b.y) < 1) {
      d.lerp = 0.2
    }
    defer(function() {
      l(b)
    })
  }

  function l(o) {
    if (Device.browser.firefox && o.x == 0 && Math.abs(o.y) < 20) {
      o.y *= 5
    }
    for (var n = 0; n < e.length; n++) {
      e[n](o)
    }
  }
  this.reset = function() {
    this.value = 0
  };
  this.link = function(n) {
    e.push(n)
  };
  this.unlink = function(o) {
    var n = e.indexOf(o);
    if (n > -1) {
      e.splice(n, 1)
    }
  };
  this.block = function() {
    k.x = f.x;
    k.y = f.y
  };
  this.unblock = function() {
    if (!k.y) {
      return
    }
    k.allow = true
  }
}, "Static");
Class(function WiggleBehavior(a) {
  Inherit(this, Component);
  var e = this;
  var c = Utils.toRadians(Utils.doRandom(0, 360));
  var b = new Vector3();
  var d = new Vector3();
  this.target = b;
  this.scale = 0.1;
  this.alpha = 0.025;
  this.speed = 1;
  this.zMove = 2;
  this.enabled = true;
  (function() {
    if (a) {
      d.copyFrom(a)
    }
  })();
  this.update = function() {
    if (!e.enabled || e.disabled) {
      return
    }
    var f = window.Render ? Render.TIME : Date.now();
    b.x = Math.cos(c + f * (0.00075 * e.speed)) * (c + Math.sin(f * (0.00095 * e.speed)) * 200);
    b.y = Math.sin(Math.asin(Math.cos(c + f * (0.00085 * e.speed)))) * (Math.sin(c + f * (0.00075 * e.speed)) * 150);
    b.x *= Math.sin(c + f * (0.00075 * e.speed)) * 2;
    b.y *= Math.cos(c + f * (0.00065 * e.speed)) * 1.75;
    b.x *= Math.cos(c + f * (0.00075 * e.speed)) * 1.1;
    b.y *= Math.sin(c + f * (0.00025 * e.speed)) * 1.15;
    b.z = Math.sin(c + b.x * 0.0025) * (100 * e.zMove);
    b.multiply(e.scale);
    b.add(d);
    if (a) {
      if (e.ease) {
        a.interp(b, e.alpha, e.ease)
      } else {
        a.lerp(b, e.alpha)
      }
    }
  };
  this.copyOrigin = function() {
    d.copyFrom(a)
  }
});
Class(function Data() {
  Inherit(this, Model);
  Inherit(this, PushState);
  var e = this;
  var c;
  (function() {
    d()
  })();

  function b(f) {
    return Config.PAGES.indexOf(f.toLowerCase()) > -1 ? f : "home"
  }

  function d() {
    e.dispatcher.events.add(HydraEvents.UPDATE, a)
  }

  function a(f) {
    e.events.fire(ATEvents.STATE_CHANGE, f)
  }
  this.getPage = function() {
    if (Global.PLAYGROUND) {
      if (Hydra.HASH.toLowerCase().strpos("lab")) {
        return "lab"
      }
      return Hydra.HASH.split("playground/")[1].split("/")[0].toLowerCase()
    }
    var f = (c || this.getState()).split("/");
    return b(f[0])
  };
  this.getDeeplink = function() {
    var f = this.getState().split("/");
    return f[1]
  };
  this.getState = function() {
    return this._getState()
  };
  this.setState = function(f) {
    c = f;
    if (Global.PLAYGROUND) {
      return
    }
    if (f != this.getState() && f != "work" && f != "lab") {
      this._setState(f)
    }
    GATracker.trackPage(f)
  };
  this.replaceState = function(f) {
    c = f;
    if (Global.PLAYGROUND) {
      return
    }
    if (!Hydra.LOCAL && f != this.getState()) {
      this._replaceState(f)
    }
  }
}, "static");

Data.Class(function Home() {
  Inherit(this, Model);
  var b = this;
  var a;
  (function() {})();
  this.init = function(f) {
    a = [];
    for (var c = 0; c < f.length; c++) {
      var h = f[c];
      var e = {};
      e.deeplink = h.deeplink;
      e.title = h.title;
      var g = h.timecode.split("/");
      e.timeIn = parseFloat(g[0]);
      e.timeOut = parseFloat(g[1]);
      a.push(e)
    }
  };
  this.getProjects = function() {
    return a
  }
});

Class(function Container() {
  Inherit(this, Controller);
  var g = this;
  var p;
  var n, f, j;
  var e;
  (function() {
    c();
    o();
    h()
  })();

  function c() {
    p = g.container;
    p.size("100%");
    Stage.add(p)
  }

  function o() {
    var r = g.initClass(Loader);
    r.events.add(Loader.LOAD_GL, l);
    r.events.add(HydraEvents.COMPLETE, function() {
      // OuterUI.instance().animateIn();
      var s = Data.getPage();
      b(s);
      r.animateOut(function() {
        r.destroy();
        // n.forceComposite()
      })
    })
  }

  function b(r) {
    // switch (r) {
      // case "work":
      // case "lab":
      //   e = g.initClass(Work);
      //   break;
      // case "about":
      //   // Data.setTitle("About / Active Theory");
      //   e = g.initClass(About);
      //   break;
      // case "home":
      // default:
        // Data.setTitle("Active Theory / Creative Digital Production / Venice, CA");
        e = f;
        // break
    // }
    if (!Hardware.FALLBACK_TRANSITION) {
      e.element.willChange(true)
    }
    e.name = r;
    e.activate()
  }

  function m() {
    g.delayedCall(j.start, 200);
    g.delayedCall(j.animateOut, 300, function() {
      j.stop()
    })
  }

  function k(r) {
    e.deactivate();
    j.color(16777215);
    j.position(0);
    j.animateIn(function() {
      if (!e.persist) {
        e.destroy()
      } else {
        e.hide(true)
      }
      b(r);
      g.delayedCall(function() {
        j.animateOut(function() {
          j.stop();
          Data.unlock()
        })
      }, 400)
    })
  }

  function h() {
    g.events.subscribe(ATEvents.STATE_CHANGE, d);
    g.events.subscribe(HydraEvents.BROWSER_FOCUS, q);
    // g.events.subscribe(KeyboardUtil.UP, a)
  }

  function a(r) {
    // if (r.keyCode == 27 && g.active("menu")) {
    //   g.menu("close");
    //   Dispatch.find(HamburgerButton, "close")()
    // }
  }

  function q(r) {
    // if (r.type == "focus") {
    //   n && n.forceComposite()
    // }
  }

  function d(r) {
    if (!e) {
      return
    }
    if (r.split[0] == e.name) {
      return
    }
    k(r.split[0])
  }

  function l(u) {
    // var v = function() {
    //   j = g.initClass(Transition);
    //   return Pact.create(j.test)
    // };
    // var s = function() {
    //   n = g.initClass(Menu);
    //   return this.exec(n.test)
    // };
    var r = function() {
      Hardware.test();
      f = g.initClass(Home);
      // return this.exec(f.test)
    };
    var t = function() {
      u.callback()
    };
    // v()/*.then(s)/**/.then(r).then(t);

    r();
    t();
  }
  // this.menu = function(s, r, t) {
  //   if (s == "open") {
  //     g.active("menu", true);
  //     Data.lock();
  //     e.deactivate();
  //     n.start();
  //     n.animateIn();
  //     defer(function() {
  //       if (!Hardware.FALLBACK_TRANSITION) {
  //         e.element.tween({
  //           scale: 0.9
  //         }, 700, "easeOutQuart")
  //       }
  //     });
  //     j.menu = true;
  //     j.color(Config.BG_COLOR);
  //     j.animateIn(function() {
  //       j.stop();
  //       j.menu = false;
  //       if (e.hide) {
  //         e.hide()
  //       }
  //     })
  //   } else {
  //     if (!Hardware.FALLBACK_TRANSITION) {
  //       g.active("menu", false);
  //       // n.animateOut(function() {
  //       //   n.stop()
  //       // }, r);
  //       defer(function() {
  //         if (!Hardware.FALLBACK_TRANSITION) {
  //           e.element.tween({
  //             scale: 1
  //           }, 800, "easeOutQuart", 100)
  //         }
  //       });
  //       e.activate();
  //       g.delayedCall(function() {
  //         j.menu = true;
  //         j.animateOut(function() {
  //           j.menu = false;
  //           j.stop();
  //           Data.unlock()
  //         })
  //       }, r && !Hardware.FALLBACK_TRANSITION ? 250 : 0)
  //     } else {
  //       g.active("menu", false);
  //       j.show();
  //       if (!Hardware.WEBGL) {
  //         n.element.setZ(99999)
  //       }
  //       n.animateOut(function() {
  //         n.stop();
  //         n.element.setZ(20);
  //         if (t) {
  //           t()
  //         }
  //         e.activate();
  //         if (r) {
  //           j.animateInLoader()
  //         }
  //         g.delayedCall(function() {
  //           j.menu = true;
  //           j.animateOut(function() {
  //             j.menu = false;
  //             j.stop();
  //             Data.unlock()
  //           })
  //         }, Hardware.LONG_LOAD && r ? 1000 : (r ? 200 : 50))
  //       }, r)
  //     }
  //   }
  // };
  // this.page = function(r) {
  //   if (r == Data.getPage()) {
  //     if (g.active("menu")) {
  //       Dispatch.find(HamburgerButton, "close")();
  //       g.menu("close")
  //     }
  //     return
  //   }
  //   Data.lock();
  //   Data.setState(r);
  //   // Dispatch.find(HamburgerButton, "close")();
  //   if (g.active("menu")) {
  //     e.deactivate();
  //     if (!e.persist) {
  //       e.destroy()
  //     }
  //     if (!Hardware.FALLBACK_TRANSITION) {
  //       b(r)
  //     }
  //     g.menu("close", true, function() {
  //       if (Hardware.FALLBACK_TRANSITION) {
  //         b(r)
  //       }
  //     })
  //   } else {
  //     j.needsLoader = true;
  //     k(r);
  //     j.needsLoader = false;
  //     defer(function() {
  //       if (!Hardware.FALLBACK_TRANSITION) {
  //         e.element.clearTransform()
  //       }
  //     })
  //   }
  // }
}, "singleton");
Class(function Home() {
  Inherit(this, Controller);
  var g = this;
  var j;
  var f, c, l, a, k;
  this.persist = true;
  (function() {
    e();
    h();
    d();
    Dispatch.register(g, "launchWork")
  })();

  function e() {
    j = g.container;
    j.size("100%");
    Stage.add(j);
    if (!Global.PLAYGROUND) {
      j.hide()
    }
  }

  function h() {
    c = HomeVideoController.instance();
    f = g.initClass(HomeSizzle, c.video);
    // l = g.initClass(HomeInfo, c.video);
    // a = g.initClass(HomeLogo);
    // k = g.initClass(HomeButton);
    defer(function() {
      if (!Device.mobile) {
        return
      }
      Mouse.x = Stage.width / 2;
      Mouse.y = Stage.height / 2;
      Dispatch.find(HomeSizzleParticleBehavior, "forceRadius")("over")
    });
    if (Global.PLAYGROUND) {
      f.start()
    }
  }

  function d() {
    g.events.subscribe(HydraEvents.RESIZE, b)
  }

  function b() {
    // a.resize()
  }
  this.test = function(m) {
    f.start();
    g.delayedCall(function() {
      f.stop();
      c.reset();
      m()
    }, 50)
  };
  this.activate = function() {
    c.play();
    f.start();
    // g.delayedCall(k.animateIn, 500);
    g.delayedCall(function() {
      if (Device.mobile) {
        Dispatch.find(HomeSizzleParticleBehavior, "forceRadius")("out")
      }
    }, 250);
    j.show()
  };
  this.deactivate = function() {
    f.stop();
    c.pause()
  };
  this.hide = function(m) {
    j.hide();
    if (m) {
      c.reset();
      // k.reset()
    }
  };
  this.launchWork = function() {
    var m = c.getCurrent() && !Global.CLICK_BUTTON ? c.getCurrent().deeplink : "work/null";
    var n = m.split("/");
    Global.CLICK_BUTTON = false;
    var o = n[0] == "work" ? Data.Work : Data.Lab;
    Container.instance().page(n[0]);
    o.setCurrent(n[1])
  }
});
Class(function HomeSizzle(a) {
  Inherit(this, Controller);
  var g = this;
  var c;
  var b, f;
  (function() {
    e();
    d()
  })();

  function e() {
    c = g.container;
    c.size("100%")
  }

  function d() {
    f = g.initClass(Hardware.WEBGL ? HomeSizzleGL : HomeSizzleBasic, a)
  }
  this.start = function() {
    f.start()
  };
  this.stop = function() {
    f.stop();
    a && a.pause()
  }
});
Class(function HomeVideoController() {
  Inherit(this, Component);
  var g = this;
  var a;
  var c = null;
  var d = null;
  this.elapsed = 0;
  (function() {
    if (Hardware.HOME_VIDEO) {
      b()
    }
  })();

  function b() {
    if(USE_VIDEO) {
      var j = "home";
      var k = Device.browser.firefox ? "webm" : "mp4";
      var h = Device.browser.safari || Device.mobile || Device.browser.ie ? Config.PROXY : Config.CDN;
      a = g.initClass(Video, {
        src: h + "assets/videos/" + j + "." + k,// + "?9",
        loop: true,
        width: 1024,
        height: 576,
        fps: 30
      });
      a.div.type = "video/" + k;
      a.volume(0);
      a.object.attr("muted", true);
      a.object.attr("playsinline", true);
      a.object.attr("webkit-playsinline", true);
      a.div.crossOrigin = "";
      a.events.add(HydraEvents.UPDATE, e);
    }
    g.video = a
  }

  function f(h) {
    g.events.fire(ATEvents.SIZZLE_CHANGE, h)
  }

  function e(l) {
    var j = Data.Home.getProjects();
    if (!j) {
      return
    }
    if (a.texture) {
      if (l.time != d && a.ready()) {
        a.texture.needsUpdate = true
      }
      d = l.time
    }
    for (var h = 0; h < j.length; h++) {
      var k = j[h];
      if (l.time > k.timeIn && l.time < k.timeOut) {
        if (c != k) {
          f(k)
        }
        c = k;
        g.elapsed = Utils.range(l.time, k.timeIn, k.timeOut, 0, 1)
      }
    }
  }
  this.play = function() {
    a && a.play()
  };
  this.pause = function() {
    a && a.pause()
  };
  this.getCurrent = function() {
    return c
  };
  this.reset = function() {
    if (a) {
      a.seek(0);
      a.stop()
    }
  }
}, "singleton");
Class(function Loader() {
  Inherit(this, Controller);
  var s = this;
  var r;
  var b, u, l;
  var d = 0;
  var g = 0;
  (function() {
    t();
    h();
    defer(c);
    Render.start(v)
  })();

  function t() {
    r = s.container;
    r.percent = 0;
    // r.size("100%").setZ(9999999).bg("#fff");
    // Stage.add(r)
  }

  function h() {
    b = s.initClass(LoaderView)
  }

  function c() {
    // AssetUtil.prependPath(Config.PROXY, ["outline.png", "still.jpg", "brightness.jpg", "frame2.jpg"]);
    // FontLoader.loadFonts(["OpenSans", "OpenSansLight", "OpenSansSemi", "OpenSansBold"]);
    u = s.initClass(AssetLoader, j());
    u.events.add(HydraEvents.PROGRESS, f);
    u.events.add(HydraEvents.COMPLETE, q);
    u.add(2);
    u.time = Date.now();
    a()
  }

  function a() {
    // Data.loadData(Config.DATA_API, function() {
    //   u.trigger(2);
    //   s.dataLoaded && s.dataLoaded()
    // })
  }

  function j() {
    AssetUtil.exclude(["common/logos", "common/awards"]);
    if (Hardware.WEBGL) {
      AssetUtil.exclude("menu/fallback")
    }
    var w = AssetUtil.loadAssets(["shaders"]);
    // var x = Data.getPage();
    // switch (x) {
    //   case "home":
    //     if (Hardware.HOME_VIDEO) {
    //       AssetUtil.exclude("home/still.jpg")
    //     } else {
    //       AssetUtil.exclude("home/frame2.jpg")
    //     }
        HomeVideoController.instance();
        // w = w.concat(e());
    // return e();
        // break;
      // case "work":
      // case "lab":
      //   w = w.concat(o(x));
      //   break;
      // case "about":
      //   w = w.concat(n());
      //   break
    // }
    return w
  }

  // function e() {
  //   HomeVideoController.instance();
  //   if (Hardware.WEBGL) {
  //     AssetUtil.exclude("home/still")
  //   }
  //   return AssetUtil.loadAssets("home")
  // }

  function o(w) {
    defer(function() {
      u.add(3)
    });
    var x = w == "work" ? Data.Work : Data.Lab;
    s.dataLoaded = function() {
      x.loadInitialAssets(function() {
        x.loadAllAssets();
        u.trigger(3)
      })
    };
    AssetUtil.removeExclude("common/awards");
    return AssetUtil.loadAssets("common/awards")
  }

  function n() {
    return AssetUtil.loadAssets("about")
  }

  function m() {
    var w = [];
    // w = w.concat(Data.Work.loadAllAssets(true));
    // w = w.concat(Data.Lab.loadAllAssets(true));
    w = w.concat(AssetUtil.loadAssets(["home"/*, "about"/**/]));
    AssetLoader.loadAssets(w);
    HomeVideoController.instance()
  }

  function v() {
    b.updatePath(r.percent, s.complete)
  }

  function f(w) {
    d = w.percent;
    // TweenManager.tween(r, {
    //   percent: w.percent
    // }, 800, "easeInOutSine", k)

    k();
  }

  function k() {
    // if (l) {
      s.events.fire(Loader.LOAD_GL, {
        callback: p
      })
    // }
  }

  function q(x) {
    var w = Date.now() - u.time;
    l = true;
    Timer.create(m, 4000)
  }

  function p() {
    d = 1;
    s.complete = true;
    s.events.fire(HydraEvents.COMPLETE)
  }
  this.animateOut = function(y) {
    var x = function() {
      r.tween({
        opacity: 0
      }, 700, "easeOutSine", w, function() {
        if (y) {
          y()
        }
        Render.stop(v)
      })
    };
    if (Data.getPage() == "home") {
      TweenManager.tween(r, {
        percent: 0
      }, 600, "easeOutExpo");
      x()
    } else {
      var w = Hardware.OLD_IE || Hardware.LONG_LOAD ? 600 : 0;
      b.element.tween({
        opacity: 0,
        scale: 1.05
      }, 300, "easeOutCubic", w, x)
    }
  }
}, function() {
  Loader.LOAD_GL = "load_gl"
});
Class(function Playground() {
  Inherit(this, Controller);
  var d = this;
  var b;
  (function() {
    c();
    a()
  })();

  function c() {
    b = d.container;
    b.size("100%");
    Stage.add(b);
    Global.PLAYGROUND = true
  }

  function a() {
    var e = Hydra.HASH.split("layground/")[1].split("/")[0];
    if (!window[e]) {
      throw "No class " + e + " found"
    }
    var f = window[e];
    if (f.instance) {
      f.instance()
    } else {
      clss = d.initClass(f)
    }
    if (clss && clss.activate) {
      clss.activate()
    }
  }
}, "singleton");
Class(function Transition() {
  Inherit(this, View);
  var f = this;
  var e;
  var c, d, a;
  (function() {
    b()
  })();

  function b() {
    c = f.element;
    c.size("100%").setZ(9999).hide();
    d = c.create("solid");
    d.size("100%").bg("#fff").setZ(2).css({
      opacity: 0
    });
    a = c.create("loader");
    a.size(40, 40).bg("assets/images/common/loader.png").center().hide().setZ(2);
    var g = new CSSAnimation();
    g.duration = 1000;
    g.loop = true;
    g.frames = [{
      rotation: 0
    }, {
      rotation: 360
    }];
    g.applyTo(a);
    g.play();
    if (Global.PLAYGROUND) {
      defer(function() {
        f.start()
      })
    }
    e = f.initClass(TransitionView)
  }
  this.test = function(g) {
    defer(g)
  };
  this.color = function(g) {};
  this.position = function(g) {
    if (f.menu && !Hardware.WEBGL) {
      return
    }
    c.show();
    e.position(g);
    d.stopTween().css({
      opacity: g
    })
  };
  this.animateIn = function(g) {
    if (!f.menu) {
      c.show()
    }
    if ((!f.menu && !MenuGL.USE_TEXTURE) || Hardware.FALLBACK_TRANSITION) {
      c.show();
      if (f.menu) {
        d.bg(Config.BG_COLOR)
      } else {
        d.bg("#fff")
      }
      d.css({
        opacity: 0
      }).tween({
        opacity: 1
      }, 300, "easeOutCubic", g);
      // if (Hardware.LONG_LOAD && f.needsLoader) {
      //   a.show().css({
      //     opacity: 0
      //   }).tween({
      //     opacity: 1
      //   }, 300, "easeOutSine", 400)
      // }
      // if (!f.needsLoader) {
        a.hide()
      // }
    } else {
      e.start();
      e.animateIn(g)
    }
  };
  this.animateOut = function(j) {
    f.clearTimers();
    if (!f.menu) {
      c.show()
    }
    if ((!f.menu && !MenuGL.USE_TEXTURE) || Hardware.FALLBACK_TRANSITION) {
      c.show();
      var g = Hardware.LONG_LOAD ? 700 : 0;
      var h = Hardware.OLD_IE || Mobile.os == "Android" || Hardware.SIMPLE_OPEN ? g : 100;
      if (!Hardware.WEBGL && f.menu) {
        g *= 1.2
      }
      a.tween({
        opacity: 0
      }, 300, "easeOutCubic", function() {
        a.hide()
      });
      if (f.menu) {
        d.tween({
          opacity: 0
        }, 300, "easeOutSine", h, j)
      } else {
        d.tween({
          opacity: 0
        }, 750, "easeOutCubic", g, j)
      }
    } else {
      e.start();
      e.animateOut(j)
    }
  };
  this.animateInLoader = function() {
    if (Hardware.LONG_LOAD) {
      a.show().css({
        opacity: 0
      }).tween({
        opacity: 1
      }, 300, "easeOutSine")
    }
  };
  this.show = function() {
    c.show().css({
      opacity: 1
    })
  };
  this.start = function() {
    if (f.menu && !Hardware.WEBGL) {
      return
    }
    if ((!f.menu && !MenuGL.USE_TEXTURE) || Hardware.FALLBACK_TRANSITION) {
      c.show()
    } else {
      e.start()
    }
  };
  this.stop = function() {
    if (Hardware.LONG_LOAD) {
      a.hide()
    }
    c.hide();
    e.stop()
  }
});
Class(function Filter() {
  Inherit(this, Controller);
  var l = this;
  var q, f, n;
  var k, r, e, o;
  (function() {
    g();
    d();
    c();
    m();
    Dispatch.register(l, "forceClose")
  })();

  function g() {
    q = l.container;
    q.size("100%").css({
      top: 0,
      right: 0
    }).hide().setZ(9);
    q.hidden = true;
    if (!Device.mobile) {
      q.mouseEnabled(false)
    }
    if (Device.mobile) {
      n = q.create(".hit");
      n.size(500, 180).css({
        right: 300,
        top: 0
      })
    }
  }

  function d() {
    k = l.initClass(FilterView)
  }

  function c() {
    if (Mobile.tablet && !Hardware.BASIC_NAV) {
      q.transform({
        x: 200
      });
      r = l.initClass(TweenTimeline);
      r.add(q, {
        x: 0
      }, 600, "linear");
      Render.start(p)
    }
  }

  function j() {
    return;
    if (l.disabled || !WorkNav.OPEN || WorkNavOpenMechanic.TRANSITION) {
      return
    }
    if (Mouse.x > Stage.width - 800 && Mouse.x < Stage.width - 500 && Mouse.y < 200) {
      if (!k.visible) {
        k.animateIn();
        l.disabled = true;
        l.delayedCall(function() {
          l.disabled = false
        }, 300)
      }
    } else {
      if (k.visible && !k.hovered) {
        k.animateOut();
        l.delayedCall(function() {
          l.disabled = false
        }, 300)
      }
    }
  }

  function p() {
    r.elapsed = WorkNavOpenMechanic.AMOUNT || 0;
    r.update();
    if (r.elapsed > 0.99) {
      if (o) {
        o = false;
        b({
          type: "open"
        })
      }
    }
    if (r.elapsed > 0 && q.hidden) {
      q.hidden = false;
      q.visible()
    } else {
      if (r.elapsed == 0 && !q.hidden) {
        q.hidden = true;
        q.invisible()
      }
    }
  }

  function m() {
    l.events.subscribe(WorkNavOpenMechanic.OPEN, b);
    l.events.subscribe(WorkNavOpenMechanic.MOBILE_OPEN, h);
    l.events.subscribe(WorkNavOpenMechanic.TOUCH_CLOSE, a)
  }

  function a() {
    if (!e) {
      b({
        type: "close"
      })
    }
  }

  function h(s) {
    if (s.type == "open") {
      o = true
    } else {
      o = false;
      if (!e) {
        b({
          type: "close"
        })
      }
    }
  }

  function b(s) {
    if (s.type == "open") {
      e = false;
      q.show();
      k.element.transform({
        x: 200
      }).css({
        opacity: 0
      }).tween({
        x: 0,
        opacity: 1
      }, 600, "easeOutQuart");
      k.line.transform({
        x: 200
      }).tween({
        x: 0
      }, 600, "easeOutQuart", 100);
      k.animateIn()
    } else {
      e = true;
      k.animateOut();
      k.element.tween({
        opacity: 0,
        x: 100
      }, 300, "easeOutCubic");
      k.line.tween({
        x: 50
      }, 300, "easeOutCubic", function() {
        q.hide()
      })
    }
  }
  this.onDestroy = function() {
    Render.stop(p);
    Render.stop(j)
  };
  this.forceClose = function() {
    if (!Device.mobile) {
      return
    }
  }
});
Class(function Work(f) {
  Inherit(this, Controller);
  var g = this;
  var m, l;
  var d, n, c, h;
  var b;
  (function() {
    e();
    k();
    j()
  })();

  function e() {
    m = g.container;
    m.size("100%").css({
      overflow: "hidden"
    })
  }

  function k() {
    b = Data.getPage() == "work" ? Data.Work : Data.Lab;
    b.setup();
    d = g.initClass(WorkLayoutManager, b);
    n = g.initClass(WorkInteraction, d);
    if (!Mobile.phone) {
      l = m.create(".overlay");
      l.size("100%").bg("#000").css({
        opacity: 0
      }).mouseEnabled(false).setZ(8)
    }
    c = g.initClass(WorkNav, b, [Container.instance().element]);
    if (!Mobile.phone && Data.getPage() == "work") {
      h = g.initClass(Filter)
    }
  }

  function j() {
    g.events.subscribe(WorkNavOpenMechanic.OPEN, a);
    g.events.subscribe(WorkNavOpenMechanic.MOBILE_OPEN, a)
  }

  function a(o) {
    if (l) {
      l.tween({
        opacity: o.type == "open" ? 0.6 : 0
      }, 400, "easeOutSine")
    }
  }
  this.deactivate = function() {
    g.clearTimers();
    g.delayedCall(function() {
      m.hide();
      c.element.hide()
    }, Hardware.FALLBACK_TRANSITION ? 300 : 500)
  };
  this.activate = function() {
    g.clearTimers();
    m.show();
    c.element.show()
  };
  this.onDestroy = function() {
    b.teardown()
  }
});
Class(function WorkInteraction(c) {
  Inherit(this, Component);
  var e = this;
  var o, p, g;
  (function() {
    h();
    f()
  })();

  function h() {
    o = e.initClass(Interaction.Input, c.element);
    o.onStart = b;
    o.onUpdate = j;
    o.onEnd = m
  }

  function f() {
    if (!Device.mobile) {
      Stage.bind("mousemove", d);
      Stage.bind("click", n);
      e.events.subscribe(KeyboardUtil.UP, a);
      ScrollUtil.link(l)
    }
  }

  function l(r) {
    if (WorkLayoutManager.TRANSITION || WorkNav.OPEN || Menu.OPEN) {
      return
    }
    TweenManager.clearTween(c);
    c.movement += r.y * 0.002;
    if (Math.abs(c.movement) > 0.2) {
      var q = c.movement < 0 ? -1 : 1;
      c.transition(q);
      ScrollUtil.block()
    } else {
      clearTimeout(g);
      g = e.delayedCall(k, 100)
    }
  }

  function k() {
    TweenManager.tween(c, {
      movement: 0
    }, 700, "easeOutCubic")
  }

  function a(q) {
    if (WorkNav.OPEN || WorkLayoutManager.TRANSITION || Menu.OPEN) {
      return
    }
    if (q.keyCode == 40) {
      c.transition(1)
    } else {
      if (q.keyCode == 38) {
        c.transition(-1)
      }
    }
  }

  function b(q) {
    WorkInteraction.TOUCHING = true;
    if (WorkNav.OPEN || WorkNavOpenMechanic.TRANSITION || WorkLayoutManager.TRANSITION || Menu.OPEN) {
      return
    }
    TweenManager.clearTween(c);
    c.alpha = 1;
    p = true
  }

  function d(r) {
    if (p || WorkNav.OPEN || WorkNavOpenMechanic.DRAGGING || WorkLayoutManager.TRANSITION || Menu.OPEN) {
      return
    }
    c.alpha = 0.07;
    var q;
    if (r.y <= 40) {
      q = Utils.range(r.y, 0, 40, 1, 0);
      c.movement = -q * 0.05
    } else {
      if (r.y >= Stage.height - 40) {
        q = Utils.range(r.y, Stage.height - 40, Stage.height, 0, 1);
        c.movement = q * 0.05
      } else {
        c.movement = 0
      }
    }
  }

  function j(s, r) {
    if (WorkNav.OPEN || WorkNavOpenMechanic.DRAGGING || WorkNavOpenMechanic.TRANSITION || WorkLayoutManager.TRANSITION || Menu.OPEN) {
      return
    }
    var q = (s.y < 0 ? 1 : -1) * Math.abs(s.y) / Stage.height;
    c.movement = q;
    WorkInteraction.TOUCHING = true
  }

  function m(r) {
    WorkInteraction.TOUCHING = false;
    if (WorkNav.OPEN || WorkNavOpenMechanic.DRAGGING || WorkNavOpenMechanic.TRANSITION || WorkLayoutManager.TRANSITION || Menu.OPEN) {
      return
    }
    c.alpha = 1;
    if (Math.abs(c.movement) > 0.5 || Math.abs(o.velocity.y) > 0.2) {
      var q = c.movement < 0 ? -1 : 1;
      c.transition(q);
      p = false;
      c.alpha = 0.07
    } else {
      TweenManager.tween(c, {
        movement: 0
      }, 750, "wipe", function() {
        p = false;
        c.alpha = 0.07
      })
    }
  }

  function n(q) {
    if (WorkNav.OPEN || q.target.className == "hit" || Menu.OPEN) {
      return
    }
    if (!Device.mobile) {
      if (Mouse.y <= 40) {
        p = false;
        return c.transition(-1)
      }
      if (Mouse.y >= Stage.height - 40) {
        p = false;
        return c.transition(1)
      }
    }
  }
  this.onDestroy = function() {
    ScrollUtil.unlink(l);
    Stage.unbind("touchmove", d);
    Stage.unbind("click", n)
  }
});
// Class(function FilterItem(d) {
//   Inherit(this, View);
//   var g = this;
//   var k, l, h, a, n;
//   g.height = 50;
//   g.active = false;
//   (function() {
//     e();
//     b();
//     c();
//     j()
//   })();
//
//   function e() {
//     k = g.element;
//     k.size("100%", g.height).css({
//       overflow: "hidden",
//       width: "125%",
//       left: "-25%"
//     }).invisible();
//     h = k.create(".wrapper");
//     h.size("100%").transform({
//       skewX: -4
//     })
//   }
//
//   function b() {
//     l = h.create(".title");
//     l.fontStyle("OpenSansLight", 30, "#fff");
//     l.css({
//       letterSpacing: 2.6,
//       right: Config.UI_NAV_WIDTH + 60,
//       lineHeight: g.height,
//       textAlign: "right",
//       width: "100%",
//       opacity: 0.9
//     });
//     l.text(d.text.toUpperCase())
//   }
//
//   function c() {
//     a = h.create(".bg");
//     a.size("100%").bg("#fff").transform({
//       y: g.height
//     }).css({
//       overflow: "hidden"
//     }).invisible();
//     n = a.create(".title");
//     n.fontStyle("OpenSansBold", 30, "#222");
//     n.css({
//       letterSpacing: 1,
//       right: Config.UI_NAV_WIDTH + 60,
//       lineHeight: g.height,
//       textAlign: "right",
//       width: "100%"
//     });
//     n.text(d.text.toUpperCase())
//   }
//
//   function j() {
//     k.interact(f, m);
//     k.hit.mouseEnabled(true);
//     k.hit.css({
//       width: 250 + Config.UI_NAV_WIDTH + Config.UI_OFFSET,
//       left: "",
//       right: 0
//     })
//   }
//
//   function f(o) {
//     if (g.active) {
//       return
//     }
//     o.index = d.index;
//     g.events.fire(HydraEvents.HOVER, o)
//   }
//
//   function m() {
//     if (g.active) {
//       return
//     }
//     g.events.fire(HydraEvents.CLICK, d)
//   }
//   this.goIn = function(o) {
//     if (g.hovered || !g.visible) {
//       return
//     }
//     g.hovered = true;
//     a.visible().stopTween().transform({
//       y: -g.height * o,
//       x: 0
//     }).tween({
//       y: 0
//     }, 500, "easeOutQuint");
//     n.stopTween().transform({
//       y: g.height * o * 1.2,
//       x: 0
//     }).tween({
//       y: 0
//     }, 500, "easeOutQuint")
//   };
//   this.goOut = function(o) {
//     if (!g.hovered) {
//       return
//     }
//     g.hovered = false;
//     a.tween({
//       y: -g.height * o * 1.02
//     }, 250, "easeOutQuint", function() {
//       a.invisible()
//     });
//     n.tween({
//       y: g.height * o * 1.2
//     }, 250, "easeOutQuint")
//   };
//   this.activate = function() {
//     if (g.active) {
//       return
//     }
//     g.active = true;
//     l.css({
//       fontFamily: "OpenSansBold",
//       letterSpacing: 1
//     });
//     k.hit.css({
//       cursor: ""
//     });
//     l.tween({
//       opacity: 1
//     }, 300, "easeOutSine");
//     g.hovered = false;
//     a.visible().stopTween().transform({
//       x: 0,
//       y: 0
//     }).tween({
//       x: Stage.width
//     }, 700, "easeInOutQuint", function() {
//       a.invisible()
//     });
//     n.stopTween().transform({
//       x: 0,
//       y: 0
//     }).tween({
//       y: 0,
//       x: -Stage.width
//     }, 700, "easeInOutQuint")
//   };
//   this.deactivate = function() {
//     if (!g.active) {
//       return
//     }
//     g.active = false;
//     g.hovered = false;
//     l.tween({
//       opacity: 0.9
//     }, 300, "easeOutSine");
//     a.tween({
//       y: g.height * 1.04
//     }, 500, "easeOutQuint");
//     n.tween({
//       y: -g.height * 1.2
//     }, 500, "easeOutQuint");
//     l.css({
//       fontFamily: "OpenSansLight",
//       letterSpacing: 2.6
//     });
//     k.hit.css({
//       cursor: "pointer"
//     })
//   };
//   this.animateIn = function() {
//     g.visible = true;
//     k.visible();
//     h.css({
//       opacity: 0
//     }).transform({
//       y: 15
//     }).tween({
//       opacity: 1,
//       y: 0
//     }, 400, "easeOutCubic", function() {
//       k.hit.show()
//     })
//   };
//   this.animateOut = function() {
//     g.visible = false;
//     k.hit.hide();
//     h.tween({
//       opacity: 0,
//       y: 5
//     }, 300, "easeInCubic", function() {
//       k.invisible()
//     })
//   }
// });
// Class(function FilterView() {
//   Inherit(this, View);
//   var j = this;
//   var m, o, q, c;
//   var l;
//   (function() {
//     e();
//     n();
//     h();
//     k();
//     b()
//   })();
//
//   function e() {
//     m = j.element;
//     m.size("100%")
//   }
//
//   function n() {
//     o = m.create(".title");
//     o.fontStyle("OpenSansBold", 10, "#fff");
//     o.css({
//       top: Config.UI_OFFSET,
//       right: Config.UI_NAV_WIDTH + 60,
//       letterSpacing: 2,
//       textAlign: "right",
//       width: "100%"
//     });
//     o.text("SHOW ME");
//     q = m.create(".line");
//     q.size(Config.UI_NAV_WIDTH + 50, 1).css({
//       top: Config.UI_OFFSET + 38,
//       right: 0
//     }).bg("#fff");
//     j.line = q
//   }
//
//   function h() {
//     var r = Data.Work.getTags();
//     j.items = [];
//     for (var s = 0; s < r.length; s++) {
//       var t = j.initClass(FilterItem, {
//         text: r[s],
//         index: s
//       });
//       t.text = r[s];
//       t.transform({
//         y: s * 36 + Config.UI_OFFSET + 12
//       });
//       j.items.push(t)
//     }
//     j.items[0].activate();
//     j.items[0].animateIn()
//   }
//
//   function g() {
//     var s = Config.UI_OFFSET + 12;
//     for (var r = 0; r < j.items.length; r++) {
//       j.items[r].tween({
//         y: j.items[r].hovered ? s + 15 : s
//       }, 400, "easeOutExpo");
//       j.items[r].element.setZ(r);
//       s += j.items[r].hovered ? 68 : 38
//     }
//   }
//
//   function k() {
//     for (var r = 0; r < j.items.length; r++) {
//       j.items[r].events.add(HydraEvents.HOVER, a);
//       j.items[r].events.add(HydraEvents.CLICK, d)
//     }
//     j.events.subscribe(ATEvents.RESET_FILTERS, f);
//     j.events.subscribe(HydraEvents.RESIZE, b)
//   }
//
//   function b() {
//     m.css({
//       top: Stage.width < 800 ? 50 : 0
//     })
//   }
//
//   function f() {
//     l = setTimeout(function() {
//       j.items[0].deactivate();
//       j.items[0].animateOut();
//       var r = 0;
//       for (var s = 0; s < j.items.length; s++) {
//         if (j.items[s].text == "everything") {
//           r = s
//         }
//       }
//       p(j.items, r, 0);
//       j.items[0].activate();
//       j.items[0].animateIn();
//       g()
//     }, 800)
//   }
//
//   function a(t) {
//     if (j.disabled) {
//       return
//     }
//     clearTimeout(l);
//     var s = t.movementY > 0 ? 1 : -1;
//     switch (t.action) {
//       case "over":
//         j.hovered = true;
//         for (var r = 0; r < j.items.length; r++) {
//           if (!j.items[r].active) {
//             if (t.object == j.items[r].element) {
//               j.items[r].goIn(s)
//             } else {
//               j.items[r].goOut(s)
//             }
//           }
//         }
//         break;
//       case "out":
//         l = setTimeout(function() {
//           j.hovered = false;
//           for (var u = 0; u < j.items.length; u++) {
//             j.items[u].goOut(-s)
//           }
//           g()
//         }, 400);
//         break
//     }
//     g()
//   }
//
//   function d(t) {
//     j.disabled = true;
//     j.delayedCall(function() {
//       j.disabled = false
//     }, 1000);
//     if (!t.reset) {
//       j.events.fire(ATEvents.FILTER_CHANGE, t)
//     }
//     j.items[0].deactivate();
//     var r = 0;
//     for (var s = 0; s < j.items.length; s++) {
//       if (j.items[s].text == t.text) {
//         r = s
//       }
//     }
//     p(j.items, r, 0);
//     if (r > 0 && t.text !== "Everything") {
//       for (var s = 0; s < j.items.length; s++) {
//         if (j.items[s].text == "Everything") {
//           r = s
//         }
//       }
//       p(j.items, r, 1)
//     }
//     g();
//     j.items[0].activate();
//     if (!t.force) {
//       j.delayedCall(g, 750)
//     }
//     j.delayedCall(function() {}, 100)
//   }
//
//   function p(r, t, u) {
//     var s = r[t];
//     r.splice(t, 1);
//     r.splice(u, 0, s)
//   }
//   this.animateIn = function() {
//     clearTimeout(l);
//     if (j.disabled) {
//       return
//     }
//     j.visible = true;
//     for (var r = 1; r < j.items.length; r++) {
//       j.delayedCall(j.items[r].animateIn, r * 40)
//     }
//   };
//   this.animateOut = function() {
//     j.visible = false;
//     for (var r = 1; r < j.items.length; r++) {
//       j.items[r].animateOut()
//     }
//   }
// });
Class(function GlitchCanvas(c, e) {
  Inherit(this, Component);
  var h = this;
  var j, d;
  var m = [];
  var k = [{
    fn: a,
    num: 6
  }, {
    fn: g,
    num: 6
  }, {
    fn: n,
    num: 1
  }, ];
  (function() {
    o();
    p()
  })();

  function p() {
    d = h.initClass(ObjectPool);
    for (var r = 0; r < 20; r++) {
      var q = new CanvasGraphics();
      d.put(q)
    }
  }

  function o() {
    j = h.initClass(Canvas, c || 1024, e || 512);
    h.texture = new THREE.Texture(j.div);
    var q = new CanvasGraphics();
    q.fillStyle = "#000";
    q.fillRect(0, 0, j.width, j.height);
    j.add(q)
  }

  function l(t) {
    var q = t.num;
    var s = t.fn;
    for (var r = 0; r < q; r++) {
      var t = d.get();
      m.push(t);
      s(t);
      j.add(t)
    }
  }

  function a(u) {
    var t = j.width;
    var q = (Utils.doRandom(1, 10) / 100) * j.height;
    u.x = 0;
    u.y = (Utils.doRandom(0, 8) / 10) * j.height;
    u.clear();
    u.fillRect(0, 0, t, q);
    u.fillStyle = "#00ff00";
    u.scale = 1;
    u.rotation = 0;
    var s = Utils.doRandom(0, 10) / 100;
    var r = Utils.doRandom(0, 10) / 100;
    var v = Utils.doRandom(0, 5) / 100;
    u.fillStyle = "rgba(" + ~~(s * 255) + "," + ~~(r * 255) + ", " + ~~(v * 255) + ", 255)"
  }

  function g(u) {
    var t = (Utils.doRandom(5, 10) / 100) * j.width * 0.5;
    var q = j.height * 2;
    u.x = (Utils.doRandom(0, 90) / 100) * j.width;
    u.y = -j.height * 0.2;
    u.clear();
    u.fillRect(0, 0, t, q);
    u.fillStyle = "#0000FF";
    var s = Utils.doRandom(2, 5) / 100;
    var r = Utils.doRandom(2, 5) / 100;
    u.rotation = 45;
    u.scale = 1;
    var v = Utils.doRandom(0, 5) / 100;
    u.fillStyle = "rgba(" + ~~(s * 255) + "," + ~~(r * 255) + ", " + ~~(v * 255) + ", 255)"
  }

  function n(s) {
    s.clear();
    s.beginPath();
    s.moveTo(0, -5);
    s.lineTo(8, 5);
    s.lineTo(-8, 5);
    s.closePath();
    s.fill();
    var r = 0;
    var q = 0;
    var t = Utils.doRandom(5, 30) / 100;
    s.fillStyle = "rgba(" + ~~(r * 255) + "," + ~~(q * 255) + ", " + ~~(t * 255) + ", 255)";
    s.x = Utils.doRandom(0, j.width);
    s.y = Utils.doRandom(0, j.height);
    s.rotation = Utils.doRandom(0, 360);
    s.scale = Utils.doRandom(20, 40) * 3
  }

  function f() {
    m.forEach(function(q) {
      j.remove(q);
      d.put(q)
    });
    m.length = 0
  }

  function b() {
    f();
    var q = k[Utils.doRandom(0, k.length - 1)];
    l(q);
    h.texture.needsUpdate = true;
    j.render()
  }
  this.start = function() {
    Render.start(b, 18)
  };
  this.stop = function() {
    Render.stop(b);
    f();
    j.render()
  }
});
// Class(function CircleLogo() {
//   Inherit(this, View);
//   var g = this;
//   var f;
//   var a = (Device.browser.firefox || Device.browser.ie) ? true : false;
//   (function() {
//     d();
//     c()
//   })();
//
//   function d() {
//     f = g.element;
//     f.size(44, 44).css({
//       bottom: Config.UI_OFFSET - 6,
//       right: Config.UI_OFFSET - 3
//     }).setZ(10000).transform({
//       z: a ? 0 : 1
//     });
//     f.bg("assets/images/common/circlelogo.png")
//   }
//
//   function c() {
//     f.interact(b, e)
//   }
//
//   function b(h) {
//     if (Data.getPage() == "home") {
//       return
//     }
//     switch (h.action) {
//       case "over":
//         f.tween({
//           opacity: 0.9
//         }, 100, "easeOutSine");
//         break;
//       case "out":
//         f.tween({
//           opacity: 1
//         }, 300, "easeOutSine");
//         break
//     }
//   }
//
//   function e() {
//     if (Data.getPage() == "home") {
//       return
//     }
//     Container.instance().page("home");
//     f.tween({
//       opacity: 1
//     }, 300, "easeOutSine")
//   }
//   this.animateIn = function() {}
// });
// Class(function HamburgerButton() {
//   Inherit(this, View);
//   var b = this;
//   var f, e;
//   var g = Mobile.phone ? 26 : 32;
//   (function() {
//     a();
//     h();
//     Dispatch.register(b, "close")
//   })();
//
//   function a() {
//     f = b.element;
//     f.size(g, g).css({
//       top: Config.UI_OFFSET,
//       left: Config.UI_OFFSET,
//       opacity: 0.9
//     }).setZ(10000).transform({
//       z: 1
//     }).hide();
//     e = f.create(".circle");
//     e.size(g * 2, g * 2).center().bg("#fff").css({
//       borderRadius: g,
//       opacity: 0.5
//     }).transform({
//       scale: 0
//     })
//   }
//
//   function h() {
//     b.lines = [];
//     for (var k = 0; k < 3; k++) {
//       var l = f.create(".line");
//       l.size(g, Math.round(g * 0.06)).css({
//         top: Math.round(k * g * 0.25 + g * 0.2)
//       }).bg("#fff").transform({
//         skewX: -40,
//         scale: Device.browser.firefox ? 0.9999 : 1
//       });
//       b.lines.push(l)
//     }
//   }
//
//   function d() {
//     f.interact(c, j);
//     f.hit.size(g * 3, g * 3).center()
//   }
//
//   function c(k) {
//     if (Device.mobile || b.active("disable")) {
//       return
//     }
//     switch (k.action) {
//       case "over":
//         f.tween({
//           opacity: 1
//         }, 300, "easeOutCubic");
//         e.stopTween().transform({
//           scale: 0
//         }).css({
//           opacity: 0.2
//         }).tween({
//           scale: 1.5,
//           opacity: 0
//         }, 1200, "easeOutQuart");
//         if (!Device.mobile) {
//           if (!b.open) {
//             b.lines[0].tween({
//               x: 2
//             }, 300, "easeOutExpo");
//             b.lines[1].tween({
//               x: -2
//             }, 300, "easeOutExpo");
//             b.lines[2].tween({
//               x: 2
//             }, 300, "easeOutExpo")
//           } else {
//             b.lines[0].tween({
//               rotation: Device.browser.firefox ? 45 : 35,
//               opacity: Device.browser.firefox ? 0.5 : 1
//             }, 300, "easeOutExpo");
//             b.lines[2].tween({
//               rotation: Device.browser.firefox ? -45 : -35,
//               opacity: Device.browser.firefox ? 0.5 : 1
//             }, 300, "easeOutExpo")
//           }
//         }
//         break;
//       case "out":
//         f.tween({
//           opacity: 0.9
//         }, 300, "easeOutCubic");
//         if (!Device.mobile) {
//           if (!b.open) {
//             b.lines[0].tween({
//               x: 0
//             }, 300, "easeOutExpo");
//             b.lines[1].tween({
//               x: 0
//             }, 300, "easeOutExpo");
//             b.lines[2].tween({
//               x: 0
//             }, 300, "easeOutExpo")
//           } else {
//             b.lines[0].tween({
//               rotation: 45,
//               opacity: 1
//             }, 300, "easeOutExpo");
//             b.lines[2].tween({
//               rotation: -45,
//               opacity: 1
//             }, 300, "easeOutExpo")
//           }
//         }
//         break
//     }
//   }
//
//   function j(k) {
//     if (b.active("disable")) {
//       return
//     }
//     b.active("disable", true);
//     b.delayedCall(function() {
//       b.active("disable", false)
//     }, 600);
//     if (!b.open) {
//       b.open = true;
//       b.lines[0].tween({
//         x: 0,
//         rotation: 45,
//         y: g * 0.25,
//         skewX: 0
//       }, 500, "easeOutExpo", 100);
//       b.lines[1].tween({
//         x: -15,
//         opacity: 0
//       }, 500, "easeOutExpo");
//       b.lines[2].tween({
//         x: 0,
//         rotation: -45,
//         y: -g * 0.25,
//         skewX: 0
//       }, 500, "easeOutExpo", 100);
//       if (k) {
//         Container.instance().menu("open")
//       }
//     } else {
//       b.open = false;
//       b.lines[0].tween({
//         x: 0,
//         rotation: 0,
//         y: 0,
//         skewX: -30
//       }, 500, "easeOutExpo");
//       b.lines[1].tween({
//         x: 0,
//         opacity: 1
//       }, 500, "easeOutExpo", 50);
//       b.lines[2].tween({
//         x: 0,
//         rotation: 0,
//         y: 0,
//         skewX: -30
//       }, 500, "easeOutExpo");
//       if (k) {
//         Container.instance().menu("close")
//       }
//     }
//   }
//   this.animateIn = function() {
//     f.show();
//     for (var k = 0; k < b.lines.length; k++) {
//       b.lines[k].transform({
//         x: -15
//       }).css({
//         opacity: 0
//       }).tween({
//         x: 0,
//         opacity: 1
//       }, 500, "easeOutExpo", k * 100)
//     }
//     b.delayedCall(d, 1000)
//   };
//   this.close = function() {
//     if (b.open) {
//       j()
//     }
//   }
// });
// Class(function HomeButton() {
//   Inherit(this, View);
//   var g = this;
//   var l, d, a, m, h, n, e, o;
//   var b;
//   var k = Device.browser.firefox ? 0 : 4;
//   g.width = 170;
//   g.height = 56;
//   (function() {
//     c();
//     j()
//   })();
//
//   function c() {
//     l = g.element;
//     l.size(g.width, g.height).center().css({
//       marginTop: Mobile.phone ? 60 : 145
//     }).invisible();
//     m = l.create(".glow");
//     m.size("100%").css({
//       boxShadow: "0 0 80px #fff",
//       opacity: 0
//     });
//     h = l.create(".wrapper");
//     h.size("100%").css({
//       overflow: "hidden"
//     });
//     a = h.create(".bg");
//     a.size(g.width - 2, g.height - 2).css({
//       left: 1,
//       top: 1
//     }).bg("#fff");
//     n = a.create(".circle");
//     n.fontStyle("OpenSansBold", 11, "#222");
//     n.css({
//       letterSpacing: 3,
//       width: "100%",
//       textAlign: "center",
//       top: "50%",
//       marginTop: -6,
//       lineHeight: 11
//     });
//     n.text("VIEW WORK");
//     e = h.create(".over");
//     e.size("100%").bg("#000").transform({
//       y: g.height
//     }).hide();
//     o = n.clone();
//     o.css({
//       color: "#fff",
//       opacity: 0
//     }).transform({
//       y: 10
//     });
//     h.add(o)
//   }
//
//   function j() {
//     l.interact(f, p)
//   }
//
//   function f(q) {
//     Dispatch.find(HomeSizzleParticleBehavior, "forceRadius")(q.action);
//     switch (q.action) {
//       case "over":
//         m.tween({
//           opacity: 0.6
//         }, 300, "easeOutSine");
//         o.stopTween().transform({
//           y: 20
//         }).tween({
//           y: 0,
//           opacity: 1
//         }, 300, "easeOutQuart");
//         e.stopTween().transform({
//           y: g.height
//         }).tween({
//           y: 0
//         }, 300, "easeOutQuart");
//         n.tween({
//           y: -20,
//           skewX: k,
//           opacity: 0
//         }, 300, "easeOutQuart");
//         break;
//       case "out":
//         if (g.active("preventOut")) {
//           return
//         }
//         m.tween({
//           opacity: 0
//         }, 300, "easeOutSine");
//         o.tween({
//           y: -20,
//           opacity: 0
//         }, 400, "easeOutQuart");
//         e.tween({
//           y: -g.height
//         }, 600, "easeOutExpo");
//         n.stopTween().transform({
//           y: 20,
//           skewX: k
//         }).tween({
//           y: 0,
//           skewX: k,
//           opacity: 1
//         }, 400, "easeOutQuart");
//         break
//     }
//   }
//
//   function p() {
//     Global.CLICK_BUTTON = true;
//     Dispatch.find(Home, "launchWork")();
//     g.clicked = true;
//     g.active("preventOut", true);
//     Timer.create(function() {
//       g.clicked = false;
//       g.active("preventOut", false);
//       f({
//         action: "out"
//       })
//     }, 500)
//   }
//   this.animateIn = function() {
//     g.active("preventOut", false);
//     if (b) {
//       return
//     }
//     b = true;
//     defer(function() {
//       l.visible().transform({
//         skewX: -(k)
//       });
//       a.transform({
//         y: -g.height
//       }).tween({
//         y: 0
//       }, 1000, "easeInOutExpo");
//       n.transform({
//         y: 30,
//         skewX: k
//       }).css({
//         opacity: 0
//       }).tween({
//         y: 0,
//         skewX: k,
//         opacity: 1
//       }, 800, "easeOutQuart", 450);
//       g.delayedCall(function() {
//         e.show()
//       }, 800)
//     })
//   };
//   this.reset = function() {
//     l.invisible();
//     b = false
//   }
// });
// Class(function HomeInfo() {
//   Inherit(this, View);
//   var e = this;
//   var g, j, a;
//   (function() {
//     b();
//     h();
//     k();
//     f()
//   })();
//
//   function b() {
//     g = e.element;
//     g.size(300, 50);
//     g.css({
//       bottom: Config.UI_OFFSET - 50,
//       left: Config.UI_OFFSET - 10,
//       opacity: 0.7
//     });
//     g.transformPoint(0, 0).transform({
//       rotation: -90
//     })
//   }
//
//   function h() {
//     j = g.create(".title");
//     j.fontStyle("OpenSansBold", 10, "#fff");
//     j.text("SLAY YOUR NEXT GIANT");
//     j.css({
//       letterSpacing: 3,
//       opacity: 0,
//       top: 10,
//       left: 10
//     })
//   }
//
//   function k() {
//     a = g.create(".bar");
//     a.size(100, 1).css({
//       top: 26,
//       left: 10,
//       overflow: "hidden"
//     });
//     a.inner = a.create(".inner");
//     a.inner.size("100%").css({
//       left: "-100%"
//     }).bg("#fff")
//   }
//
//   function f() {
//     g.interact(d, l);
//     e.events.subscribe(ATEvents.SIZZLE_CHANGE, c)
//   }
//
//   function d(m) {
//     switch (m.action) {
//       case "over":
//         g.tween({
//           opacity: 1
//         }, 100, "easeOutSine");
//         break;
//       case "out":
//         g.tween({
//           opacity: 0.7
//         }, 300, "easeOutSine");
//         break
//     }
//   }
//
//   function l() {
//     Dispatch.find(Home, "launchWork", {
//       button: false
//     })()
//   }
//
//   function c(n) {
//     if (e.width) {
//       a.inner.tween({
//         x: e.width * 2
//       }, 300, "easeOutQuart", m)
//     } else {
//       m()
//     }
//     j.tween({
//       y: 5,
//       opacity: 0
//     }, 300, "easeInCubic");
//
//     function m() {
//       j.text(n.title.toUpperCase());
//       defer(function() {
//         e.width = CSS.textSize(j).width - 4;
//         a.size(e.width, 2);
//         var o = (n.timeOut - n.timeIn) * 1000 - 500;
//         a.inner.transform({
//           x: 0
//         }).tween({
//           x: e.width
//         }, o, "linear");
//         j.stopTween().transform({
//           y: -7
//         }).tween({
//           y: 0,
//           opacity: 1
//         }, 700, "easeOutQuart")
//       })
//     }
//   }
//   this.animateIn = function() {}
// });
// Class(function HomeLogo() {
//   Inherit(this, View);
//   var d = this;
//   var c;
//   (function() {
//     b();
//     a()
//   })();
//
//   function b() {
//     c = d.element
//   }
//
//   function a() {
//     var g = 634 / 354;
//     var f = 634;
//     var e = f / g;
//     if (Stage.height < 550) {
//       var j = Stage.height / 550;
//       f *= j;
//       e *= j
//     }
//     if (Stage.width < 600) {
//       var j = Stage.width / 600;
//       f *= j;
//       e *= j
//     }
//     var h = -(e / 2) - 0.75;
//     if (Mobile.phone) {
//       h -= 30
//     }
//     c.size(f, e).bg("assets/images/home/logo_2.png").css({
//       top: "50%",
//       left: "50%",
//       marginLeft: -(f / 2),
//       marginTop: h
//     })
//   }
//   this.animateIn = function() {};
//   this.resize = a
// });
Class(function HomeSizzleBasic(a) {
  Inherit(this, View);
  var g = this;
  var f, b;
  (function() {
    e();
    d();
    c();
  })();

  function e() {
    f = g.element;
    f.size("100%");
    if (Device.mobile || !Hardware.HOME_VIDEO) {
      b = f.create("video");
      b.bg("img/bg-intro.jpg");
      b.object = b
    } else {
      f.add(a)
    }
  }

  function d() {
    g.events.subscribe(HydraEvents.RESIZE, c)
  }

  function c() {
    var j = Stage.width;
    var h = j * (720 / 1280);
    if (h < Stage.height) {
      h = Stage.height;
      j = h * (1280 / 720)
    }
    var k = b || a;
    k.size(j, h);
    k.object.css({
      left: Stage.width / 2 - j / 2,
      top: Stage.height / 2 - h / 2
    });
  }
  this.start = function() {};
  this.stop = function() {}
});
Class(function HomeSizzleGL() {
  Inherit(this, View);
  var g = this;
  var l;
  var k, renderer, camera;
  var mesh, plane, texture, c;
  (function() {
    e();
    q();
    p();
    j()

    setTimeout(function() {
      jQuery(function($) {
        $(window).trigger('intro_canvas_ready', [true]);
      });
    }, 500);
  })();

  function e() {
    l = g.element;
    l.size("100%")
  }

  function q() {
    renderer = new THREE.WebGLRenderer({
      preserveDrawingBuffer: true
    });
    renderer.setPixelRatio(0.8);
    renderer.setSize(Stage.width, Stage.height);
    renderer.autoClear = false;
    camera = new THREE.PerspectiveCamera(45, Stage.width / Stage.height, 10, 10000);
    camera.position.z = h();
    l.add(renderer.domElement);
  }

  function h() {
    return 1.3 * Stage.height / Math.tan(45 / 2) / 2
  }

  function p() {
    texture = g.initClass(HomeSizzleVideoTexture, renderer);
    plane = g.initClass(HomeSizzlePlane, texture);
    mesh = g.initClass(HomeSizzleMesh, texture);
    // c = g.initClass(HomeSizzleLogoContainer, d)
  }

  function f() {
    mesh.update();
    texture.render();
    plane.update();
    /*c.update();/**/
    renderer.clearDepth();
    renderer.render(plane.scene, camera);
    renderer.clearDepth();
    renderer.render(mesh.scene, camera);
    // // if (Stage.width > 600 && !Mobile.phone) {
    // //   renderer.render(c.scene, c.camera)
    // // }

      }

  function j() {
    g.events.subscribe(ATEvents.RESIZE, b)
  }

  function b() {
    camera.position.z = h();
    camera.aspect = Stage.width / Stage.height;
    camera.updateProjectionMatrix();
    renderer.setSize(Stage.width, Stage.height);
    mesh.resize();
    plane.resize()
  }
  this.start = function() {
    Render.start(f)
  };
  this.stop = function() {
    Render.stop(f)
  }
});
Class(function HomeSizzleMesh(g) {
  Inherit(this, Component);
  var d = this;
  var f, a, e, b;
  var j = [];
  this.scene = new THREE.Scene();
  (function() {
    c();
    h();
    l()
  })();

  function c() {
    e = d.initClass(ParticlePhysics);
    b = d.initClass(HomeSizzleParticleBehavior);
    e.addBehavior(b)
  }

  function h() {
    a = d.initClass(HomeSizzleMeshShader, g);
    f = d.initClass(HomeSizzleMeshGenerator, a);
    k()
  }

  function k() {
    f.exec(function(m) {
      d.scene.add(m);
      j.push(m)
    })
  }

  function l() {
    j.forEach(function(n) {
      var m = new Particle(new Vector3(n.position.x, n.position.y, 0));
      e.addParticle(m);
      m.mesh = n;
      n.particle = m
    })
  }
  this.update = function() {
    b.update();
    e.update();
    a.update()
  };
  this.resize = function() {
    j.forEach(function(m) {
      e.removeParticle(m.particle);
      d.scene.remove(m);
      m.geometry.dispose()
    });
    j.length = 0;
    k();
    l()
  }
});
Class(function HomeSizzleMeshGenerator(a) {
  Inherit(this, Component);
  var g = this;
  var b;
  var f = new Vector2();
  var e = new Vector2();
  (function() {})();

  function c() {
    b = g.initClass(DelaunayTriangulation, Stage.width, Stage.height)
  }

  function h() {
    var l = (function() {
      if (Mobile.tablet) {
        return 150
      }
      if (Mobile.phone) {
        return 10
      }
      return 300
    })();
    var n = [];
    for (var m = 0; m < l; m++) {
      n.push({
        x: Utils.doRandom(0, Stage.width),
        y: Utils.doRandom(0, Stage.height)
      })
    }
    return n
  }

  function j() {
    var w = [];
    var r = (function() {
      if (Mobile.tablet) {
        return 14
      }
      if (Mobile.phone) {
        return 7
      }
      if (Device.browser.ie) {
        return 10
      }
      if (Hardware.REDUCED_RETINA) {
        return 15
      }
      if (Device.browser.safari) {
        return 17
      }
      return 20
    })();
    var m = r;
    var t = r;
    var l = Stage.width / m;
    var q = Stage.height / t;
    var v;
    var n = function() {
      v = []
    };
    var u = function(z, A) {
      z += Utils.doRandom(-10, 10);
      A += Utils.doRandom(-10, 10);
      v.push({
        x: z,
        y: A
      })
    };
    var p = function() {
      w.push(v)
    };
    var o = function(C) {
      var B = -1;
      for (var z = 0; z < Stage.width * 1.1; z += l) {
        var A = B++ % 2 == 0;
        n();
        if (A) {
          u(z, C + q);
          u(z + (l / 2), C);
          u(z + l, C + q)
        } else {
          u(z, C + q);
          u(z - (l / 2), C);
          u(z + (l / 2), C);
          z -= l / 2
        }
        p()
      }
    };
    for (var s = 0; s < Stage.height; s += q) {
      o(s)
    }
    return w
  }

  function d(u) {
    var r = new THREE.BufferGeometry();
    var q = new Float32Array(u.length * 3);
    var l = new Float32Array(u.length * 2);
    for (var o = 0; o < u.length; o++) {
      var s = u[o];
      q[o * 3 + 0] = s.x - u.centroid.x;
      q[o * 3 + 1] = s.y - u.centroid.y;
      q[o * 3 + 2] = s.z || 0
    }
    r.addAttribute("position", new THREE.BufferAttribute(q, 3));
    r.applyMatrix(new THREE.Matrix4().makeRotationZ(Utils.toRadians(Utils.doRandom(0, 360))));
    var m = Utils.doRandom(10, 40) / 10;
    r.applyMatrix(new THREE.Matrix4().makeScale(m, m, m));
    for (o = 0; o < u.length; o++) {
      var p = q[o * 3 + 0] + u.centroid.x;
      var n = q[o * 3 + 1] + u.centroid.y;
      l[o * 2 + 0] = Utils.range(p, 0, Stage.width, 0, 1);
      l[o * 2 + 1] = Utils.range(n, 0, Stage.height, 0, 1)
    }
    r.addAttribute("uv", new THREE.BufferAttribute(l, 2));
    r.computeFaceNormals();
    r.computeVertexNormals();
    r.centroid = u.centroid;
    return r
  }

  function k(l) {
    f.clear();
    l.forEach(function(m) {
      f.add(m)
    });
    f.divide(l.length);
    l.centroid = new Vector2().copy(f);
    l.forEach(function(m) {
      m.angle = e.solveAngle(f, m)
    });
    l.sort(function(n, m) {
      return n.angle - m.angle
    });
    return l
  }
  this.exec = function(m) {
    var l = j();
    l.forEach(function(p) {
      var n = d(k(p));
      var o = new THREE.Mesh(n, a.material);
      o.centroid = n.centroid;
      o.locked = n.locked;
      o.position.x = n.centroid.x - Stage.width / 2;
      o.position.y = n.centroid.y - Stage.height / 2;
      delete n.centroid;
      delete n.locked;
      m(o)
    })
  }
});
Class(function HomeSizzleMeshShader(b) {
  Inherit(this, Component);
  var g = this;
  var f, b, a;
  (function() {
    e();
    d();
    c()
  })();

  function e() {
    f = g.initClass(Shader, "Sizzle", "Sizzle");
    f.uniforms = {
      tMap: {
        type: "t",
        value: b.texture
      },
      aspect: {
        type: "fv1",
        value: []
      },
    };
    f.material.side = THREE.DoubleSide;
    g.material = f.material
  }

  function d() {
    g.events.subscribe(HydraEvents.RESIZE, c)
  }

  function c() {
    var l = Stage.width;
    var h = l * (720 / 1280);
    if (h < Stage.height) {
      h = Stage.height;
      l = h * (1280 / 720)
    }
    var k = l / Stage.width;
    var j = h / Stage.height;
    f.uniforms.aspect.value = [0, 0, 0, 0, 0, k, j, 0.5, 0.5]
  }
  this.update = function() {
    f.uniforms.tMap.value = b.texture
  }
});
Class(function HomeSizzleParticleBehavior() {
  Inherit(this, Component);
  var f = this;
  var c = false;
  var e = 0;
  var b = 0;
  var d = new Vector2();
  var g = new Vector2();
  var h = new Vector2();
  var a = new Vector2();
  var k = ["easeOutQuad", "easeOutCubic", "easeOutQuint", "easeOutCirc"];
  (function() {
    Dispatch.register(f, "forceRadius")
  })();

  function j(l) {
    l.target = new Vector3(l.pos.x, l.pos.y, 0);
    l.origin = new Vector2(l.pos.x, l.pos.y);
    l.dir = Utils.headsTails(-1, 1);
    l.mult = Utils.doRandom(10, 100) / 100;
    l.rx = Utils.toRadians(Utils.doRandom(0, 40));
    l.ry = Utils.toRadians(Utils.doRandom(0, 40));
    l.rz = Utils.toRadians(Utils.doRandom(0, 40));
    l.ease = k[Utils.doRandom(0, k.length - 1)];
    l.speed = Utils.doRandom(20, 40) / 100
  }
  this.update = function(m) {
    g.x = Mouse.x - Stage.width / 2;
    g.y = -(Mouse.y - Stage.height / 2);
    a.subVectors(Mouse, h).divide(Render.DELTA);
    h.copy(Mouse);
    var l = a.length();
    e = l > 0.01 ? 300 : 0;
    if (l > 5) {
      e = 500
    }
    b += (e - b) * 0.07;
    if (c) {
      b = 500
    }
  };
  this.applyBehavior = function(l) {
    if (!l.target) {
      j(l)
    }
    d.subVectors(l.origin, g);
    var n = d.length();
    if (n < b) {
      var m = Math.atan2(d.y, d.x);
      l.target.copy(l.origin).addAngleRadius(m, (b - n) * l.mult * 1.2);
      l.target.z = Utils.range(n, 0, b, -300, 0)
    } else {
      l.target.copy(l.origin)
    }
    var o = Utils.range(l.pos.z, 0, -300, 0, 1);
    l.mesh.rotation.x = l.rx * o * l.dir;
    l.mesh.rotation.y = l.ry * o * l.dir;
    l.mesh.rotation.z = l.rz * o * l.dir;
    l.pos.interp(l.target, l.speed, l.ease);
    l.pos.copyTo(l.mesh.position)
  };
  this.forceRadius = function(l) {
    if (l == "over") {
      c = true
    } else {
      c = false
    }
  }
});

Class(function HomeSizzlePlane(b) {
  Inherit(this, Component);
  var h = this;
  var f, g;
  this.scene = new THREE.Object3D();
  (function() {
    e();
    a();
    d();
    c()
  })();

  function e() {
    g = h.initClass(Shader, "SizzleBG", "SizzleBG");
    g.uniforms = {
      tMap: {
        type: "t",
        value: b.texture
      },
      aspect: {
        type: "fv1",
        value: []
      },
      res: {
        type: "v2",
        value: new THREE.Vector2(Stage.width, Stage.height)
      }
    }
  }

  function a() {
    var j = new THREE.PlaneGeometry(Stage.width, Stage.height);
    f = new THREE.Mesh(j, g.material);
    h.scene.add(f);
    f.position.z += 200
  }

  function d() {
    h.events.subscribe(HydraEvents.RESIZE, c)
  }

  function c() {
    var m = Stage.width;
    var j = m * (720 / 1280);
    if (j < Stage.height) {
      j = Stage.height;
      m = j * (1280 / 720)
    }
    var l = m / Stage.width;
    var k = j / Stage.height;
    g.uniforms.aspect.value = [0, 0, 0, 0, 0, l, k, 0.5, 0.5]
  }
  this.resize = function() {
    f.geometry.dispose();
    g.uniforms.res.value.set(Stage.width, Stage.height);
    a()
  };
  this.update = function() {
    g.uniforms.tMap.value = b.texture
  }
});
Class(function HomeSizzleVideoTexture() {
  Inherit(this, Component);
  var g = this;
  var b, a, d, e;
  (function() {
    f()
  })();

  function f() {
    if (Hardware.HOME_VIDEO) {

      if( USE_VIDEO ) {
        a = HomeVideoController.instance().video;
        b = new THREE.Texture(a.div);
        b.minFilter = b.magFilter = THREE.LinaearFilter;
        d = Utils3D.getTexture("img/bg-intro.jpg");
        a.texture = b;
        g.texture = d
      }
      else {
        d = Utils3D.getTexture("img/bg-intro.jpg");
        g.texture = d
      }

    } else {
      b = Utils3D.getTexture("img/bg-intro.jpg");
      g.texture = b
    }
  }

  function c() {
    if (e) {
      return
    }
    e = true;
    g.delayedCall(function() {
      g.texture = a.texture
    }, 500)
  }
  this.render = function() {
    if (a) {
      if (a.ready()/* || Hydra.LOCAL/**/) {
        if (!e) {
          c()
        }
      }
    }
  }
});
Class(function LoaderView() {
  Inherit(this, View);
  var d = this;
  var f;
  var h;
  var g = require("LoaderPathConfig");
  (function() {
    c();
    j();
    b();
    a();
    e()
  })();

  function c() {
    f = d.element;
    f.size("100%").css({
      opacity: 0
    });
    if (Mobile.phone) {
      f.css({
        top: -30
      })
    }
  }

  function j() {
    h = d.initClass(LoaderSVG, g, f);
    d.svg = h.svg
  }

  function b() {
    d.events.subscribe(HydraEvents.RESIZE, a)
  }

  function a() {
    f.size("100%");
    h.resize()
  }

  function e() {
    f.tween({
      opacity: 1
    }, 300, "easeOutCubic", 100)
  }
  this.updatePath = h.updatePath
});
Module(function LoaderPathConfig() {
  this.exports = {
    ns: "http://www.w3.org/2000/svg",
    viewWidth: 634,
    viewHeight: 354,
    type: ['<polygon class="st0" points="131,98.4 122.7,98.4 99.2,152.4 110,152.4 114.9,140.1 118.2,131.8 126.6,109.8 134.9,131.8 138.3,140.1 143.3,152.4 154.3,152.4"/>', '<path class="st0" d="M230.5,111.1c-1.4-1.9-3.3-3.3-5.6-4.2c-2.3-0.9-4.5-1.3-6.7-1.3c-2.8,0-5.3,0.5-7.6,1.5c-2.3,1-4.3,2.4-5.9,4.2c-1.7,1.8-2.9,3.9-3.8,6.3c-0.9,2.4-1.3,5-1.3,7.8c0,3,0.4,5.6,1.3,8.1c0.9,2.4,2.1,4.5,3.7,6.3c1.6,1.8,3.5,3.1,5.8,4.1c2.2,1,4.8,1.4,7.6,1.4c2.9,0,5.5-0.6,7.7-1.7c2.2-1.1,4-2.7,5.4-4.5l7.7,5.4c-2.4,3-5.3,5.3-8.8,7c-3.5,1.7-7.5,2.5-12.1,2.5c-4.2,0-8.1-0.7-11.6-2.1c-3.5-1.4-6.6-3.4-9.1-5.9c-2.5-2.5-4.5-5.5-6-9c-1.4-3.5-2.1-7.3-2.1-11.6c0-4.3,0.7-8.2,2.3-11.7c1.5-3.5,3.6-6.4,6.2-8.9c2.6-2.4,5.7-4.3,9.3-5.6c3.6-1.3,7.4-2,11.6-2c1.7,0,3.5,0.2,5.4,0.5c1.9,0.3,3.7,0.8,5.4,1.5c1.7,0.7,3.4,1.5,4.9,2.6c1.5,1,2.8,2.2,3.9,3.7L230.5,111.1z"/>', '<path class="st0" d="M291.4,106.8h-16.6v-8.4h42.7v8.4H301v45.6h-9.6V106.8z"/>', '<path class="st0" d="M361.2,98.4h9.6v54h-9.6V98.4z"/>', '<path class="st0" d="M411.2,98.4H422l14.9,41.6h0.3l15-41.6h10.4l-21.7,54h-8.3L411.2,98.4z"/>', '<path class="st0" d="M499.5,98.4h37.2v8.4h-27.5v13.7h27.5v8.1h-27.5v15.3h27.5v8.5h-37.2V98.4z"/>', '<path class="st0" d="M122.8,256.5h-5.5v-49.1H99.2v-4.9h41.7v4.9h-18.1V256.5z"/>', '<path class="st0" d="M182.4,202.5h5.5v23.4h29.4v-23.4h5.5v54h-5.5v-25.6h-29.4v25.6h-5.5V202.5z"/>', '<path class="st0" d="M268.5,251.6h29.1v4.9H263v-54h34.6v4.9h-29.1v18.5h29.1v4.9h-29.1V251.6z"/>', '<path class="st0" d="M387.7,229.5c0,4.1-0.7,7.9-2.1,11.4c-1.4,3.5-3.4,6.4-5.9,9c-2.5,2.5-5.5,4.5-8.9,5.9c-3.4,1.4-7.2,2.1-11.2,2.1c-4,0-7.7-0.7-11.2-2.1c-3.4-1.4-6.4-3.4-8.9-5.9c-2.5-2.5-4.5-5.5-5.9-9c-1.4-3.5-2.1-7.2-2.1-11.4c0-4.1,0.7-7.9,2.1-11.4c1.4-3.5,3.4-6.4,5.9-9c2.5-2.5,5.5-4.5,8.9-5.9c3.4-1.4,7.2-2.1,11.2-2.1c4,0,7.7,0.7,11.2,2.1c3.4,1.4,6.4,3.4,8.9,5.9c2.5,2.5,4.5,5.5,5.9,9C387,221.6,387.7,225.4,387.7,229.5z M381.9,229.5c0-3.1-0.5-6.1-1.5-9c-1-2.8-2.5-5.3-4.4-7.5c-1.9-2.2-4.3-3.9-7.1-5.1c-2.8-1.3-5.9-1.9-9.3-1.9c-3.5,0-6.6,0.6-9.3,1.9c-2.8,1.3-5.1,3-7.1,5.1c-1.9,2.2-3.4,4.7-4.4,7.5c-1,2.8-1.5,5.8-1.5,9c0,3.1,0.5,6.1,1.5,9c1,2.8,2.5,5.3,4.4,7.5c1.9,2.1,4.3,3.8,7.1,5.1c2.8,1.3,5.9,1.9,9.3,1.9c3.5,0,6.6-0.6,9.3-1.9c2.8-1.3,5.1-3,7.1-5.1c1.9-2.1,3.4-4.6,4.4-7.5C381.4,235.7,381.9,232.7,381.9,229.5z"/>', '<path class="st0" d="M443.2,230.8c2-0.2,3.8-0.7,5.5-1.4c1.7-0.7,3.1-1.7,4.3-2.9c1.2-1.2,2.2-2.6,2.9-4.2c0.7-1.6,1.1-3.4,1.1-5.4c0-2.6-0.5-4.9-1.4-6.7c-0.9-1.8-2.2-3.3-3.9-4.5c-1.7-1.1-3.6-2-5.9-2.5c-2.3-0.5-4.7-0.8-7.4-0.8h-15.9v4.7h5.5h9.8c2,0,3.9,0.2,5.6,0.5c1.7,0.4,3.1,0.9,4.3,1.7c1.2,0.8,2.1,1.8,2.7,3c0.7,1.2,1,2.7,1,4.5c0,3.2-1.2,5.6-3.6,7.2c-2.4,1.6-5.8,2.4-10.2,2.4h-9.5v0h-5.5v30h5.5v-25.3h9.2l15,25.3h6.7L443.2,230.8z"/>', '<path class="st0" d="M516.4,256.5H511v-23.3l-20.2-30.7h6.9l16.3,26.2l16.4-26.2h6.4l-20.2,30.7V256.5z"/>'],
    mask: [{
      svg: '<polyline class="st2" points="101.8,159.8 126.8,98.4 148.8,152.4"/>',
      mask: 0,
      stroke: 9
    }, {
      svg: '<path class="st3" d="M234.5,108c-6.8-10.9-40.4-10.9-39.6,17.4c0.8,30.6,34.7,27,40.2,16.3"/>',
      mask: 1,
      stroke: 12
    }, {
      svg: '<line class="st4" x1="269.5" y1="102.3" x2="318" y2="102.3"/>',
      mask: 2,
      start: 0,
      end: 0.5,
      stroke: 9
    }, {
      svg: '<line class="st4" x1="296.2" y1="157" x2="296.2" y2="109"/>',
      mask: 2,
      start: 0.5,
      end: 1,
      stroke: 12
    }, {
      svg: '<line class="st4" x1="366" y1="90.8" x2="366" y2="152.4"/>',
      mask: 3,
      stroke: 12
    }, {
      svg: '<polyline class="st4" points="413.2,89 436.9,152.4 457.8,98.4"/>',
      mask: 4,
      stroke: 9
    }, {
      svg: '<polyline class="st4" points="542.2,102.3 504.2,102.3 504.2,147.8 536.7,147.8"/>',
      mask: 5,
      start: 0,
      end: 0.75,
      stroke: 9
    }, {
      svg: '<line class="st4" x1="542.2" y1="125.1" x2="509.2" y2="124.8"/>',
      mask: 5,
      start: 0.75,
      end: 1,
      stroke: 12
    }, {
      svg: '<line class="st2" x1="95.8" y1="204.8" x2="140.9" y2="204.8"/>',
      mask: 6,
      start: 0,
      end: 0.5,
      stroke: 6
    }, {
      svg: '<line class="st2" x1="120.1" y1="262" x2="120.1" y2="207.8"/>',
      mask: 6,
      start: 0.5,
      end: 1,
      stroke: 6
    }, {
      svg: '<line class="st2" x1="185.3" y1="197.2" x2="185.3" y2="256.5"/>',
      mask: 7,
      start: 0,
      end: 0.5,
      stroke: 6
    }, {
      svg: '<line class="st2" x1="188.7" y1="228.4" x2="216.8" y2="228.4"/>',
      mask: 7,
      start: 0.5,
      end: 1,
      stroke: 5
    }, {
      svg: '<line class="st2" x1="219.8" y1="265.8" x2="219.8" y2="202.5"/>',
      mask: 7,
      start: 0,
      end: 0.5,
      stroke: 5
    }, {
      svg: '<polyline class="st2" points="301.5,204.8 266.2,204.8 266.2,254 297.6,254"/>',
      mask: 8,
      start: 6,
      end: 0.5,
      stroke: 6
    }, {
      svg: '<line class="st2" x1="297.6" y1="228.4" x2="266.2" y2="228.4"/>',
      mask: 8,
      start: 0.5,
      end: 1,
      stroke: 6
    }, {
      svg: '<circle class="st2" cx="359.6" cy="229.4" r="25.8"/>',
      mask: 9,
      stroke: 6
    }, {
      svg: '<path class="st2" d="M416.8,204.8c0,0,12.3,0,23.1,0s13.9,5.5,13.9,11c0,3.8,0.8,13-14.1,13c-13.7,0-13.9,0-13.9,0l-0.1,27.7"/>',
      mask: 10,
      start: 0,
      end: 0.5,
      stroke: 6
    }, {
      svg: '<line class="st2" x1="457.8" y1="260.2" x2="439.6" y2="228.8"/>',
      mask: 10,
      start: 0.5,
      end: 1,
      stroke: 6
    }, {
      svg: '<polyline class="st2" points="490.7,197.2 513.7,232.2 513.7,256.5"/>',
      mask: 11,
      start: 0,
      end: 0.5,
      stroke: 6
    }, {
      svg: '<polyline class="st2" points="536.7,197.2 513.7,232.2 513.7,257"/>',
      mask: 11,
      start: 0.5,
      end: 1,
      stroke: 6
    }],
    rectangle: ['<polyline class="st1" points="317,6 5,6 5,349 317,349"/>', '<polyline class="st1" points="317,349 629,349 629,6 317,6"/>']
  }
});
Class(function LoaderSVG(g, c) {
  Inherit(this, Component);
  var f = this;
  var h, e, j;
  (function() {
    d();
    b()
  })();

  function d() {
    h = document.createElementNS(g.ns, "svg");
    h.setAttribute("viewBox", "0 0 " + g.viewWidth + " " + g.viewHeight);
    h.setAttribute("version", "1.1");
    j = $(h);
    c.add(h);
    f.svg = $(h)
  }

  function b() {
    e = f.initClass(LoaderSVGItems, g, h)
  }

  function a() {
    var l = g.viewWidth;
    var k = g.viewHeight;
    if (Stage.height < 550) {
      var m = Stage.height / 550;
      l *= m;
      k *= m
    }
    if (Stage.width < 600) {
      var m = Stage.width / 600;
      l *= m;
      k *= m
    }
    j.size(l, k).center();
    e.resize()
  }
  this.resize = a;
  this.updatePath = e.updatePath
});
Class(function LoaderSVGItems(l, d) {
  Inherit(this, Component);
  var h = this;
  var o = [];
  var n = [];
  var m = [];
  var j = [];
  var f = (Device.browser.ie || Mobile.phone) ? true : false;
  (function() {
    if (!f) {
      a()
    }
    k();
    e();
    g();
    p()
  })();

  function a() {
    l.rectangle.forEach(function(t, r) {
      var q = {};
      var s = t.substr(t.indexOf("<") + 1, t.indexOf("class") - 2);
      q.path = new LoaderSVGObject(s, t);
      q.length = q.path.getLength();
      q.path.object.css({
        fill: "none",
        stroke: "black",
        strokeWidth: 10,
        strokeDasharray: q.length + " " + q.length,
        strokeDashoffset: q.length
      });
      q.path.addTo(d);
      m.push(q)
    })
  }

  function g() {
    l.type.forEach(function(t, q) {
      var r = t.substr(t.indexOf("<") + 1, t.indexOf("class") - 2);
      var s = new LoaderSVGObject(r, t);
      s.addTo(d);
      s.object.css({
        fill: "black"
      });
      o.push(s)
    })
  }

  function k() {
    var q = 0;
    l.mask.forEach(function(t, r) {
      var s;
      if (q === t.mask) {
        s = new LoaderSVGObject("mask", "clip-" + q);
        s.addTo(d)
      } else {
        q = t.mask;
        s = j[j.length - 1]
      }
      q++;
      j.push(s)
    })
  }

  function e() {
    l.mask.forEach(function(t, r) {
      var q = {};
      var s = t.svg.substr(t.svg.indexOf("<") + 1, t.svg.indexOf("class") - 2);
      q.path = new LoaderSVGObject(s, t.svg);
      q.length = q.path.getLength();
      q.path.object.css({
        fill: "none",
        stroke: "white",
        opacity: 0,
        strokeWidth: t.stroke,
        "stroke-linecap": "square",
        "stroke-linejoin": "miter",
        strokeDasharray: q.length + " " + q.length,
        strokeDashoffset: q.length
      });
      q.path.addTo(j[r]);
      q.start = t.start;
      q.end = t.end;
      n.push(q)
    })
  }

  function p() {
    o.forEach(function(r, q) {
      r.attribute = "mask,url(#clip-" + q + ")"
    })
  }

  function c(r, q) {
    r *= 100;
    n.forEach(function(x, u) {
      var v = x.path.object;
      var w = 0;
      var t = 100;
      if (x.start) {
        w = x.start * 100;
        t = x.end * 100
      }
      var s = Utils.convertRange(r, 0, 100, x.length, 0);
      v.div.style.strokeDashoffset = s + "px";
      v.div.style.opacity = 1
    });
    if (!q && !f) {
      m.forEach(function(v, t) {
        var u = v.path.object;
        var s = Utils.convertRange(r, 0, 100, v.length, 0);
        u.div.style.strokeDashoffset = s + "px"
      })
    }
  }

  function b() {
    if (f) {
      return
    }
    m.forEach(function(q) {
      q.path.object.css({
        display: (Stage.width <= 600) ? "none" : "block"
      })
    })
  }
  this.updatePath = c;
  this.resize = b
});
Class(function LoaderSVGObject(d, n) {
  Inherit(this, Component);
  var p = this;
  var a, r;
  var g = {};
  var m = 0;
  var h = require("LoaderPathConfig");
  (function() {
    c()
  })();

  function c() {
    var t = d;
    if (d === "line" || d === "polyline" || d === "circle" || d === "polygon") {
      t = "path"
    }
    a = document.createElementNS(h.ns, t);
    if (d === "clipPath" || d === "mask") {
      return k()
    }
    if (d === "line" || d === "polyline" || d === "circle" || d === "polygon" || d === "path") {
      q();
      s()
    }
  }

  function q() {
    var t = "d",
        v = 3;
    switch (d) {
      case "polygon":
        n = j();
        break;
      case "polyline":
        t = "points";
        v = 8;
        break;
      case "line":
        n = l();
        break;
      case "circle":
        n = o();
        break
    }
    var w = (n.indexOf(t + "=") + v);
    var u = n.lastIndexOf('"') - w;
    n = n.substr(w, u);
    if (d === "polyline") {
      t = "d";
      n = "M" + n
    }
    a.setAttributeNS(null, t, n)
  }

  function o() {
    var t, w, v, u;
    t = n.substr(n.indexOf("cx=") + 4, (n.indexOf("cy=") - 2) - (n.indexOf("cx=") + 4));
    w = n.substr(n.indexOf("cy=") + 4, (n.indexOf("r=") - 2) - (n.indexOf("cy=") + 4));
    v = n.substr(n.indexOf("r=") + 3, (n.length - 6) - n.indexOf("r="));
    u = parseFloat(v * 2);
    m = v;
    return 'd="M' + t + ", " + w + " m" + (-v) + ", 0 a " + v + ", " + v + " 0 1,0 " + u + ",0 a " + v + ", " + v + " 0 1,0 " + (-u) + ',0"'
  }

  function l() {
    var u, t, w, v;
    u = n.substr(n.indexOf("x1=") + 4, (n.indexOf("y1=") - 2) - (n.indexOf("x1=") + 4));
    w = n.substr(n.indexOf("y1=") + 4, (n.indexOf("x2=") - 2) - (n.indexOf("y1=") + 4));
    t = n.substr(n.indexOf("x2=") + 4, (n.indexOf("y2=") - 2) - (n.indexOf("x2=") + 4));
    v = n.substr(n.indexOf("y2=") + 4, (n.length - 7) - n.indexOf("y2="));
    return 'd="M' + u + "," + w + " " + t + "," + v + '"'
  }

  function j() {
    var u = (n.indexOf("points=") + 8);
    var t = n.lastIndexOf('"') - u;
    return 'd="M' + n.substr(u, t) + '"'
  }

  function s() {
    r = $(a)
  }

  function k() {
    a.setAttribute("id", n)
  }

  function f(t) {
    if (t.points) {
      t = t.points
    }
    t.appendChild(a)
  }

  function e(u) {
    var t = u.substr(0, u.indexOf(","));
    var v = u.substr(u.indexOf(",") + 1, u.length);
    g[t] = v;
    return {
      style: t,
      property: g[t]
    }
  }

  function b() {
    return a.getTotalLength()
  }
  this.addTo = f;
  this.getLength = b;
  this.set("attribute", function(t) {
    var u = e(t);
    a.setAttribute(u.style, u.property)
  });
  this.get("attribute", function() {
    return g
  });
  this.get("points", function() {
    return a
  });
  this.get("object", function() {
    return r
  })
});

Class(function Main() {
  (function() {
    b();
    __body.bg("#000");
    Mouse.capture();
    if (Hydra.HASH.strpos("playground")) {
      a()
    } else {
      c()
    }
  })();

  function b() {
    AssetUtil.PATH = Config.CDN;
    Hydra.CDN = Config.CDN;
    Utils3D.PATH = Config.PROXY
  }

  function a() {
    Global.PLAYGROUND = true;
    // Data.loadData(Config.DATA_API, function() {
    //   AssetLoader.loadAllAssets(function() {
        Playground.instance()
      // })
    // })
  }

  function c() {
    Utils3D.disableWarnings();
    Container.instance()
  }
});
window._MINIFIED_ = true;
window.USE_VIDEO = false;