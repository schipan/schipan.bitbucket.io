(function ($) {
  var App = (function () {
    function App() {
      /**
       *
       * @type {Object}
       */
      this.params = {};
      /**
       *
       * @type {boolean}
       * @private
       */
      this._isInitialized = false;
      /**
       *
       * @type {boolean}
       * @private
       */
      this._isReady = false;
      /**
       *
       * @type {Array}
       * @private
       */
      this._readyCb = [];
      /**
       *
       * @type {Array}
       * @private
       */
      this._scopeArguments = [];
    }
    /**
     *
     * @returns {Object}
     */
    App.prototype.getParams = function () {
      return this.params;
    };
    /**
     *
     * @param {string} key
     * @param {*} value
     */
    App.prototype.setParam = function (key, value) {
      this.params[key] = value;
    };
    /**
     *
     * @param {string} key
     * @returns {*}
     */
    App.prototype.getParam = function (key) {
      return this.params[key];
    };
    //////////////////////////
    //////////////////////////
    App.prototype.init = function (params) {
      var self = this;
      if (self._isInitialized) {
        console.warn('app re-init denied', [Error().stack]);
        return;
      }
      console.log('[app init]', params);
      self._isInitialized = true;
      if (!params) {
        params = {};
      }
      if (params && typeof (params) !== 'object') {
        console.error('[app init failed] params not specified');
        return;
      }
      self.initCore(params);
    };
    /**
     *
     * @param {object} params
     */
    App.prototype.initCore = function (params) {
      var app = this;
      app.setJQuery($);
        // extend initial params
        $.extend(app.params, params);
        // init app with public context
        var core = new Core();
        core.init(app)
          .then(
              // app now is ready
              function () {
                // set scope arguments
                app._scopeArguments = [$, core];
                // set ready
                app._setReady();
              },
              // app failed to init
              function (err) {
                console.error('app init failed via error: ', err);
              });
    };
    // jq
    App.prototype.setJQuery = function(jQueryInstance) {
      this._jq = jQueryInstance;
    };
    App.prototype.getJQuery = function() {
      return this._jq;
    };
    App.prototype.jq = function() {
      return this.getJQuery();
    };
    /**
     *
     * @param {Function} cb -
     *  функция, которая будет вызвана,
     *  когда приложение будет готово
     */
    App.prototype.ready = function (cb) {
      if (typeof (cb) !== 'function')
        return;
      if (this._isReady) {
        cb.apply(this, this._scopeArguments);
      }
      if (!this._readyCb)
        this._readyCb = [];
      this._readyCb.push(cb);
    };
    /**
     *
     * @private
     */
    App.prototype._setReady = function () {
      this._isReady = true;
      if (!this._readyCb)
        return;
      var cb = this._readyCb;
      var i = 0, ii = cb.length;
      for (; i < ii; ++i) {
        cb[i].apply(this, this._scopeArguments);
      }
      this._readyCb = null;
    };
    return App;
  }());
  if (parent !== window) {
    throw new Error('cannot init into iframe or popup');
  }
  window.app = new App();
})(jQuery.noConflict());