///<reference path="typings/require.d.ts" />
///<reference path="typings/jquery.d.ts" />
///<reference path="typings/react.d.ts" />
///<reference path="typings/Error.ts" />

// Avoid `console` errors in browsers that lack a console.
(function () {
  var method: string;
  var noop = function () {
  };
  var methods = [
    'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
    'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
    'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
    'timeStamp', 'trace', 'warn'
  ];

  var length = methods.length;
  var console = (window.console = window.console || <any>{});

  while (length--) {
    method = methods[length];

    // Only stub undefined methods.
    if (!console[method]) {
      console[method] = noop;
    }
  }
}());

(function () {
  "use strict";

  class App {

    /**
     *
     * @type {Object}
     */
    params: any = {};

    /**
     *
     * @type {Object}
     */
    path: any = null;

    /**
     *
     * @type {boolean}
     * @private
     */
    _isInitialized: boolean = false;
    /**
     *
     * @type {boolean}
     * @private
     */
    _isReady: boolean = false;

    /**
     *
     * @type {Array}
     * @private
     */
    _readyCb: Function[] = [];

    /**
     *
     * @type {Array}
     * @private
     */
    _scopeArguments: any[] = [];

    /**
     *
     * @type {string}
     * @private
     */
    _error: string = null;

    /**
     *
     * @returns {Object}
     */
    getParams() {
      return this.params;
    }

    /**
     *
     * @param {string} key
     * @param {*} value
     */
    setParam(key: string, value: any) {
      this.params[key] = value;
    }

    /**
     *
     * @param {string} key
     * @returns {*}
     */
    getParam(key: string) {
      return this.params[key];
    }

    //////////////////////////
    //////////////////////////

    init(params: any) {

      var self = this;
      if (self._isInitialized) {
        console.warn('app re-init denied', [Error().stack]);
        return;
      }

      console.log('[app init]', params);
      self._isInitialized = true;

      if (!params) {
        params = {};
      }

      if (params && typeof(params) !== 'object') {
        console.error('[app init failed] params not specified');
        return;
      }

      var scriptsPath = params.scriptsPath;
      if (!scriptsPath) {
        console.error('[app init failed] scripts path not specified');
        return;
      }

      require([scriptsPath + 'app.rjsConf'], function (rjsConf: any) {
        // config
        rjsConf.baseUrl = scriptsPath; // js root
        rjsConf.urlArgs = '_=' + params.buildVersion; // anti-cache
        require.config(rjsConf);
        // export paths (as `app.path` global variable)
        self.path = rjsConf.paths;
        // require core
        self.requireCore(params);
      });

      // require.onError = function (err) {
      //   self._setError(err);
      //   throw err;
      // };
    }

    /**
     *
     * @param {object} params
     */
    requireCore(params: any) {

      var app = this;

      var req = ['core'];
      var sub = ['react', 'react-dom'];

      // Start the main app logic.
      require(req,
          function (Core: any, CoreJs: any) {

            // extend initial params
            // TODO : include core-js
            Object.assign(app.params, params);

            // required sub-modules
            require(sub, function (React: any, ReactDOM: __React.ReactDOM) {
                  // init app with public context
                  var core = new Core();
                  core.init(app)
                      .then(
                          // app now is ready
                          function () {

                            // set scope arguments
                            app._scopeArguments = [core];

                            // set ready
                            app._setReady();
                          },
                          // app failed to init
                          function (err: Error) {
                            console.error('app init failed via error: ', err);
                          }
                      );
                },
                function (err: Error) {
                  console.error('app additional scripts load error: ', err);
                }
            );
          },
          function (err: Error) {
            console.error('app required scripts load error: ', err);
          }
      );

    }

    /**
     *
     * @param {Function} cb -
     *  функция, которая будет вызвана,
     *  когда приложение будет готово
     */
    ready(cb: Function) {
      if (typeof(cb) !== 'function') return;
      if (this._isReady) {
        cb.apply(this, this._scopeArguments);
      }
      if (!this._readyCb) this._readyCb = [];
      this._readyCb.push(cb);
    }

    /**
     *
     * @private
     */
    _setReady() {
      this._isReady = true;
      if (!this._readyCb) return;
      var cb = this._readyCb;
      var i = 0, ii = cb.length;
      for (; i < ii; ++i) {
        cb[i].apply(this, this._scopeArguments);
      }
      this._readyCb = null;
    }

    /**
     *
     * @private
     */
    _setError(err: string) {
      this._error = err;
    }

    /**
     *
     * @returns {boolean}
     */
    hasErrors() {
      return !!this._error;
    }

  }

  if (parent !== window) {
    throw new Error('cannot init into iframe or popup');
  }

  interface Window {
    app: App
  }

  window.app = new App();
})();