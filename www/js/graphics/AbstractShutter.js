class AbstractShutter {

  /**
   *
   * @type {{string : THREE.Texture}}
   */
  texturesHash = {};

  /**
   * @type {Stage}
   */
  Stage;

  /**
   * @type {WebGLRenderer}
   */
  renderer;

  /**
   * @type {PerspectiveCamera}
   */
  camera;

  /**
   *
   * @param {Stage} Stage
   */
  constructor(Stage) {
    var self = this;
    self.Stage = Stage;

    let renderer = self.renderer = new THREE.WebGLRenderer({
      preserveDrawingBuffer: true
    });

    console.log('renderer', renderer);

    renderer.setPixelRatio(0.8);
    renderer.setSize(Stage.width, Stage.height);
    renderer.autoClear = false;

    // let camera = this.camera = new THREE.PerspectiveCamera(45, Stage.width / Stage.height, 10, 10000);
    // let camera = this.camera = new THREE.PerspectiveCamera(45, Stage.width / Stage.height, 1, 1000);
    // let camera = this.camera = new THREE.PerspectiveCamera(75, Stage.width / Stage.height, 0.1, 1000);
    // camera.position.z = this.getCameraPosition();
    Stage.element.appendChild(renderer.domElement);

    this.getTexture("img/bg-intro.jpg").then(function( texture, image ) {

      var width, height;

      if( Stage.width > Stage.height ) {
        width = Stage.width;
        height = image.height * (Stage.width / image.width);
      }
      else {
        height = Stage.height;
        width = image.width * (Stage.height / image.height);
      }

      var scene = new THREE.Scene();
      let camera = self.camera = new THREE.PerspectiveCamera(45, Stage.width / Stage.height, 10, 10000);
      camera.position.z = self.getCameraPosition();

      var material = new THREE.MeshBasicMaterial({map: texture});
      var geometry = new THREE.PlaneGeometry(width, height);
      var mesh = new THREE.Mesh(geometry, material);

      mesh.position.z += 200;
      mesh.material.depthTest = false;
      mesh.material.depthWrite = false;

      scene.add(mesh);

      renderer.clearDepth();
      renderer.render(scene, camera);
    });

    $(window).on('resize', this.onResize.bind(this));
  }

  baseUrl() {
    // TODO
    // return fn.baseUrl();
    return location.origin + '/';
    // return '/';
  }

  /**
   *
   */
  getCameraPosition(){
    return 1.3 * this.Stage.height / Math.tan(45 / 2) / 2
  }

  /**
   *
   */
  onResize() {
    let Stage = this.Stage;
    let camera = this.camera;
    let renderer = this.renderer;

    // TODO
    // if( camera ) {
    //   camera.position.z = this.getCameraPosition();
    //   camera.aspect = Stage.width / Stage.height;
    //   camera.updateProjectionMatrix();
    // }
    //
    // if( renderer ) {
    //   renderer.setSize(Stage.width, Stage.height);
    // }
  }

  /**
   *
   * @param {string} url
   * @param {boolean} [isNoFilter]
   * @returns {Deferred}
   */
  getTexture(url, isNoFilter) {
    if (!this.texturesHash[url]) {
      var def = $.Deferred();
      var image = new Image();
      image.crossOrigin = "";
      image.src = this.baseUrl() + url;
      var texture = new THREE.Texture(image);
      image.onerror = def.reject;
      image.onload = function() {
        texture.needsUpdate = true;
        def.resolve(texture, image);
      };
      this.texturesHash[url] = def;
      if (!isNoFilter) {
        texture.minFilter = THREE.LinearFilter;
        // texture.minFilter = texture.magFilter = THREE.LinaearFilter;
      }
    }
    return this.texturesHash[url];
  }

}

// Class(function HomeSizzleGL() {
//   Inherit(this, View);
//   var g = this;
//   var l;
//   var k, n, a;
//   var o, m, d, c;
//   (function() {
//     e();
//     q();
//     p();
//     j()
//   })();
//
//   function e() {
//     l = g.element;
//     l.size("100%")
//   }
//
//   function q() {
//     n = new THREE.WebGLRenderer({
//       preserveDrawingBuffer: true
//     });
//     n.setPixelRatio(0.8);
//     n.setSize(Stage.width, Stage.height);
//     n.autoClear = false;
//     a = new THREE.PerspectiveCamera(45, Stage.width / Stage.height, 10, 10000);
//     a.position.z = h();
//     l.add(n.domElement);
//   }
//
//   function h() {
//     return 1.3 * Stage.height / Math.tan(45 / 2) / 2
//   }
//
//   function p() {
//     d = g.initClass(HomeSizzleVideoTexture, n);
//     m = g.initClass(HomeSizzlePlane, d);
//     o = g.initClass(HomeSizzleMesh, d);
//     c = g.initClass(HomeSizzleLogoContainer, d)
//   }
//
//   function f() {
//     o.update();
//     d.render();
//     m.update();
//     c.update();
//     n.clearDepth();
//     n.render(m.scene, a);
//     n.clearDepth();
//     n.render(o.scene, a);
//     if (Stage.width > 600 && !Mobile.phone) {
//       n.render(c.scene, c.camera)
//     }
//   }
//
//   function j() {
//     g.events.subscribe(ATEvents.RESIZE, b)
//   }
//
//   function b() {
//     a.position.z = h();
//     a.aspect = Stage.width / Stage.height;
//     a.updateProjectionMatrix();
//     n.setSize(Stage.width, Stage.height);
//     o.resize();
//     m.resize()
//   }
//   this.start = function() {
//     Render.start(f)
//   };
//   this.stop = function() {
//     Render.stop(f)
//   }
// });