class Stage {
  element;
  width;
  height;

  /**
   *
   * @param {HTMLElement} el
   */
  constructor(el) {
    this.element = el;
    this.width = el.offsetWidth;
    this.height = el.offsetHeight;
  }
}