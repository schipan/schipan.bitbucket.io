define([], function () {

  /////////////////////////////////////////
  /////////////////////////////////////////

  var rjsConf = {
    // If set to true, document.createElementNS() will be used to create script elements.
    xhtml: true,
    // skips the data-main attribute scanning done to start module loading
    skipDataMain: true,
    //To get timely, correct error triggers in IE, force a define/shim exports check.
    enforceDefine: false,
    // The number of seconds to wait before giving up on loading a script.
    // Setting it to 0 disables the timeout. The default is 7 seconds.
    waitSeconds: 5,
    // initial root of modules
    // will be replaced on init
    baseUrl: '/'
  };

  /////////////////////////////////////////
  /////////////////////////////////////////

  // fake app variable
  // same name as global app variable
  // so IDEA shortcuts will work on autocomplete for `app.`
  // type `app.` on a keyboard, then use ctrl\cmd +space to autocomplete
  var app = {};

  /////////////////////////////////////////

  app.path = {
    'text': 'vendor/require/text',
    'async': 'vendor/require/async',
    'json': 'vendor/require/json',
    'noext': 'vendor/require/noext',
    'promise': 'vendor/require/promise',

    'react': 'vendor/react/react',
    'react-dom': 'vendor/react/react-dom',
    'core-js': 'vendor/core-js',
    'axios': 'vendor/axios.min',
    'styles': 'tools/styles',
    'dimensions': 'tools/dimensions',
    'utils': 'tools/utils',

    'core': 'base/core',
    'ui': 'base/ui',

    'uniqId': 'tools/uniqId',
    'emitter': 'tools/emitter',

    'datepickerPlugin'  : 'plugins/datepicker/bootstrap-datepicker.min',
    'datepickerLocale'  : 'plugins/datepicker/locales/bootstrap-datepicker.ru.min'
  };

  /////////////////////////////////////////

  app.shim = {
    'jquery': {exports: 'jQuery'},
    'react': {exports: 'React'},
    'react-dom': {deps: ['react'], exports: 'ReactDOM'},
    'ymaps': {exports: 'ymaps'},
    'datepickerPlugin': {deps: ['jquery'], exports: "jQuery.fn.datepicker"},
    'datepickerLocale': {deps: ['datepickerPlugin']}
  };

  /////////////////////////////////////////

  app.packages = [
      //
  ];

  /////////////////////////////////////////

  app.map = {
    // fake
  };

  /////////////////////////////////////////
  /////////////////////////////////////////

  rjsConf.packages = app.packages;
  rjsConf.paths = app.path;
  rjsConf.shim = app.shim;
  rjsConf.map = app.map;

  /////////////////////////////////////////
  /////////////////////////////////////////

  // array keys
  var k, p = app.path, r = {};
  for (k in p) p.hasOwnProperty(k) ? (r[k] = k) : void 0;
  // not understandable for IDEA reassign
  app[['p', 'a', 't', 'h'].join('')] = r;

  /////////////////////////////////////////
  /////////////////////////////////////////

  return rjsConf;
});