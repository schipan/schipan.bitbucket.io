define([], function() {
  var iterator = 0;
  return function() {
    ++iterator;
    return [
      Array(6+2-iterator.toString().length).join('0'),
      iterator,
      (new Date()).getTime()
    ].join('_')
  };
});