define(
    [],
    function () {

      /**
       * вспомогательные утилиты работы
       * и взаимодействия с формой и её данными
       *
       * @constructor
       */
      function Utils() {
      }

      /**
       * переводит первую букву в верхний регистр
       *
       * @param {string} str
       * @returns {string}
       */
      Utils.prototype.pascalFirstLetter = function (str) {

        if(!str || typeof(str) !== 'string') {
          logger.error('wrong camel case transform', str, Error().stack);
          return '';
        }

        return str.charAt(0).toUpperCase() + str.slice(1);
      };

      return new Utils();
    });