define(['jquery', 'logger', 'mask/SxMask'],
    function ($, logger, SxMask) {

      var params = {
        onFieldAlertOn: function (msg, field) {
          // nothing (all will be done with a react component state)
        },
        onFieldAlertOff: function (field) {
          // nothing (all will be done with a react component state)
        }
      };

      return new SxMask(params);
    });
