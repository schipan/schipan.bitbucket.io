///<reference path="../typings/require.d.ts" />

define(['vendor/EventEmitter.min'], function (BaseEventEmitter : any) {

  var events = {
    _: ''
    , app: {
      _: ''
      // выставить приложение в режим финального сообщения.
      // финальное сообщение используется чтобы сообщить пользователю о конце работы с системой
      , setActiveMode : 'setActiveMode'
    }
  };

  var emitter = new BaseEventEmitter();
  emitter.events = events;
  return emitter;
});