///<reference path="../typings/require.d.ts" />
define(['vendor/EventEmitter.min'], function (BaseEventEmitter) {
    var events = {
        _: '',
        app: {
            _: '',
            setActiveMode: 'setActiveMode'
        }
    };
    var emitter = new BaseEventEmitter();
    emitter.events = events;
    return emitter;
});
