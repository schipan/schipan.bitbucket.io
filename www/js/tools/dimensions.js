define([], function(){

  function Dimensions(){}

  // react supports native objects as Dimensions
  Dimensions.get = function(objKey) {
    var obj = window[objKey];
    var height = obj.innerHeight;
    var width = obj.innerWidth;
    return [height, width];
  };

  return Dimensions;
});