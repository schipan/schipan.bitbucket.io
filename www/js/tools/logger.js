///<reference path="../typings/require.d.ts" />
///<reference path="../typings/window.d.ts" />
///<reference path="../typings/document.d.ts" />
(function (root, factory, exp) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define([], factory);
    }
    else {
        // Browser globals
        root[exp] = factory();
    }
}(this, function () {
    'use strict';
    var console = window.console;
    // compatibility
    var browser = {};
    browser.isFirefox = /firefox/i.test(navigator.userAgent);
    browser.isIE = document.documentMode;
    var support = {};
    support.consoleApply = !browser.isIE || document.documentMode && document.documentMode > 9;
    support.functionGetters = support.consoleApply;
    support.console = !!window.console;
    support.modifiedConsole = !browser.isIE && support.console && console.log.toString().indexOf('apply') !== -1;
    support.consoleStyles = !!window.chrome || !!(browser.isFirefox && support.modifiedConsole);
    support.consoleGroups = !!(window.console && console.group);
    function Logger() { }
    Logger.prototype = {
        constructor: Logger,
        fn: function (fn) { return Function.prototype.bind.call(fn, console); },
        log: function () {
            this.fn(console.log).apply(console, arguments);
        },
        critical: function () {
            var args = Array.prototype.slice.call(arguments);
            var cargs = Array.prototype.slice.call(args);
            args[0] = "%c" + args[0];
            args.splice(1, 0, "font-weight:bold;color:#ea6153;background:#2c3e50;");
            if (support.consoleStyles)
                cargs = args;
            this.fn(console.error).apply(console, cargs);
        },
        error: function () {
            var args = Array.prototype.slice.call(arguments);
            var cargs = Array.prototype.slice.call(args);
            args[0] = "%c" + args[0];
            args.splice(1, 0, "color:#ea6153;background:#ecf0f1;");
            if (support.consoleStyles)
                cargs = args;
            this.fn(console.error).apply(console, cargs);
        },
        warn: function () {
            var args = Array.prototype.slice.call(arguments);
            var cargs = Array.prototype.slice.call(args);
            args[0] = "%c" + args[0];
            args.splice(1, 0, "color:#d35400;background:#f1c40f;");
            if (support.consoleStyles)
                cargs = args;
            this.fn(console.warn).apply(console, cargs);
        },
        info: function () {
            var args = Array.prototype.slice.call(arguments);
            var cargs = Array.prototype.slice.call(args);
            args[0] = "%c" + args[0];
            args.splice(1, 0, "color:#2980b9;background:#f4f8fb;");
            if (support.consoleStyles)
                cargs = args;
            this.fn(console.info).apply(console, cargs);
        },
        report: function () {
            var args = Array.prototype.slice.call(arguments);
            var cargs = Array.prototype.slice.call(args);
            args[0] = "%c" + args[0];
            args.splice(1, 0, "color:#27ae60;background:#aefcd1;");
            if (support.consoleStyles)
                cargs = args;
            this.fn(console.info).apply(console, cargs);
        },
        //////////////////////////////////////////////////
        time: function (key) {
            this.fn(console.time).apply(console, arguments);
        },
        timeEnd: function (key) {
            this.fn(console.timeEnd).apply(console, arguments);
        }
    };
    return new Logger();
}, 'logger'));
