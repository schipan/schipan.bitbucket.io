window.Core = (function($, app){
  function Core() {

  }

  /**
   * TODO
   * @type {Array<string>}
   */
  Core.prototype.imagesToPreload = [

  ];

  /**
   *
   * @param {App} _app
   * @returns {Deferred}
   */
  Core.prototype.init = function(_app) {
    app = _app;
    $ = app.jq();

    return $.when(
        this.preloadImages(),
        this.bindEvents(),
        this.initIntro(),
        this.initReveal()
    ).then(function() {
      var $loader = $('#loader');
      var $app = $('#' + app.params.rootElId);
      $loader.addClass('loader-hidden');
      setTimeout(function(){$loader.remove()}, 1100);
      $app.addClass('ready');
    });
  };

  /**
   *
   * @returns {Deferred}
   */
  Core.prototype.bindEvents = function() {
    return $.when(
        this.bindOnScroll(),
        this.bindOnResize()
    )
  };

  /**
   *
   */
  Core.prototype.bindOnScroll = function() {
    var self = this;
    var $nav = $('.nav');

    var _onScroll = function (ev) {

      //get the delta to determine the mousewheel scroll UP and DOWN
      var delta = ev.originalEvent.detail < 0 || ev.originalEvent.wheelDelta > 0 ? 1 : -1;

      var contentTop = $('.intro').innerHeight();
      var scrollTop = $(window).scrollTop();

      var navHeight = $nav.outerHeight();
      var isFixed = scrollTop > contentTop - navHeight;
      var navBottom = isFixed ? window.innerHeight - navHeight : '';
      $nav.css({bottom: navBottom}).toggleClass('nav__fixed', isFixed);

      if( self.__scrollIntroProcess ) {
        ev.stopImmediatePropagation();
        return false;
      }

      //if the delta value is negative, the user is scrolling down
      if (ev.type !== 'scroll' && delta < 0) {
        var ih = contentTop;//window.innerHeight;
        if( scrollTop < ih/2 ) {
          self.scrollIntro(ih);
        }
      }
    };

    var _onKeyDown = function(ev) {
      var keys = [32, 34, 35, 39, 40], i = keys.length;
      for (; i--;) {
        if(ev.keyCode === keys[i]) {
          var contentTop = $('.intro').innerHeight();
          var scrollTop = $(window).scrollTop();
          var ih = contentTop;//window.innerHeight;
          if( scrollTop < ih/2 ) {
            self.scrollIntro(ih);
          }
          return;
        }
      }
    };

    $(document).on('scroll mousewheel DOMMouseScroll', _onScroll);
    $(document).on('keydown', _onKeyDown);
    _onScroll({type: 'scroll', originalEvent:{detail:0}});
  };

  /**
   *
   */
  Core.prototype.bindOnResize = function() {
    var _onResize = function() {

      var bgWidth = 1920;
      var bgHeight = 1132;
      var scale = bgWidth / bgHeight;

      var ih = window.innerHeight;
      var iw = window.innerWidth;
      var height = ih;
      var width = height * scale;
      if(width < iw) {
        width = iw;
        height = width / scale;
      }

      $('.intro-bg,.nav-bg-overlap')
          .css({'background-size': width + 'px ' + height + 'px'});
    };
    $(window).on('resize', _onResize);
    _onResize();
  };

  /**
   *
   */
  Core.prototype.initIntro = function() {
    var self = this;
    var def = $.Deferred();

    $(window)
        .on('intro_canvas_ready', function() {
          console.info('intro canvas ready');
          def.resolve();
        });

    $('.intro-arrow-inner')
        .on('click', function() {
          self.scrollIntro();
        });

    $('.intro-btn')
        .on('mouseenter', function() {
          try {
            Dispatch.find(HomeSizzleParticleBehavior, "forceRadius")("over");
          } catch(e) {
            // particles not initialized
          }
        })
        .on('mouseleave', function() {
          try {
            Dispatch.find(HomeSizzleParticleBehavior, "forceRadius")("out");
          } catch(e) {
            // particles not initialized
          }
        })
        .on('mousedown', function(e) {
          var $btn = $(this);
          var $ripple = $btn.find(".ripple").removeClass("ripple__animated");
          if(!$ripple.length) {
            $ripple = $('<div class="ripple"></div>');
            $btn.append($ripple);
          }
          var offset = $btn.offset();
          var x = parseInt(e.pageX - offset.left) - ($ripple.width() / 2);
          var y = parseInt(e.pageY - offset.top) - ($ripple.height() / 2);
          $ripple.css({
            top: y,
            left: x
          }).addClass("ripple__animated");
        })
        .on('click', function(){
          self.scrollIntro();
        })
    ;

    return def;
  };

  /**
   *
   */
  Core.prototype.initReveal = function() {
    var sr = ScrollReveal({
      // Time in milliseconds.
      duration: 1200,
      // Change when an element is considered in the viewport. The default value
      // of 0.20 means 20% of an element must be visible for its reveal to occur.
      viewFactor: 0.25,
      // Starting scale value, will transition from this value to 1
      scale : 1
    });

    var items = [
      '.intro-logo-outer',
      '.intro-text-outer',
      '.intro-btn-outer',
//      '.intro-arrow'
    ].join(',');

    sr.reveal(items, 200);
  };

  /**
   *
   * @param {number} [top]
   * @param {number} [duration]
   * @returns {Deferred}
   */
  Core.prototype.scrollIntro = function(top, duration) {
    if(this.__scrollIntroProcess) {
      return this.__scrollIntroPromise;
    }
    this.__scrollIntroProcess = true;

    var self = this;
    var def = this.__scrollIntroPromise = $.Deferred();

    if( top === void 0 ) {
      top = $('.intro').innerHeight();
    }

    $("html, body").stop(true)
        .animate({scrollTop: top}, {duration: Math.max(0, duration || 1500), easing : "easeOutCirc",
          done : def.resolve, fail: def.reject });

    return def.always(function() {
      self.__scrollIntroProcess = false;
    });
  };

  /**
   *
   */
  Core.prototype.preloadImages = function() {
    var path = app.params.imagesPath;
    var preload = this.imagesToPreload;
    var promises = [];
    for (var i = 0; i < preload.length; i++) {
      (function(imgFileName, promise) {
        var img = new Image();
        img.onload = promise.resolve;
        img.onerror = promise.resolve;
        img.src = path + imgFileName;
      })(preload[i], promises[i] = $.Deferred());
    }
    return $.when.apply($, promises);
  };

  return Core;
})();