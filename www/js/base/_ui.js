///<reference path="../typings/react.d.ts" />
///<reference path="../typings/require.d.ts" />
///<reference path="../typings/jquery.d.ts" />
///<reference path="../typings/Error.ts" />
///<reference path="../typings/es6-promise.d.ts"/>
define(['react', 'react-dom', 'components/app/App'], function (React, ReactDOM, ComponentApp) {
    "use strict";
    /**
     *
     * @param {HTMLElement} rootEl
     * @constructor
     */
    var UI = (function () {
        function UI(rootEl) {
            /**
             * корневой объект, в котором рендерится весь интерфейс приложения
             *
             * @type {HTMLElement}
             */
            this.rootEl = null;
            /**
             *
             * @type {Component}
             */
            this.appComponent = null;
            // /**
            //  *
            //  */
            // _modalOpenStates : Array<Component<any, any>> = [];
            /**
             * селекторы классов интерфейса
             *
             * @type {{string: string}}
             */
            this.className = {
                ready: 'app__ready',
                complete: 'app__complete',
                error: 'app__error'
            };
            if (!(rootEl instanceof HTMLElement)) {
                throw new Error('UI root element is not a HTMLElement', (Error()).stack);
            }
            this.rootEl = rootEl;
        }
        /**
         * первоначальная инициализация интерфейса
         *
         * @returns {Promise}
         */
        UI.prototype.init = function () {
            this.appComponent = React.createElement(ComponentApp, {});
            React.render(this.appComponent, this.rootEl);
            this.setReady(true);
            return Promise.resolve(this);
        };
        /**
         *
         * @returns {Promise}
         */
        UI.prototype.error = function () {
            this.setError(true);
            return Promise.resolve(this);
        };
        /**
         * переводит приложение из состояния загрузки в рабочее и наоборот
         *
         * @param {boolean} state
         *  true - переводит переводит приложение из состояния загрузки в рабочее;
         *  false - переводит приложение в состояние загрузки.
         */
        UI.prototype.setReady = function (state) {
            state = !!state;
            var self = this, cn = self.className;
            document.body.classList.toggle(state ? cn.ready : cn.complete, state);
            setTimeout(function () {
                document.body.classList.toggle(state ? cn.complete : cn.ready, state);
            }, 250);
        };
        /**
         * переводит приложение из состояния глобальной ошибки приложения в рабочее и наоборот
         *
         * @param {boolean} state
         *  true - переводит переводит приложение из обычного состояния в состояние ошибки;
         *  false - переводит приложение в обычное состояние.
         */
        UI.prototype.setError = function (state) {
            state = !!state;
            var self = this, cn = self.className;
            document.body.classList.toggle(cn.error, state);
        };
        /**
         *
         * @returns {Component}
         */
        UI.prototype.getAppComponent = function () {
            return this.appComponent;
        };
        /**
         *
         * @param {boolean} state
         */
        UI.prototype.toggleGlobalLoadingProcess = function (state) {
            this.rootEl.classList.toggle('app__loading', !!state);
        };
        return UI;
    }());
    return UI;
});
