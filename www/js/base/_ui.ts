///<reference path="../typings/react.d.ts" />
///<reference path="../typings/require.d.ts" />
///<reference path="../typings/jquery.d.ts" />
///<reference path="../typings/Error.ts" />
///<reference path="../typings/es6-promise.d.ts"/>

import ReactDOM = __React.ReactDOM;

define(['react', 'react-dom', 'components/app/App'],
    function (React : any, ReactDOM : ReactDOM, ComponentApp : any) {
      "use strict";

      /**
       *
       * @param {HTMLElement} rootEl
       * @constructor
       */
      class UI {

        constructor(rootEl : HTMLElement) {

          if(!(rootEl instanceof HTMLElement)) {
            throw new Error('UI root element is not a HTMLElement', (Error()).stack);
          }

          this.rootEl = rootEl;
        }

        /**
         * корневой объект, в котором рендерится весь интерфейс приложения
         *
         * @type {HTMLElement}
         */
        rootEl : HTMLElement = null;

        /**
         *
         * @type {Component}
         */
        appComponent : any = null;

        // /**
        //  *
        //  */
        // _modalOpenStates : Array<Component<any, any>> = [];

        /**
         * селекторы классов интерфейса
         *
         * @type {{string: string}}
         */
        className : any = {
          ready: 'app__ready',
          complete: 'app__complete',
          error: 'app__error'
        };

        /**
         * первоначальная инициализация интерфейса
         *
         * @returns {Promise}
         */
        init() {

          this.appComponent = React.createElement(ComponentApp, {});

          React.render(
              this.appComponent,
              this.rootEl
          );

          this.setReady(true);

          return Promise.resolve(this);
        }

        /**
         *
         * @returns {Promise}
         */
        error() {

          this.setError(true);

          return Promise.resolve(this);
        }

        /**
         * переводит приложение из состояния загрузки в рабочее и наоборот
         *
         * @param {boolean} state
         *  true - переводит переводит приложение из состояния загрузки в рабочее;
         *  false - переводит приложение в состояние загрузки.
         */
        setReady(state : boolean) {
          state = !!state;

          var self = this, cn = self.className;
          document.body.classList.toggle(state ? cn.ready : cn.complete, state);
          setTimeout(function () {
            document.body.classList.toggle(state ? cn.complete : cn.ready, state);
          }, 250);
        }

        /**
         * переводит приложение из состояния глобальной ошибки приложения в рабочее и наоборот
         *
         * @param {boolean} state
         *  true - переводит переводит приложение из обычного состояния в состояние ошибки;
         *  false - переводит приложение в обычное состояние.
         */
        setError(state : boolean) {
          state = !!state;

          var self = this, cn = self.className;
          document.body.classList.toggle(cn.error, state);
        }

        /**
         *
         * @returns {Component}
         */
        getAppComponent() {
          return this.appComponent;
        }

        /**
         *
         * @param {boolean} state
         */
        toggleGlobalLoadingProcess(state : boolean) {
          this.rootEl.classList.toggle('app__loading', !!state);
        }

        // /**
        //  *
        //  * @param {Component} modalComponent
        //  * @param {boolean} isOpen
        //  */
        // syncModalOpenState(modalComponent : Component, isOpen : boolean) {
        //   var componentExists = false;
        //
        //   this._modalOpenStates.map(function (comp, idx) {
        //     if(comp === modalComponent) {
        //       if(isOpen) componentExists = true;
        //       else this._modalOpenStates.splice(idx, 1);
        //     }
        //   }.bind(this));
        //
        //   if(!componentExists && isOpen) {
        //     this._modalOpenStates.push(modalComponent);
        //   }
        //
        //   this.$root.toggleClass('modal-open', this._modalOpenStates.length > 0);
        // }
        //
        // /**
        //  *
        //  * @param {HTMLElement|jQuery} el
        //  */
        // scrollToElement(el : any) {
        //
        //   if(!el) {
        //     return;
        //   }
        //
        //   var $el = $(el);
        //   if(!$el.length) {
        //     return;
        //   }
        //
        //   var $wnd = $(window);
        //   var scrollTopSpace = $wnd.height() / 6;
        //   var offsetTop = $el.offset().top - scrollTopSpace;
        //
        //   var $modal = $el.closest('.modal');
        //   if($modal && $modal.length && $modal.jquery) {
        //     offsetTop += $modal.scrollTop();
        //     $modal.scrollTop(offsetTop);
        //   }
        //   else {
        //     $wnd.scrollTop(offsetTop);
        //   }
        //
        // }
        //
        // scrollTop() {
        //   var $modal = $('.modal:visible').last();
        //   if($modal.length) $modal.scrollTop(0);
        //   else $(window).scrollTop(0);
        // }
      }

      return UI;
    });