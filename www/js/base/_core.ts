///<reference path="../typings/require.d.ts" />
///<reference path="../typings/es6-promise.d.ts"/>

define([], function () {
    "use strict";

    class Core {

        /**
         *
         * @type {App}
         */
        app: any = null;

        /**
         *
         * @type {UI}
         */
        ui: any = null;

        /**
         *
         * @param {App} app
         *
         * @returns {Promise}
         */
        init(app) {
            var self = this;

            var rootElId = app.getParam('rootElId');
            if (!rootElId) {
                return Promise.reject('root element id not specified or wrong');
            }

            var rootEl = document.getElementById(rootElId);
            if (!rootEl) {
                return Promise.reject('root element not found');
            }

            // core is linked to app
            // pass reference
            self.app = app;

            return this.require('base/ui')
                .then(function (UI : any) {
                    // init user interface
                    var ui = new UI(rootEl);
                    // pass reference
                    self.ui = ui;
                    // do render
                    // return app.hasErrors() ? ui.error() : ui.init();
                    return ui.init();
                });
        }

        /**
         *
         * @param {string} path
         * @returns {Promise}
         */
        require(path) {
          var self = this;
          return new Promise(function(resolve, reject) {
            require([path], resolve.bind(self), reject.bind(self));
          });
        };
    }

    return Core;
});