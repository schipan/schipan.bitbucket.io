interface ErrorConstructor {
  new (...args : any[]): Error;
  (message?: string): Error;
  stack: string;
}

interface Error {
  stack: string;
}

declare var Error: ErrorConstructor;